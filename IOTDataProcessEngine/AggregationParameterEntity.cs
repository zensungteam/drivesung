﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTDataProcessEngine
{
  public  class AggregationParameterEntity
    {
        public Int64 deviceno { get; set; }
        public DateTime recorddate { get; set; }
        public decimal distance { get; set; }
        public decimal avgspeed { get; set; }
        public decimal maxspeed { get; set; }
        public decimal harshbreak { get; set; }
        public decimal harshacclerate { get; set; }
    }
}
