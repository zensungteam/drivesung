﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace IOTDataProcessEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
              
                Console.WriteLine("start Process  " + DateTime.Now.ToString());
                using (DailyDataProcess DailyDataProcess = new DailyDataProcess())
                {
                    DailyDataProcess.DailyDataProcessEngine();
                }
                Console.WriteLine("end Process  " + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("process error: " + ex.ToString());
                Console.ReadLine();
            }
            finally
            {
                Console.WriteLine("finish");
               
            }


        }
    }
}
