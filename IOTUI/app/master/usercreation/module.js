﻿"use strict";

angular.module('app.usercreation', ['ui.router'])

angular.module('app.usercreation').config(function ($stateProvider) {

    $stateProvider
        .state('app.usercreation', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.userlist', {
            url: '/userlist',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/master/usercreation/views/userlist.html',
                    controller: 'userlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

        .state('app.userdetails', {
            url: '/UserDetails',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/master/usercreation/views/userdetails.html',
                    controller: 'userdetailsCtrl'
                }
            }
           
        })
});
