﻿'use strict'

angular.module('app.policyclaimadmin').controller('policyclaimadmindetailsCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, policyclaimFactory, policyclaimadminFactory, $rootScope) {
    var actionCS;
    function loadInitData() {

        //GetRoleList();
        //GetOrganizationList();

        $scope.claimdocument = [];
        $scope.claimdocument.push({
            'id': 'claim' + 1
        });
        actionCS = JSON.parse($window.sessionStorage["policyclaimadmin"]);
        $scope.action = actionCS.action;
        $scope.CSMasterIndex = 0;
        if (actionCS.action == "New") {
            $scope.lblheading = "New PolicyClaim";
            $scope.lblheading1 = 'Details';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";

            $scope.user_id = 0;
            //$scope.role_Id = "Select User Type";
            //$scope.o_id = "Select Organization";

        }
        else {
            //$scope.options = [{ Text: "public", value: "public" }, { Text: "private", value: "private" }];
            if (actionCS.action == "Edit")
                $scope.lblheading1 = 'Submit';
            $scope.lblheading1 = 'Details';
            //$scope.btnSaveText="Update"
            $scope.refer = {

                claim_id: actionCS.claim_id,
                s_user_id: $rootScope.userInfo.user_id,
                s_role_Id: 1,
            }

            policyclaimFactory.getPolicyClaimById($scope.refer).then(function (result) {
                var dt = new Date();
                var resultData = result.data.data;
                $scope.claim_id = resultData[0].claim_id;
                $scope.user_id = resultData[0].user_id;
                $scope.policy_no = resultData[0].policy_no;
                $scope.period_from = $filter('date')(resultData[0].period_from, "dd-MM-yyyy");
                $scope.period_to = $filter('date')(resultData[0].period_to, "dd-MM-yyyy");
                $scope.date_accident_or_loss = $filter('date')(resultData[0].date_accident_or_loss, "dd-MM-yyyy");
                $scope.name_of_driver = resultData[0].name_of_driver;
                $scope.driver_birth_date = $filter('date')(resultData[0].driver_birth_date, "dd-MM-yyyy");
                //$scope.desc = resultData.desc;
                //$scope.date_of_birth = resultData[0].date_of_birth;
                $scope.driving_licence_no = resultData[0].driving_licence_no;
                $scope.accident_or_loss_details = resultData[0].accident_or_loss_details;
                $scope.status = resultData[0].status;
                $scope.remark = resultData[0].remark;
                //$scope.contact_no2 = resultData.contact_no2;
                $scope.claimdocument = resultData[0].policyDocEntities;
                showPolicydocumentList(resultData[0].policyDocEntities);
            }, function (error) {
                console.log(error);
            });
        }

    }

    function showPolicydocumentList(ListData) {

        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Download = "";
            //data.fullname = data.first_name + " " + data.last_name;

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "doc_name" },
                //{ data: "status" },
                { data: "Download" }
          ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 1 + ')', nRow).html('<a target="_self" href="' + aData.doc_file_url + '" download="' + aData.doc_name + '".pdf">Download </a>');

             },
         };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.viewdocument = function (doc_file_path) {
        
        $location.path(doc_file_path);
    }

    $scope.UpdateAdminPolicyclaimRecord = function () {
       
        var policyClaimData = {
            'claim_id': actionCS.claim_id,
            'status': $scope.status,
            'remark': $scope.remark,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
           
        };
       
         policyclaimadminFactory.UpdateAdminPolicyClaim(policyClaimData).then(function (result) {
            $.smallBox({
                title: "Policy Claim Updated Succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go("app.policyclaimadminlist");
        }, function (error) {
            console.log(error);
        });

    }

    $scope.finalCancelAdminPolicyclaimRecord = function () {

        $.SmartMessageBox({
            title: "Policy Claim Details",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.policyclaimadminlist');
            }
        });
        return;

    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
