﻿'use strict'

angular.module('app.organization').controller('organizationdetailsCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, organizationFactory, $rootScope) {
    var actionCS;
    function loadInitData() {

        //GetRoleList();
        //GetOrganizationList();
        $scope.person = [];
        $scope.orgContacts = [];
        $scope.orgContacts.push({
            'id': 'person' + 1
        });
        actionCS = JSON.parse($window.sessionStorage["organization"]);
        $scope.action = actionCS.action;
        $scope.CSMasterIndex = 0;
        if (actionCS.action == "New") {
            $scope.lblheading = "New Organization";
            $scope.lblheading1 = 'Details';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";
           
            $scope.user_id = 0;
            //$scope.role_Id = "Select User Type";
            //$scope.o_id = "Select Organization";

        }
        else {
            //$scope.options = [{ Text: "public", value: "public" }, { Text: "private", value: "private" }];
            if (actionCS.action == "Edit")
                $scope.lblheading1 = 'Update';
            $scope.lblheading1 = 'View';
            //$scope.btnSaveText="Update"
            $scope.refer = {

                o_id: actionCS.o_id,
                s_user_id: $rootScope.userInfo.user_id,
                s_role_Id: 2,
            }

            organizationFactory.getOrganizationById($scope.refer).then(function (result) {
                var dt = new Date();
                var resultData = result.data.data;

                $scope.o_id = resultData.o_id;
                $scope.o_code = resultData.o_code;
                $scope.o_name = resultData.o_name;
                $scope.o_type = resultData.o_type;
                $scope.address1 = resultData.address1;
                $scope.address2 = resultData.address2;
                $scope.address3 = resultData.address3;
                $scope.email_id1 = resultData.email_id1;
                //$scope.desc = resultData.desc;
                //$scope.date_of_birth = resultData[0].date_of_birth;
                $scope.pincode_no = resultData.pincode_no;
                $scope.email_id2 = resultData.email_id2;
                $scope.contact_no1 = resultData.contact_no1;
                $scope.o_logo1 = resultData.o_logo1;
                $scope.o_logo2 = resultData.o_logo2;
                $scope.o_logo3 = resultData.o_logo3;
                //$scope.contact_no2 = resultData.contact_no2;
                $scope.orgContacts = resultData.orgContactEntities
                $scope.o_logoname1 = resultData.o_logoname1;
                $scope.o_logoname2 = resultData.o_logoname2;
                $scope.o_logoname3 = resultData.o_logoname3;

            }, function (error) {
                console.log(error);
            });
        }

    }
  

    $scope.addNeworgContact = function () {
        var newItemNo = $scope.orgContacts.length + 1;

        $scope.orgContacts.push({
            'id': newItemNo
        });
    };

    $scope.removeorgContact = function (id) {
        var newid = id;
        $.SmartMessageBox({
            title: "Remove Contact Person",
            content: "Are you sure want to Remove Contact Person",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok")

            {
                var lastItem = $scope.orgContacts.length - 1;
                $scope.orgContacts.splice(newid,1);
            }
        });
        
    };

  
    $scope.SaveOrganizationRecord = function () {
       

        var organizationData = {
           
            'o_code': $scope.o_code,
            'o_name': $scope.o_name,
            'o_type': $scope.o_type,
            'address1': $scope.address1,
            'email_id1': $scope.email_id1,
            'pincode_no': $scope.pincode_no,
            //'desc': $scope.desc,
            //'contact_no': $scope.contact_no,
            'contact_no1': $scope.contact_no1,
            'o_logo1': $scope.o_logo1[0].name,
            'o_logo2': $scope.o_logo2[0].name,
            'o_logo3': $scope.o_logo3[0].name,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
            'orgContactEntities': $scope.orgContacts,
            

        };

        var fd = new FormData();
        fd.append("data", JSON.stringify(organizationData));
        if ($scope.o_logo1 != null)
        {
            fd.append("file0", $scope.o_logo1[0]);
        }
        if ($scope.o_logo2 != null) {
            fd.append("file1", $scope.o_logo2[0]);
        }
        if ($scope.o_logo3 != null) {
            fd.append("file2", $scope.o_logo3[0]);
        }

        organizationFactory.SaveOrganization(fd).then(function (result) {

            $.smallBox({
                title: "User register succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.organizationlist');

        }, function (error) {
            console.log(error);
        });
    }

    $scope.finalCancelOrganizationRecord = function () {

        $.SmartMessageBox({
            title: "Organization Details",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.organizationlist');
            }
        });
        return;

    }

    $scope.UpdateOrganizationRecord = function () {
       

        var organizationData = {

            'o_id': $scope.o_id,
            'o_code': $scope.o_code,
            'o_name': $scope.o_name,
            'o_type': $scope.o_type,
            'address1': $scope.address1,
            'email_id1': $scope.email_id1,
            'pincode_no': $scope.pincode_no,
            'desc': $scope.desc,
            'o_logo1':$scope.o_logo1[0].name == null ? $scope.o_logoname1: $scope.o_logo1[0].name,
            'o_logo2': $scope.o_logo2[0].name == null ? $scope.o_logoname2 : $scope.o_logo2[0].name,
            'o_logo3': $scope.o_logo3[0].name == null ? $scope.o_logoname3 : $scope.o_logo3[0].name,
            //'contact_no': $scope.contact_no,
            'contact_no1': $scope.contact_no1,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
            'orgContactEntities': $scope.orgContacts,

        };
        var fd = new FormData();
        fd.append("data", JSON.stringify(organizationData));
        if ($scope.o_logo1 != null) {
            fd.append("file0", $scope.o_logo1[0]);
        }
        if ($scope.o_logo2 != null) {
            fd.append("file1", $scope.o_logo2[0]);
        }
        if ($scope.o_logo3 != null) {
            fd.append("file2", $scope.o_logo3[0]);
        }


        organizationFactory.UpdateOrganization(fd).then(function (result) {

            $.smallBox({
                title: "User  Updated Succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.organizationlist');

        }, function (error) {
            console.log(error);
        });

    }


    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
