﻿"use strict";

angular.module('app.organization', ['ui.router'])

angular.module('app.organization').config(function ($stateProvider) {

    $stateProvider
        .state('app.organization', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.organizationlist', {
            url: '/Organizationlist',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/master/organization/views/organizationlist.html',
                    controller: 'organizationlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

    .state('app.organizationdetails', {
        url: '/OrganizationDetails',
        data: {
            title: ''
        },
        views: {
            "content@app": {
                templateUrl: 'app/master/organization/views/organizationdetail.html',
                controller: 'organizationdetailsCtrl'
            }
        }

    })
});
