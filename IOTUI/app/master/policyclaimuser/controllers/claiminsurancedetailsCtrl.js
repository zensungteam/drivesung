﻿'use strict'
angular.module('app.policyclaimuser').controller('claiminsurancedetaisCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, policyclaimFactory, $rootScope) {

    function loadInitData() {

        $scope.claimdocument = [];
        $scope.claimdocument.push({
            'id': 'claim' + 1
        });
        $scope.lblheading1 = 'Details';

    }

    $scope.addNewdocument = function () {
        var newItemNo = $scope.claimdocument.length + 1;

        $scope.claimdocument.push({
            'id': newItemNo
        });
    };





    $scope.removedocument = function (id) {
        var newid = id;
        $.SmartMessageBox({
            title: "Remove Claim Document",
            content: "Are you sure want to Remove Claim Document",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                var lastItem = $scope.claimdocument.length - 1;
                $scope.claimdocument.splice(newid, 1);
            }
        });

    };

    $scope.sendToInsuranceCompany = function () {

        $scope.claimdocumentdata = [];
        // $scope.files = [];
        //var filedata1 = $scope.files;
        angular.forEach($scope.claimdocument, function (data, index) {
            $scope.claimdocumentdata.push({
                doc_name: data.doc_name,
                doc_file_path: data.doc_file_path[0].name,

            })
        });
        var policyClaimData = {
            'policy_no': $scope.policy_no,
            'location': $scope.location,
            'note': $scope.note,
            'email_id':$scope.email_id,
            //'remark': $scope.remark,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
            'policyDocEntities': $scope.claimdocumentdata,
        };
        var fd = new FormData();
        fd.append("data", JSON.stringify(policyClaimData));
        for (var i = 0; i < $scope.claimdocument.length; i++) {
            fd.append("file" + i, $scope.claimdocument[i].doc_file_path[0]);
        }

        policyclaimFactory.SendClaimInsuranceDoc(fd).then(function (result) {

            $.smallBox({
                title: "Send Claim Insurance Succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
           
        }, function (error) {
            console.log(error);
        });
    }


    function init() {

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }
    init();
});