﻿'use strict';
angular.module('app.policyclaimuser').controller('policyclaimlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, policyclaimFactory) {

    function loadInitData() {

        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };


        policyclaimFactory.getPolicyClaimList(param).then(function (result) {
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }


    function showData(ListData) {
        showPolicyClaimList(ListData);
    }

    function showPolicyClaimList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
            data.period_from = $filter('date')(data.period_from, "dd-MM-yyyy");
            data.period_to = $filter('date')(data.period_to, "dd-MM-yyyy");
            data.date_accident_or_loss = $filter('date')(data.date_accident_or_loss, "dd-MM-yyyy");
           //data.fullname = data.first_name + " " + data.last_name;

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "policy_no" },
                { data: "period_from" },
                { data: "period_to" },
                { data: "name_of_driver" },
                { data: "date_accident_or_loss" },
                //{ data: "status" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" },

            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                if (aData.status == 'p')
                {
                    $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default'  ng-click='editRow(" + aData.claim_id + ")'><i class='fa fa-edit'></i></button>");
                    $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.claim_id + ")'><i class='fa fa-reorder'></i></button>");
                    $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default'  ng-click='deleteRow(" + aData.claim_id + ")'><i class='fa fa-times'></i></button>");

                 
                }
                else
                {
                    $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default'  ng-hide=true  ng-click='editRow(" + aData.claim_id + ")'><i class='fa fa-edit'></i></button>");
                    $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.claim_id + ")'><i class='fa fa-reorder'></i></button>");
                    $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default'  ng-hide=true  ng-click='deleteRow(" + aData.claim_id + ")'><i class='fa fa-times'></i></button>");
                    
                }
               

            },
            //, "aoColumnDefs": [

            ////     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {

        var params = { 'action': 'New', 'claim_id': 0 };
        $window.sessionStorage["policyclaim"] = JSON.stringify(params);
        $state.go('app.policyclaimuserdetails');
    }

    $scope.editRow = function (claim_id) {
        debugger;
        var params = { 'action': 'Edit', 'claim_id': claim_id };
        $window.sessionStorage["policyclaim"] = JSON.stringify(params);
        $state.go('app.policyclaimuserdetails');
    }

    $scope.viewRow = function (claim_id) {
        var params = { 'action': 'View', 'claim_id': claim_id };
        $window.sessionStorage["policyclaim"] = JSON.stringify(params);
        $state.go('app.policyclaimuserdetails');
    }

    $scope.deleteRow = function (claim_id) {
        var params = { 'action': 'Edit', 'claim_id': claim_id };
        $window.sessionStorage["policyclaim"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Policy Claim Details",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deletePolicyclaimRecord(claim_id);
            }
        });
    }


    function deletePolicyclaimRecord(claim_id) {
        var param = {
            'claim_id': claim_id,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2

        };

        policyclaimFactory.DeletePolicyClaim(param)
             .then(function (result) {

                 var hexcolor = "#F00";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "PolicyClaim Delete succesfully",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 //if (result.data.isComplete == true)
                 loadInitData();

             },
              function (error) {
                  console.log(error);
              });

    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});