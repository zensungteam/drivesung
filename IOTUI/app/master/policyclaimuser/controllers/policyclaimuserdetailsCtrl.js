﻿'use strict'

angular.module('app.policyclaimuser').controller('policyclaimuserdetailsCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, policyclaimFactory, $rootScope) {
    var actionCS;
    function loadInitData() {

        //GetRoleList();
        //GetOrganizationList();

        $scope.claimdocument = [];
        $scope.claimdocument.push({
            'id': 'claim' + 1
        });
        actionCS = JSON.parse($window.sessionStorage["policyclaim"]);
        $scope.action = actionCS.action;
        $scope.CSMasterIndex = 0;
        if (actionCS.action == "New") {
            $scope.lblheading = "New PolicyClaim";
            $scope.lblheading1 = 'Details';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";

            $scope.user_id = 0;
            $('#cmbdate_of_birth').combodate({
                value: $scope.driver_birth_date,
                minYear: '1960',
                maxYear: moment().format('YYYY')
            });
            //$scope.role_Id = "Select User Type";
            //$scope.o_id = "Select Organization";

        }
        else {
            //$scope.options = [{ Text: "public", value: "public" }, { Text: "private", value: "private" }];
            if (actionCS.action == "Edit")
                $scope.lblheading1 = 'Update';
            $scope.lblheading1 = 'View';
            //$scope.btnSaveText="Update"
            $scope.refer = {

                claim_id: actionCS.claim_id,
                s_user_id: $rootScope.userInfo.user_id,
                s_role_Id: 1,
            }

            policyclaimFactory.getPolicyClaimById($scope.refer).then(function (result) {
                var dt = new Date();
                var resultData = result.data.data;

                $scope.claim_id = resultData[0].claim_id;
                $scope.user_id = resultData[0].user_id;
                $scope.policy_no = resultData[0].policy_no;
                $scope.period_from = $filter('date')(resultData[0].period_from, "dd-MM-yyyy");
                $scope.period_to = $filter('date')(resultData[0].period_to, "dd-MM-yyyy");
                $scope.date_accident_or_loss = $filter('date')(resultData[0].date_accident_or_loss, "dd-MM-yyyy");
                $scope.name_of_driver = resultData[0].name_of_driver;
                $scope.driver_birth_date = resultData[0].dbd;
                //$scope.desc = resultData.desc;
                //$scope.date_of_birth = resultData[0].date_of_birth;
                $scope.driving_licence_no = resultData[0].driving_licence_no;
                $scope.accident_or_loss_details = resultData[0].accident_or_loss_details;
                $scope.status = resultData[0].status;
                $scope.remark = resultData[0].remark;
                //$scope.contact_no2 = resultData.contact_no2;
                $scope.claimdocument = resultData[0].policyDocEntities;

                $('#cmbdate_of_birth').combodate({
                    value: $scope.driver_birth_date,
                    minYear: '1960',
                    maxYear: moment().format('YYYY')
                });

            }, function (error) {
                console.log(error);
            });
        }

    }


    $scope.addNewdocument = function () {
        var newItemNo = $scope.claimdocument.length + 1;

        $scope.claimdocument.push({
            'id':  newItemNo
        });
    };

    $scope.removedocument = function (id) {
        var newid = id;
        $.SmartMessageBox({
            title: "Remove Claim Document",
            content: "Are you sure want to Remove Claim Document",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                var lastItem = $scope.claimdocument.length - 1;
                $scope.claimdocument.splice(newid,1);
            }
        });
       
    };


    $scope.SavePolicyclaimRecord = function () {
      
        $scope.claimdocumentdata = [];
       // $scope.files = [];
        //var filedata1 = $scope.files;
        angular.forEach($scope.claimdocument, function (data, index) {
           $scope.claimdocumentdata.push({
                doc_name: data.doc_name,
                doc_file_path: data.doc_file_path[0].name,
             
            })
       });
        var policyClaimData = {
            'policy_no': $scope.policy_no,
            'spf': $scope.period_from,
            'spt': $scope.period_to,
            'sdaccount': $scope.date_accident_or_loss,
            'name_of_driver': $scope.name_of_driver,
            'driver_birth_date': $scope.driver_birth_date,
            'driving_licence_no': $scope.driving_licence_no,
            'accident_or_loss_details': $scope.accident_or_loss_details,
            'status': "p",
            //'remark': $scope.remark,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
            'policyDocEntities': $scope.claimdocumentdata,
        };
        var fd = new FormData();
        fd.append("data", JSON.stringify(policyClaimData));
        for (var i = 0; i < $scope.claimdocument.length; i++) {
            fd.append("file" + i, $scope.claimdocument[i].doc_file_path[0]);
        }
      
        policyclaimFactory.SavePolicyClaim(fd).then(function (result) {

            $.smallBox({
                title: "Policy Claim Add Succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.policyclaimuserlist');
        }, function (error) {
            console.log(error);
        });
    }

    $scope.finalCancelOrganizationRecord = function () {

        $.SmartMessageBox({
            title: "Policy Claim Details",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.policyclaimuserlist');
            }
        });
        return;

    }

    $scope.UpdatePolicyclaimRecord = function () {
        $scope.claimdocumentdata = [];
        // $scope.files = [];
        //var filedata1 = $scope.files;
        angular.forEach($scope.claimdocument, function (data, index) {
            $scope.claimdocumentdata.push({
                doc_name: data.doc_name,
                doc_file_path: data.doc_file_path[0].name,
                doc_file_save: data.doc_file_save
            })
        });

        var policyClaimData = {

            'claim_id': actionCS.claim_id,
            'policy_no': $scope.policy_no,
            'spf': $scope.period_from,
            'spt': $scope.period_to,
            'sdaccount': $scope.date_accident_or_loss,
            'name_of_driver': $scope.name_of_driver,
            'driver_birth_date': $scope.driver_birth_date,
            'driving_licence_no': $scope.driving_licence_no,
            'accident_or_loss_details': $scope.accident_or_loss_details,
            'status': "p",
            //'remark': $scope.remark,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2,
            'policyDocEntities': $scope.claimdocumentdata,
        };
        var fd = new FormData();
        fd.append("data", JSON.stringify(policyClaimData));
        for (var i = 0; i < $scope.claimdocument.length; i++) {
            fd.append("file" + i, $scope.claimdocument[i].doc_file_path[0]);
        }
        policyclaimFactory.UpdatePolicyClaim(fd).then(function (result) {
          $.smallBox({
                title: "Policy Claim Updated Succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
          $state.go('app.policyclaimuserlist');
        }, function (error) {
            console.log(error);
        });

    }


    function init() {

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
