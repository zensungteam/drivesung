﻿"use strict";

angular.module('app.policyclaimuser', ['ui.router'])

angular.module('app.policyclaimuser').config(function ($stateProvider) {

    $stateProvider
        .state('app.policyclaimuser', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.policyclaimuserlist', {
            url: '/PolicyClaimUserList',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/master/policyclaimuser/views/policyclaimuserlist.html',
                    controller: 'policyclaimlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

    .state('app.policyclaimuserdetails', {
        url: '/PolicyClaimUserdetails',
        data: {
            title: ''
        },
        views: {
            "content@app": {
                templateUrl: 'app/master/policyclaimuser/views/policyclaimuserdetail.html',
                controller: 'policyclaimuserdetailsCtrl'
            }
        }

    })

    .state('app.claiminsurancedetails', {
        url: '/ClaimInsuranceDetails',
        data: {
            title: ''
        },
        views: {
            "content@app": {
                templateUrl: 'app/master/policyclaimuser/views/claiminsurancedetails.html',
                controller: 'claiminsurancedetaisCtrl'
            }
        }

    })

    
});
