﻿"use strict";

angular.module('app.Iotdashboardlist').factory('dashboardFactory', function ($http, $q, APP_CONFIG) {

    var getDashBoardLiveData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetDashBoardList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
       
    var getSubHeaderCount = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetSubHeaderCount";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {
        getDashBoardLiveData: getDashBoardLiveData,
        getVehicleList: getVehicleList,
        getSubHeaderCount:getSubHeaderCount
    };

}).factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) { return $window.btoa(unescape(encodeURIComponent(s))); },
        format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return {
        tableToExcel: function (tableId, worksheetName) {
            var table = $(tableId),
                ctx = { worksheet: worksheetName, table: table.html() },
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
})











//// Code goes here
//var myApp = angular.module("myApp", []);
//myApp.
//    .controller('MyCtrl', function (Excel, $timeout, $scope) {
//        $scope.exportToExcel = function (tableId) { // ex: '#my-table'
//            var exportHref = Excel.tableToExcel(tableId, 'WireWorkbenchDataExport');
//            $timeout(function () { location.href = exportHref; }, 100); // trigger download
//        }
//    });
