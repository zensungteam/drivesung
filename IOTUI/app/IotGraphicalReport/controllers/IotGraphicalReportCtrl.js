﻿'use strict';

angular.module('app.IotGraphicalReport').controller('IotGraphicalReportCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IotGraphicalReportFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function SetGraphData(result) {
        //var daily_agg = JSON.stringify(result.data.data.daily_agg)
        //var stoppagedata = JSON.stringify(result.data.data.stoppagedata)
        var daily_agg = result.data.data.daily_agg;
        var stoppagedata = result.data.data.stoppagedata;

        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(daily_agg, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddate;
            var distance = data.distance;
            var distance_d = data.distance_d;
            var distance_n = data.distance_n;

            chartData.push({
                date: cdatetime,
                //distance: distance,
                distanceday: distance_d,
                distancenight: distance_n
            });

        });


        var chart = AmCharts.makeChart("chartdivDR", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Distance"

            },
            "dataProvider": chartData,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "Distance Travel (km)"
            }],
            "graphs": [{
                "balloonText": "Date [[category]], Distance Travel at Day: <b>[[value]]</b> (km)",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Day Distance",
                "type": "column",
                "color": "#01A9DB",
                "valueField": "distanceday"
            }, {
                "balloonText": "Date [[category]], Distance Travel at Night: <b>[[value]]</b> (km)",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Night Distance",
                "type": "column",
                "color": "#d28924",
                "valueField": "distancenight"
            }
            ],
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "colors": ["#01A9DB", "#d28924"],
            "export": {
                "enabled": true
            }
        });



        //chartdivAvgMaxSpeed
        var chart = AmCharts.makeChart("chartdivAvgMaxSpeed", {
            "type": "serial",
            "theme": "light",
            "dataProvider": daily_agg,
            "valueAxes": [{
                "gridColor": "#FFFFFF",
                "gridAlpha": 0.2,
                "dashLength": 0,
                "title": "Speed (km/hr)"
            }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                "title": "Max Speed",
                "balloonText": "[[title]]: <b>[[value]]</b>",
                "bullet": "round",
                "bulletSize": 10,
                "bulletBorderColor": "#ffffff",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 2,
                "valueField": "maxspeed"
            }, {
                "title": "Avg Speed",
                "balloonText": "[[title]]: <b>[[value]]</b>",
                "bullet": "round",
                "bulletSize": 10,
                "bulletBorderColor": "#ffffff",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 2,
                "valueField": "avgspeed"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "recorddate",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "title": "Date (dd-mm)"
            },
            "colors": ["#01A9DB", "#d28924"],
            "legend": {}
        });


        //chartdivDrivingHours
        //var chart = AmCharts.makeChart("chartdivDrivingHours", {
        //    "type": "serial",
        //    "theme": "light",
        //    "dataProvider": daily_agg,
        //    "valueAxes": [{
        //        "gridColor": "#FFFFFF",
        //        "gridAlpha": 0.2,
        //        "dashLength": 0
        //    }],
        //    "gridAboveGraphs": true,
        //    "startDuration": 1,
        //    "graphs": [{
        //        "balloonText": "Date :- [[category]]: <b> Driving Hours:- [[value]]</b>",
        //        "fillAlphas": 0.8,
        //        "lineAlpha": 0.2,
        //        "type": "column",
        //        "valueField": "drivinghrs"
        //    }],
        //    "chartCursor": {
        //        "categoryBalloonEnabled": false,
        //        "cursorAlpha": 0,
        //        "zoomable": false
        //    },
        //    "categoryField": "recorddate",
        //    "categoryAxis": {
        //        "gridPosition": "start",
        //        "gridAlpha": 0,
        //        "tickPosition": "start",
        //        "tickLength": 20,
        //        "title": "Date (dd-mm)"
        //    },
        //    "export": {
        //        "enabled": true
        //    }

        //});


        //chartdivHBHATime
        //var chart = AmCharts.makeChart("chartdivHBHATime", {
        //    "theme": "light",
        //    "type": "serial",
        //    "dataProvider": daily_agg,
        //    "valueAxes": [{
        //        "unit": "",
        //        "position": "left",
        //        "title": "Harsh Event Count",
        //    }],
        //    "startDuration": 1,
        //    "graphs": [{
        //        "balloonText": "Harsh Accel Event Date:- [[category]]  Count :-  <b>[[value]]</b>",
        //        "fillAlphas": 0.9,
        //        "lineAlpha": 0.2,
        //        "title": "Harsh Accel.",
        //        "type": "column",
        //        "valueField": "hardacceleration"
        //    }, {
        //        "balloonText": "Harsh Breaking Event Date:- [[category]]  Count :-  <b>[[value]]</b>",
        //        "fillAlphas": 0.9,
        //        "lineAlpha": 0.2,
        //        "title": "Harsh Braking",
        //        "type": "column",
        //        "clustered": false,
        //        "columnWidth": 0.5,
        //        "valueField": "hardbraking"
        //    }],
        //    "plotAreaFillAlphas": 0.1,
        //    "categoryField": "recorddate",
        //    "categoryAxis": {
        //        "gridPosition": "start",
        //        "title": "Date (dd-mm)"
        //    },
        //    "colors": ["#863ba2", "#28628e"],
        //    "export": {
        //        "enabled": true
        //    }

        //});



        var chartData = generateChartData(daily_agg);
        var chart = AmCharts.makeChart("chartdivHBHATime", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Harsh Event "

            },
            "dataProvider": chartData,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "No of Events"
            }],
            "graphs": [{
                "balloonText": "Date:[[category]], Harsh Breaking : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Breaking",
                "type": "column",
                "color": "#863ba2",
                "valueField": "BreakingEvents"
            }, {
                "balloonText": "Date:[[category]], Harsh Accelaration : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Accelaration",
                "type": "column",
                "color": "#28628e",
                "valueField": "AccelarationEvents"
            }, {
                "balloonText": "Date:[[category]], Harsh Cornering : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Harsh Cornering",
                "type": "column",
                "color": "#298a52",
                "valueField": "Cornering"
            }, {
                "balloonText": "Date:[[category]],Over Speed: <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Over Speed",
                "type": "column",
                "color": "#c53c2e",
                "valueField": "OverspeedstartedEvents"
            }, {
                "balloonText": "Date:[[category]], Phone Call : <b>[[value]]</b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Phone Call",
                "type": "column",
                "color": "#5c5c5c",
                "valueField": "MobilecallEvents"
            }
            ],
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "colors": ["#863ba2", "#28628e", "#298a52", "#c53c2e", "#5c5c5c"],
            "export": {
                "enabled": true
            }
        });

        //chartStopagediv

        var chartkey = [];
        if (stoppagedata.length > 0) {
            $.each(stoppagedata[0], function (key, value) {
                chartkey.push(key);
            });
        }

        var chart = AmCharts.makeChart("chartStopagediv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Stoppage in Minutes "

            },
            "dataProvider": stoppagedata,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "No of Stoppage Instances"
            }],
            "graphs": [{
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[1],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[1]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[2],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[2]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[3],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[3]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[4],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[4]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[5],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[5]
            }
                ,
                //{
            //    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>Time: <b>[[value]] Min</b></span>",
            //    "fillAlphas": 0.8,
            //    "labelText": "[[value]]",
            //    "lineAlpha": 0.3,
            //    "title": "Stop6",
            //    "type": "column",
            //    "color": "#000000",
            //    "valueField": "Stop6"
            //}
            ],
            "categoryField": "recorddatetime",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });
    }

    function generateChartData(alldata) {
       
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddate;
            var hardacceleration = data.hardacceleration;
            var hardbraking = data.hardbraking;
            var cornerpacket = data.cornerpacket;
            var overspeedstarted = data.overspeedstarted;
            var mcall = data.mcall;



            chartData.push({
                date: cdatetime,
                BreakingEvents: hardbraking,
                AccelarationEvents: hardacceleration,
                Cornering: cornerpacket,
                OverspeedstartedEvents: overspeedstarted,
                MobilecallEvents: mcall
            });

        });


        return chartData;
    }
    $scope.GetGraphData = function () {
        var filters = {
            DeviceNo: $scope.vehicleRefId,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IotGraphicalReportFactory.GetGraphData(filters).then(function (result) {
            SetGraphData(result);
        }, function (error) {
            console.log(error);
        });
    }


    function init() {
        if ($window.sessionStorage["dashaction"]) {
            var params = JSON.parse($window.sessionStorage["dashaction"]);
            if (params.action == "graph") {
                $scope.vehicleRefId = params.deviceno;
                $scope.GetGraphData();
            }
        }

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();

});