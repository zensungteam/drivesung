﻿"use strict";

angular.module('app.IotGraphicalReport').factory('IotGraphicalReportFactory', function ($http, $q, APP_CONFIG) {

    var GetGraphData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetGraphData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    return {
        GetGraphData: GetGraphData
    };

});