﻿"use strict";

angular.module('app.updateprofile').controller('updateregisterCtrl', function ($scope, $rootScope, $state, $http, $window, $location, updateprofileFactory, loginFactory) {



    function Showprofile() {
       
        $scope.refer = {

            user_id: $rootScope.userInfo.user_id,
            s_user_id: 1,
            s_role_Id: 1,
        }

        updateprofileFactory.Showregistration($scope.refer).then(function (result) {
            var dt = new Date();
            var resultData = result.data.data;
         
                $scope.user_name = resultData[0].user_name;
                $scope.email_id1 = resultData[0].email_id1;
                $scope.user_pwd = resultData[0].user_pwd;
                $scope.passwordConfirm = resultData[0].passwordConfirm;
                $scope.first_name = resultData[0].first_name;
                $scope.middle_name = resultData[0].middle_name;
                $scope.last_name = resultData[0].last_name;
                $scope.mobileno = resultData[0].mobileno;
               //$scope.date_of_birth = resultData[0].date_of_birth;
                $scope.date_of_birth = resultData[0].wdb;
                $scope.gender = resultData[0].gender;
                $scope.user_id = resultData[0].user_id;
                $scope.profile_picture_uri = resultData[0].profile_picture_uri;
                $scope.address1 = resultData[0].address1;
                $scope.deviceno = resultData[0].deviceno;
                $scope.vehicle_no = resultData[0].vehicle_no;
                $scope.license_no = resultData[0].license_no;
               //$scope.license_expiry_date = resultData[0].license_expiry_date;
                $scope.license_expiry_date = resultData[0].wled;
                $scope.vehicle_color = resultData[0].vehicle_color == "" ? "0" : resultData[0].vehicle_color;
                $scope.vehicle_type = resultData[0].vehicle_type == "" ? "0" : resultData[0].vehicle_type;
                $scope.country = resultData[0].country;
                $scope.pincode = resultData[0].pincode;
                $scope.region = resultData[0].region;
                $scope.city = resultData[0].city;
                $scope.state = resultData[0].state;
                $scope.insurance_mapping_key = resultData[0].insurance_mapping_key;
                //$scope.registration_no = resultData[0].registration_no;
                $scope.vehiclengine_type = resultData[0].vehiclengine_type
                $scope.insurance_provider = resultData[0].insurance_provider,
                $scope.insurer_emailid = resultData[0].insurer_emailid,
                $scope.engine_capacity = resultData[0].engine_capacity,
                $scope.insurance_comp_contact = resultData[0].insurance_comp_contact,
                $scope.sos_contact = resultData[0].sos_contact,
                $scope.family_contact = resultData[0].family_contact,

             
                        
                $('#cmbdate_of_birth').combodate({
                    value: $scope.date_of_birth,
                    minYear: '1960',
                    maxYear: moment().format('YYYY')
                });
                $('#cmbexpirydate').combodate({
                    value: $scope.license_expiry_date,
                    minYear: '1960',
                    maxYear: moment().format('YYYY')
                });
                

               
            
        }, function (error) {
            console.log(error);
        });
    }


    $scope.updateregister = function () {

        var registerData = {
            'user_name': $scope.user_name,
            'email_id1': $scope.email_id1,
            'user_pwd': $scope.user_pwd,
            'passwordConfirm': $scope.passwordConfirm,
            'first_name': $scope.first_name,
            'middle_name': $scope.middle_name,
            'last_name': $scope.last_name,
            'mobileno': $scope.mobileno,
            'date_of_birth': $scope.date_of_birth,
            'gender': $scope.gender,
            'address1': $scope.address1,
            'deviceno': $scope.deviceno,
            'vehicle_no': $scope.vehicle_no,
            'license_no': $scope.license_no,
            'license_expiry_date': $scope.license_expiry_date,
            'vehicle_color': $scope.vehicle_color,
            'vehicle_type': $scope.vehicle_type,
            'country': $scope.country,
            'pincode': $scope.pincode,
            'region': $scope.region,
            'city': $scope.city,
            'state': $scope.state,
            's_role_Id': 1,
            'user_id': $scope.user_id,
            's_user_id': $scope.user_id,
            'insurance_mapping_key': $scope.insurance_mapping_key,
            //'registration_no': $scope.registration_no,
            'vehiclengine_type': $scope.vehiclengine_type,
            'insurance_provider': $scope.insurance_provider,
            'insurer_emailid': $scope.insurer_emailid,
            'engine_capacity': $scope.engine_capacity,
            'insurance_comp_contact': $scope.insurance_comp_contact,
            'sos_contact': $scope.sos_contact,
            'family_contact': $scope.family_contact,
            'o_id': 1
         };

        var fd = new FormData();
        fd.append('file', $scope.profile_picture_uri[0]);
        fd.append('data', JSON.stringify(registerData));

        updateprofileFactory.Updateregistration(fd).then(function (result) {

            $.smallBox({
                title: "User register succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            init();
           

        }, function (error) {
            console.log(error);
        });

    }


    function getEngineCapacityList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        loginFactory.getEngineCapacityList(filters)
        .then(function (result) {
            $scope.EngineCapacityList = JSON.parse(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }


    function getEngineTypeList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        loginFactory.getEngineTypeList(filters)
        .then(function (result) {
            $scope.EngineTypeList = JSON.parse(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function getVehicleTypeList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        loginFactory.getVehicleTypeList(filters)
        .then(function (result) {
            $scope.VehicleTypeList = JSON.parse(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    var init = function () {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            //$rootScope.userInfo.lastLoginDate = $filter('date')($rootScope.userInfo.lastLoginDate, APP_CONFIG.dateFormat + ' hh:mm:ss a');
        }
        Showprofile();
        getEngineCapacityList();
        getEngineTypeList();
        getVehicleTypeList();
        
     
    }

    init();
})
