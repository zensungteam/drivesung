﻿"use strict";

angular.module('app.updateprofile', ['ui.router'])

angular.module('app.updateprofile').config(function ($stateProvider) {

 
    $stateProvider
      
       .state('app.updateprofile', {
           url: '/updateprofile',
           //data: {
           //    title: 'Parameters updateprofile List'
           //},
           views: {
               "content@app": {
                   templateUrl: 'app/updateprofile/views/updateregister.html',
                   controller: 'updateregisterCtrl'
               }
           },
           resolve: {
               scripts: function (lazyScript) {
                   return lazyScript.register([
                       'build/vendor.ui.js',
                       'build/vendor.datatables.js'
                   ]);
               }
           }
       })



       
});
