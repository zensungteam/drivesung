﻿'use strict';

angular.module('app.updateprofile').factory('updateprofileFactory', function ($http, $q, APP_CONFIG) {
  

    var Updateregistration = function (fd) {


        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/UpdateUserProfile";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}


        var deferred = $q.defer();

        $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var Showregistration = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/GetUserById";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
   
    return {
      
        Updateregistration: Updateregistration,
        Showregistration: Showregistration
       
    }
});
