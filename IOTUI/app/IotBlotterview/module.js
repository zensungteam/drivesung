'use strict';

angular.module('app.IotBlotterView', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
 

.config(function ($stateProvider) {
    $stateProvider
        .state('app.IotBlotterViewAdmin', {
            url: '/IotBlotterView',
            views: {
                "content@app": {
                    controller: 'IotBlotterViewACtrl',
                    templateUrl: 'app/IotBlotterview/views/blotterViewAdmin.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },


           
        })
    
    .state('app.IotBlotterViewUser', {
            url: '/IotBlotterViewUser',
            views: {
                "content@app": {
                    controller: 'IotBlotterViewUserCtrl',
                    templateUrl: 'app/IotBlotterview/views/blotterViewUser.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },


           
        });
});
