'use strict';

angular.module('app.IotBlotterView').controller('IotBlotterViewUserCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IotBlotterViewFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        $rootScope.Timer = null;
        SetCountzero();
        $scope.circle = []
        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });

    }

    function SetCount() {
        var count = JSON.parse($scope.count);
        $scope.HB = count[0].hardbraking;
        $scope.HA = count[0].hardacceleration;
        $scope.HC = count[0].cornerpacket;
        $scope.OS = count[0].overspeedstarted;
        $scope.CT = count[0].mcall;

    }
    function SetCountzero() {

        $scope.HB = 0;
        $scope.HA = 0;
        $scope.HC = 0;
        $scope.OS = 0;
        $scope.CT = 0;

    }

    function SetChartData() {

        var overall_rating = JSON.parse($scope.overall_rating);
        var daily_rating = JSON.parse($scope.daily_rating);
        var overall_trip_rating = JSON.parse($scope.overall_trip_rating);
        var speeddata = JSON.parse( $scope.speeddata );
        if (overall_rating.length > 0) {
            var chart = AmCharts.makeChart("driverSpiderdiv", {
                "type": "serial",
                "theme": "light",
                "marginRight": 70,
                "dataProvider": [{
                    "parameters": "Over Speed",
                    "score": overall_rating[0].overspeedscore,
                    "color": "#c53c2e"
                }, {
                    "parameters": "Harsh Event",
                    "score": overall_rating[0].harsh_score,
                    "color": "#28628e"
                }, {
                    "parameters": "Calls Taken",
                    "score": overall_rating[0].calling_score,
                    "color": "#5c5c5c"
                }],
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Parameter score"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "<b>[[category]]: [[value]]</b>",
                    "fillColorsField": "color",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "score"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": true
                },
                "categoryField": "parameters",
                "categoryAxis": {
                    "gridPosition": "start",
                    "labelRotation": 45
                },
                "export": {
                    "enabled": true
                }

            });
        }

        if (daily_rating.length >0) {
            var chart = AmCharts.makeChart("DayWiseScorediv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": daily_rating,
                "synchronizeGrid": true,
                "valueAxes": [{
                    "id": "v1",
                    "axisColor": "#22958a",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "left",
                    "title": "Score"
                }, {
                    "id": "v2",
                    "axisColor": "#d28924",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "right",
                    "title": "Distance Travel (km)"
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "lineColor": "#22958a",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Score",
                    "valueField": "final_score",
                    "fillAlphas": 0
                }, {
                    "valueAxis": "v2",
                    "lineColor": "#d28924",
                    "bullet": "square",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Distance(km)",
                    "valueField": "distance",
                    "fillAlphas": 0
                }],
                "chartScrollbar": {},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                "categoryField": "recorddate",
                "categoryAxis": {
                    //"parseDates": true,
                    "axisColor": "#DADADA",
                    //"minPeriod": "hh:mm:ss",
                    "minorGridEnabled": true,
                    "title": "Date"

                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
        }
        if (speeddata.length > 0) {
            var data = generateSpeedData(speeddata);
            var chart = AmCharts.makeChart("chartdivspeed", {
                "type": "serial",
                "theme": "light",
                "marginRight": 70,
                "dataProvider": data,
                "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Percentage(%)"
                }],
                "startDuration": 1,
                "graphs": [{
                    "balloonText": " Speed Range-[[category]] (km/hr): <b>[[value]] %</b>",
                    "fillColorsField": "color",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "line",
                    "valueField": "percentage",

                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "speedrange",
                "categoryAxis": {
                    "gridPosition": "start",
                    "labelRotation": 0,
                    "title": "Speed Range (km/hr)"
                },
                "export": {
                    "enabled": true
                }

            });
        }

        var chartdivscore = AmCharts.makeChart("chartdivscore", {
            "theme": "light",
            "type": "gauge",
            "axes": [{
                "topTextFontSize": 20,
                "topTextYOffset": 70,
                "axisColor": "#31d6ea",
                "axisThickness": 1,
                "endValue": 100,
                "gridInside": true,
                "inside": true,
                "radius": "50%",
                "valueInterval": 20,
                "tickColor": "#67b7dc",
                "startAngle": -90,
                "endAngle": 90,
                "unit": "",
                "bandOutlineAlpha": 0,
                "bands": [{
                    "color": "#FF5533",
                    "endValue": 60,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 0
                }, {
                    "color": "#FFE633",
                    "endValue": 80,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 60
                }, {
                    "color": "#93FF33",
                    "endValue": 100,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 80
                }]
            }],
            "arrows": [{
                "alpha": 1,
                "innerRadius": "35%",
                "nailRadius": 0,
                "radius": "170%",
                "value":$rootScope.SubHeaderDataCnt[0].rating
            }]
        });

      
        //setInterval(randomValue, 2000);
        //// set random value
        //function randomValue() {
        //    //  var value = Math.round(Math.random() * 100)
        //    var value = $rootScope.SubHeaderDataCnt[0].rating;
        //    chartdivscore.arrows[0].setValue(value);
        //    chartdivscore.axes[0].setTopText(value);
        //    // adjust darker band to new value
        //    // chart.axes[0].bands[1].setEndValue(value);
        //}


       



        // chart.dataDateFormat = "dd:mm:yy";
        //chart.addListener("dataUpdated", zoomChart);
        //zoomChart(chart);


        //var chart = AmCharts.makeChart("TripWiseScorediv", {
        //    "type": "serial",
        //    "theme": "light",
        //    "legend": {
        //        "useGraphSettings": true,
        //    },
        //    "dataProvider": overall_trip_rating,
        //    "synchronizeGrid": true,
        //    "valueAxes": [{
        //        "id": "v1",
        //        "axisColor": "#22958a",
        //        "axisThickness": 2,
        //        "axisAlpha": 1,
        //        "position": "left",
        //        "title": "Score"
        //    }, {
        //        "id": "v2",
        //        "axisColor": "#d28924",
        //        "axisThickness": 2,
        //        "axisAlpha": 1,
        //        "position": "right",
        //        "title": "Distance Travel (km)"
        //    }],
        //    "graphs": [{
        //        "valueAxis": "v1",
        //        "lineColor": "#22958a",
        //        "bullet": "round",
        //        "bulletBorderThickness": 1,
        //        "hideBulletsCount": 30,
        //        "title": "Score",
        //        "valueField": "rating",
        //        "fillAlphas": 0
        //    }, {
        //        "valueAxis": "v2",
        //        "lineColor": "#d28924",
        //        "bullet": "square",
        //        "bulletBorderThickness": 1,
        //        "hideBulletsCount": 30,
        //        "title": "Distance(km)",
        //        "valueField": "distance",
        //        "fillAlphas": 0
        //    }],
        //    "chartScrollbar": {},
        //    "chartCursor": {
        //        "cursorPosition": "mouse"
        //    },
        //    "categoryField": "trip_name",
        //    "categoryAxis": {
        //        //"parseDates": true,
        //        "axisColor": "#DADADA",
        //        //"minPeriod": "hh:mm:ss",
        //        "minorGridEnabled": true,
        //        "title": "Trip Name"

        //    },
        //    "export": {
        //        "enabled": true,
        //        "position": "bottom-right"
        //    }
        //});
        //// chart.dataDateFormat = "dd:mm:yy";
        ////chart.addListener("dataUpdated", zoomChart);
        ////zoomChart(chart);


    }

    function zoomChart(chart) {
        if (chart.dataProvider.length > 0) {
            chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
        }
    }

    function generateSpeedData(speeddata) {
        var chartData = [];
        angular.forEach(speeddata, function (data, index) {
            chartData.push({
                speedrange: data.speedrange,
                percentage: data.percentage,
                color: data.color
                //rpm: crpm
            });

        });

        return chartData;
    }

    function SetVehicleData() {

        //var live = JSON.parse(live);
        //var count = JSON.parse(count);
        //var overall_rating = JSON.parse(overall_rating);
        //var daily_rating = JSON.parse(daily_rating);
        //var overall_trip_rating = JSON.parse(overall_trip_rating);

        loadDefaultMap();
        SetCountzero();
        SetMapData();
        SetCount();
        SetChartData();


    }

    function SetMapData() {
        var alldata = JSON.parse($scope.live);
        var markersr = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var options;
        var alertorstatus;
        var address;
        angular.forEach(alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;


            if (data.status == 'disconnect') {
                options = { icon: 'styles/img/markersicon/disconnect.png' };
                alertorstatus = "Disconnect";
            }

            if (data.status == 'stop') {
                options = { icon: 'styles/img/markersicon/parked.png' };
                alertorstatus = "stop";
            }

            if (data.status == 'moving') {
                options = { icon: 'styles/img/markersicon/moving.png' };
                alertorstatus = "Moving";
            }

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Hard Breaking";
                }
            }

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Hard Breaking";
                }
            }
            if (data.overspeedstarted != null) {
                if (data.overspeedstarted) {
                    options = { icon: 'styles/img/markersicon/overspeed.png' };
                    alertorstatus = "Over Speed";
                }
            }

            if (data.cornerpacket != null) {
                if (data.cornerpacket) {
                    options = { icon: 'styles/img/markersicon/ignitionon.png' };
                    alertorstatus = "Cornering";
                }
            }

            if (data.mcall != null) {
                if (data.mcall) {
                    options = { icon: 'styles/img/markersicon/parked.png' };
                    alertorstatus = "Calls Taken";
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    options = { icon: 'styles/img/markersicon/hardaccels.png' };
                    alertorstatus = "Hard Acceleration";
                }
            }

            var vehicledetails = {
                vehicle_no: data.deviceno,
                speed: data.speed,
                drivername: data.first_name + ' ' + data.middle_name + ' ' + data.last_name,
                derivermobileno: data.mobileno,
                emailid: data.email_id1,
                recorddatetime: data.recorddatetime,
                latitude: data.latitude,
                longitude: data.longitude,
                address: data.address1,
                alert: alertorstatus
            }

            markersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                options: options,
                vehicledetails: vehicledetails
            });


        });
        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng }, zoom: 15, bounds: {},
            control: {},
            markersr: markersr
        };

        $scope.circle = [];

        var newObj = {
            latitude: cureentlat,
            longitude: currentlng
        };
        $scope.circle.push(newObj);


    }

    function loadDefaultMap() {
        $scope.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 5, bounds: {},
            control: {},
            markersr: []
        };
    }


    $scope.GetVehicleData = function () {

        loadInitData();
        loadDefaultMap();

        var filters = {
            DeviceNo: $rootScope.userInfo.deviceno,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        IotBlotterViewFactory.getVehicleData(filters)
        .then(function (result) {
            $scope.live = JSON.stringify(result.data.data.live);
            $scope.count = JSON.stringify(result.data.data.count);
            $scope.overall_rating = JSON.stringify(result.data.data.overall_rating);
            $scope.daily_rating = JSON.stringify(result.data.data.daily_rating);
            $scope.overall_trip_rating = JSON.stringify(result.data.data.overall_trip_rating);
            $scope.speeddata = JSON.stringify(result.data.data.speedrpt);
            SetVehicleData();


        }, function (error) {
            console.log(error);
        });
    }


    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {

            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.windowOptions = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.windowOptions.visible = false;
    };

    //start Auto referesh
    function StartAutoRefresh() {
        $rootScope.Timer = $interval(function () {
            $scope.GetVehicleData();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };

    function init() {
       
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        $scope.GetVehicleData();
    }

    init();

});