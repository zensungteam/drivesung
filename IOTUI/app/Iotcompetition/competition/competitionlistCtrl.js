﻿'use strict';

angular.module('app.configsetting').controller('competitionlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, competitionFactory) {

    function loadInitData() {
        var param = {
            "s_user_id": 1,
            "s_role_Id": 1
        };


        competitionFactory.getCompetitionList(param).then(function (result) {
            debugger;
            setInitialData(result.data.data.all);
            showData(result.data.data.all);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.getcompetitionListDataModel = ListData;
    }

    function showData(ListData) {
        showcompetitionList(ListData);
    }

    function showcompetitionList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "cmpt_id" },
                { data: "cmpt_name" },
                { data: "start_date" },
                { data: "end_date" },
                { data: "min_km_travel" },
                { data: "prize_desc1" },
                { data: "prize_desc2" },
                { data: "prize_desc3" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" }
            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 8 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.cmpt_id + ")'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 9 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.cmpt_id + ")'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 10 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.cmpt_id + ")'><i class='fa fa-times'></i></button>");
            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {
      
        var params = { 'action': 'New', 'cmpt_id': 0 };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.editRow = function (cmpt_id) {
        debugger;
        var params = { 'action': 'Edit', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.viewRow = function (cmpt_id) {
        var params = { 'action': 'View', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.deleteRow = function (cmpt_id) {
        var params = { 'action': 'Edit', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Competitions",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteCompetitionRecord(cmpt_id);
            }
        });
    }

   
    function deleteCompetitionRecord(cmpt_id) {
      
        var param = {
            'cmpt_id':cmpt_id,
            's_user_id': 1,
            's_role_Id': 2

        };

        competitionFactory.deleteCompetitionRecord(param)
             .then(function (result) {
                 debugger;
                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "Parameters Config Setting",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                 $state.go('app.competition.list');

             },
              function (error) {
                 console.log(error);
             });

    }

    function init() {
        loadInitData();
    }

    init();
});
