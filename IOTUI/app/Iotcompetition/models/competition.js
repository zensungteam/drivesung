﻿"use strict"

angular.module('app.competition').factory('competitionFactory', function ($http, $q, APP_CONFIG) {

    var getCompetitionList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/getCompetitionList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var deleteCompetitionRecord = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/CancelCompitition";
        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }




    var getCompetitionDetails = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/GetCompitionbyId";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var saveCompetitionRecord = function (fd) {
        debugger;
        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/CreateCompitition";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}


        var deferred = $q.defer();

        $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var updateCompetitionRecord = function (fd) {
        debugger;
        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/Compitition/UpdateCompitition";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}


        var deferred = $q.defer();

        $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {
        getCompetitionList: getCompetitionList,
        getCompetitionDetails: getCompetitionDetails,
        saveCompetitionRecord: saveCompetitionRecord,
        updateCompetitionRecord: updateCompetitionRecord,
        deleteCompetitionRecord: deleteCompetitionRecord
    };



});
