﻿"use strict";

angular.module('app.Iotcompetition', ['ui.router'])

angular.module('app.Iotcompetition').config(function ($stateProvider) {

    $stateProvider
        .state('app.Iotcompetition', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.Iotcompetition.list', {
            url: '/competitionlist',
            data: {
                title: 'Parameters Competition List'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/Iotcompetition/views/competitionlist.html',
                    controller: 'competitionlistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

        .state('app.Iotcompetition.detail', {
            url: '/competition',
            data: {
                title: 'Competition Details'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/Iotcompetition/views/competitiondetails.html',
                    controller: 'IotcompetitionAddEditCtrl'
                }
            }
            //resolve: {
            //    srcipts: function (lazyScript) {
            //        return lazyScript.register([
            //               'build/vendor.ui.js',
            //            'build/vendor.datatables.js'

            //        ])
            //    }
            //}
        })
});
