﻿"use strict";

angular.module('app.home').directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        //required: 'ngModel',
        link: function (scope, element, attrs) {

            element.bind('change', function () {
                if (element[0].files[0].size > 5000000) {
                    $.SmartMessageBox({
                        title: "Files size.",
                        content: "Files are larger than the specified max (5MB).",
                        buttons: '[Ok]'
                    }, function (ButtonPressed) {
                    });
                    return;
                }
                else {

                    var strFileName = element[0].files[0].name;
                    var value = strFileName.split(".")
                    var extension = value[1];
                    var allowedFormat = attrs.allowedFormat;
                    if (allowedFormat != null && allowedFormat.length > 0) {

                        var splittedAllowedFormat = allowedFormat.split(",");
                        if (splittedAllowedFormat.indexOf(extension.toLowerCase()) != -1) {
                            var model = $parse(attrs.fileModel);
                            var modelSetter = model.assign;

                            scope.$apply(function () {
                                modelSetter(scope, element[0].files);
                            });
                        } else {
                            $.SmartMessageBox({
                                title: "Files Type.",
                                content: extension.toUpperCase() + " File Not Allowed.",
                                buttons: '[Ok]'
                            }, function (ButtonPressed) {
                            });
                            return;
                        }
                    } else {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        scope.$apply(function () {
                            modelSetter(scope, element[0].files);

                        });
                    }


                }

            });

          

            //  var validFormats = ['pdf', 'gif', 'jpg', 'jpeg'];
            //ngModel.$render = function () {
            //    ngModel.$setViewValue(elem.val());
            //};
            //function validImage(bool) {
            //    ngModel.$setValidity('extension', bool);
            //}
            //ngModel.$parsers.push(function (value) {
            //    var ext = value.substr(value.lastIndexOf('.') + 1);
            //    if (ext == '') return;
            //    if (validFormats.indexOf(ext) == -1) {
            //        return value;
            //    }
            //    validImage(true);
            //    return value;
            //});
        }
    };
}]);

angular.module('app.home').directive('fileModel1', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        //require: 'ngModel',
        link: function (scope, element, attrs) {

            element.bind('change', function () {
                if (element[0].files[0].size > 5000000) {
                    $.SmartMessageBox({
                        title: "Files size.",
                        content: "Files are larger than the specified max (5MB).",
                        buttons: '[Ok]'
                    }, function (ButtonPressed) {
                    });
                    return;
                }
                else {
                    var strFileName = element[0].files[0].name;
                    if (!strFileName.match("^[0-9A-Za-z.-]*$"))
                    {
                        $.SmartMessageBox({
                            title: "Files name.",
                            content: "The Uploaded File can consist of alphabet, number and spaces only.",
                            buttons: '[Ok]'
                        }, function (ButtonPressed) {
                        });
                        return;
                    }

                    var model = $parse(attrs.fileModel);
                    var modelSetter = model.assign;

                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);


                    });
                }

            });

            //var validFormats = ['jpg', 'jpeg', 'png'];
            //ngModel.$render = function ()
            //{
            //    ngModel.$setViewValue(elem.val());
            //};
            //function validImage(bool) {
            //    ngModel.$setValidity('extension', bool);
            //}
            //ngModel.$parsers.push(function (value)
            //{
            //    var ext = value.substr(value.lastIndexOf('.') + 1);
            //    if (ext == '') return;
            //    if (validFormats.indexOf(ext) == -1) {
            //        return value;
            //    }
            //    validImage(true);
            //    return value;
            //});

        }
    };
}]);




// Here is where the magic works
angular.module('app.home').directive('date', function (dateFilter) {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['date'] || 'dd-MM-yyyy';

            ctrl.$formatters.unshift(function (modelValue) {
                return dateFilter(modelValue, dateFormat);
            });
        }
    };
})

angular.module('app.home').directive('inputDateRange', ['$compile', '$parse', '$filter', function ($compile, $parse, $filter) {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function ($scope, $element, $attributes, ngModel) {

            if ($attributes.type !== 'daterange' || ngModel === null) {
                return;
            }

            var options = {};
            options.format = $attributes.format || 'DD-MM-YYYY';
            options.separator = $attributes.separator || ' - ';
            options.minDate = $attributes.minDate && moment($attributes.minDate);
            options.maxDate = $attributes.maxDate && moment($attributes.maxDate);
            options.dateLimit = $attributes.limit && moment.duration.apply(this, $attributes.limit.split(' ').map(function (elem, index) {
                return index === 0 && parseInt(elem, 10) || elem;
            }));
            options.ranges = $attributes.ranges && $parse($attributes.ranges)($scope);
            options.locale = $attributes.locale && $parse($attributes.locale)($scope);
            options.opens = $attributes.opens || $parse($attributes.opens)($scope);

            if ($attributes.enabletimepicker) {
                options.timePicker = true;
                angular.extend(options, $parse($attributes.enabletimepicker)($scope));
            }

            function datify(date) {
                return moment.isMoment(date) ? date.toDate() : date;
            }

            function momentify(date) {
                return (!moment.isMoment(date)) ? moment(date) : date;
            }

            function format(date) {
                return $filter('date')(datify(date), options.format.replace(/Y/g, 'y').replace(/D/g, 'd')); //date.format(options.format);
            }

            function formatted(dates) {
                return [format(dates.startDate), format(dates.endDate)].join(options.separator);
            }

            ngModel.$render = function () {
                if (!ngModel.$viewValue || !ngModel.$viewValue.startDate) {
                    return;
                }
                $element.val(formatted(ngModel.$viewValue));
            };

            $scope.$watch(function () {
                return $attributes.ngModel;
            }, function (modelValue, oldModelValue) {

                if (!$scope[modelValue] || (!$scope[modelValue].startDate)) {
                    ngModel.$setViewValue({
                        startDate: moment().startOf('day'),
                        endDate: moment().startOf('day')
                    });
                    return;
                }

                if (oldModelValue !== modelValue) {
                    return;
                }

                $element.data('daterangepicker').startDate = momentify($scope[modelValue].startDate);
                $element.data('daterangepicker').endDate = momentify($scope[modelValue].endDate);
                $element.data('daterangepicker').updateView();
                $element.data('daterangepicker').updateCalendars();
                $element.data('daterangepicker').updateInputText();

            });

            $element.daterangepicker(options, function (start, end, label) {

                var modelValue = ngModel.$viewValue;

                if (angular.equals(start, modelValue.startDate) && angular.equals(end, modelValue.endDate)) {
                    return;
                }

                $scope.$apply(function () {
                    ngModel.$setViewValue({
                        startDate: (moment.isMoment(modelValue.startDate)) ? start : start.toDate(),
                        endDate: (moment.isMoment(modelValue.endDate)) ? end : end.toDate()
                    });
                    ngModel.$render();
                });

            });

        }

    };

}]);

angular.module('app.home').directive('embedSrc', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var current = element;
            scope.$watch(function () { return attrs.embedSrc; }, function () {
                var clone = element
                              .clone()
                              .attr('src', attrs.embedSrc);
                current.replaceWith(clone);
                current = clone;
            });
        }
    };
});

angular.module('app.home').directive('myUpload', [function () {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var reader = new FileReader();
            reader.onload = function (e) {
                scope.myuploadimage = e.target.result;
                scope.$apply();
            }

            elem.on('change', function () {
                reader.readAsDataURL(elem[0].files[0]);
            });
        }
    };
}]);

angular.module('app.home').directive('loadAnimationWrap', function ($rootScope, $timeout, $state, $location) {
    return {
        restrict: 'A',
        scope: false,
        link: function (scope, element, attributes) {

            element.removeAttr('smart-router-animation-wrap data-smart-router-animation-wrap wrap-for data-wrap-for');
            element.addClass('router-animation-container');

            var $loader = $('<div class="router-animation-loader"><i class="fa fa-gear fa-4x fa-spin"></i></div>')
                .css({
                    position: 'absolute',
                    top: 50,
                    left: 10
                }).hide().appendTo(element);


            var animateElementSelector = attributes.wrapFor;

            var needRunContentViewAnimEnd = false;
            function contentViewAnimStart() {
                needRunContentViewAnimEnd = true;
                element.css({
                    height: element.height() + 'px',
                    overflow: 'hidden'
                }).addClass('active');
                $loader.fadeIn();

                $(animateElementSelector).addClass('animated faster fadeOutDown');
            }

            function contentViewAnimEnd() {
                if (needRunContentViewAnimEnd) {
                    element.css({
                        height: 'auto',
                        overflow: 'visible'
                    }).removeClass('active');


                    $(animateElementSelector).addClass('animated faster fadeInUp');

                    needRunContentViewAnimEnd = false;

                    $timeout(function () {
                        $(animateElementSelector).removeClass('animated');
                    }, 10);
                }
                $loader.fadeOut();
            }

            var destroyForEnd = $rootScope.$on('$viewHTTPContentLoadedEnd', function (event) {
                contentViewAnimEnd();
            });

            var destroyForStart = $rootScope.$on('$viewHTTPContentLoadedStart', function (event) {
                contentViewAnimStart();
            });

            $rootScope.$on('$sessionExpiredMsg', function (event) {
                $.SmartMessageBox({
                    title: "Session Expired!",
                    content: "Session expired please login again.",
                    buttons: '[OK]'
                }, function (ButtonPressed) {
                    if (ButtonPressed === "OK") {
                        $location.path('/login');
                    }
                });
            });

            element.on('$destroy', function () {
                destroyForStart();
                destroyForEnd();

            });

        }
    }
});

angular.module('app.home').directive('printDiv', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            function printElement(elemToPrint) {
                //var printContents = document.getElementById('print-frame').innerHTML;
                var printContents = elemToPrint.innerHTML;
                var originalContents = document.body.innerHTML;

                if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                    var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWin.window.focus();
                    popupWin.document.write('<!DOCTYPE html><html><head>' +
                        '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
                    popupWin.onbeforeunload = function (event) {
                        popupWin.close();
                        return '.\n';
                    };
                    popupWin.onabort = function (event) {
                        popupWin.document.close();
                        popupWin.close();
                    }
                } else {
                    var popupWin = window.open('', '_blank', 'width=800,height=600');
                    popupWin.document.open();
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
                    popupWin.document.close();
                }
                popupWin.document.close();

                popupWin.onafterprint = function () {
                    popupWin.document.close();
                    popupWin.close();
                }
            }

            element.on('click', function () {
                var elemToPrint = document.getElementById(attrs.printelementid);
                if (elemToPrint) {
                    printElement(elemToPrint);
                }
            });


        }
    };
});

angular.module('app.home').directive('onfocusSelected', function () {
    return {
        restrict: 'A',
        link: function (scope, $element) {
            $element.on('focus', function () {
                angular.element(this).select();
            });
        }
    };
});

