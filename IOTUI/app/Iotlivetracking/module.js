'use strict';

angular.module('app.iotlivetracking', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
  .config(function (uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyA7bGFFmYR_pRQeU84EYuPVmv__bpSbqdo',
          v: '3.0', //defaults to latest 3.X anyhow
          libraries: 'weather,geometry,visualization,drawing'
      });
  })

.config(function ($stateProvider) {
    $stateProvider
        .state('app.iotlivetracking', {
            url: '/iotlivetracking',
            views: {
                "content@app": {
                    controller: 'iotlivetrackingCtrl',
                    templateUrl: 'app/Iotlivetracking/views/iotlivetracking.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },


        });
});
