﻿"use strict";

angular.module('app.iotlivetracking').factory('iotlivetrackingFactory', function ($http, $q, APP_CONFIG) {

    var GetLiveTrackingData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetLiveTrackingData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getGetTrips = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetTrips";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
       

    return {
        GetLiveTrackingData: GetLiveTrackingData,
        getVehicleList: getVehicleList,
        getGetTrips:getGetTrips
    };

});