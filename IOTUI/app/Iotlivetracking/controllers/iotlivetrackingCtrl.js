'use strict';

angular.module('app.iotlivetracking').controller('iotlivetrackingCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, iotlivetrackingFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {

        $scope.$on('$destroy', function () {
            StopAutoRefresh();
            $window.sessionStorage.removeItem("dashaction");
        });
        $scope.rdate = '';
        $scope.Timer = null;
        $scope.vehicleRefId = "";
        $scope.NoOfRecords = 20;
        $scope.HB = 0;
        $scope.HA = 0;
        $scope.HC = 0;
        $scope.OS = 0;
        $scope.CT = 0;
        $scope.CO2 = 0;
        $scope.info = "";


        //  $scope.date = new Date();

    }

    function loadDefaultMapAndChart() {
        $scope.map = {
            center: { latitude: 1.294861737596944, longitude: 103.84409782242489 }, zoom: 12, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: 0,
                longitude: 0
            }],
            Stoppagemarkersr: []
        };

        uiGmapGoogleMapApi.then(function (maps) {
            $scope.polylines = [];
        });

    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        iotlivetrackingFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function SetMapData(alldata) {
        var pl = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;

        var tempmarkersralert = [];
        var options;
        var alertorstatus;

        angular.forEach(alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;
            if (index > 0) {
                if (data.speed >= 80) {
                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#f91517',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }
                else if (data.speed > 60 && data.speed <= 79) {
                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#16872f',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }
                else {

                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#1451f9',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }

                if (data.hardbraking != null) {
                    if (data.hardbraking) {
                        options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                        alertorstatus = "Harsh Breaking";
                        var vehicledetails = {
                            speed: data.speed,
                            recorddatetime: data.recorddatetime,
                            alert: alertorstatus
                        }

                        tempmarkersralert.push({
                            id: index,
                            latitude: cureentlat,
                            longitude: currentlng,
                            options: options,
                            vehicledetails: vehicledetails
                        });
                    }
                }

                if (data.overspeedstarted != null) {
                    if (data.overspeedstarted) {
                        options = { icon: 'styles/img/markersicon/overspeed.png' };
                        alertorstatus = "Over Speed";
                        var vehicledetails = {
                            speed: data.speed,
                            recorddatetime: data.recorddatetime,
                            alert: alertorstatus
                        }

                        tempmarkersralert.push({
                            id: index,
                            latitude: cureentlat,
                            longitude: currentlng,
                            options: options,
                            vehicledetails: vehicledetails
                        });
                    }
                }

                if (data.cornerpacket != null) {
                    if (data.cornerpacket) {
                        options = { icon: 'styles/img/markersicon/cornerpacket.png' };
                        alertorstatus = "Cornering";
                        var vehicledetails = {
                            speed: data.speed,
                            recorddatetime: data.recorddatetime,
                            alert: alertorstatus
                        }

                        tempmarkersralert.push({
                            id: index,
                            latitude: cureentlat,
                            longitude: currentlng,
                            options: options,
                            vehicledetails: vehicledetails
                        });
                    }
                }

                if (data.mcall != null) {
                    if (data.mcall) {
                        options = { icon: 'styles/img/markersicon/mcall.png' };
                        alertorstatus = "Calls Taken";
                        var vehicledetails = {
                            speed: data.speed,
                            recorddatetime: data.recorddatetime,
                            alert: alertorstatus
                        }

                        tempmarkersralert.push({
                            id: index,
                            latitude: cureentlat,
                            longitude: currentlng,
                            options: options,
                            vehicledetails: vehicledetails
                        });
                    }
                }

                if (data.hardacceleration != null) {
                    if (data.hardacceleration) {
                        options = { icon: 'styles/img/markersicon/hardaccels.png' };
                        alertorstatus = "Harsh Acceleration";
                        var vehicledetails = {
                            speed: data.speed,
                            recorddatetime: data.recorddatetime,
                            alert: alertorstatus
                        }

                        tempmarkersralert.push({
                            id: index,
                            latitude: cureentlat,
                            longitude: currentlng,
                            options: options,
                            vehicledetails: vehicledetails
                        });
                    }
                }

            }
            prvlat = cureentlat;
            prvlng = currentlng;
        });

        $scope.polylines = pl;
        $scope.alertmarkersr = tempmarkersralert;

        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng }, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: cureentlat,
                longitude: currentlng
            }]
        };

    }

    function SetStoppageData(StoppageData) {
        var cureentlat;
        var currentlng;
        var stopdetails;
        var Stmarkersr = [];

        angular.forEach(StoppageData, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;
            stopdetails = 'From: ' + data.minrecorddatetime + ',   To :' + data.maxrecorddatetime + ',   stoppage :' + data.diffinmin + '(in min),  Location:' + cureentlat + ',' + currentlng;
            Stmarkersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                stopdetails: stopdetails
            });

        });

        $scope.map.Stoppagemarkersr = Stmarkersr;

    }
    // generate some random data, quite different range

    function generateChartData(alldata) {
        var chartData = [];
        var recorddatetime;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //  recorddatetime = Date.parse(data.recorddatetime);
            //   newDate = new Date(recorddatetime);

            var cdatetime = data.recorddatetime;
            var cspeed = data.speed;
            var ccoolanttemperature = data.coolanttemperature;
            var crpm = data.rpm;

            chartData.push({
                date: cdatetime,
                speed: cspeed,
                coolanttemperature: ccoolanttemperature,
                rpm: crpm
            });

        });

        //for (var i = 0; i < 100; i++) {
        //    // we create date objects here. In your data, you can have date strings
        //    // and then set format of your dates using chart.dataDateFormat property,
        //    // however when possible, use date objects, as this will speed up chart rendering.
        //    var newDate = new Date(firstDate);
        //    newDate.setDate(newDate.getDate() + i);

        //    var visits = Math.round(Math.sin(i * 5) * i);
        //    var hits = Math.round(Math.random() * 80) + 500 + i * 3;
        //    var views = Math.round(Math.random() * 6000) + i * 4;

        //    chartData.push({
        //        date: newDate,
        //        visits: visits,
        //        hits: hits,
        //        views: views
        //    });
        //}
        return chartData;
    }

    function SetCount(hdata) {

        $scope.HB = hdata[0].hb;
        $scope.HA = hdata[0].ha;
        $scope.HC = hdata[0].cor;
        $scope.OS = hdata[0].os;
        $scope.CT = hdata[0].mcall;


    }

    function SetvehicleInfo(livedata) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(livedata[0].latitude, livedata[0].longitude);
        var location;
        $scope.rdate = livedata[0].recorddatetime;
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location = "Location:" + results[1].formatted_address;
                }
                else {
                    location = "Location:" + livedata[0].latitude + ',' + livedata[0].longitude;

                }
                $scope.info = 'Device No: ' + livedata[0].deviceno + ',   Status :' + livedata[0].status + ',   Mobile No:' + livedata[0].mobileno + ',   ' + location;
            }
        })

    }

    function SetChartData(alldata) {
        if ($rootScope.userInfo.showobd) {
            var chartData = generateChartData(alldata);
            var chart = AmCharts.makeChart("chartdiv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "synchronizeGrid": true,
                "valueAxes": [{
                    "id": "v1",
                    "axisColor": "#22958a",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "left"
                }, {
                    "id": "v2",
                    "axisColor": "#d28924",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "right"
                }, {
                    "id": "v3",
                    "axisColor": "#bf4839",
                    "axisThickness": 2,
                    "gridAlpha": 0,
                    "offset": 50,
                    "axisAlpha": 1,
                    "position": "left"
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "lineColor": "#22958a",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Speed",
                    "valueField": "speed",
                    "fillAlphas": 0
                }, {
                    "valueAxis": "v2",
                    "lineColor": "#d28924",
                    "bullet": "square",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "RPM",
                    "valueField": "rpm",
                    "fillAlphas": 0
                }, {
                    "valueAxis": "v3",
                    "lineColor": "#bf4839",
                    "bullet": "triangleUp",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Coolant Temperature",
                    "valueField": "coolanttemperature",
                    "fillAlphas": 0
                }],
                "chartScrollbar": {},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"parseDates": true,
                    "axisColor": "#DADADA",
                    //"minPeriod": "hh:mm:ss",
                    "minorGridEnabled": true,

                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });

            // chart.dataDateFormat = "dd:mm:yy";
            chart.addListener("dataUpdated", zoomChart);
            zoomChart(chart);
        }
        if (!$rootScope.userInfo.showobd) {
            var chartData = generateChartData(alldata);
            var chart = AmCharts.makeChart("chartdivM", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "synchronizeGrid": true,
                "valueAxes": [{
                    "id": "v1",
                    "axisColor": "#22958a",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "left"
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "lineColor": "#22958a",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Speed",
                    "valueField": "speed",
                    "fillAlphas": 0
                }],
                "chartScrollbar": {},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"parseDates": true,
                    "axisColor": "#DADADA",
                    //"minPeriod": "hh:mm:ss",
                    "minorGridEnabled": true,

                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
            // chart.dataDateFormat = "dd:mm:yy";
            chart.addListener("dataUpdated", zoomChart);
            zoomChart(chart);
        }
    }

    function zoomChart(chart) {
        chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
    }

    function fillDashBoardLiveData(livedata, alldata, stoppage, count) {
        var alldata = JSON.parse(alldata);
        var livedata = JSON.parse(livedata);
        var count = JSON.parse(count);
        var stoppage = stoppage == undefined ? [] : JSON.parse(stoppage);
        if (livedata.length > 0) {
            fillVehicleDataGrid(alldata);
            SetChartData(alldata);
            SetMapData(alldata);
            SetCount(count);
            SetvehicleInfo(livedata);
            SetStoppageData(stoppage);
        }
        else {
            loadDefaultMapAndChart();
            fillVehicleDataGrid(alldata);
            SetChartData(alldata);
        }

    }

    function fillVehicleDataGrid(alldata) {
        if (!$rootScope.userInfo.showobd) {
            var tableOptions = {
                "data": alldata,
                columns: [
                    { data: "recorddatetime" },
                    { data: "speed" },
                    { data: "alert" }
                ],
                "order": [[0, 'desc']],
                "mColumns": [0, 1, 2],
                destroy: true
                          ,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    var locationdata = {
                        id: iDataIndex,
                        lat: aData.latitude,
                        lng: aData.longitude
                    };
                    $('td:eq(0)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                }
            };

            $scope.dashTablebindm(tableOptions, $scope);
        }
        else {
            var tableOptions = {
                "data": alldata,
                columns: [
                    { data: "recorddatetime" },
                    { data: "speed" },
                    { data: "rpm" },
                    { data: "coolanttemperature" },
                    { data: "alert" }
                ],
                "order": [[0, 'desc']],
                "mColumns": [0, 1, 2, 3, 4],
                destroy: true
               ,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    var locationdata = {
                        id: iDataIndex,
                        lat: aData.latitude,
                        lng: aData.longitude
                    };
                    $('td:eq(0)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                }
            };

            $scope.dashTablebindo(tableOptions, $scope);
        }
    }

    $scope.locationinfo = function (locationdata) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(locationdata.lat, locationdata.lng);
        var location;

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location = results[1].formatted_address;;
                }
                else {
                    location = 'lat-' + locationdata.lat + ', lng-' + locationdata.lng;
                }
                document.getElementById("container" + locationdata.id + "").innerHTML = '</small>' + location + '</small>';
            }
        })

    }

    $scope.GetLiveTrackingData = function () {
        var VechileNo = $scope.vehicleRefId;
        var DeviceNo = $scope.vehicleRefId;
        var fromdate;
        var todate;
        var fromtime;
        var totime;
        var NoOfRecords = $scope.NoOfRecords;

        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            fromdate: fromdate,
            todate: todate,
            fromtime: fromtime,
            totime: totime,
            NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        iotlivetrackingFactory.GetLiveTrackingData(filters)
        .then(function (result) {
            //format result
            fillDashBoardLiveData(JSON.stringify(result.data.data.live), JSON.stringify(result.data.data.history), JSON.stringify(result.data.data.stoppage), JSON.stringify(result.data.data.count));
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.windowOptions = {
        visible: false
    };
    $scope.windowOptionsinfo = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptionsinfo.visible = !$scope.windowOptionsinfo.visible;

    };

    $scope.closeClick = function () {
        $scope.windowOptionsinfo.visible = false;
    };

    $scope.onstopClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;

    };

    $scope.closestopClick = function () {
        $scope.windowOptions.visible = false;
    };

    //start Auto referesh
    function StartAutoRefresh() {
        //$scope.Message = "Timer started. ";

        $rootScope.Timer = $interval(function () {
            $scope.GetLiveTrackingData();
            //var lat = $scope.map.center.latitude;
            //var lng = $scope.map.center.longitude;
            //lat = lat + 0.001;
            //lng = lng - 0.001;
            ////   $scope.polylines[0].path = [];
            //$scope.polylines[0].path.push({ latitude: lat, longitude: lng });
            //$scope.map.center.latitude = lat;
            //$scope.map.center.longitude = lng;
            //$scope.map.markersr[0].latitude = lat;
            //$scope.map.markersr[0].longitude = lng;
            ////  getDashBoardLiveData();
            //$scope.$applyAsync();
        }, 10000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };

    function init() {

        loadInitData();
        loadDefaultMapAndChart();
        SetChartData([]);
        getVehicleList()

        if ($window.sessionStorage["dashaction"]) {
            var params = JSON.parse($window.sessionStorage["dashaction"]);
            if (params.action == "live") {
                $scope.vehicleRefId = params.deviceno;
                $scope.GetLiveTrackingData();
            }
        }

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }

        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

    }

    init();

});