﻿'use strict';

angular.module('app.Iotobd').controller('IotobdCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IotobdFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {

        $scope.$on('$destroy', function () {
            StopAutoRefresh();
            $window.sessionStorage.removeItem("dashaction");
        });

        $scope.idealrunning = 0;
        $scope.co2 = 0;
        $scope.os = 0;
        $scope.criticaltemp = 0;
        $scope.faultcode = 0;
    };


    function fillVehicleList(allVehicle) {
       // alert(allVehicle);
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }



    function getVehicleList() {

        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IotobdFactory.getVehicleList(filters)
        .then(function (result) {
           fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function showpanelLivedata(alldata) {
      
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
          
            $scope.idealrunning = alldata[0].idealrunning;
            $scope.co2 = alldata[0].co2;
            $scope.os = alldata[0].os;
            $scope.criticaltemp = alldata[0].criticaltemp;
            $scope.faultcode = alldata[0].faultcode;
        }
        else {
            $scope.idealrunning = 0;
            $scope.co2 = 0;
            $scope.os = 0;
            $scope.criticaltemp = 0;
            $scope.faultcode = 0;
        }
    }

    /// Obd Grid  data 
    function fillObdGridData(alldata) {
     
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
            $scope.alldata = alldata;
            fillVehicleDataGrid(alldata);
            SetChartData(alldata);

        }
        else {

            fillVehicleDataGrid(alldata);
        }

    }

    function fillVehicleDataGrid(alldata) {


        //angular.forEach(alldata, function (data, index) {
        //    var loc;
        //    if (data.mobileno == null) {
        //        data.mobileno = "";
        //    }
        //    data.timelocation = "Location: <br> Date: " + data.recorddatetime + "<br> lat:" + data.latitude + "<br> long:" + data.longitude;
        //    data.userdeatail = "";
        //    data.Graph = " ";
        //    data.Map = "Map";
        //    data.obd = " ";


        //});
        var tableOptions = {
            "data": alldata,
            //"iDisplayLength": 10,
            columns: [
                { data: "timestamp" },
                //{ data: "clientid" },
                //{ data: "deviceno" },
                //{ data: "eventcode" },
                //{ data: "recorddatetime" },
                //{ data: "latitude" },
                //{ data: "latitudedir" },
                //{ data: "longitude" },
                //{ data: "longitudedir" },
                { data: "coolanttemperature" },
                { data: "rpm" },
                { data: "speedcan" },
               // { data: "distancecan" },
                { data: "dtcdetected" },
                { data: "throttlepos" },
                { data: "runtimesinceenginestart" },
              //  { data: "averagefuelconsumption" },
               // { data: "fuellevel" },
                { data: "calculatedengineload" },
                { data: "mafairflowrate" },
             //   { data: "ambientairtemp" },
             //   { data: "fuelrate" },
                { data: "barometricpressure" },
              //  { data: "shorttermfueltrim" },
              //  { data: "fuelpressure" },
                { data: "intakemanifoldabsolutepressure" },
               // { data: "timingadvance" },
                { data: "intakeairtemp" },
                //{ data: "relativefuelrailpressure" },
                //{ data: "directfuelrailpressure" },
                //{ data: "commandedegr" },
                //{ data: "egrerror" },
                //{ data: "distsincecodecleared" },
                //{ data: "controlmodulevoltage" },
                //{ data: "absoluteloadvalue" },
                //{ data: "timerunwithmil" },
                //{ data: "timesincetroublecodescleared" },
                //{ data: "deltadist" },
                //{ data: "sequencenumber" },
                //{ data: "sysdatetime" },
                //{ data: "isactive" },
                //{ data: "malfunctionindicatorlamp" },
                //{ data: "longtermfueltrim" },
                //{ data: "oxygensensorspresent" },
                //{ data: "oxygensensorvoltage1" },
                //{ data: "oxygensensorvoltage2" },
                //{ data: "obdstandards" },
                //{ data: "distancetraveledmil" },
                //{ data: "commandedevaporativepurge" },
                //{ data: "warmupssc" },
                //{ data: "distancetraveledscc" },
                //{ data: "equivalenceratiocurrent" },
                //{ data: "fuelaircommandedequratio" },
                //{ data: "relativethrottleposition" },
                { data: "absthrottlepositionb" },
               { data: "acceleratorpedalpositiond" },
               { data: "acceleratorpedalpositione" },
               //{ data: "commandedthrottleactuator" }


            ],
            "order": [[2, 'desc']],
            "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19.20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                //    //  $('td:eq(0)', nRow).html("<img src='styles/img/crawifi.png' height='30px' width='30px' alt='connect' class='margin-bottom-5 margin-top-5'>");
                //    $('td:eq(0)', nRow).html('');
                //    if (aData.status == 'disconnect') {
                //        $('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #4169e1">');
                //    }
                //    if (aData.status == 'stop') {
                //        $('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #ccc">');

                //    }
                //    if (aData.status == 'moving') {
                //        $('td:eq(0)', nRow).append('<i class="fa fa-car fa-3x" aria-hidden="true" style="color: #9db56a">');

                //    }

                //    var DriverName = aData.first_name != null && aData.first_name != '' ? aData.first_name : aData.user_name;
                //    var VehicleNo = aData.vehicle_no != null && aData.vehicle_no != '' ? aData.vehicle_no : aData.deviceno;

                //    $('td:eq(1)', nRow).html("Driver Name:" + DriverName + "<br><a ng-click='viewIottripplayback(" + aData.deviceno + ")' > Vehicle No:" + VehicleNo + "</a><br> Contact No: " + aData.mobileno);
                //    $('td:eq(2)', nRow).html("Date:" + aData.recorddatetime + "<br> Lat:" + aData.latitude + "<br> Long:" + aData.longitude);

                //    if (aData.alert == 'HB' || aData.alert == 'HC' || aData.alert == 'HA' || aData.alert == 'OS') {
                //        $('td:eq(5)', nRow).html('');
                //        $('td:eq(5)', nRow).append('<i class="fa fa-bell fa-2x" style="color:red"></i>');
                //    }
                //    else {

                //        $('td:eq(5)', nRow).append('<i class="fa fa-bell-o fa-2x" style="color:#ff6a00"></i>');
                //    }
                //    $('td:eq(6)', nRow).append("<a data-ui-sref='app.IotGraphicalReport'><i class='fa fa-bar-chart fa-2x' style='color:#9db56a'></i></a>");
                //    $('td:eq(7)', nRow).html("<a ng-click='viewIotlivetracking(" + aData.deviceno + ")' class='fa fa-map-marker fa-2x' aria-hidden='true' style='color:#075e07'></a>");
                //    $('td:eq(8)', nRow).html("<a data-ui-sref='app.IotOBD'><img src='styles/img/ODI1.png' height='30px' width='30px' alt='img' class='margin-bottom-5 margin-top-5'></a>");
            }
        };

        $scope.dashTablebind(tableOptions, $scope);
    }

    /// Obd fault data 
    function fillObdfaultData(alldata) {
     
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
            $scope.alldata = alldata;
            fillobdfaltcodedata(alldata);

        }
        else {

            fillobdfaltcodedata(alldata);
        }

    }

    function fillobdfaltcodedata(alldata) {

         var tableOptions = {
            "data": alldata,
            //"iDisplayLength": 10,
            columns: [
                { data: "pid" },
                { data: "parameterNm" },
                { data: "recorddatetime" },
                { data: "faultDesc" }
            
             ],
            "order": [[0, 'desc']],
            "mColumns": [0, 1, 2, 3],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
          
            }
        };

         $scope.faultcodeTablebind(tableOptions, $scope);
    }

    $scope.getObdGridData = function () {
        debugger;
        var VechileNo = $scope.vehicleRefId;
        var DeviceNo = $scope.vehicleRefId;
        var fromdate;
        var todate;
        var fromtime;
        var totime;
        var NoOfRecords = $scope.NoOfRecords;

        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            NoOfRecords:NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        IotobdFactory.getObdLiveData(filters)
        .then(function (result) {
            //format result
            fillObdGridData(JSON.stringify(result.data.data.obddata));
           // fillObdfaultData(JSON.stringify(result.data.data.dtcdetected));
            showpanelLivedata(JSON.stringify(result.data.data.count));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        var chartData = [];
        var recorddatetime;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            recorddatetime = Date.parse(data.recorddatetime);
            newDate = new Date(recorddatetime);

            var cdatetime = newDate;
            var cspeed = data.speedcan;
            var ccoolanttemperature = data.coolanttemperature;
            var crpm = data.rpm;

            chartData.push({
                date: cdatetime,
                speed: cspeed,
                coolanttemperature: ccoolanttemperature,
                rpm: crpm
            });

        });

        //for (var i = 0; i < 100; i++) {
        //    // we create date objects here. In your data, you can have date strings
        //    // and then set format of your dates using chart.dataDateFormat property,
        //    // however when possible, use date objects, as this will speed up chart rendering.
        //    var newDate = new Date(firstDate);
        //    newDate.setDate(newDate.getDate() + i);

        //    var visits = Math.round(Math.sin(i * 5) * i);
        //    var hits = Math.round(Math.random() * 80) + 500 + i * 3;
        //    var views = Math.round(Math.random() * 6000) + i * 4;

        //    chartData.push({
        //        date: newDate,
        //        visits: visits,
        //        hits: hits,
        //        views: views
        //    });
        //}
        return chartData;
    }

    function SetChartData(alldata) {
      
        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": chartData,
            "synchronizeGrid": true,
            "valueAxes": [{
                "id": "v1",
                "axisColor": "#22958a",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "left"
            }, {
                "id": "v2",
                "axisColor": "#d28924",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "right"
            }, {
                "id": "v3",
                "axisColor": "#bf4839",
                "axisThickness": 2,
                "gridAlpha": 0,
                "offset": 50,
                "axisAlpha": 1,
                "position": "left"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "lineColor": "#22958a",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Speed",
                "valueField": "speed",
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "lineColor": "#d28924",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "RPM",
                "valueField": "rpm",
                "fillAlphas": 0
            }, {
                "valueAxis": "v3",
                "lineColor": "#bf4839",
                "bullet": "triangleUp",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Coolant Temperature",
                "valueField": "coolanttemperature",
                "fillAlphas": 0
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "categoryField": "date",
            "categoryAxis": {
                //"parseDates": true,
                "axisColor": "#DADADA",
                //"minPeriod": "hh:mm:ss",
                "minorGridEnabled": true,

            },
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });
        // chart.dataDateFormat = "dd:mm:yy";
        chart.addListener("dataUpdated", zoomChart);
        zoomChart(chart);

    }

    function zoomChart(chart) {
        chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
    }


    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    //start Auto referesh
    function StartAutoRefresh() {
        //$scope.Message = "Timer started. ";

        $rootScope.Timer = $interval(function () {
            $scope.getObdGridData();
            //var lat = $scope.map.center.latitude;
            //var lng = $scope.map.center.longitude;
            //lat = lat + 0.001;
            //lng = lng - 0.001;
            ////   $scope.polylines[0].path = [];
            //$scope.polylines[0].path.push({ latitude: lat, longitude: lng });
            //$scope.map.center.latitude = lat;
            //$scope.map.center.longitude = lng;
            //$scope.map.markersr[0].latitude = lat;
            //$scope.map.markersr[0].longitude = lng;
            ////  getDashBoardLiveData();
            //$scope.$applyAsync();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };


    function init() {
        loadInitData();
        getVehicleList();
       
      
        if ($window.sessionStorage["dashaction"]) {
            var params = JSON.parse($window.sessionStorage["dashaction"]);
            if (params.action == "obd") {
                $scope.vehicleRefId = params.deviceno;
                $scope.getObdGridData();
            }
        }

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }


    }

    init();
   
});