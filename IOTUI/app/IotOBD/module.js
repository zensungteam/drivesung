﻿'use strict';

angular.module('app.Iotobd', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
  //.config(function (uiGmapGoogleMapApiProvider) {
  //    uiGmapGoogleMapApiProvider.configure({
  //        key: 'AIzaSyA7bGFFmYR_pRQeU84EYuPVmv__bpSbqdo',
  //        v: '3.0', 
  //        libraries: 'weather,geometry,visualization'
  //    });
  //})

.config(function ($stateProvider) {
    $stateProvider
        .state('app.Iotobd', {
            url: '/Iotobd',
            views: {
                "content@app": {
                    controller: 'IotobdCtrl',
                    templateUrl: 'app/IotOBD/views/Iotobd.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },


           
        });
});
