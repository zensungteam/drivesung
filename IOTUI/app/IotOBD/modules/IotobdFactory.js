﻿"use strict";
angular.module('app.Iotobd').factory('IotobdFactory', function ($http, $q, APP_CONFIG) {


    var getVehicleList = function (filters) {
       
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getObdLiveData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetLiveOBDData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {
     
        getVehicleList: getVehicleList,
        getObdLiveData: getObdLiveData
    
    };
});