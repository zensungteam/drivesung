﻿"use strict"

angular.module('app.Iotreports').factory('IotreportsFactory', function ($http, $q, APP_CONFIG) {

    var getdriverRatingDataList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetDrivingRating";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
  
    var getAggregateDataList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetAggregateData";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getstoppageList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetStoppage";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getdistanceList = function (param) {
       
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetDistancesRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getTempretureList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetTempretureRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getspeedList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetSpeedRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var gettripsList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetlisttripsRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var getHA_HB_HCRptList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetHB_HARpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getHA_vs_timeRptList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetHA_timeRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var GetDay_NightHrsRptList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetDay_NightHrsRpt";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var GetDriverperformanceRptList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Reports/GetDriverreportData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var StartTrip = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Trip/StartTrip";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var UpdateTrip = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Trip/UpdateTrip";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    var EndTrip = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Trip/EndTrip";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var ActiveTrip = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Trip/GetActiveTrips";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

     return {
        getdriverRatingDataList: getdriverRatingDataList,
        getAggregateDataList: getAggregateDataList,
        getstoppageList: getstoppageList,
        getdistanceList: getdistanceList,
        getVehicleList: getVehicleList,
        getTempretureList: getTempretureList,
        getspeedList: getspeedList,
        gettripsList: gettripsList,
        getHA_HB_HCRptList: getHA_HB_HCRptList,
        getHA_vs_timeRptList: getHA_vs_timeRptList,
        GetDay_NightHrsRptList: GetDay_NightHrsRptList,
        GetDriverperformanceRptList: GetDriverperformanceRptList,
        StartTrip: StartTrip,
        EndTrip: EndTrip,
        ActiveTrip: ActiveTrip,
        UpdateTrip:UpdateTrip
    };



});
