﻿'use strict';

angular.module('app.Iotreports').controller('driverratingdataCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var param = {
            "o_id": 1,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        };


        IotreportsFactory.getdriverRatingDataList(param).then(function (result) {

            setInitialData(result.data.data.all);
            showData(result.data.data.all);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.driverRatingDataModel = ListData;
    }

    function showData(ListData) {
        showdriverRatingDataList(ListData);
    }

    function showdriverRatingDataList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                        { data: "final_score" },
                        { data: "deviceno" },
                        { data: "recorddate" },
                        { data: "overspeedlow_d" },
                        { data: "overspeed10_d" },
                        { data: "overspeed20_d" },
                        { data: "overspeed30_d" },
                        { data: "overspeedlow_n" },
                        { data: "overspeed10_n" },
                        { data: "overspeed20_n" },
                        { data: "overspeed30_n" },
                        { data: "overspeedwt_avg" },
                        { data: "overspeedscore" },
                        { data: "harsh_ha_d" },
                        { data: "harsh_hb_d" },
                        { data: "harsh_cornering_d" },
                        { data: "harsh_ha_n" },
                        { data: "harsh_hb_n" },
                        { data: "harsh_cornering_n" },
                        { data: "harsh_score" },
                        { data: "age_criteria1" },
                        { data: "age_criteria2" },
                        { data: "age_criteria3" },
                        { data: "age_criteria4" },
                        { data: "age_score" },
                        { data: "gender_male" },
                        { data: "gender_female" },
                        { data: "gender_other" },
                        { data: "gender_score" },
                        { data: "car_white" },
                        { data: "car_lightother" },
                        { data: "car_metallic_light" },
                        { data: "car_red" },
                        { data: "car_metallic_dark" },
                        { data: "car_darkother" },
                        { data: "car_black" },
                        { data: "car_score" }


            ],
            "order": [[0, 'desc']],
            destroy: true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function init() {
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
