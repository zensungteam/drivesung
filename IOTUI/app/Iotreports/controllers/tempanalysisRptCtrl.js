﻿'use strict';

angular.module('app.Iotreports').controller('tempanalysisRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };
        if (nowday < 10) {
            nowday = "0" + nowday;
        };
        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;

    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        }
     IotreportsFactory.getTempretureList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);
            SetChartData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {
        showStoppageDataList(ListData);
    }

    function showStoppageDataList(ListData) {
        var IsDelete = true;
        //angular.forEach(ListData, function (data, index) {
        //    data.Edit = "";
        //    data.View = "";
        //    data.Delete = "";
        //});

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "deviceno" },
	            { data: "recorddatetime" },
                { data: "avgcoolanttemperature" },
               
            ],
            "order": [[0, 'desc']],
            destroy: true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {
      
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
     
        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        debugger;
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddatetime;
            var avgcoolanttemperature = data.avgcoolanttemperature;
          

            chartData.push({
                date: cdatetime,
                 temp: avgcoolanttemperature,
            });

        });
        

        return chartData;
    }

    function SetChartData(alldata) {

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartcoolentdiv", {
            "type": "serial",
            "theme": "light",

            "dataProvider": chartData,
            "valueAxes": [{
                "integersOnly": true,
                "maximum": 150,
                "minimum": 0,
                "reversed": false,
                "axisAlpha": 1,
                "dashLength": 5,
                "gridCount": 10,
                "position": "left",
                "title": "Temperature(In °C)"
            }],
            "startDuration": 0.5,
            "graphs": [{

                "balloonText": "Temperature: [[value]]°C",
                "bullet": "round",
                "title": "Max Speed",
                "valueField": "temp",
                "fillAlphas": 0

            }],
            "chartCursor": {
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "fillAlpha": 0.05,
                "fillColor": "#000000",
                "gridAlpha": 0,
                "position": "bottom",
                "title": "Date"
            },
            "colors": ["#B40431"],
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });


    }

    function init() {
        getVehicleList();
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
