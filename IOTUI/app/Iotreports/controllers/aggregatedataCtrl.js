﻿'use strict';

angular.module('app.Iotreports').controller('aggregatedataCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var param = {
            "o_id": 1,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        };
             
        IotreportsFactory.getAggregateDataList(param).then(function (result) {

            setInitialData(result.data.data.all);
            showData(result.data.data.all);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.aggregatedataDataModel = ListData;
    }

    function showData(ListData) {
        showAggregateDataList(ListData);
    }

    function showAggregateDataList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
        });

        var tableOptions = {
            "data": ListData,
            columns: [
           	            { data: "deviceno" },
	                    { data: "recorddatetime" },
                        { data: "hardacceleration" },
                        { data: "hardacceleration_day" },
                        { data: "hardacceleration_night" },
                        { data: "hardbraking" },
                        { data: "hardbraking_day" },
                        { data: "hardbraking_night" },
                        { data: "cornerpacket" },
                        { data: "cornerpacket_day" },
                        { data: "cornerpacket_night" },
                        { data: "avgspeed" },
                        { data: "avgspeed_day" },
                        { data: "avgspeed_night" },
                        { data: "maxspeed" },
                        { data: "maxspeed_day" },
                        { data: "maxspeed_night" },
                        { data: "distance" },
                        { data: "distance_day" },
                        { data: "distance_night" },
                        { data: "drivinghrs" },
                        { data: "drivinghrsideal" },
                        { data: "daydrivinghrs" },
                        { data: "daydrivinghrsideal" },
                        { data: "nightdrivinghrs" },
                        { data: "nightdrivinghrsideal" },
                        { data: "avgrpm" },
                        { data: "avgrpm_day" },
                        { data: "avgrpm_night" },
                        { data: "maxrpm" },
                        { data: "maxrpm_day" },
                        { data: "maxrpm_night" },
                        { data: "minrpm" },
                        { data: "minrpm_day" },
                        { data: "minrpm_night" },
                        { data: "avgcoolanttemperature" },
                        { data: "avgcoolanttemperature_day" },
                        { data: "avgcoolanttemperature_night" },
                        { data: "maxcoolanttemperature" },
                        { data: "maxcoolanttemperature_day" },
                        { data: "maxcoolanttemperature_night" },
                        { data: "mincoolanttemperature" },
                        { data: "mincoolanttemperature_day" },
                        { data: "mincoolanttemperature_night" },
                        { data: "avgcalculatedengineload" },
                        { data: "avgcalculatedengineload_day" },
                        { data: "avgcalculatedengineload_night" },
                        { data: "maxcalculatedengineload" },
                        { data: "maxcalculatedengineload_day" },
                        { data: "maxcalculatedengineload_night" },
                        { data: "mincalculatedengineload" },
                        { data: "mincalculatedengineload_day" },
                        { data: "mincalculatedengineload_night" },
                        { data: "sysdatetime" }
            ],
            "order": [[0, 'desc']],
            destroy: true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function init() {
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
