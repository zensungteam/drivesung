﻿'use strict';

angular.module('app.Iotreports').controller('driverperformanceRptCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    $scope.display = function () {
        var param = {
            DeviceNo: $scope.vehicleRefId,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        IotreportsFactory.GetDriverperformanceRptList(param).then(function (result) {
            Setoverall_ratingData(result.data.data.overall_rating);
            Setoverall_trip_ratingData(result.data.data.overall_trip_rating);
            Setdaily_ratingData(result.data.data.daily_rating);

        }, function (error) {
            console.log(error);
        });

    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };
        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function Setoverall_ratingData(data) {

        var overall_rating = data;
        //. JSON.parse($scope.overall_rating);
        var chart = AmCharts.makeChart("driverperformancediv", {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": [{
                "parameters": "Over Speed",
                "score": overall_rating[0].overspeedscore,
                "color": "#c53c2e"
            }, {
                "parameters": "Harsh Event",
                "score": overall_rating[0].harsh_score,
                "color": "#28628e"
            }, {
                "parameters": "Calls Taken",
                "score": overall_rating[0].calling_score,
                "color": "#5c5c5c"
            }],
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Parameter score"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "<b>[[category]]: [[value]]</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "score"
            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": true
            },
            "categoryField": "parameters",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 45
            },
            "export": {
                "enabled": true
            }

        });

        //function updateLabels(event) {
        //    var labels = event.chart.chartDiv.getElementsByClassName("amcharts-axis-title");
        //    for (var i = 0; i < labels.length; i++) {
        //        var color = event.chart.dataProvider[i].color;
        //        if (color !== undefined) {
        //            labels[i].setAttribute("fill", color);
        //        }
        //    }
        //}

        var chart = AmCharts.makeChart("chartdivscore", {
            "theme": "light",
            "type": "gauge",
            "axes": [{
                "topTextFontSize": 20,
                "topTextYOffset": 70,
                "axisColor": "#31d6ea",
                "axisThickness": 1,
                "endValue": 100,
                "gridInside": true,
                "inside": true,
                "radius": "50%",
                "valueInterval": 20,
                "tickColor": "#67b7dc",
                "startAngle": -90,
                "endAngle": 90,
                "unit": "",
                "bandOutlineAlpha": 0,
                "bands": [{
                    "color": "#FF5533",
                    "endValue": 60,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 0
                }, {
                    "color": "#FFE633",
                    "endValue": 80,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 60
                }, {
                    "color": "#93FF33",
                    "endValue": 100,
                    "innerRadius": "105%",
                    "radius": "170%",
                    "gradientRatio": [0.5, 0, -0.5],
                    "startValue": 80
                }]
            }],
            "arrows": [{
                "alpha": 1,
                "innerRadius": "35%",
                "nailRadius": 0,
                "radius": "170%"
            }]
        });

        setInterval(randomValue, 2000);
        // set random value
        function randomValue() {
            //  var value = Math.round(Math.random() * 100)
            var value = overall_rating[0].rating;
            chart.arrows[0].setValue(value);
            chart.axes[0].setTopText(value);
            // adjust darker band to new value
            // chart.axes[0].bands[1].setEndValue(value);
        }
    }

    function Setoverall_trip_ratingData(data) {
        var chart = AmCharts.makeChart("TripWiseScorediv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": data,
            "synchronizeGrid": true,
            "valueAxes": [{
                "id": "v1",
                "axisColor": "#22958a",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "left",
                "title": "Score"
            }, {
                "id": "v2",
                "axisColor": "#d28924",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "right",
                "title": "Distance Travel (km)"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "lineColor": "#22958a",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Score",
                "valueField": "rating",
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "lineColor": "#d28924",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Distance (km)",
                "valueField": "distance",
                "fillAlphas": 0,
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "categoryField": "trip_name",
            "categoryAxis": {
                //"parseDates": true,
                "axisColor": "#DADADA",
                //"minPeriod": "hh:mm:ss",
                "minorGridEnabled": true,
                "title": "Trip Name"

            },
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });
        // chart.dataDateFormat = "dd:mm:yy";
        //chart.addListener("dataUpdated", zoomChart);
        //zoomChart(chart);
    }

    function Setdaily_ratingData(data) {

        var chart = AmCharts.makeChart("DayWiseScorediv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "useGraphSettings": true
            },
            "dataProvider": data,
            "synchronizeGrid": true,
            "valueAxes": [{
                "id": "v1",
                "axisColor": "#22958a",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "left",
                "title": "Score"
            }, {
                "id": "v2",
                "axisColor": "#d28924",
                "axisThickness": 2,
                "axisAlpha": 1,
                "position": "right",
                "title": "Distance Travel (km)"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "lineColor": "#22958a",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Score",
                "valueField": "final_score",
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "lineColor": "#d28924",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Distance (km)",
                "valueField": "distance",
                "fillAlphas": 0
            }],
            "chartScrollbar": {},
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "categoryField": "recorddate",
            "categoryAxis": {
                //"parseDates": true,
                "axisColor": "#DADADA",
                //"minPeriod": "hh:mm:ss",
                "minorGridEnabled": true,
                "title": "Date"

            },
            "export": {
                "enabled": true,
                "position": "bottom-right"
            }
        });
        // chart.dataDateFormat = "dd:mm:yy";
        //chart.addListener("dataUpdated", zoomChart);
        //zoomChart(chart);
    }

    function zoomChart(chart) {
        chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
    }

    function init() {
        getVehicleList();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
