﻿'use strict';

angular.module('app.Iotreports').controller('stoppageCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };


        if (nowday < 10) {
            nowday = "0" + nowday;

        };

        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;


    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        }

        IotreportsFactory.getstoppageList(param).then(function (result) {

            setInitialData(result.data.data.griddata);
            showData(result.data.data.griddata);
            SetChartData(result.data.data.stoppagedata);

        }, function (error) {
            console.log(error);
        });
    }



    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {
        showStoppageDataList(ListData);
    }

    function showStoppageDataList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
          data.location=""
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "deviceno" },
	            { data: "gridrecorddate" },
                { data: "location" },
                //{ data: "longitude" },
                { data: "startdatetime" },
                { data: "enddatetime" },
                { data: "stoppage_minut" },
                //{ data: "sysdatetime" },
                //{ data: "noofrecords" }
            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var locationdata = { 'latitude': aData.latitude, 'longitude': aData.longitude, 'id': iDataIndex };
          
                $('td:eq(1)', nRow).html("<a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-map-marker fa-2x' aria-hidden='true' style='color:#075e07; margin-left:20px;' ></a><div id='container"+iDataIndex+"'></div>");
            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }


    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }


    function SetChartData(alldata) {
        var chartkey = [];
        $.each(alldata[0], function (key, value) {
            chartkey.push(key);
        });


        var chart = AmCharts.makeChart("chartStopagediv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Stoppage in Minutes "

            },
            "dataProvider": alldata,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "No of Stoppage Instances"
            }],
            "graphs": [{
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[1],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[1]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[2],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[2]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[3],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[3]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[4],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[4]
            }, {
                "balloonText": "Stoppage in Minutes:<b>[[title]] , </b><br>Date:<b>[[recorddatetime]]</b>, No of Stoppage Instances : <b>[[value]] </b>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": chartkey[5],
                "type": "column",
                "color": "#000000",
                "valueField": chartkey[5]
            }
                ,
                //{
            //    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>Time: <b>[[value]] Min</b></span>",
            //    "fillAlphas": 0.8,
            //    "labelText": "[[value]]",
            //    "lineAlpha": 0.3,
            //    "title": "Stop6",
            //    "type": "column",
            //    "color": "#000000",
            //    "valueField": "Stop6"
            //}
            ],
            "categoryField": "recorddatetime",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "export": {
                "enabled": true
            }
        });


    }

    $scope.locationinfo = function (locationdata) {

        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(locationdata.latitude, locationdata.longitude);
        var location;
       
        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location =  results[1].formatted_address;
                }
                else {
                    location = locationdata.latitude + ',' + locationdata.longitude;
                }
               document.getElementById("container" + locationdata.id + "").innerHTML = location
            }
        })

    }

    function init() {
        getVehicleList();
        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
    }

    init();
});
