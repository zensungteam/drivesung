﻿'use strict';

angular.module('app.Iotreports').controller('dayvsnight_hrsCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };
        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };
        if (nowday < 10) {
            nowday = "0" + nowday;
        };
        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;
    }

    $scope.display = function () {

        var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };
        IotreportsFactory.GetDay_NightHrsRptList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);
            SetChartData(result.data.data);

        }, function (error) {
            console.log(error);
        });

    }

    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {

        showdistanceDataList(ListData);
    }

    function showdistanceDataList(ListData) {
        var IsDelete = true;
        //angular.forEach(ListData, function (data, index) {
        //    data.Edit = "";
        //    data.View = "";
        //    data.Delete = "";
        //});

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "deviceno" },
	            { data: "gridrecorddate" },
                { data: "drivinghrs" },
                { data: "daydrivinghrs" },
                { data: "nightdrivinghrs" }

            ],
            "order": [[0, 'desc']],
            destroy: true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
        debugger;
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddatetime;
            //var distance = data.distance;
            var daydrivinghrs = data.daydrivinghrs;
            var nightdrivinghrs = data.nightdrivinghrs;

            chartData.push({
                date: cdatetime,
                //distance: distance,
                daydrivinghrs: daydrivinghrs,
                nightdrivinghrs: nightdrivinghrs
            });

        });


        return chartData;
    }

    function SetChartData(alldata) {

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartdayvsnightRptdiv", {
            "theme": "light",
            "type": "serial",

            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": chartData,
            "valueAxes": [{
                "position": "verticle",
                "title": "",
            }],
            "startDuration": 1,
            "graphs": [, {
                "balloonText": "Date [[category]], Day Driving Hrs : <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Day Driving Hrs",
                "type": "column",
                "clustered": false,
                "columnWidth": 0.5,
                "valueField": "daydrivinghrs"
            }, {
                "balloonText": "Date [[category]], Night Driving Hrs : <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Night Driving Hrs",
                "type": "column",
                "clustered": false,
                "columnWidth": 0.5,
                "valueField": "Night Driving Hrs"
            }],
            "plotAreaFillAlphas": 0.1,
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "title": "Date",
            },
            "export": {
                "enabled": true
            }
        });

    }




    function init() {

        getVehicleList();

        loadInitData();
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }


    }

    init();
});



