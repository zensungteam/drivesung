﻿'use strict';

angular.module('app.Iotreports').controller('distanceCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotreportsFactory) {

    function loadInitData() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        if (month < 10) {
            month = "0" + month;
        };
        var nowday = d.getDate();
        var predate = d.setDate(d.getDate() - 30)
        var preday = d.getDate();
        if (preday < 10) {
            preday = "0" + preday;

        };

        var premonth = d.getMonth() + 1;
        if (premonth < 10) {
            premonth = "0" + premonth;
        };
        if (nowday < 10) {
            nowday = "0" + nowday;

        };

        $scope.fromdate = preday + "-" + premonth + "-" + year;
        $scope.todate = nowday + "-" + month + "-" + year;


    }

    $scope.display = function () {

        //var date = new Date($scope.todate)
        var param = {
            DeviceNo: $scope.vehicleRefId,
            sfDate: $scope.fromdate,
            stDate: $scope.todate,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };



        IotreportsFactory.getdistanceList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);
            SetChartData(result.data.data);
          

        }, function (error) {
            console.log(error);
        });

    }

    function setInitialData(ListData) {

        $scope.stoppageDataModel = ListData;
    }

    function showData(ListData) {

        showdistanceDataList(ListData);
    }

    function showdistanceDataList(ListData) {
        var IsDelete = true;
        //angular.forEach(ListData, function (data, index) {
        //    data.Edit = "";
        //    data.View = "";
        //    data.Delete = "";
        //});

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "deviceno" },
	            { data: "gridrecorddate" },
                { data: "distance" },
                { data: "distance_d" },
                { data: "distance_n" }

            ],
            "order": [[0, 'desc']],
            "destroy": true
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

    function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IotreportsFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function generateChartData(alldata) {
       
        var chartData = [];
        var recorddate;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            //recorddate = Date.parse(data.recorddate);
            //newDate = new Date(recorddate);

            var cdatetime = data.recorddate;
            var distance = data.distance;
            var distance_d = data.distance_d;
            var distance_n = data.distance_n;

            chartData.push({
                date: cdatetime,
                //distance: distance,
                distanceday: distance_d,
                distancenight: distance_n
            });

        });


        return chartData;
    }

    //function SetChartData(alldata) {

    //    var chartData = generateChartData(alldata);
    //    var chart = AmCharts.makeChart("chartharsbreakingdiv", {
    //        "theme": "light",
    //        "type": "serial",
    //        "dataProvider": chartData,
    //        "valueAxes": [{
    //            "unit": " km",
    //            "position": "left",
    //            "title": "Distance Travel (km)",
    //        }],
    //        "startDuration": 1,
    //        "graphs": [

    //        {
    //            "balloonText": "Date [[category]], Distance Travel at Day: <b>[[value]]</b> (km)",
    //            "fillAlphas": 0.9,
    //            "lineAlpha": 0.2,
    //            "title": " Day Distance",
    //            "type": "column",
    //            "clustered": false,
    //            "columnWidth": 0.6,
    //            "valueField": "distanceday"
    //        }, {
    //            "balloonText": "Date [[category]], Distance Travel at Night: <b>[[value]]</b> (km)",
    //            "fillAlphas": 0.9,
    //            "lineAlpha": 0.2,
    //            "title": "Night Distance",
    //            "type": "column",
    //            "clustered": false,
    //            "columnWidth": 0.5,
    //            "valueField": "distancenight"
    //        }],
    //        "plotAreaFillAlphas": 0.1,
    //        "categoryField": "date",
    //        "categoryAxis": {
    //            "gridPosition": "start"
    //        },
    //        "colors": ["#01A9DB", "#d28924"],
    //        "export": {
    //            "enabled": true
    //        }

    //    });
        
    //}

    function SetChartData(alldata) {
        //var chartkey = [];
        //$.each(alldata[0], function (key, value) {
        //    chartkey.push(key);
        //});

        var chartData = generateChartData(alldata);
        var chart = AmCharts.makeChart("chartharsbreakingdiv", {
            "type": "serial",
            "theme": "light",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 5,
                "position": "bottom",
                "useGraphSettings": true,
                "markerSize": 10,
                "title": "Distance"

            },
            "dataProvider": chartData,
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "title": "Distance Travel (km)"
            }],
            "graphs": [{
                "balloonText": "Date [[category]], Distance Travel at Day: <b>[[value]]</b> (km)",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Day Distance",
                "type": "column",
                "color": "#01A9DB",
                "valueField": "distanceday"
            }, {
                "balloonText": "Date [[category]], Distance Travel at Night: <b>[[value]]</b> (km)",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Night Distance",
                "type": "column",
                "color": "#d28924",
                "valueField": "distancenight"
            }
            ],
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 1,
                "gridAlpha": 0,
                "position": "left"
            },
            "colors": ["#01A9DB", "#d28924"],
            "export": {
                "enabled": true
            }
        });


    }

    function init() {

        getVehicleList();

        loadInitData();

        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

    }

    init();
});



