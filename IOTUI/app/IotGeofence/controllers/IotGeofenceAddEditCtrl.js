﻿'use strict'
angular.module('app.configsetting').controller('IotGeofenceAddEditCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotGeofenceFactory) {

    var actiongf;
    var map;
    var drawingManager;


    function loadInitData() {
        actiongf = JSON.parse($window.sessionStorage["Geofenceaction"]);
        $scope.lat_lng = '';
       
        if (actiongf.action == "New") {
            $scope.lblheading = "New Geofence";
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";
            $scope.is_active = true;
        } else {
            var param = {
                'gc_id': actiongf.gc_id,
                's_user_id': $rootScope.userInfo.user_id,
                's_role_Id': $rootScope.userInfo.role_Id
            };

            IotGeofenceFactory.getGeofenceById(param).then(function (result) {
                showData(result.data.data);
            },
            function (error) {
                console.log(error);
            });
        }

    }

    function customeAlert(description) {
        var hexcolor = "#5F895F";
        //hexcolor = "#FF0000";

        $.smallBox({
            title: "Geofence",
            content: "<i class='fa fa-clock-o'></i> <i>" + description + "</i>",
            color: hexcolor,
            iconSmall: "fa fa-check bounce animated",
            timeout: 4000
        });
    }

    function showData(data) {

        if (data != null && data.isComplete == false) {

            $state.go('app.IotGeofence.list');
        }

        $scope.gc_id = data[0].gc_id;
        $scope.geofence_name = data[0].geofence_name;
        $scope.geofenec_desc = data[0].geofenec_desc;
        $scope.lat_lng = data[0].lat_lng;
        $scope.icon_file_name = data[0].icon_file_name;
        $scope.is_active = data[0].is_active;


        var Adrawlat_lng = [];
        var to = data[0].lat_lng;;
        var toSplit = to.split(";");
        for (var i = 0; i < toSplit.length; i++) {
            var latlngSplit = toSplit[i].split(",");
            Adrawlat_lng.push({ lat: parseFloat(latlngSplit[0]), lng: parseFloat(latlngSplit[1]) });
        }

        //var Adrawlat_lng=[];
        //Adrawlat_lng.push({ lat: 25.774, lng: -80.190 });
        //Adrawlat_lng.push({ lat: 18.466, lng: -66.118 });
        //Adrawlat_lng.push({ lat: 32.321, lng: -64.757 });
        //Adrawlat_lng.push({ lat: 25.774, lng: -80.190 });
        $scope.drawlat_lng = Adrawlat_lng;


        if (actiongf.action == "Edit") {
            $scope.lblheading = "Update Geofence";
            $scope.editMode = true;
            $scope.btnSaveText = "Update";
        }
        else if (actiongf.action == "View") {
            $scope.lblheading = "View Geofence";
            $scope.editMode = false;
            drawPolygon();

        }


    }

    $scope.CancelGeofeneceRecord = function () {

        $.SmartMessageBox({
            title: "Geofenece",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.IotGeofence.list');
            }
        });
        return;

    }

    $scope.saveGeofence = function () {
        var param = {
            'gc_id': actiongf.gc_id,
            'action': actiongf.action

        };
        var data = {
            'actionEntity': param,
            'gc_id': $scope.gc_id,
            'geofence_name': $scope.geofence_name,
            'geofenec_desc': $scope.geofenec_desc,
            'lat_lng': $scope.lat_lng,
            'isactive': $scope.is_active,
            'created_by': $rootScope.userInfo.user_id,
            'updated_by': $rootScope.userInfo.user_id
        }
        IotGeofenceFactory.saveGeofence(data).then(function (result) {
            customeAlert(result.data.description);
            if (result.data.isComplete == true)
                $state.go('app.IotGeofence.list');

        }, function (error) {
            console.log(error);
        });
    }

    function init() {
        loadGeofeneceMap();
        loadInitData();



    }

    function drawPolygon() {

        // Define the LatLng coordinates for the polygon's path.
        var Coords = $scope.drawlat_lng;

        var map = new google.maps.Map(document.getElementById('divmap'), {
            zoom: 18,
            center: { lat: Coords[0].lat, lng: Coords[0].lng },
            // mapTypeId: 'terrain'
        });

        // map.center = { lat: Coords[0].lat, lng: Coords[0].lng };

        // Construct the polygon.
        var Coordspoly = new google.maps.Polygon({
            paths: Coords,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#d28924',
            fillOpacity: 0.35
        });
        Coordspoly.setMap(map);

        //drawingManager.polygonOptions.paths = Coords
        //drawingManager.setMap(map);
        //drawingManager.setDrawingMode(null);
        //////Write code to select the newly selected object.
        ////var newShape = event.overlay;
        ////newShape.type = "polygon";
        //google.maps.event.addListener(Coordspoly, 'click', function () {
        //    setSelection(Coordspoly);
        //});

        //setSelection(Coordspoly);
    }


    function loadGeofeneceMap() {
        if (navigator.geolocation) {
            //navigator.geolocation.getCurrentPosition(function (position) {

            map = new google.maps.Map(document.getElementById('divmap'), {
                center: {
                    lat: 18.50891,
                    lng: 73.92603

                },
                zoom: 18
            });
            //google.maps.event.addListener(map, 'click', function (event) {
            //    // placeMarker(event.latLng);
            //    console.log(event.latLng.toUrlValue(5));
            //});

            //function placeMarker(location) {
            //    var marker = new google.maps.Marker({
            //        position: location,
            //        map: map
            //    });

            //}
            var all_overlays = [];
            var selectedShape;
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: [
                        //google.maps.drawing.OverlayType.MARKER,
                       // google.maps.drawing.OverlayType.CIRCLE,
                        google.maps.drawing.OverlayType.POLYGON,
                       // google.maps.drawing.OverlayType.RECTANGLE
                    ]
                },
                //markerOptions: {
                //    icon: 'images/beachflag.png'
                //},
                //circleOptions: {
                //    fillColor: '#ffff00',
                //    fillOpacity: 0.2,
                //    strokeWeight: 3,
                //    clickable: false,
                //    editable: true,
                //    zIndex: 1
                //},
                polygonOptions: {
                    clickable: true,
                    draggable: true,
                    editable: true,
                    fillColor: '#d28924',
                    fillOpacity: 1,

                }
                //rectangleOptions: {
                //    clickable: true,
                //    draggable: true,
                //    editable: true,
                //    fillColor: '#ffff00',
                //    fillOpacity: 0.5,
                //}
            });

            function clearSelection() {
                if (selectedShape) {
                    selectedShape.setEditable(false);
                    selectedShape = null;
                }
            }

            function setSelection(shape) {
                clearSelection();
                selectedShape = shape;
                shape.setEditable(true);
                google.maps.event.addListener(selectedShape.getPath(), 'insert_at', getPolygonCoords(shape));
                google.maps.event.addListener(selectedShape.getPath(), 'set_at', getPolygonCoords(shape));
            }

            function deleteSelectedShape() {
                if (selectedShape) {
                    selectedShape.setMap(null);
                }
            }

            //function deleteAllShape() {
            //    for (var i = 0; i < all_overlays.length; i++) {
            //        all_overlays[i].overlay.setMap(null);
            //    }
            //    all_overlays = [];
            //}

            function CenterControl(controlDiv, map) {
                // Set CSS for the control border.
                var controlUI = document.createElement('div');
                controlUI.style.backgroundColor = '#fff';
                controlUI.style.border = '2px solid #fff';
                controlUI.style.borderRadius = '3px';
                controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
                controlUI.style.cursor = 'pointer';
                controlUI.style.marginBottom = '22px';
                controlUI.style.textAlign = 'center';
                controlUI.title = 'Select to delete the shape';
                controlDiv.appendChild(controlUI);

                // Set CSS for the control interior.
                var controlText = document.createElement('div');
                controlText.style.color = 'rgb(25,25,25)';
                controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
                controlText.style.fontSize = '16px';
                controlText.style.lineHeight = '38px';
                controlText.style.paddingLeft = '5px';
                controlText.style.paddingRight = '5px';
                controlText.innerHTML = 'Delete Selected Area';
                controlUI.appendChild(controlText);

                // Setup the click event listeners: simply set the map to Chicago.
                controlUI.addEventListener('click', function () {
                    deleteSelectedShape();
                });

                //// Set CSS for the control border.
                //var controlUI = document.createElement('div');
                //controlUI.style.backgroundColor = '#fff';
                //controlUI.style.border = '2px solid #fff';
                //controlUI.style.borderRadius = '3px';
                //controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
                //controlUI.style.cursor = 'pointer';
                //controlUI.style.marginBottom = '22px';
                //controlUI.style.textAlign = 'center';
                //controlUI.title = 'Select to delete the shape';
                //controlDiv.appendChild(controlUI);

                //// Set CSS for the control interior.
                //var controlText = document.createElement('div');
                //controlText.style.color = 'rgb(25,25,25)';
                //controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
                //controlText.style.fontSize = '16px';
                //controlText.style.lineHeight = '38px';
                //controlText.style.paddingLeft = '5px';
                //controlText.style.paddingRight = '5px';
                //controlText.innerHTML = 'Delete all Area';
                //controlUI.appendChild(controlText);

                //// Setup the click event listeners: simply set the map to Chicago.
                //controlUI.addEventListener('click', function () {
                //    deleteAllShape();
                //});

            }

            drawingManager.setMap(map);

            var getPolygonCoords = function (newShape) {
                // console.log("We are one");
                $scope.lat_lng = '';
                var len = newShape.getPath().getLength();
                for (var i = 0; i < len; i++) {
                    //  console.log(newShape.getPath().getAt(i).toUrlValue(6));
                    if ($scope.lat_lng == '') {
                        $scope.lat_lng = newShape.getPath().getAt(i).toUrlValue(6)
                    }
                    else {
                        $scope.lat_lng = $scope.lat_lng + ";" + newShape.getPath().getAt(i).toUrlValue(6)
                    }
                }
            };

            //google.maps.event.addListener(drawingManager, 'polygoncomplete', function (event) {

            //    event.getPath().getLength();
            //    google.maps.event.addListener(event.getPath(), 'insert_at', function () {
            //        var len = event.getPath().getLength();
            //        for (var i = 0; i < len; i++) {
            //            console.log(event.getPath().getAt(i).toUrlValue(5));
            //        }
            //    });
            //    google.maps.event.addListener(event.getPath(), 'set_at', function () {
            //        var len = event.getPath().getLength();
            //        for (var i = 0; i < len; i++) {
            //            console.log(event.getPath().getAt(i).toUrlValue(5));
            //        }
            //    });
            //});

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {

                all_overlays.push(event);
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    drawingManager.setDrawingMode(null);
                    //Write code to select the newly selected object.

                    var newShape = event.overlay;
                    newShape.type = event.type;
                    google.maps.event.addListener(newShape, 'click', function () {
                        setSelection(newShape);
                    });

                    setSelection(newShape);
                }
            });


            var centerControlDiv = document.createElement('div');
            var centerControl = new CenterControl(centerControlDiv, map);

            centerControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
            //});
            //    }
        }

    }




    //if (navigator.geolocation) {
    //    //navigator.geolocation.getCurrentPosition(function (position) {

    //    //var map = new google.maps.Map(document.getElementById('divmap'), {
    //    //    center: {
    //    //        lat: 18.50891,
    //    //        lng: 73.92603

    //    //    },
    //    //    zoom: 18
    //    //});
    //    //google.maps.event.addListener(map, 'click', function (event) {
    //    //    // placeMarker(event.latLng);
    //    //    console.log(event.latLng.toUrlValue(5));
    //    //});

    //    //function placeMarker(location) {
    //    //    var marker = new google.maps.Marker({
    //    //        position: location,
    //    //        map: map
    //    //    });

    //    //}
    //    var all_overlays = [];
    //    var selectedShape;



    //    function clearSelection() {
    //        if (selectedShape) {
    //            selectedShape.setEditable(false);
    //            selectedShape = null;
    //        }
    //    }

    //    function setSelection(shape) {
    //        clearSelection();
    //        selectedShape = shape;
    //        shape.setEditable(true);
    //        google.maps.event.addListener(selectedShape.getPath(), 'insert_at', getPolygonCoords(shape));
    //        google.maps.event.addListener(selectedShape.getPath(), 'set_at', getPolygonCoords(shape));
    //    }

    //    function deleteSelectedShape() {
    //        if (selectedShape) {
    //            $scope.newlat_lng = '';
    //            selectedShape.setMap(null);
    //        }
    //    }

    //    function deleteAllShape() {
    //        for (var i = 0; i < all_overlays.length; i++) {
    //            all_overlays[i].overlay.setMap(null);
    //        }
    //        all_overlays = [];
    //    }

    //    function CenterControl(controlDiv, map) {

    //        // Set CSS for the control border.
    //        var controlUI = document.createElement('div');
    //        controlUI.style.backgroundColor = '#fff';
    //        controlUI.style.border = '2px solid #fff';
    //        controlUI.style.borderRadius = '3px';
    //        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
    //        controlUI.style.cursor = 'pointer';
    //        controlUI.style.marginBottom = '22px';
    //        controlUI.style.textAlign = 'center';
    //        controlUI.title = 'Select to delete the shape';
    //        controlDiv.appendChild(controlUI);

    //        // Set CSS for the control interior.
    //        var controlText = document.createElement('div');
    //        controlText.style.color = 'rgb(25,25,25)';
    //        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    //        controlText.style.fontSize = '14px';
    //        controlText.style.lineHeight = '38px';
    //        controlText.style.paddingLeft = '5px';
    //        controlText.style.paddingRight = '5px';
    //        controlText.innerHTML = 'Delete Selected Area';
    //        controlUI.appendChild(controlText);

    //        // Setup the click event listeners: simply set the map to Chicago.
    //        controlUI.addEventListener('click', function () {
    //            deleteSelectedShape();
    //        });

    //    }

    //    drawingManager.setMap(map);

    //    var getPolygonCoords = function (newShape) {
    //        // console.log("We are one");
    //        $scope.lat_lng = '';
    //        var len = newShape.getPath().getLength();
    //        for (var i = 0; i < len; i++) {
    //            //  console.log(newShape.getPath().getAt(i).toUrlValue(6));
    //            if ($scope.lat_lng == '') {
    //                $scope.lat_lng = newShape.getPath().getAt(i).toUrlValue(6)
    //            }
    //            else {
    //                $scope.lat_lng = $scope.lat_lng + ";" + newShape.getPath().getAt(i).toUrlValue(6)
    //            }
    //        }
    //    };

    //    google.maps.event.addListener(drawingManager, 'polygoncomplete', function (event) {

    //        event.getPath().getLength();
    //        google.maps.event.addListener(event.getPath(), 'insert_at', function () {
    //            var len = event.getPath().getLength();
    //            for (var i = 0; i < len; i++) {
    //                console.log(event.getPath().getAt(i).toUrlValue(5));
    //            }
    //        });
    //        google.maps.event.addListener(event.getPath(), 'set_at', function () {
    //            var len = event.getPath().getLength();
    //            for (var i = 0; i < len; i++) {
    //                console.log(event.getPath().getAt(i).toUrlValue(5));
    //            }
    //        });
    //    });

    //    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {

    //        all_overlays.push(event);
    //        if (event.type !== google.maps.drawing.OverlayType.MARKER) {
    //            drawingManager.setDrawingMode(null);
    //            //Write code to select the newly selected object.

    //            var newShape = event.overlay;
    //            newShape.type = event.type;
    //            google.maps.event.addListener(newShape, 'click', function () {
    //                setSelection(newShape);
    //            });

    //            setSelection(newShape);
    //        }
    //    });


    //    var centerControlDiv = document.createElement('div');
    //    var centerControl = new CenterControl(centerControlDiv, map);

    //    centerControlDiv.index = 1;
    //    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);
    //    //});
    //}
    ////}

    init();

});