﻿'use strict';

angular.module('app.IotGeofence').controller('IotGeofenecUserlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotGeofenceFactory) {
    function loadInitData() {
        $scope.selection = [];
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        };
        IotGeofenceFactory.getUserList(param).then(function (result) {
            showUserList(result.data.data);
        }, function (error) {
            console.log(error);
        });
    }

    function showUserList(ListData) {
        angular.forEach(ListData, function (data, index) {
            data.assign_geofenece = "";
        });
        var tableOptions = {
            "data": ListData,
            columns: [
                 { data: "user_id" },
                 { data: "user_name" },
                 { data: "first_name" },
                 { data: "last_name" },
                 { data: "deviceno" },
                 { data: "vehicle_no" },
                 { data: "assign_geofenece" }

            ],
            "order": [[0, 'desc']],
            destroy: true
            ,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var data = {
                    'user_id': aData.user_id,
                    'user_name': aData.user_name,
                    'deviceno': aData.deviceno
                }
                $('td:eq(' + 6 + ')', nRow).html('<button class="btn btn-default" ng-click=assignGeofenece(' + aData.user_id + ',"' + aData.user_name + '",' + aData.deviceno + ') title = "Assign Geofenece"><i class="fa fa-user"></i></button>');
                //$('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default' ng-click='assignGeofenece(" + aData.user_id + "," + aData.user_name + "," + aData.deviceno + ")' title = 'Assign Geofenece'><i class='fa fa-user'></i></button>");
            }
        };

        $scope.tablebind(tableOptions, $scope);

    }

    /*   $scope.assignGeofenece = function (user_id, user_name, deviceno) {
           var params = { 'action': 'Assign', 'user_id': user_id, 'user_name': user_name, 'deviceno': deviceno };
           $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
           $state.go('app.IotGeofence.IotGeofenecMaplist');
       }*/

    $scope.assignGeofenece = function (user_id, user_name, deviceno) {
        var params = { 'action': 'Assign', 'user_id': user_id, 'user_name': user_name, 'deviceno': deviceno };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $state.go('app.IotGeofence.IotGeofenecMaplist');
    }

    function init() {
        loadInitData();
    }

    init();
});


