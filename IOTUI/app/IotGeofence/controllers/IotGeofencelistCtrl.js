﻿'use strict';

angular.module('app.IotGeofence').controller('IotGeofenceCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, IotGeofenceFactory) {
    function loadInitData() {


        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        };

        IotGeofenceFactory.getGeofenceList(param).then(function (result) {
            showGeofenceList(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

    function showGeofenceList(ListData) {

        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
        });
        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "gc_id" },
                { data: "geofence_name" },
                { data: "geofenec_desc" },
                { data: "created_at" },
                { data: "updated_at" },
                //{ data: "assign" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" }
            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                //$('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default' ng-click='assignUser(" + aData.gc_id + ")' title = 'Assign Users'><i class='fa fa-user'></i></button>");
                $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.gc_id + ")' title = 'Edit Geofenece'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.gc_id + ")' title = 'View Geofenece'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.gc_id + ")' title = 'Delete Geofenece'><i class='fa fa-times'></i></button>");
            }
        };


        //      <input type="checkbox" name="start_interval_map" ng-model="OnOffStatus"
        //      ng-change="AutoOnOff()"
        //                                         class="onoffswitch-checkbox" id="start_interval_map" ng-disabled="detailForm.VechicleNo.$invalid">
        //                                  <label class="onoffswitch-label" for="start_interval_map" ng-disabled="detailForm.VechicleNo.$invalid">
        //                                      <span class="onoffswitch-inner"
        //                                            data-swchon-text="Auto"
        //          data-swchoff-text="Off"></span>
        //    <span class="onoffswitch-switch"></span>
        //</label>

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {
        var params = { 'action': 'New', 'gc_id': 0 };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $state.go('app.IotGeofence.detail');
    }

    $scope.editRow = function (gc_id) {
        var params = { 'action': 'Edit', 'gc_id': gc_id };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $state.go('app.IotGeofence.detail');
    }

    $scope.viewRow = function (gc_id) {
        var params = { 'action': 'View', 'gc_id': gc_id };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $state.go('app.IotGeofence.detail');
    }

    $scope.deleteRow = function (gc_id) {
        var params = { 'action': 'Edit', 'gc_id': gc_id };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Geofence",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteGeofenceRecord(gc_id);
            }
        });
    }

    $scope.assignUser = function (gc_id) {
        var params = { 'action': 'Assign', 'gc_id': gc_id };
        $window.sessionStorage["Geofenceaction"] = JSON.stringify(params);
        $state.go('app.IotGeofence.GeofenceUseMapping');
    }


    function deleteGeofenceRecord(gc_id) {
        var param = {
            'gc_id': gc_id,
            'updated_by': $rootScope.userInfo.user_id,
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": $rootScope.userInfo.role_Id
        }

        IotGeofenceFactory.deleteGeofence(param)
             .then(function (result) {

                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";
                 $.smallBox({
                     title: "Geofence",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                     loadInitData();

             }, function (error) {
                 console.log(error);
             });

    }

    function init() {
        loadInitData();
    }

    init();
});
