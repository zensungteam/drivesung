'use strict';

angular.module('app.IotGeofence', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
  .config(function (uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyA7bGFFmYR_pRQeU84EYuPVmv__bpSbqdo',
          v: '3.0', //defaults to latest 3.X anyhow
          libraries: 'weather,geometry,visualization,drawing'
      });
  })

.config(function ($stateProvider) {
    $stateProvider
         .state('app.IotGeofence', {
             abstract: true,
             data: {
                 title: ''
             }
         })

        .state('app.IotGeofence.list', {
            url: '/Geofencelist',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/IotGeofence/views/IotGeofencelist.html',
                    controller: 'IotGeofenceCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                       'build/vendor.datatables.js',
                       'build/vendor.graphs.js'
                    ]);
                }
            }
        })

        .state('app.IotGeofence.detail', {
            url: '/GeofenceDetails',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/IotGeofence/views/IotGeofenceDetails.html',
                    controller: 'IotGeofenceAddEditCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                       'build/vendor.datatables.js',
                       'build/vendor.graphs.js'
                    ]);
                }
            }

        })

     .state('app.IotGeofence.IotGeofenecUserList', {
         url: '/GeofenceUseMapping',
         data: {
             title: ''
         },
         views: {
             "content@app": {
                 templateUrl: 'app/IotGeofence/views/IotGeofenecUserList.html',
                 controller: 'IotGeofenecUserlistCtrl'
             }
         },
         resolve: {
             scripts: function (lazyScript) {
                 return lazyScript.register([
                     'build/vendor.ui.js',
                    'build/vendor.datatables.js',
                    'build/vendor.graphs.js'
                 ]);
             }
         }

     })

      .state('app.IotGeofence.IotGeofenecMaplist', {
          url: '/IotGeofenecMapping',
          data: {
              title: ''
          },
          views: {
              "content@app": {
                  templateUrl: 'app/IotGeofence/views/IotGeofenecMaplist.html',
                  controller: 'IotGeofenecMaplistCtrl'
              }
          },
          resolve: {
              scripts: function (lazyScript) {
                  return lazyScript.register([
                      'build/vendor.ui.js',
                     'build/vendor.datatables.js',
                     'build/vendor.graphs.js'
                  ]);
              }
          }

      })


});
