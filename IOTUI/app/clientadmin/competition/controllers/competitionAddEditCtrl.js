﻿'use strict'

angular.module('app.competition').controller('competitionAddEditCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, competitionFactory, $rootScope) {

    var actionCS;
    function loadInitData() {
       
        actionCS = JSON.parse($window.sessionStorage["createcompetition"]);
        $scope.action = actionCS.action;
        $scope.CSMasterIndex = 0;
       if (actionCS.action == "New") {
            $scope.lblheading = "New Competition";
            $scope.lblheading1 = 'Details';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";
            $scope.AuthcheckboxModel = {
                value: true,
            };
            $scope.cmpt_id = 0;
            $scope.isclose_cmpt = false;

         }
        else {
           //$scope.options = [{ Text: "public", value: "public" }, { Text: "private", value: "private" }];
           $scope.lblheading1 = 'View';
            var param = {
                'cmpt_id': actionCS.cmpt_id,
                's_user_id': $rootScope.userInfo.user_id,
                's_role_Id': 2

            };

            competitionFactory.getCompetitionDetails(param).then(function (result) {
                showData(result.data.data);
            },
            function (error) {
                console.log(error);
            });
        }

    }

    function showData(CPDetail) {

        if (CPDetail != null && CPDetail.isComplete == false) {
            var hexcolor = "#5F895F";
            hexcolor = "#FF0000";

            $.smallBox({
                title: "Competition",
                content: "<i class='fa fa-clock-o'></i> <i>" + CPDetail.description + "</i>",
                color: hexcolor,
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.competition.list');
        }
        var dt = new Date();
        $scope.cmpt_id = CPDetail[0].cmpt_id;
        $scope.cmpt_name = CPDetail[0].cmpt_name;
        $scope.cmpt_profile_picture_path = CPDetail[0].cmpt_profile_picture_path;
        $scope.cmpt_type = CPDetail[0].cmpt_type;
        $scope.start_date =$filter('date')(CPDetail[0].start_date, "dd-MM-yyyy");
        $scope.end_date = $filter('date')(CPDetail[0].end_date, "dd-MM-yyyy");
        $scope.terms_conditions = CPDetail[0].terms_conditions;
        $scope.min_km_travel = CPDetail[0].min_km_travel;
        $scope.total_km_travel = CPDetail[0].total_km_travel;
        $scope.cmpt_for_driving = CPDetail[0].cmpt_for_driving;
        $scope.vehicle_manufacturer = CPDetail[0].vehicle_manufacturer;
        $scope.ec_agefrom = CPDetail[0].ec_agefrom;
        $scope.ec_ageto = CPDetail[0].ec_ageto;
        $scope.radioValue = CPDetail[0].gender;
        $scope.AuthcheckboxModel = { value: CPDetail[0].authentication_required };
        $scope.car_type = CPDetail[0].car_type;
        $scope.cmpt_country = CPDetail[0].cmpt_country;
        $scope.cmpt_uniquecode = CPDetail[0].cmpt_uniquecode;
        //$scope.prize_desc1 = CPDetail[0].prize_desc1;
        //$scope.prize_desc2 = CPDetail[0].prize_desc2;
        //$scope.prize_desc3 = CPDetail[0].prize_desc3;
        $scope.created_at = CPDetail[0].created_at;
        $scope.updated_at = CPDetail[0].updated_at;
        $scope.isactive = CPDetail[0].isactive;
        $scope.deleted = CPDetail[0].deleted;
        $scope.isclose_cmpt = CPDetail[0].isclose_cmpt;
        $scope.cmpt_code = CPDetail[0].cmpt_code



        if (actionCS.action == "Edit") {
            $scope.lblheading = "Update Competition";
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Update";
        }
        else if (actionCS.action == "View") {
            $scope.lblheading = "View Competition";
            $scope.editMode = false;
            $scope.deleteMode = false;
        }
    }

    $scope.finalCancelCompetitionRecord = function () {

        $.SmartMessageBox({
            title: "Competition",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.competition.list');
            }
        });
        return;

    }

    $scope.deleteCompetition = function () {
        $.SmartMessageBox({
            title: "Competition",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteCompetitionRecord();
            }
        });
        return;
    }

    function deleteCompetitionRecord() {

        var param = {
            'cmpt_id': actionCS.cmpt_id,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2

        };

        competitionFactory.deleteCompetitionRecord(param)
             .then(function (result) {

                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "Parameters Config Setting",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                     $state.go('app.competition.list');

             }, function (error) {
                 console.log(error);
             });

    }


    $scope.SaveCompetitionRecord = function () {
      
        //var gender = {
        //    //'o_id': actionCS.o_id,
        //    'action': actionCS.action

        //};
        var data = {
            'action': actionCS.action,
            'cmpt_name': $scope.cmpt_name,
            'cmpt_type': "public",
            's_date': $scope.start_date,
            'e_date': $scope.end_date,
            'terms_conditions': $scope.terms_conditions,
            'min_km_travel': $scope.min_km_travel,
            'total_km_travel': $scope.total_km_travel,
            'isclose_cmpt': $scope.isclose_cmpt,
            'cmpt_code':$scope.cmpt_code,
            //'ec_agefrom': $scope.ec_agefrom,
            //'ec_ageto': $scope.ec_ageto,
            'prize1_desc': 0,
            'prize2_desc': 0,
            'prize3_desc': 0,
            //'gender': "male",
            //'authentication_required': $scope.AuthcheckboxModel.value,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2
        }
        var fd = new FormData();
        if ($scope.cmpt_profile_picture_path != null)
        {
            fd.append('file', $scope.cmpt_profile_picture_path[0]);
        }
        else {
            fd.append('file', $scope.cmpt_profile_picture_path);
        }
        
        fd.append('data', JSON.stringify(data));

        competitionFactory.saveCompetitionRecord(fd).then(function (result) {

            $.smallBox({
                title: "Competition Saved Successfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.competition.list');

        }, function (error) {
            console.log(error);
        });



    }

    $scope.UpdateCompetitionRecord = function () {
        debugger;
        //var gender = {
        //    //'o_id': actionCS.o_id,
        //    'action': actionCS.action

        //};
        var data = {
            'cmpt_id': actionCS.cmpt_id,
            'cmpt_name': $scope.cmpt_name,
            'cmpt_type': "public",
            's_date': $scope.start_date,
            'e_date': $scope.end_date,
            'terms_conditions': $scope.terms_conditions,
            'min_km_travel': $scope.min_km_travel,
            'total_km_travel': $scope.total_km_travel,
            'isclose_cmpt': $scope.isclose_cmpt,
            'cmpt_code': $scope.cmpt_code,
            //'ec_agefrom': $scope.ec_agefrom,
            //'ec_ageto': $scope.ec_ageto,
            'prize1_desc': 0,
            'prize2_desc': 0,
            'prize3_desc': 0,
            //'gender': "male",
            //'authentication_required': $scope.AuthcheckboxModel.value,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2
        }
        var fd = new FormData();
        fd.append('file', $scope.cmpt_profile_picture_path);
        fd.append('data', JSON.stringify(data));

        competitionFactory.updateCompetitionRecord(fd).then(function (result) {

            $.smallBox({
                title: "Competition Updated Successfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.competition.list');

        }, function (error) {
            console.log(error);
        });



    }

    function init() {
        loadInitData();
    }

    init();
});