﻿'use strict';

angular.module('app.configsetting').controller('competitionlistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, competitionFactory) {

    function loadInitData() {
        $scope.Emailbody = "Hey joint me at newcompHttp://localhost:8888/#/userating";
        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };


        competitionFactory.getCompetitionList(param).then(function (result) {
           
            showData(result.data.data.all);

        }, function (error) {
            console.log(error);
        });
    }

    //function setInitialData(ListData) {

    //    $scope.getcompetitionListDataModel = ListData;
    //}

    function showData(ListData) {
        showcompetitionList(ListData);
    }

    function showcompetitionList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
            data.Invite = "";
            data.created_by = data.first_name + " " + data.last_name;
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                //{ data: "cmpt_id" },
                { data: "cmpt_name" },
                { data: "start_date" },
                { data: "end_date" },
                { data: "min_km_travel" },
                { data: "created_by" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" },
                { data: "Invite" }
            ],
            //"order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.cmpt_id + ")'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.cmpt_id + ")'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 7 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.cmpt_id + ")'><i class='fa fa-times'></i></button>");
                $('td:eq(' + 8 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal" ng-click="joincompetitionvalue(' + aData.cmpt_id + ')" data-target="#myModal">Invite</button>');
            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {
      
        var params = { 'action': 'New', 'cmpt_id': 0 };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.editRow = function (cmpt_id) {
        debugger;
        var params = { 'action': 'Edit', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.viewRow = function (cmpt_id) {
        var params = { 'action': 'View', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $state.go('app.competition.detail');
    }

    $scope.deleteRow = function (cmpt_id) {
        var params = { 'action': 'Edit', 'cmpt_id': cmpt_id };
        $window.sessionStorage["createcompetition"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Competitions",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteCompetitionRecord(cmpt_id);
            }
        });
    }

   
    function deleteCompetitionRecord(cmpt_id) {
      
        var param = {
            'cmpt_id':cmpt_id,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': 2

        };

        competitionFactory.deleteCompetitionRecord(param)
             .then(function (result) {
                 debugger;
                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "Parameters Config Setting",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                 $state.go('app.competition.list');

             },
              function (error) {
                 console.log(error);
             });

    }

    $scope.sendInvite = function () {
        var email_adress = $scope.emailto + "," + $scope.emailidCc;
        var param = {
            Email_id: email_adress,
            subject:$scope.subject,
            Emailbody: $scope.Emailbody
        };



        competitionFactory.sendInvite(param).then(function (result) {
            $.smallBox({
                title: "Send mail succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });


        }, function (error) {
            console.log(error);
        });

    }

    function init() {
        loadInitData();
    }

    init();
});
