﻿"use strict";

angular.module('app.configsetting', ['ui.router'])

angular.module('app.configsetting').config(function ($stateProvider) {

    $stateProvider
        .state('app.configsetting', {
            abstract: true,
            data: {
                title: ''
            }
        })

        .state('app.configsetting.list', {
            url: '/configsettinglist',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientadmin/parametersconfigsetting/views/parametersconfigsettinglist.html',
                    controller: 'configsettinglistCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

        .state('app.configsetting.detail', {
            url: '/configsetting',
            data: {
                title: ''
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientadmin/parametersconfigsetting/views/parametersconfigsettingdetails.html',
                    controller: 'configsettingAddEditCtrl'
                }
            }
            //resolve: {
            //    srcipts: function (lazyScript) {
            //        return lazyScript.register([
            //               'build/vendor.ui.js',
            //            'build/vendor.datatables.js'

            //        ])
            //    }
            //}
        })
});
