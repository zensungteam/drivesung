﻿"use strict"

angular.module('app.configsetting').factory('configsettingFactory', function ($http, $q, APP_CONFIG) {

    var getConfigsettingList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/GetConfigSettingList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    var getOrganizationList = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/GetOrganizationList";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    
    var deleteConfigsettingRecord = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/DeleteConfigSetting";
        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }




    var getConfigSettingDetails = function (param) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/GetConfigSettingById";
        var deferred = $q.defer();
        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }



    var saveConfigsettingRecord = function (param) {
        var apiUrl
        if (param.actionEntity.action == "New") {
            apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/CreateConfigSetting";
        }
        else if (param.actionEntity.action == "Edit") {
            apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        }


        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return {
        getConfigsettingList: getConfigsettingList,
        getOrganizationList:getOrganizationList,
        getConfigSettingDetails: getConfigSettingDetails,
        saveConfigsettingRecord: saveConfigsettingRecord,
        deleteConfigsettingRecord: deleteConfigsettingRecord
    };



});
