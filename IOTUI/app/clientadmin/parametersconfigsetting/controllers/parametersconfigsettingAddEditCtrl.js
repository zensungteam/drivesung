﻿'use strict'

angular.module('app.configsetting').controller('configsettingAddEditCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, configsettingFactory, $rootScope) {

    var actionCS;
    function loadInitData() {
        actionCS = JSON.parse($window.sessionStorage["configsettingaction"]);
        var filters = {
            o_id: actionCS.o_id,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id,
            action: actionCS

        };

        getOrganizationList(filters)

        $scope.CSMasterIndex = 0;
        if (actionCS.action == "New") {
            $scope.lblheading = "New Parameters Config Setting";
            $scope.lblheading1 = '';
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Submit";
        }
        else {
            configsettingFactory.getConfigSettingDetails(filters).then(function (result) {
                showData(result.data.data);
            }, function (error) {
                console.log(error);
            });
        }

    }

    function fillOrganizationList(alldata) {
        $scope.dtOrganaisationList = JSON.parse(alldata);
    }

    function getOrganizationList(filters) {
        configsettingFactory.getOrganizationList(filters)
        .then(function (result) {
            fillOrganizationList(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function showData(CSDetail) {

        if (CSDetail != null && CSDetail.isComplete == false) {
            var hexcolor = "#5F895F";
            hexcolor = "#FF0000";

            $.smallBox({
                title: "Parameters Config Setting",
                content: "<i class='fa fa-clock-o'></i> <i>" + CSDetail.description + "</i>",
                color: hexcolor,
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('app.configsetting.list');
        }

        $scope.o_id = CSDetail[0].o_id;
        $scope.maxspeed = CSDetail[0].maxspeed;
        $scope.minspeed = CSDetail[0].minspeed;
        $scope.overspeedlow = CSDetail[0].overspeedlow;
        $scope.overspeed10 = CSDetail[0].overspeed10;
        $scope.overspeed20 = CSDetail[0].overspeed20;
        $scope.overspeed30 = CSDetail[0].overspeed30;
        $scope.overspeedlow_d_wt = CSDetail[0].overspeedlow_d_wt;
        $scope.overspeed10_d_wt = CSDetail[0].overspeed10_d_wt;
        $scope.overspeed20_d_wt = CSDetail[0].overspeed20_d_wt;
        $scope.overspeed30_d_wt = CSDetail[0].overspeed30_d_wt;
        $scope.overspeedlow_n_wt = CSDetail[0].overspeedlow_n_wt;
        $scope.overspeed10_n_wt = CSDetail[0].overspeed10_n_wt;
        $scope.overspeed20_n_wt = CSDetail[0].overspeed20_n_wt;
        $scope.overspeed30_n_wt = CSDetail[0].overspeed30_n_wt;
        $scope.overspeed_percentile = CSDetail[0].overspeed_percentile;
        $scope.overspeed_overall_wt = CSDetail[0].overspeed_overall_wt;
        $scope.harsh_ha_d_percentile = CSDetail[0].harsh_ha_d_percentile;
        $scope.harsh_hb_d_percentile = CSDetail[0].harsh_hb_d_percentile;
        $scope.harsh_cornering_d_percentile = CSDetail[0].harsh_cornering_d_percentile;
        $scope.harsh_ha_n_percentile = CSDetail[0].harsh_ha_n_percentile;
        $scope.harsh_hb_n_percentile = CSDetail[0].harsh_hb_n_percentile;
        $scope.harsh_cornering_n_percentile = CSDetail[0].harsh_cornering_n_percentile;
        $scope.harsh_overall_wt = CSDetail[0].harsh_overall_wt;
        $scope.age_criteria1 = CSDetail[0].age_criteria1;
        $scope.age_criteria2 = CSDetail[0].age_criteria2;
        $scope.age_criteria3 = CSDetail[0].age_criteria3;
        $scope.age_criteria4 = CSDetail[0].age_criteria4;
        $scope.age_criteria1_wt = CSDetail[0].age_criteria1_wt;
        $scope.age_criteria2_wt = CSDetail[0].age_criteria2_wt;
        $scope.age_criteria3_wt = CSDetail[0].age_criteria3_wt;
        $scope.age_criteria4_wt = CSDetail[0].age_criteria4_wt;
        $scope.age_overall_wt = CSDetail[0].age_overall_wt;
        $scope.gender_male_wt = CSDetail[0].gender_male_wt;
        $scope.gender_female_wt = CSDetail[0].gender_female_wt;
        $scope.gender_other_wt = CSDetail[0].gender_other_wt;
        $scope.gender_overall_wt = CSDetail[0].gender_overall_wt;
        $scope.car_white_wt = CSDetail[0].car_white_wt;
        $scope.car_lightother_wt = CSDetail[0].car_lightother_wt;
        $scope.car_metallic_light_wt = CSDetail[0].car_metallic_light_wt;
        $scope.car_red_wt = CSDetail[0].car_red_wt;
        $scope.car_metallic_dark_wt = CSDetail[0].car_metallic_dark_wt;
        $scope.car_darkother_wt = CSDetail[0].car_darkother_wt;
        $scope.car_black_wt = CSDetail[0].car_black_wt;
        $scope.car_overall_wt = CSDetail[0].car_overall_wt;
        $scope.created_at = CSDetail[0].created_at;
        $scope.updated_at = CSDetail[0].updated_at;
        $scope.isactive = CSDetail[0].isactive;
        $scope.deleted = CSDetail[0].deleted;

        $scope.overspeed_per_def = CSDetail[0].overspeed_per_def;
        $scope.ha_d_per_def = CSDetail[0].ha_d_per_def;
        $scope.ha_n_per_def = CSDetail[0].ha_n_per_def;
        $scope.hb_d_per_def = CSDetail[0].hb_d_per_def;
        $scope.hb_n_per_def = CSDetail[0].hb_n_per_def;
        $scope.hc_d_per_def = CSDetail[0].hc_d_per_def;
        $scope.hc_n_per_def = CSDetail[0].hc_n_per_def;
        $scope.call_d_percentile = CSDetail[0].call_d_percentile;
        $scope.weather_d_percentile = CSDetail[0].weather_d_percentile;
        $scope.weather_n_percentile = CSDetail[0].weather_n_percentile;
        $scope.weather_d_per_def = CSDetail[0].weather_d_per_def;
        $scope.weather_n_per_def = CSDetail[0].weather_n_per_def;
        $scope.weather_wt = CSDetail[0].weather_wt;
        $scope.call_n_percentile = CSDetail[0].call_n_percentile;
        $scope.call_d_per_def = CSDetail[0].call_d_per_def;
        $scope.call_n_per_def = CSDetail[0].call_n_per_def;
        $scope.call_wt = CSDetail[0].call_wt;

        if (actionCS.action == "Edit") {
            $scope.lblheading = "Update Parameters Config Setting";
            $scope.editMode = true;
            $scope.deleteMode = false;
            $scope.btnSaveText = "Update";
        }
        else if (actionCS.action == "View") {
            $scope.lblheading = "View Parameters Config Setting";
            $scope.editMode = false;
            $scope.deleteMode = false;
        }
    }

    $scope.finalCancelConfigSettingRecord = function () {

        $.SmartMessageBox({
            title: "Parameters Config Setting",
            content: "Are you sure want to Cancel",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                $state.go('app.configsetting.list');
            }
        });
        return;

    }

    $scope.SaveConfigSettingRecord = function () {
        var param = {
            'o_id': actionCS.o_id,
            'action': actionCS.action

        };
        var data = {
            'actionEntity': param,
            'o_id': $scope.o_id,
            'maxspeed': $scope.maxspeed,
            'minspeed': $scope.minspeed,
            'overspeedlow': $scope.overspeedlow,
            'overspeed10': $scope.overspeed10,
            'overspeed20': $scope.overspeed20,
            'overspeed30': $scope.overspeed30,
            'overspeedlow_d_wt': $scope.overspeedlow_d_wt,
            'overspeed10_d_wt': $scope.overspeed10_d_wt,
            'overspeed20_d_wt': $scope.overspeed20_d_wt,
            'overspeed30_d_wt': $scope.overspeed30_d_wt,
            'overspeedlow_n_wt': $scope.overspeedlow_n_wt,
            'overspeed10_n_wt': $scope.overspeed10_n_wt,
            'overspeed20_n_wt': $scope.overspeed20_n_wt,
            'overspeed30_n_wt': $scope.overspeed30_n_wt,
            'overspeed_percentile': $scope.overspeed_percentile,
            'overspeed_overall_wt': $scope.overspeed_overall_wt,
            'harsh_ha_d_percentile': $scope.harsh_ha_d_percentile,
            'harsh_hb_d_percentile': $scope.harsh_hb_d_percentile,
            'harsh_cornering_d_percentile': $scope.harsh_cornering_d_percentile,
            'harsh_ha_n_percentile': $scope.harsh_ha_n_percentile,
            'harsh_hb_n_percentile': $scope.harsh_hb_n_percentile,
            'harsh_cornering_n_percentile': $scope.harsh_cornering_n_percentile,
            'harsh_overall_wt': $scope.harsh_overall_wt,
            'age_criteria1': $scope.age_criteria1,
            'age_criteria2': $scope.age_criteria2,
            'age_criteria3': $scope.age_criteria3,
            'age_criteria4': $scope.age_criteria4,
            'age_criteria1_wt': $scope.age_criteria1_wt,
            'age_criteria2_wt': $scope.age_criteria2_wt,
            'age_criteria3_wt': $scope.age_criteria3_wt,
            'age_criteria4_wt': $scope.age_criteria4_wt,
            'age_overall_wt': $scope.age_overall_wt,
            'gender_male_wt': $scope.gender_male_wt,
            'gender_female_wt': $scope.gender_female_wt,
            'gender_other_wt': $scope.gender_other_wt,
            'gender_overall_wt': $scope.gender_overall_wt,
            'car_white_wt': $scope.car_white_wt,
            'car_lightother_wt': $scope.car_lightother_wt,
            'car_metallic_light_wt': $scope.car_metallic_light_wt,
            'car_red_wt': $scope.car_red_wt,
            'car_metallic_dark_wt': $scope.car_metallic_dark_wt,
            'car_darkother_wt': $scope.car_darkother_wt,
            'car_black_wt': $scope.car_black_wt,
            'car_overall_wt': $scope.car_overall_wt,
            'created_at': $scope.created_at,
            'updated_at': $scope.updated_at,
            'isactive': $scope.isactive,
            'deleted': $scope.deleted,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': $rootScope.userInfo.role_Id,
            'overspeed_per_def': $scope.overspeed_per_def,
            'ha_d_per_def': $scope.ha_d_per_def,
            'ha_n_per_def': $scope.ha_n_per_def,
            'hb_d_per_def': $scope.hb_d_per_def,
            'hb_n_per_def': $scope.hb_n_per_def,
            'hc_d_per_def': $scope.hc_d_per_def,
            'hc_n_per_def': $scope.hc_n_per_def,
            'call_d_percentile': $scope.call_d_percentile,
            'weather_d_percentile': $scope.weather_d_percentile,
            'weather_n_percentile': $scope.weather_n_percentile,
            'weather_d_per_def': $scope.weather_d_per_def,
            'weather_n_per_def': $scope.weather_n_per_def,
            'weather_wt': $scope.weather_wt,
            'call_n_percentile': $scope.call_n_percentile,
            'call_d_per_def': $scope.call_d_per_def,
            'call_n_per_def': $scope.call_n_per_def,
            'call_wt': $scope.call_wt
        }
        configsettingFactory.saveConfigsettingRecord(data).then(function (result) {

            var hexcolor = "#5F895F";
            if (result.data.isComplete == false)
                hexcolor = "#FF0000";

            $.smallBox({
                title: "Parameters Config Setting Details",
                content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                color: hexcolor,
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });

            if (result.data.isComplete == true)
                $state.go('app.configsetting.list');

        }, function (error) {
            console.log(error);
        });
    }

    function init() {

        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        var param = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };

        loadInitData();
    }

    init();

});