﻿'use strict';

angular.module('app.configsetting').controller('configsettinglistCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, configsettingFactory) {

    function loadInitData() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

        var param = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };

        configsettingFactory.getConfigsettingList(param).then(function (result) {

            setInitialData(result.data.data);
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.getconfigsettingListDataModel = ListData;
    }

    function showData(ListData) {
        showconfigsettingList(ListData);
    }

    function showconfigsettingList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.Edit = "";
            data.View = "";
            data.Delete = "";
        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "o_id" },
                { data: "o_code" },
                { data: "o_name" },
                { data: "o_type" },
                { data: "Edit" },
                { data: "View" },
                { data: "Delete" }
            ],
            "order": [[0, 'desc']],
            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                $('td:eq(' + 4 + ')', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.o_id + ")'><i class='fa fa-edit'></i></button>");
                $('td:eq(' + 5 + ')', nRow).html("<button class='btn btn-default'  ng-click='viewRow(" + aData.o_id + ")'><i class='fa fa-reorder'></i></button>");
                $('td:eq(' + 6 + ')', nRow).html("<button class='btn btn-default' ng-click='deleteRow(" + aData.o_id + ")'><i class='fa fa-times'></i></button>");
            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.tablebind(tableOptions, $scope);
    }

    $scope.newRow = function () {
        var params = { 'action': 'New', 'o_id': 0 };
        $window.sessionStorage["configsettingaction"] = JSON.stringify(params);
        $state.go('app.configsetting.detail');
    }

    $scope.editRow = function (o_id) {
        var params = { 'action': 'Edit', 'o_id': o_id };
        $window.sessionStorage["configsettingaction"] = JSON.stringify(params);
        $state.go('app.configsetting.detail');
    }

    $scope.viewRow = function (o_id) {
        var params = { 'action': 'View', 'o_id': o_id };
        $window.sessionStorage["configsettingaction"] = JSON.stringify(params);
        $state.go('app.configsetting.detail');
    }

    $scope.deleteRow = function (o_id) {
        var params = { 'action': 'Edit', 'o_id': o_id };
        $window.sessionStorage["configsettingaction"] = JSON.stringify(params);
        $.SmartMessageBox({
            title: "Parameters Config Setting",
            content: "Are you sure want to delete Record",
            buttons: '[Cancel],[Ok]'
        }, function (ButtonPressed) {
            if (ButtonPressed === "Ok") {
                deleteonfigsettingRecord(o_id);
            }
        });
    }
       

    function deleteonfigsettingRecord(o_id) {

        var param = {
            'o_id': o_id,
            's_user_id': $rootScope.userInfo.user_id,
            's_role_Id': $rootScope.userInfo.role_Id
        }

        configsettingFactory.deleteConfigsettingRecord(param)
             .then(function (result) {

                 var hexcolor = "#5F895F";
                 if (result.data.isComplete == false)
                     hexcolor = "#FF0000";

                 $.smallBox({
                     title: "Parameters Config Setting",
                     content: "<i class='fa fa-clock-o'></i> <i>" + result.data.description + "</i>",
                     color: hexcolor,
                     iconSmall: "fa fa-check bounce animated",
                     timeout: 4000
                 });

                 if (result.data.isComplete == true)
                     loadInitData();

             }, function (error) {
                 console.log(error);
             });

    }

    function init() {
        loadInitData();
    }

    init();
});
