﻿"use strict";

angular.module('app.monitorallvehicles').factory('monitorallvehiclesFactory', function ($http, $q, APP_CONFIG) {

    var getVehicleData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleMonitoringData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    return {
        getVehicleData: getVehicleData
    };

});