'use strict';

angular.module('app.monitorallvehicles').controller('monitorallvehiclesCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, monitorallvehiclesFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        $scope.date = new Date();
        $rootScope.Timer = null;
        $scope.OverSpeedChecked = false,
        $scope.IgnitionOnChecked = false,
        $scope.MovingChecked = false,
        $scope.ParkedChecked = false,
        $scope.UnReachableChecked = false,
        $scope.HardAccelsChecked = false,
        $scope.HardBreakingChecked = false,
        $scope.TamperAlertChecked = false
        $scope.circle=[]
        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });
        
        if ($window.sessionStorage["userInfo"]) {
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
    }



    function setVehicleData(alldata) {
        var alldata = JSON.parse(alldata);
        if (alldata.length > 0) {
            $scope.alldata = alldata;
            fillVehicleDataGrid();
            SetMapData();
        }
        else {
            loadDefaultMapAndChart();
            fillVehicleDataGrid();
        }

    }

    function SetMapData() {
        var markersr = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var options;
        var alertorstatus;
        var address;
        angular.forEach($scope.alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;

            //var geocoder = new google.maps.Geocoder;
            //var latlng = { lat: parseFloat(cureentlat), lng: parseFloat(currentlng) };
            //geocoder.geocode({ 'location': latlng }, function (results, status) {
            //    if (status === 'OK') {
            //        if (results[1]) {
            //            address = results[1].formatted_address
            //        } else {
            //            address = latlng + " : " + 'No results found';
            //        }
            //    } else {
            //        address = latlng + " : " + 'Geocoder failed due to: ' + status;
            //    }
            //});


            options = { icon: 'styles/img/markersicon/moving.png' };
            alertorstatus = "Moving";
            if (data.speed != null) {
                if (data.speed == 0) {
                    options = { icon: 'styles/img/markersicon/parked.png' };
                    alertorstatus = "Parked";
                }
                if (data.speed > 0 && data.speed <= 100) {
                    options = { icon: 'styles/img/markersicon/moving.png' };
                    alertorstatus = "Moving";
                }
                if (data.speed >= 100) {
                    options = { icon: 'styles/img/markersicon/overspeed.png' };
                    alertorstatus = "Over Speed";
                }
            }
            if (data.ignitionstatus != null) {
                if (data.ignitionstatus) {
                    options = { icon: 'styles/img/markersicon/ignitionon.png' };
                    alertorstatus = "Ignition On";
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    options = { icon: 'styles/img/markersicon/hardaccels.png' };
                    alertorstatus = "Hard Acceleration";
                }
            }

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Hard Breaking";
                }
            }


            if (data.diffinmin != null) {
                if (data.diffinmin > 30) {
                    options = { icon: 'styles/img/markersicon/unreachable.png' };
                    alertorstatus = "Un-Reachable";
                }
            }

            if (data.tamperalert != null) {
                if (data.tamperalert) {
                    options = { icon: 'styles/img/markersicon/tamperalert.png' };
                    alertorstatus = "Tamper Alert";
                }
            }

            var vehicledetails = {
                vehicle_no: data.vehicle_no,
                speed: data.speed,
                drivername: "",
                derivermobileno: "",
                recorddatetime: data.recorddatetime,
                latitude: data.latitude,
                longitude: data.longitude,
                address: address,
                alert: alertorstatus
            }

            markersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                options: options,
                vehicledetails: vehicledetails
            });


        });
        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng }, zoom: zoom, bounds: {},
            control: {},
            markersr: markersr
        };


    }

    function fillVehicleDataGrid() {
        //Addding new columns
        angular.forEach($scope.alldata, function (data, index) {
            // data.recorddatetime = $filter('date')(data.recorddatetime, APP_CONFIG.dateFormat);
            //  data.recorddatetime = $filter('date')(data.recorddatetime, "dd-MM-yyyy hh:mma");
            data.type = "";
            data.location = "Location Details";
            data.distance = "";
            data.viewonmap = "";
        });
        var tableOptions = {
            "data": $scope.alldata,
            //"iDisplayLength": 10,
            columns: [
                { data: "vehicle_type" },
                { data: "deviceno" },
                { data: "recorddatetime" },
                { data: "location" },
                { data: "latitude" },
                { data: "longitude" },
                { data: "speed" },
                { data: "ignitionstatus" },
                { data: "hardacceleration" },
                { data: "hardbraking" },
                { data: "viewonmap" },

            ],
            "order": [[2, 'desc']],
            "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
            destroy: true
            ,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $('td:eq(10)', nRow).html("<button class='btn btn-default' ng-click='viewRow(" + aData.latitude + "," + aData.longitude + ")'><i class='fa fa-map-marker' aria-hidden='true'></i></button>");
            }
        };

        $scope.dashTablebind(tableOptions, $scope);
    }

    function loadDefaultMap() {
        $scope.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 5, bounds: {},
            control: {},
            markersr: []
        };
    }
    
    $scope.getVehicleData = function () {

        var filters = {
            VechileNo: "",
            DeviceNo: "",
            fromdate: "",
            todate: "",
            fromtime: "",
            totime: "",
            NoOfRecords: "",
            OverSpeed: $scope.OverSpeedChecked,
            IgnitionOn: $scope.IgnitionOnChecked,
            Moving: $scope.MovingChecked,
            Parked: $scope.ParkedChecked,
            UnReachable: $scope.UnReachableChecked,
            HardAccels: $scope.HardAccelsChecked,
            HardBreaking: $scope.HardBreakingChecked,
            TamperAlert: $scope.TamperAlertChecked,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        monitorallvehiclesFactory.getVehicleData(filters)
        .then(function (result) {
            //format result
            setVehicleData(JSON.stringify(result.data.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList() {
        $scope.dtVehicleList = JSON.parse($scope.alldata);
    }

    $scope.viewRow = function (latitude, longitude) {
        //var zoom = $scope.map.zoom;
        //$scope.map = {
        //    center: { latitude: latitude, longitude: longitude },
        //    zoom: 20,
        //};
        $scope.map.center.latitude = latitude;
        $scope.map.center.longitude = longitude;
        $scope.map.zoom = 15;
        $scope.circle = [];
       
        var newObj = {
            latitude: latitude,
            longitude: longitude
        };
        $scope.circle.push(newObj);
       
    }


    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.windowOptions = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.windowOptions.visible = false;
    };

    //$scope.title = "Window Title!";

    //start Auto referesh
    function StartAutoRefresh() {
        $rootScope.Timer = $interval(function () {
            $scope.getVehicleData();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };


    function init() {
        loadInitData();
        loadDefaultMap();
        $scope.getVehicleData();

    }

    init();

});