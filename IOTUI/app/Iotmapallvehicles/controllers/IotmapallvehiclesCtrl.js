'use strict';

angular.module('app.Iotmapallvehicles').controller('IotmapallvehiclesCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IotmapallvehiclesFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        //$scope.date = new Date();
        $rootScope.Timer = null;
        $scope.OSChecked;
        $scope.HAChecked;
        $scope.HBChecked;
        $scope.HCChecked;
        $scope.CTChecked;
        //$scope.OverSpeedChecked = false,
        //$scope.IgnitionOnChecked = false,
        //$scope.MovingChecked = false,
        //$scope.ParkedChecked = false,
        //$scope.UnReachableChecked = false,
        //$scope.HardAccelsChecked = false,
        //$scope.HardBreakingChecked = false,
        //$scope.TamperAlertChecked = false
        $scope.circle = []
        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });

        if ($window.sessionStorage["userInfo"]) {
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
    }

    function SetCount(hdata) {

        $scope.HB = hdata[0].hb;
        $scope.HA = hdata[0].ha;
        $scope.HC = hdata[0].cor;
        $scope.OS = hdata[0].os;
        $scope.CT = hdata[0].mcall;


    }


    function setVehicleData(alldata, hdata) {
        var alldata = JSON.parse(alldata);
        var hdata = JSON.parse(hdata);
        SetCount(hdata);
        fillVehicleDataGrid(alldata);
        if (alldata.length > 0) {
            SetMapData(alldata);
        }
        else {
            loadDefaultMap();
        }

    }

    function SetMapData(alldata) {
        var markersr = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var options;
        var alertorstatus;
        var address;
        angular.forEach(alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;

            options = { icon: 'styles/img/markersicon/moving.png' };
            alertorstatus = "Moving";

            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Hard Breaking";
                }
            }
            if (data.overspeedstarted != null) {
                if (data.overspeedstarted) {
                    options = { icon: 'styles/img/markersicon/overspeed.png' };
                    alertorstatus = "Over Speed";
                }
            }

            if (data.cornerpacket != null) {
                if (data.cornerpacket) {
                    options = { icon: 'styles/img/markersicon/ignitionon.png' };
                    alertorstatus = "Cornering";
                }
            }

            if (data.mcall != null) {
                if (data.mcall) {
                    options = { icon: 'styles/img/markersicon/parked.png' };
                    alertorstatus = "Calls Taken";
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    options = { icon: 'styles/img/markersicon/hardaccels.png' };
                    alertorstatus = "Hard Acceleration";
                }
            }

            var vehicledetails = {
                vehicle_no: data.deviceno,
                speed: data.speed,
                drivername: data.first_name + ' ' + data.middle_name + ' ' + data.last_name,
                derivermobileno: data.mobileno,
                emailid: data.email_id1,
                recorddatetime: data.recorddatetime,
                latitude: data.latitude,
                longitude: data.longitude,
                address: data.address1,
                alert: alertorstatus
            }

            markersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                options: options,
                vehicledetails: vehicledetails
            });


        });
        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng }, zoom: zoom, bounds: {},
            control: {},
            markersr: markersr
        };


    }

    function fillVehicleDataGrid(alldata) {
        //Addding new columns
        angular.forEach(alldata, function (data, index) {
            // data.recorddatetime = $filter('date')(data.recorddatetime, APP_CONFIG.dateFormat);
            //data.recorddatetime = $filter('date')(data.recorddatetime, "dd-MM-yyyy hh:mma");
            //  data.recorddatetime = $filter('date')(data.recorddatetime, "yyyy-mm-dd hh:mm:ss");
            //data.type = "";
            //  data.location = "";

        });
        var tableOptions = {
            "data": alldata,
            //"iDisplayLength": 10,
            columns: [
                //{ data: "type" },
                { data: "deviceno" },
                { data: "recorddatetime" },
                //{ data: "location" },
                { data: "latitude" },
                { data: "longitude" },
                { data: "speed" },
                { data: "distance" },
                { data: "distancecan" },
                { data: "rpm" },
                { data: "coolanttemperature" },
                //{ data: "fuellevel" },

            ],
            "order": [[1, 'desc']],
            "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8],
            destroy: true

        };

        $scope.dashTablebind(tableOptions, $scope);
    }

    function loadDefaultMap() {
        $scope.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 5, bounds: {},
            control: {},
            markersr: []
        };
    }

    $scope.getVehicleData = function () {
        var filters = {
            VechileNo: "",
            DeviceNo: "",
            fromdate: "",
            todate: "",
            fromtime: "",
            totime: "",
            NoOfRecords: "",
            OverSpeed: $scope.OSChecked,
            HA: $scope.HAChecked,
            HB: $scope.HBChecked,
            HC: $scope.HCChecked,
            CT: $scope.CTChecked,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        IotmapallvehiclesFactory.getVehicleData(filters)
        .then(function (result) {
            //format result
            setVehicleData(JSON.stringify(result.data.data.all), JSON.stringify(result.data.data.count));
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList() {
        $scope.dtVehicleList = JSON.parse($scope.alldata);
    }

    $scope.viewRow = function (latitude, longitude) {
        //var zoom = $scope.map.zoom;
        //$scope.map = {
        //    center: { latitude: latitude, longitude: longitude },
        //    zoom: 20,
        //};
        $scope.map.center.latitude = latitude;
        $scope.map.center.longitude = longitude;
        $scope.map.zoom = 15;
        $scope.circle = [];

        var newObj = {
            latitude: latitude,
            longitude: longitude
        };
        $scope.circle.push(newObj);

    }


    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }

    $scope.windowOptions = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.windowOptions.visible = false;
    };

    //$scope.title = "Window Title!";

    //start Auto referesh
    function StartAutoRefresh() {
        $rootScope.Timer = $interval(function () {
            $scope.getVehicleData();
        }, 30000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };


    function init() {
        loadInitData();
        loadDefaultMap();
        $scope.getVehicleData();

    }

    init();

});