'use strict';

angular.module('app.dashboardtelematics').controller('dashboardtelematics', function ($q, $scope,$filter, $interval,$rootScope,$window, CalendarEvent, SmartMapStyle, dashboardFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {
        //$scope.requestDate_filter = null;
        //$scope.escalationDate_filter = null;
        //$scope.livedata = [{ rpm: 1400 }];
        //Speed
        $rootScope.Timer = null;
        $scope.$on('$destroy', function () {
            StopAutoRefresh();
        });

        if ($window.sessionStorage["userInfo"]) {
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            //$rootScope.userInfo.lastLoginDate = $filter('date')($rootScope.userInfo.lastLoginDate, APP_CONFIG.dateFormat + ' hh:mm:ss a');
        }
        $scope.vehicleRefId = "1000001";
        $scope.NoOfRecords = 20;
        $scope.chartModel = {
            Speed: {
                SpeedPercent: 0,
                Speed: 0,
                maxvalue:255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#014476',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            Rpm: {
                RpmPercent: 0,
                Rpm: 0,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#383c3f',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            CoolantTemp: {
                CoolantTempPercent: 0,
                CoolantTemp: 0,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            Fuellevel: {
                FuellevelPercent: 0,
                Fuellevel: 0,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            }
        };

        //End Speed

        //RPM
        //$scope.RpmPercent = 0;
        //$scope.RPM = 0
        //$scope.Options = {
        //    animate: {
        //        duration: 0,
        //        enabled: false
        //    },
        //    barColor: '#aabed1',
        //    scaleColor: false,
        //    lineWidth: 20,
        //    lineCap: 'circle'
        //};

        //End RPM


        //$scope.chartModel = {
        //    speed: {
        //        Percent: 0,
        //        speed: 0,
        //        Options: {
        //            animate: {
        //                duration: 0,
        //                enabled: true
        //            },
        //            barColor: '#aabed1',
        //            scaleColor: false,
        //            lineWidth: 20,
        //            lineCap: 'circle'
        //        },
        //    },
        //    rpm: {
        //        Percent: 0,
        //        rpm: 0,
        //        Options: {
        //            animate: {
        //                duration: 0,
        //                enabled: true
        //            },
        //            barColor: '#aabed1',
        //            scaleColor: false,
        //            lineWidth: 20,
        //            lineCap: 'circle'
        //        },
        //    }
        //}


    }

    function SetMapData(alldata) {
        ////sol 1
        //var latlon = [{ "latitude": 19.009460, "longitude": 74.862213 }];
        //angular.forEach(alldata, function (data, index) {
        //    var lat = data.latitude;
        //    var lng = data.longitude;
        //    latlon.push({ latitude: lat, longitude: lng });
        //});
        //$scope.polylines[0].path = latlon
        //$scope.$applyAsync();

        ////sol 2
        //var latlon = [];
        //angular.forEach(alldata, function (data, index) {
        //    var lat = data.latitude;
        //    var lng = data.longitude;
        //    latlon.push({ latitude: lat, longitude: lng });
        //});
        //$scope.polylines[0].path = latlon
        //$scope.$applyAsync();

        var pl = [];
        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        angular.forEach(alldata, function (data, index) {
            cureentlat = data.latitude;
            currentlng = data.longitude;
            if (index > 0) {
                if (data.speed >= 80) {
                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#f91517',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }
                else if (data.speed > 60 && data.speed <= 79) {
                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#16872f',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }
                else {

                    pl.push({
                        id: index,
                        path:
                            [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                        stroke: {
                            color: '#1451f9',
                            weight: 3
                        },
                        icons: [{
                            icon: {
                                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                            },
                            offset: '25px',
                            repeat: '50px'
                        }
                        ]
                    })
                }

            }
            prvlat = cureentlat;
            prvlng = currentlng;
        });
        $scope.polylines = pl;

        ////$scope.map.center.latitude = cureentlat;
        ////$scope.map.center.longitude = currentlng;
        //$scope.map.center = {
        //    latitude:cureentlat,
        //    longitude: currentlng,
        //    zoom:20
        //};

        //$scope.map.markersr[0].latitude = cureentlat;
        //$scope.map.markersr[0].longitude = currentlng;
        
        var zoom = $scope.map.zoom;
        $scope.map = {
            center: { latitude: cureentlat, longitude: currentlng },  bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: cureentlat,
                longitude: currentlng
            }]
        };

        //uiGmapGoogleMapApi.then(function (maps) {
        //    $scope.polylines = pl;
        //});

        //   $scope.$watch('map.markersr[0].latitude', loadDefaultMap1);
        //        $scope.$applyAsync();

    }

    function SetChartData(livedata) {

        //Speed
        var CalSpeedPercentage = 0;
        var Speed = 0;
        if (livedata[0].speed != undefined) {
            CalSpeedPercentage = ((100 * livedata[0].speed) / 255);
            Speed = livedata[0].speed;
        }
        var CalRpmPercentage = 0;
       var Rpm = 0;
        if (livedata[0].rpm != undefined) {
            CalRpmPercentage = ((100 * livedata[0].rpm) / 16383);
            Rpm = livedata[0].rpm;
        }
        var CalCoolantTempPercentage = 0
        var CoolantTemp = 0;
        if (livedata[0].coolanttemperature != undefined) {
            CalCoolantTempPercentage = ((100 * livedata[0].coolanttemperature) / 215);
            CoolantTemp = livedata[0].coolanttemperature;
        }
        var CalFuellevelPercent = 0;
        var Fuellevel = 0;
        if (livedata[0].fuellevel != undefined) {
            CalFuellevelPercent = ((100 * livedata[0].fuellevel) / 100);
            Fuellevel = livedata[0].fuellevel;
        }

        $scope.chartModel = {
            Speed: {
                SpeedPercent: CalSpeedPercentage,
                Speed: Speed,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#014476',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            Rpm: {
                RpmPercent: CalRpmPercentage,
                Rpm: Rpm,
                maxvalue: 16383.75,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#383c3f',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            },
            CoolantTemp: {
                CoolantTempPercent: CalCoolantTempPercentage,
                CoolantTemp: CoolantTemp,
                maxvalue: 255,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth:0,
                    lineCap: 'circle'
                }
            }
            ,
            Fuellevel: {
                FuellevelPercent: CalFuellevelPercent,
                Fuellevel: Fuellevel,
                maxvalue: 215,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 0,
                    lineCap: 'circle'
                }
            }
        };


        //$scope.Percent = CalSpeedPercentage;
        //$scope.speed = parseInt(livedata[0].speed)
        //$scope.Options = {
        //    animate: {
        //        duration: 0,
        //        enabled: false
        //    },
        //    barColor: '#aabed1',
        //    scaleColor: false,
        //    lineWidth: 20,
        //    lineCap: 'circle'
        //};

        //$scope.speed = {
        //     Percent: CalSpeedPercentage,
        //    speed: parseInt(livedata[0].speed),
        //    Options: {
        //        animate: {
        //            duration: 0,
        //            enabled: true
        //        },
        //        barColor: '#aabed1',
        //        scaleColor: false,
        //        lineWidth: 20,
        //        lineCap: 'circle'
        //    }

        //}

        //$scope.chartModel = {
        //    speed: {
        //        Percent: CalSpeedPercentage,
        //        speed: parseInt(livedata[0].speed),
        //        Options: {
        //            animate: {
        //                duration: 0,
        //                enabled: true
        //            },
        //            barColor: '#aabed1',
        //            scaleColor: false,
        //            lineWidth: 20,
        //            lineCap: 'circle'
        //        },
        //    },
        //    rpm: {
        //        Percent: CalRpmPercentage,
        //        rpm: parseInt(livedata[0].rpm),
        //        Options: {
        //            animate: {
        //                duration: 0,
        //                enabled: true
        //            },
        //            barColor: '#aabed1',
        //            scaleColor: false,
        //            lineWidth: 20,
        //            lineCap: 'circle'
        //        },
        //    }
        //}

        //End Speed




    }

    function fillDashBoardLiveData(livedata, alldata) {
        var alldata = JSON.parse(alldata);
        var livedata = JSON.parse(livedata);
        if (livedata.length > 0) {
            $scope.alldata = alldata;
            $scope.livedata = livedata;
            //  $scope.livedata = [{ rpm: 90 }]
            fillVehicleDataGrid(alldata);
            SetChartData(livedata);
            SetMapData(alldata);
        }
        else {
            loadDefaultMapAndChart();
            fillVehicleDataGrid(alldata);
        }

    }

    function fillVehicleDataGrid(alldata) {
        //Addding new columns
        angular.forEach(alldata, function (data, index) {
           // data.recorddatetime = $filter('date')(data.recorddatetime, APP_CONFIG.dateFormat);
            data.recorddatetime = $filter('date')(data.recorddatetime, "dd-MM-yyyy hh:mma");
            data.type = "";
            data.location = "Location Details";
            data.distance = "";
        });
        var tableOptions = {
            "data": alldata,
            //"iDisplayLength": 10,
            columns: [
                { data: "type" },
                { data: "deviceno" },
                { data: "recorddatetime" },
                { data: "location" },
                { data: "latitude" },
                { data: "longitude" },
                { data: "speed" },
                { data: "distance" },
                { data: "rpm" },
                { data: "coolanttemperature" },
                { data: "fuellevel" },

            ],
            "order": [[2, 'desc']],
            "mColumns": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
            destroy: true
            //,
            //"fnCreatedRow": function (nRow, aData, iDataIndex) {

            //    var IsEdit = true;
            //    if (aData.park_qry == true && aData.post_qry == null && aData.close_qry == null && aData.close_date == null)
            //        IsEdit = false;
            //    if (aData.close_qry == null && aData.close_date != null)
            //        IsEdit = false;
            //    if (aData.close_qry == true)
            //        IsEdit = false;
            //    if (IsEdit)
            //        $('td:eq(7)', nRow).html("<button class='btn btn-default' ng-click='editRow(" + aData.query_Ref_Id + "," + aData.parent_Request_No + ")'><i class='fa fa-edit'></i></button>");

            //    $('td:eq(8)', nRow).html("<button class='btn btn-default' ng-click='viewRow(" + aData.query_Ref_Id + "," + aData.parent_Request_No + ")'><i class='fa fa-reorder'></i></button>");

            //    var IsSubQuery = true;
            //    if (!aData.close_qry) {
            //        if (aData.park_qry == true && aData.post_qry == null && aData.close_qry == null && aData.close_date == null)
            //            IsSubQuery = false;
            //        if (aData.open_qry == true && aData.park_qry == null && aData.post_qry == null)
            //            IsSubQuery = false;
            //    }
            //    if (IsSubQuery)
            //        $('td:eq(9)', nRow).html("<button class='btn btn-xs btn-default' ng-click='addsubquery(" + aData.query_Ref_Id + "," + aData.parent_Request_No + ")'>Add Sub Query</button>");
            //}
        };

        $scope.dashTablebind(tableOptions, $scope);
    }

    function loadDefaultMapAndChart() {
        $scope.map = {
            center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 12, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: 0,
                longitude: 0
            }]
        };

        uiGmapGoogleMapApi.then(function (maps) {
            $scope.polylines = [];
        });

        $scope.chartModel = {
            Speed: {
                SpeedPercent: 0,
                Speed: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#014476',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            Rpm: {
                RpmPercent: 0,
                Rpm: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#383c3f',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            CoolantTemp: {
                CoolantTempPercent: 0,
                CoolantTemp: 0,
                Options: {
                    animate: {
                        duration: 0,
                        enabled: false
                    },
                    barColor: '#2C3E50',
                    scaleColor: false,
                    lineWidth: 20,
                    lineCap: 'circle'
                }
            },
            Fuellevel: {
                FuellevelPercent: 0,
                Fuellevel: 0,
                Options: {
                    animate: {
                        duration: 5,
                        enabled: true
                    },
                    barColor: '#E67E22',
                    scaleColor: false,
                    lineWidth: 3,
                    lineCap: 'butt'
                }
            }
        };


    }

    $scope.date = new Date();

    $scope.getDashBoardLiveData = function () {
        var VechileNo = $scope.vehicleRefId;
        var DeviceNo = $scope.vehicleRefId;
        var fromdate;
        var todate;
        var fromtime;
        var totime;
        var NoOfRecords = $scope.NoOfRecords;

        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            fromdate: fromdate,
            todate: todate,
            fromtime: fromtime,
            totime: totime,
            NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        dashboardFactory.getDashBoardLiveData(filters)
        .then(function (result) {
            //format result
            fillDashBoardLiveData(JSON.stringify(result.data.data.livedata), JSON.stringify(result.data.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function fillVehicleList(allVehicle) {
        $scope.dtVehicleList = JSON.parse(allVehicle);
    }

  function getVehicleList() {
        var filters = {
            //VechileNo: VechileNo,
            //DeviceNo: DeviceNo,
            //fromdate: fromdate,
            //todate: todate,
            //fromtime: fromtime,
            //totime: totime,
            //NoOfRecords: NoOfRecords,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
                         
        };
        dashboardFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

 

    $scope.AutoOnOff = function () {

        if ($scope.OnOffStatus) {
            StartAutoRefresh();
        }
        else {
            StopAutoRefresh();
        }
    }
    
    $scope.windowOptions = {
        visible: false
    };

    $scope.onClick = function () {
        $scope.windowOptions.visible = !$scope.windowOptions.visible;
    };

    $scope.closeClick = function () {
        $scope.windowOptions.visible = false;
    };

    //$scope.title = "Window Title!";




    //start Auto referesh
    function StartAutoRefresh() {
        //$scope.Message = "Timer started. ";

        $rootScope.Timer = $interval(function () {
            $scope.getDashBoardLiveData();
            //var lat = $scope.map.center.latitude;
            //var lng = $scope.map.center.longitude;
            //lat = lat + 0.001;
            //lng = lng - 0.001;
            ////   $scope.polylines[0].path = [];
            //$scope.polylines[0].path.push({ latitude: lat, longitude: lng });
            //$scope.map.center.latitude = lat;
            //$scope.map.center.longitude = lng;
            //$scope.map.markersr[0].latitude = lat;
            //$scope.map.markersr[0].longitude = lng;
            ////  getDashBoardLiveData();
            //$scope.$applyAsync();
        }, 10000);
    };

    //Stop Auto referesh
    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.Timer)) {
            $interval.cancel($rootScope.Timer);
        }
    };


    function init() {
        loadInitData();
        loadDefaultMapAndChart();
        getVehicleList()

    }

    init();

});