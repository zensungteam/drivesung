﻿"use strict";

angular.module('app.dashboardtelematics').factory('dashboardFactory', function ($http, $q, APP_CONFIG) {

    var getDashBoardLiveData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetDashboardData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
       

    return {
        getDashBoardLiveData: getDashBoardLiveData,
        getVehicleList: getVehicleList
    };

});