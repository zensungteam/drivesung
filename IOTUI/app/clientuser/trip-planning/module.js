﻿"use strict";

angular.module('app.tripplanning', ['ui.router'])

angular.module('app.tripplanning').config(function ($stateProvider) {

    $stateProvider
         .state('app.tripplanning', {
             abstract: true,
             data: {
                 title: ''
             }
         })
         .state('app.tripplanning.detail', {
             url: '/tripplanning',
            data: {
                title: 'Trip planning'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/clientuser/trip-planning/views/tripplanningdetails.html',
                    controller: 'tripplanningCtrl'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                        'build/vendor.ui.js',
                        'build/vendor.datatables.js'
                    ]);
                }
            }
        })

});
