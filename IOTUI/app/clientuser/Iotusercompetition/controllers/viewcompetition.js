﻿'use strict';

angular.module('app.usercompetition').controller('viewcompetitionCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, usercompetitionFactory) {

    function loadInitData() {
        $scope.condition = false;
        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };
        usercompetitionFactory.getViewCompetitionList(param).then(function (result) {
            setInitialData(result.data.data);
            showData(result.data.data);

        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.getcompetitionListDataModel = ListData;
    }

    function showData(ListData) {
        showPrivateCompetitionList(ListData.private);
        showPubliccompetitionList(ListData.public);
    }

    function showPrivateCompetitionList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.pic = "";
            data.join = "";
            data.created_by = data.first_name + " " + data.last_name;

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "pic" },
                { data: "cmpt_name" },
	            { data: "min_km_travel" },
                { data: "no_users" },
                { data: "created_by" },
                { data: "join" }

            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                if (aData.cmpt_profile_picture_path != null) {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.cmpt_profile_picture_path + '" alt="img" class="compcar">');
                }
                else {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="styles/img/crawifi.png" alt="img" class="compcar">');
                }
                var competitiondata = { 'terms_conditions': aData.terms_conditions, 'cmpt_id': aData.cmpt_id };
               
                //$('td:eq(' + 4 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal" ng-click="joincompetitionvalue(' + aData.cmpt_id + ')" data-target="#myModal">Join</button>');
                $('td:eq(' + 5 + ')', nRow).html("<button type='button' class='btn btn-primary' data-toggle='modal' ng-click='joincompetitionvalue(" +JSON.stringify(competitiondata)+ ")' data-target='#myModal'>Join</button>");

            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.privatetablebind(tableOptions, $scope);
    }

    function showPubliccompetitionList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.pic = "";
            data.join = "";
            data.created_by = data.first_name + " " + data.last_name;

        });

        var tableOptions = {
            "data": ListData,
            columns: [
               { data: "pic" },
                { data: "cmpt_name" },
	            { data: "min_km_travel" },
                { data: "no_users" },
                { data: "created_by" },
                { data: "join" }

            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                if (aData.cmpt_profile_picture_path != null) {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.cmpt_profile_picture_path + '" alt="img" class="compcar">');
                }
                else {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="styles/img/crawifi.png" alt="img" class="compcar">');
                }
                var competitiondata = { 'terms_conditions': aData.terms_conditions, 'cmpt_id': aData.cmpt_id, 'isclose_cmpt': aData.isclose_cmpt };

                //$('td:eq(' + 4 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal" ng-click="joincompetitionvalue(' + aData.cmpt_id + ')" data-target="#myModal">Join</button>');
                $('td:eq(' + 5 + ')', nRow).html("<button type='button' class='btn btn-primary' data-toggle='modal' ng-click='joincompetitionvalue(" + JSON.stringify(competitiondata) + ")' data-target='#myModal'>Join</button>");

            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.publictablebind(tableOptions, $scope);
    }

    $scope.joincompetition = function () {
        var dt = new Date();
        var param = {
            userid: $rootScope.userInfo.user_id,
            cmpt_id: $scope.joincomp_id,
            deviceno: $rootScope.userInfo.deviceno,
            cmpt_code:$scope.cmpt_code,
            jointimestam: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "yyyy-MM-dd HH:mm:ss"),
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: 1
        };



        usercompetitionFactory.joinCompetitionRecord(param).then(function (result) {
            $.smallBox({
                title: "Join competition successfully .",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            loadInitData();

        }, function (error) {
            console.log(error);
        });
    }

    $scope.joincompetitionvalue = function (data) {

        $scope.joincomp_id = data.cmpt_id;
        $scope.terms_conditions = data.terms_conditions;
        $scope.isclose_cmpt = data.isclose_cmpt;
    }


    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
