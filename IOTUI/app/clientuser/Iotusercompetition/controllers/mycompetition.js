﻿'use strict';

angular.module('app.usercompetition').controller('mycompetitionCtrl', function ($scope, $window, $filter, $state, APP_CONFIG, $rootScope, usercompetitionFactory) {

    function loadInitData() {
        $scope.Emailbody = "Hey joint me at newcompHttp://localhost:8888/#/userating";
        var param = {
            "s_user_id": $rootScope.userInfo.user_id,
            "s_role_Id": 1
        };


        usercompetitionFactory.getMyCompetitionList(param).then(function (result) {
            debugger;
            setInitialData(result.data.data);
            showData(result.data.data);


        }, function (error) {
            console.log(error);
        });
    }

    function setInitialData(ListData) {

        $scope.getcompetitionListDataModel = ListData;
    }

    function showData(ListData) {
        showPrivateCompetitionList(ListData.private);
        showPubliccompetitionList(ListData.public);
    }

    function showPrivateCompetitionList(ListData) {
       
        //var currenttime = "2017-08-23";
        angular.forEach(ListData, function (data, index) {
            data.pic = "";
            data.join = "";
            data.compNm = "";
           

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "pic" },
                { data: "compNm" },
                //{ data: "day" },
	            { data: "min_km_travel" },
                { data: "score" },
                { data: "rankno" },
                { data: "join" }

            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                if (aData.cmpt_profile_picture_path != null) {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.cmpt_profile_picture_path + '" alt="img" class="compcar">');
                }
                else {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="styles/img/crawifi.png" alt="img" class="compcar">');
                }
                //$('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.cmpt_profile_picture_path + '" alt="img" class="compcar">');
                $('td:eq(' + 1 + ')', nRow).html('<span class="compname"><a ng-click="ratingandranking(' + aData.cmpt_id + ')">' + aData.cmpt_name + '</a></span>');
                $('td:eq(' + 5 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal" ng-click="joincompetitionvalue(' + aData.cmpt_id + ')" data-target="#myModal">Invite</button>');

            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.privatetablebind(tableOptions, $scope);
    }

    function showPubliccompetitionList(ListData) {
        var IsDelete = true;
        angular.forEach(ListData, function (data, index) {
            data.pic = "";
            data.join = "";
            data.compNm = "";

        });

        var tableOptions = {
            "data": ListData,
            columns: [
                { data: "pic" },
                { data: "cmpt_name" },
                //{ data: "day" },
	            { data: "min_km_travel" },
                { data: "score" },
                { data: "rankno" },
                { data: "join" }

            ],

            destroy: true,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {

                if (aData.cmpt_profile_picture_path != null) {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="' + aData.cmpt_profile_picture_path + '" alt="img" class="compcar">');
                }
                else {
                    $('td:eq(' + 0 + ')', nRow).html(' <img src="styles/img/crawifi.png" alt="img" class="compcar">');
                }
                $('td:eq(' + 1 + ')', nRow).html('<span class="compname"><a ng-click="ratingandranking(' + aData.cmpt_id + ')">' + aData.cmpt_name + '</a></span>');
                $('td:eq(' + 5 + ')', nRow).html('<button type="button" class="btn btn-primary" data-toggle="modal"  data-target="#myModal">Invite</button>');


            }
            //, "aoColumnDefs": [

            //     //{ "bVisible": IsDelete, "aTargets": [-1] }
            //     { "bVisible": false, "aTargets": [0] }

            //]

        };

        $scope.publictablebind(tableOptions, $scope);
    }

    $scope.ratingandranking = function (cmpt_id) {

        var params = { 'action': 'New', 'cmpt_id': cmpt_id };
        $window.sessionStorage["userratingandranking"] = JSON.stringify(params);
        $state.go('app.usercompetition.userratingandranking');
    }
   
    $scope.joincompetitionvalue = function (cmpt_id) {

        $scope.joincomp_id = cmpt_id;
    }

    $scope.sendInvite = function () {
        var email_adress = $scope.emailto + "," + $scope.emailidCc;
        var param = {
            Email_id: email_adress,
            subject: $scope.subject,
            Emailbody: $scope.Emailbody
        };


        usercompetitionFactory.sendInvite(param).then(function (result) {
            $.smallBox({
                title: "Send mail succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });


        }, function (error) {
            console.log(error);
        });

    }

    function init() {
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }
        loadInitData();
    }

    init();
});
