﻿"use strict";

angular.module('app.tripplayback').factory('tripplaybackFactory', function ($http, $q, APP_CONFIG) {

    var getPlayTripData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetTripPlayData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
       

    return {
        getPlayTripData: getPlayTripData,
        getVehicleList: getVehicleList
    };

});