'use strict';

angular.module('app.tripplayback', [
    'ui.router',
    'ngResource',
    'datatables',
    'datatables.bootstrap',
    'uiGmapgoogle-maps',
    'easypiechart',
    'angularjs-gauge'
])
  .config(function (uiGmapGoogleMapApiProvider) {
      uiGmapGoogleMapApiProvider.configure({
          key: 'AIzaSyA7bGFFmYR_pRQeU84EYuPVmv__bpSbqdo',
          v: '3.0', //defaults to latest 3.X anyhow
          libraries: 'weather,geometry,visualization,drawing'
      });
  })

.config(function ($stateProvider) {
    $stateProvider
        .state('app.tripplayback', {
            url: '/tripplayback',
            views: {
                "content@app": {
                    controller: 'tripplaybackCtrl',
                    templateUrl: 'app/clientuser/trip-playback/views/tripplayback.html'
                }
            },
            resolve: {
                scripts: function (lazyScript) {
                    return lazyScript.register([
                         'build/vendor.ui.js',
                        'build/vendor.datatables.js',
                        'build/vendor.graphs.js'
                    ]);
                }
            },

           
            data: {
                title: 'Trip Playback'
            }
        });
});
