﻿"use strict";

angular.module('app.Iottripplayback').factory('IottripplaybackFactory', function ($http, $q, APP_CONFIG) {

    var GetTripPlayData = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetTripPlayData";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    
    var getVehicleList = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Dashboard/GetVehicleList";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getGetTrips = function (filters) {
        var apiUrl = APP_CONFIG.apiUrl + "/api/Trip/GetTrips";
        var deferred = $q.defer();
        $http.post(apiUrl, filters).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
       

    return {
        GetTripPlayData: GetTripPlayData,
        getVehicleList: getVehicleList,
        getGetTrips:getGetTrips
    };

});