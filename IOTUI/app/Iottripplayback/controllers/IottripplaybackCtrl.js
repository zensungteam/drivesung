'use strict';

angular.module('app.Iottripplayback').controller('IottripplaybackCtrl', function ($q, $scope, $filter, $interval, $rootScope, $window, CalendarEvent, SmartMapStyle, IottripplaybackFactory, uiGmapGoogleMapApi, DTOptionsBuilder, DTColumnBuilder, APP_CONFIG) {

    function loadInitData() {

        $scope.address = ""
        $scope.GridData = false;
        $scope.chartData = [];
        $scope.playtrip = {
            newplay: true,
            play: false,
            pause: false,
            stop: false
        };
        var dt = new Date();

        $scope.$on('$destroy', function () {
            StopAutoRefresh();
            $window.sessionStorage.removeItem("dashaction");
        });

        $scope.HB = 0;
        $scope.HA = 0;
        $scope.HC = 0;
        $scope.OS = 0;
        $scope.CT = 0;
        $scope.CO2 = 0;
        $scope.info = "";

        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel = {
            rdate: '',
            playSpeed: 100,
            chartModel: {
                Speed: {
                    SpeedPercent: 0,
                    Speed: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#014476',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                Rpm: {
                    RpmPercent: 0,
                    Rpm: 0,
                    maxvalue: 6000,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#383c3f',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                CoolantTemp: {
                    CoolantTempPercent: 0,
                    CoolantTemp: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#2C3E50',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                },
                Fuellevel: {
                    FuellevelPercent: 0,
                    Fuellevel: 0,
                    maxvalue: 255,
                    Options: {
                        animate: {
                            duration: 0,
                            enabled: false
                        },
                        barColor: '#2C3E50',
                        scaleColor: false,
                        lineWidth: 0,
                        lineCap: 'circle'
                    }
                }
            },
            date: new Date(),
            Timer: null,
            windowOptions: {
                visible: false
            },
            windowOptionsinfo: {
                visible: false
            }
            ,
            fromdate: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), 0, 0), "dd-MM-yyyy HH:mm:ss"),
            todate: $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "dd-MM-yyyy HH:mm:ss")


        };
    };

    function loadDefaultMapAndChart() {
        $scope.playtripmodel.map = {
            //center: { latitude: 18.50891, longitude: 73.92603 }, zoom: 10, bounds: {},
            center: { latitude: 1.294861737596944, longitude: 103.84409782242489 }, zoom: 12, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: 0,
                longitude: 0
            }],
            stoppagemarkers: []
        };

        uiGmapGoogleMapApi.then(function (maps) {
            $scope.playtripmodel.polylines = [];
        });


    }

    function fillVehicleList(allVehicle) {
        $scope.playtripmodel.dtVehicleList = JSON.parse(allVehicle);
    }

    function fillTrips(allTrips) {
        $scope.playtripmodel.dtallTrips = JSON.parse(allTrips);
    }

    function getVehicleList() {
        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        IottripplaybackFactory.getVehicleList(filters)
        .then(function (result) {
            fillVehicleList(JSON.stringify(result.data.all));
        }, function (error) {
            console.log(error);
        });
    }

    function getGetTrips() {
        var TripEntity = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id
        };
        IottripplaybackFactory.getGetTrips(TripEntity)
        .then(function (result) {
            fillTrips(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function SetMapData() {
        var pl = []

        var tempmarkersralert = [];
        var options;
        var alertorstatus;

        if ($scope.polylines != null) {
            pl = $scope.polylines;
        }

        if ($scope.alertmarkersr != null) {
            tempmarkersralert = $scope.alertmarkersr;
        }


        var cureentlat;
        var currentlng;
        var prvlat;
        var prvlng;
        var index = $scope.count;
        var data = $scope.playtripmodel.alldata[index];

        if ($scope.count > 0) {
            cureentlat = data.latitude;
            currentlng = data.longitude;
            prvlat = $scope.playtripmodel.alldata[$scope.count - 1].latitude;
            prvlng = $scope.playtripmodel.alldata[$scope.count - 1].longitude;

            $scope.playtripmodel.rdate = data.recorddatetime;

            if (data.speed >= 80) {
                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#f91517',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }
            else if (data.speed > 60 && data.speed <= 79) {
                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#16872f',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }
            else {

                pl.push({
                    id: index,
                    path:
                        [{ "latitude": prvlat, "longitude": prvlng }, { "latitude": cureentlat, "longitude": currentlng }],
                    stroke: {
                        color: '#1451f9',
                        weight: 3
                    },
                    icons: [{
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        },
                        offset: '25px',
                        repeat: '50px'
                    }
                    ]
                })
            }


            if (data.hardbraking != null) {
                if (data.hardbraking) {
                    options = { icon: 'styles/img/markersicon/hardbreaking.png' };
                    alertorstatus = "Harsh Breaking";
                    var vehicledetails = {
                        speed: data.speed,
                        recorddatetime: data.recorddatetime,
                        alert: alertorstatus
                    }

                    tempmarkersralert.push({
                        id: index,
                        latitude: cureentlat,
                        longitude: currentlng,
                        options: options,
                        vehicledetails: vehicledetails
                    });
                }
            }

            if (data.overspeedstarted != null) {
                if (data.overspeedstarted) {
                    options = { icon: 'styles/img/markersicon/overspeed.png' };
                    alertorstatus = "Over Speed";
                    var vehicledetails = {
                        speed: data.speed,
                        recorddatetime: data.recorddatetime,
                        alert: alertorstatus
                    }

                    tempmarkersralert.push({
                        id: index,
                        latitude: cureentlat,
                        longitude: currentlng,
                        options: options,
                        vehicledetails: vehicledetails
                    });
                }
            }

            if (data.cornerpacket != null) {
                if (data.cornerpacket) {
                    options = { icon: 'styles/img/markersicon/cornerpacket.png' };
                    alertorstatus = "Cornering";
                    var vehicledetails = {
                        speed: data.speed,
                        recorddatetime: data.recorddatetime,
                        alert: alertorstatus
                    }

                    tempmarkersralert.push({
                        id: index,
                        latitude: cureentlat,
                        longitude: currentlng,
                        options: options,
                        vehicledetails: vehicledetails
                    });
                }
            }

            if (data.mcall != null) {
                if (data.mcall) {
                    options = { icon: 'styles/img/markersicon/mcall.png' };
                    alertorstatus = "Calls Taken";
                    var vehicledetails = {
                        speed: data.speed,
                        recorddatetime: data.recorddatetime,
                        alert: alertorstatus
                    }

                    tempmarkersralert.push({
                        id: index,
                        latitude: cureentlat,
                        longitude: currentlng,
                        options: options,
                        vehicledetails: vehicledetails
                    });
                }
            }

            if (data.hardacceleration != null) {
                if (data.hardacceleration) {
                    options = { icon: 'styles/img/markersicon/hardaccels.png' };
                    alertorstatus = "Harsh Acceleration";
                    var vehicledetails = {
                        speed: data.speed,
                        recorddatetime: data.recorddatetime,
                        alert: alertorstatus
                    }

                    tempmarkersralert.push({
                        id: index,
                        latitude: cureentlat,
                        longitude: currentlng,
                        options: options,
                        vehicledetails: vehicledetails
                    });
                }
            }
        }

        $scope.polylines = pl;
        $scope.alertmarkersr = tempmarkersralert;

        var zoom = $scope.playtripmodel.map.zoom;
        $scope.playtripmodel.map = {
            center: { latitude: cureentlat, longitude: currentlng }, bounds: {},
            control: {},
            markersr: [{
                id: 101,
                latitude: cureentlat,
                longitude: currentlng
            }],
            stoppagemarkers: $scope.playtripmodel.map.stoppagemarkers
        };

        //var geocoder = new google.maps.Geocoder;
        //var latlng = { lat: parseFloat(cureentlat), lng: parseFloat(currentlng) };
        //geocoder.geocode({ 'location': latlng }, function (results, status) {
        //    if (status === 'OK') {
        //        if (results[1]) {
        //            $scope.address = results[1].formatted_address
        //        } else {
        //            $scope.address = latlng + " : " + 'No results found';
        //        }
        //    } else {
        //        $scope.address = latlng + " : " + 'Geocoder failed due to: ' + status;
        //    }
        //});

        //uiGmapGoogleMapApi.then(function (maps) {
        //    $scope.polylines = pl;
        //});

        //   $scope.$watch('map.markersr[0].latitude', loadDefaultMap1);
        //        $scope.$applyAsync();

    }

    function SetStoppageData(StoppageData) {
        var cureentlat;
        var currentlng;
        var stopdetails;
        var Stmarkersr = [];

        angular.forEach(StoppageData, function (data, index) {
            data.location = '';
            cureentlat = data.latitude;
            currentlng = data.longitude;
            stopdetails = 'From: ' + data.minrecorddatetime + ',   To :' + data.maxrecorddatetime + ',   stoppage :' + data.diffinmin + '(in min),  Location:' + cureentlat + ',' + currentlng;
            Stmarkersr.push({
                id: index,
                latitude: cureentlat,
                longitude: currentlng,
                stopdetails: stopdetails
            });

        });

        $scope.playtripmodel.map.stoppagemarkers = Stmarkersr;

        var tableOptions = {
            "data": StoppageData,
            columns: [
                { data: "location" },
                { data: "minrecorddatetime" },
                { data: "maxrecorddatetime" },
                { data: "diffinmin" },
            ],
            "order": [[3, 'desc']],
            "mColumns": [0, 1, 2, 3],
            destroy: true
             ,
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                var locationdata = {
                    id: iDataIndex,
                    lat: aData.latitude,
                    lng: aData.longitude
                };
                $('td:eq(0)', nRow).html("<a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-map-marker fa-2x' aria-hidden='true' style='color:#075e07; margin-left:20px;' ></a><div id='container" + iDataIndex + "'></div>");
            }
        };
        $scope.stoppageTablebind(tableOptions, $scope);

    }

    function SetCount(hdata) {

        $scope.HB = hdata[0].hb;
        $scope.HA = hdata[0].ha;
        $scope.HC = hdata[0].cor;
        $scope.OS = hdata[0].os;
        $scope.CT = hdata[0].mcall;


    }

    function SetPlayTripData(livedata, alldata, stoppage, count, speeddata) {
        $scope.datalength = 0

        var alldata = JSON.parse(alldata);
        var livedata = JSON.parse(livedata);
        var count = JSON.parse(count);
        var stoppage = JSON.parse(stoppage);
        var speeddata = JSON.parse(speeddata);

        if (alldata.length > 0) {
            $scope.playtripmodel.livedata = livedata;
            $scope.playtripmodel.alldata = alldata;
            $scope.playtripmodel.count = count;
            $scope.playtripmodel.stoppage = stoppage;

            $scope.datalength = alldata.length;

            SetCount(count);
            SetStoppageData(stoppage);
            fillVehicleDataGrid(alldata);
            SetGraphData(alldata, stoppage, speeddata);
        }
        else {
            $scope.playtripmodel.livedata = null;
            $scope.playtripmodel.alldata = null;
            $scope.playtripmodel.count = null;
            $scope.playtripmodel.stoppage = null;

            loadDefaultMapAndChart();
            fillVehicleDataGrid(alldata);
        }

    }

    function generateChartData(alldata) {
        var chartData = [];
        var recorddatetime;
        var newDate;
        angular.forEach(alldata, function (data, index) {
            var cdatetime = data.recorddatetime;
            var cspeed = data.speed;
            var ccoolanttemperature = data.coolanttemperature;
            var crpm = data.rpm;

            chartData.push({
                date: cdatetime,
                speed: cspeed,
                coolanttemperature: ccoolanttemperature,
                rpm: crpm
            });

        });

        return chartData;
    }

    function SetGraphData(alldata, stoppage, speeddata) {
        if ($rootScope.userInfo.showobd) {
            var chartData = generateChartData(alldata);
            var chart = AmCharts.makeChart("graphdiv", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "synchronizeGrid": true,
                "valueAxes": [{
                    "id": "v1",
                    "axisColor": "#22958a",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "left",
                    "title": "Speed (km/hr)"
                }, {
                    "id": "v2",
                    "axisColor": "#d28924",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "right",
                    "title": "RPM (rpm)"
                }, {
                    "id": "v3",
                    "axisColor": "#bf4839",
                    "axisThickness": 2,
                    "gridAlpha": 0,
                    "offset": 50,
                    "axisAlpha": 1,
                    "position": "left",
                    "title": "Temperature(�C)"
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "lineColor": "#22958a",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Speed",
                    "valueField": "speed",
                    "fillAlphas": 0
                }, {
                    "valueAxis": "v2",
                    "lineColor": "#d28924",
                    "bullet": "square",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "RPM",
                    "valueField": "rpm",
                    "fillAlphas": 0
                }, {
                    "valueAxis": "v3",
                    "lineColor": "#bf4839",
                    "bullet": "triangleUp",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Coolant Temperature",
                    "valueField": "coolanttemperature",
                    "fillAlphas": 0
                }],
                "chartScrollbar": {},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"parseDates": true,
                    "axisColor": "#DADADA",
                    //"minPeriod": "hh:mm:ss",
                    "minorGridEnabled": true,
                    "title": "DateTime( dd-mm-yyyy hh:mm:ss)"

                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });

            var graphstoppagediv = AmCharts.makeChart("graphstoppagediv", {
                "type": "serial",
                "theme": "light",
                "dataProvider": stoppage,
                "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "<b>From : [[category]], To:[[maxrecorddatetime]], Time:[[value]] (in Min)</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "diffinmin"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "minrecorddatetime",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20
                },
                "export": {
                    "enabled": true
                }

            });

        }

        if (!$rootScope.userInfo.showobd) {
            var chartData = generateChartData(alldata);
            var chart = AmCharts.makeChart("graphdivM", {
                "type": "serial",
                "theme": "light",
                "legend": {
                    "useGraphSettings": true
                },
                "dataProvider": chartData,
                "synchronizeGrid": true,
                "valueAxes": [{
                    "id": "v1",
                    "axisColor": "#22958a",
                    "axisThickness": 2,
                    "axisAlpha": 1,
                    "position": "left",
                    "title": "Speed (km/hr)"
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "lineColor": "#22958a",
                    "bullet": "round",
                    "bulletBorderThickness": 1,
                    "hideBulletsCount": 30,
                    "title": "Speed",
                    "valueField": "speed",
                    "fillAlphas": 0
                }],
                "chartScrollbar": {},
                "chartCursor": {
                    "cursorPosition": "mouse"
                },
                "categoryField": "date",
                "categoryAxis": {
                    //"parseDates": true,
                    "axisColor": "#DADADA",
                    //"minPeriod": "hh:mm:ss",
                    "minorGridEnabled": true,
                    "title": "DateTime( dd-mm-yyyy hh:mm:ss)"

                },
                "export": {
                    "enabled": true,
                    "position": "bottom-right"
                }
            });
        }


        var data = generateSpeedData(speeddata);
        var chart = AmCharts.makeChart("chartdivspeed", {
            "type": "serial",
            "theme": "light",
            "marginRight": 70,
            "dataProvider": data,
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left",
                "title": "Percentage(%)"
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": " Speed Range-[[category]] (km/hr): <b>[[value]] %</b>",
                "fillColorsField": "color",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "type": "line",
                "valueField": "percentage",

            }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "speedrange",
            "categoryAxis": {
                "gridPosition": "start",
                "labelRotation": 0,
                "title": "Speed Range (km/hr)"
            },
            "export": {
                "enabled": true
            }

        });

    }

    function generateSpeedData(speeddata) {
        var chartData = [];
        angular.forEach(speeddata, function (data, index) {
            chartData.push({
                speedrange: data.speedrange,
                percentage: data.percentage,
                color: data.color
                //rpm: crpm
            });

        });

        return chartData;
    }

    function zoomChart(chart) {
        if (chart.dataProvider.length > 0) {
            chart.zoomToIndexes(chart.dataProvider.length - 20, chart.dataProvider.length - 1);
        }
    }

    function fillVehicleDataGrid(alldata) {
        if (!$rootScope.userInfo.showobd) {
            var tableOptions = {
                "data": alldata,
                columns: [
                    { data: "recorddatetime" },
                    { data: "speed" },
                    { data: "alert" }
                ],
                "order": [[0, 'desc']],
                "mColumns": [0, 1, 2],
                destroy: true
                          ,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    var locationdata = {
                        id: iDataIndex,
                        lat: aData.latitude,
                        lng: aData.longitude
                    };
                    $('td:eq(0)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                }
            };

            $scope.dashTablebindm(tableOptions, $scope);
        }
        else {
            var tableOptions = {
                "data": alldata,
                columns: [
                    { data: "recorddatetime" },
                    { data: "speed" },
                    { data: "rpm" },
                    { data: "coolanttemperature" },
                    { data: "alert" }
                ],
                "order": [[0, 'desc']],
                "mColumns": [0, 1, 2, 3, 4],
                destroy: true
               ,
                "fnCreatedRow": function (nRow, aData, iDataIndex) {
                    var locationdata = {
                        id: iDataIndex,
                        lat: aData.latitude,
                        lng: aData.longitude
                    };
                    $('td:eq(0)', nRow).html("<i class='fa fa-calendar' style='color:red'  title = 'Date & Time'></i> &nbsp" + aData.recorddatetime + "<br><a id='#viewlocation" + iDataIndex + "' ng-click='locationinfo(" + JSON.stringify(locationdata) + ")' class='fa fa-location-arrow'style='color:green' title = 'click here for location details' ></a><div id='container" + iDataIndex + "'></div>");
                }
            };

            $scope.dashTablebindo(tableOptions, $scope);
        }




    }

    $scope.getPlayTripData = function () {
        var VechileNo = $scope.playtripmodel.vehicleRefId;
        var DeviceNo = $scope.playtripmodel.vehicleRefId;
        var filters = {
            VechileNo: VechileNo,
            DeviceNo: DeviceNo,
            sfDate: $scope.playtripmodel.fromdate,
            stDate: $scope.playtripmodel.todate,
            NoOfRecords: 2000,
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };
        //call api using factory
        IottripplaybackFactory.GetTripPlayData(filters)
        .then(function (result) {
            //format result
            SetPlayTripData(JSON.stringify(result.data.data.live), JSON.stringify(result.data.data.history), JSON.stringify(result.data.data.stoppage), JSON.stringify(result.data.data.count), JSON.stringify(result.data.data.speedrpt));
            StartAutoRefresh();
        }, function (error) {
            console.log(error);
        });
    }

    function StopAutoRefresh() {
        //$scope.Message = "Timer stopped.";
        if (angular.isDefined($rootScope.TimerPlayTrip)) {
            $interval.cancel($rootScope.TimerPlayTrip);
            $scope.playtripmodel.playSpeed = 0;
        }
    };

    function StartAutoRefresh() {
        $rootScope.TimerPlayTrip = $interval(function () {
            if ($scope.datalength > 0) {
                $scope.count++;
                if ($scope.count < $scope.datalength) {
                    SetMapData();
                }
                else {
                    StopAutoRefresh();
                    $scope.playtrip = {
                        newplay: true,
                        play: false,
                        pause: false,
                        stop: false,
                    };
                }
            }
            else {
                StopAutoRefresh();
            }

        }, $scope.playtripmodel.playSpeed);
    };

    $scope.newplayOn = function () {

        $scope.count = 1;
        $scope.polylines = null;
        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel.playSpeed = 100;
        $scope.getPlayTripData();
        $scope.playtrip = {
            newplay: false,
            play: false,
            pause: true,
            stop: true,
        };

    }

    $scope.playOn = function () {
        StartAutoRefresh();
        $scope.playtrip = {
            newplay: false,
            play: false,
            pause: true,
            stop: true,
        };
    }

    $scope.pauseOn = function () {
        StopAutoRefresh();
        $scope.playtrip = {
            newplay: false,
            play: true,
            pause: false,
            stop: true,
        };
    }

    $scope.stopOn = function () {
        StopAutoRefresh();
        $scope.playtrip = {
            newplay: true,
            play: false,
            pause: false,
            stop: false,
        };
        $scope.playtripmodel.alldata = undefined;
        $scope.datalength = 0;
        $scope.count = 1;
        $scope.polylines = null;
        $scope.alertmarkersr = null;
        $scope.grirdPlayTripdt = [];
        $scope.playtripmodel.playSpeed = 100;
        loadDefaultMapAndChart();
    }

    $scope.onClick = function () {
        $scope.playtripmodel.windowOptionsinfo.visible = !$scope.playtripmodel.windowOptionsinfo.visible;
    };

    $scope.closeClick = function () {
        $scope.playtripmodel.windowOptions.visible = false;
    };

    $scope.onstopClick = function () {
        $scope.playtripmodel.windowOptions.visible = !$scope.windowOptions.visible;

    };

    $scope.closestopClick = function () {
        $scope.playtripmodel.windowOptions.visible = false;
    };

    $scope.locationinfo = function (locationdata) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(locationdata.lat, locationdata.lng);
        var location;

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    location = results[1].formatted_address;;
                }
                else {
                    location = 'lat-' + locationdata.lat + ', lng-' + locationdata.lng;
                }
                document.getElementById("container" + locationdata.id + "").innerHTML = '</small>' + location + '</small>';
            }
        })

    }

    function init() {
        loadInitData();
        loadDefaultMapAndChart();
        SetGraphData([]);
        //InitGauagectrl(0, 0, 0, 0);
        getVehicleList();
        // getGetTrips();
        if ($window.sessionStorage["dashaction"]) {

            var params = JSON.parse($window.sessionStorage["dashaction"]);
            if (params.action == "history") $scope.playtripmodel.vehicleRefId = params.deviceno;
            if (params.action == "trip") {
                $scope.playtripmodel.fromdate = params.trip_from;
                $scope.playtripmodel.todate = params.trip_to;
                if (params.trip_to == null) {
                    var dt = new Date();
                    $scope.playtripmodel.todate = $filter('date')(new Date(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes()), "dd-MM-yyyy HH:mm:ss")
                }
            }
            $scope.playtripmodel.vehicleRefId = params.deviceno;
            //$window.sessionStorage.removeItem("dashaction");
        }
        if ($window.sessionStorage["userInfo"]) {
            //used for navigation 
            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        if ($window.sessionStorage["SubHeaderDataCnt"]) {
            //used for sub heading 
            $rootScope.SubHeaderDataCnt = JSON.parse($window.sessionStorage["SubHeaderDataCnt"]);
        }

    }

    init();

});