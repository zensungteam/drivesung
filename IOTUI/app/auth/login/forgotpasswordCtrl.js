﻿"use strict";

angular.module('app.auth').controller('forgotpasswordCtrl', function ($scope, $state, $http, $window, $location, loginFactory) {
   

    $scope.forgotpass = {
        user_name: "",
        email_id1: "",
        s_user_id :1,
        s_role_Id: 1

    };

    $scope.forgotpassword = function () {
       
       // debugger;
        loginFactory.forgot_password($scope.forgotpass).then(function (result) {

            $.smallBox({
                title: "Send mail succesfully.",
                content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                color: "#5F895F",
                iconSmall: "fa fa-check bounce animated",
                timeout: 4000
            });
            $state.go('login');

        }, function (error) {
            console.log(error);
        });



    }
    var init = function () {

        
      
    }

    init();
})
