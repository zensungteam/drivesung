"use strict";

angular.module('app.auth').controller('LoginCtrl', function ($scope, $state, $http, $window, $location, $rootScope, loginFactory, dashboardFactory, AuthenticationService, authData) {
    //$scope.$on('event:google-plus-signin-success', function (event, authResult) {
    //    if (authResult.status.method == 'PROMPT') {
    //        GooglePlus.getUser().then(function (user) {
    //            User.username = user.name;
    //            User.picture = user.picture;
    //            $state.go('app.Iotdashboardlist');
    //        });
    //    }
    //});

    //$scope.$on('event:facebook-signin-success', function (event, authResult) {
    //    ezfb.api('/me', function (res) {
    //        User.username = res.name;
    //        User.picture = 'https://graph.facebook.com/' + res.id + '/picture';
    //        $state.go('app.Iotdashboardlist');
    //    });
    //});

    $rootScope.$on('event:social-sign-in-success', function (event, userDetails) {
        $scope.socialdata = true;
        $scope.userinfo = {
            user_name: userDetails.uid,
            user_pwd: userDetails.uid,
            //first_name: userDetails.name,
            social_id: userDetails.uid,
            //profile_picture_uri: userDetails.imageUrl,
            user_login_type: userDetails.provider == "google" ? "g" : "f",
        };
        $scope.loginData.userName = userDetails.uid;
        $scope.loginData.password = userDetails.uid;
        if ($scope.flag == true) {
            $scope.login();
            $scope.flag = false;
        }


    })

    $scope.login = function () {

        loginFactory.login($scope.loginData.userName, $scope.loginData.password, AuthenticationService, authData).then(function (response) {

            if (response != null && response.error != undefined) {
                $scope.message = response.error_description;
            }
            else {
                if ($scope.socialdata == false) {
                    $scope.userinfo = {
                        user_name: $scope.loginData.userName,
                        user_pwd: $scope.loginData.password
                    }
                }
                loginFactory.validatelogin($scope.userinfo).then(function (response) {
                    var data = response.data;
                    if (data.isComplete) {
                        data.data.showobd = false;
                        if ((data.data.role_Id == 1 && data.data.obdavaliable) || data.data.role_Id == 2 || data.data.role_Id == 3) {
                            data.data.showobd = true;
                        }
                        $window.sessionStorage["userInfo"] = JSON.stringify(data.data);
                        if ($window.sessionStorage["userInfo"]) {
                            //used for navigation 
                            $rootScope.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
                            //$rootScope.userInfo.lastLoginDate = $filter('date')($rootScope.userInfo.lastLoginDate, APP_CONFIG.dateFormat + ' hh:mm:ss a');
                        }
                        //if (data.data.customParam) {
                        //    $.smallBox({
                        //        title: "",
                        //        content: "<i class='fa fa-clock-o'></i> <i>" + data.data.customParam + "</i>",
                        //        color: "#5F895F",
                        //        iconSmall: "fa fa-check bounce animated",
                        //        timeout: 4000
                        //    });
                        //}
                        if (data.description == 'success') {
                            checkPasswordExpired(data.data.role_Id);
                        }
                        else if (data.description == 'FirstLogin') {
                            checkPasswordExpired(data.data.role_Id);
                        }
                        else if (data.description == 'LockedSuccess') {
                            checkPasswordExpired(data.data.role_Id);
                        }
                    }
                    else {
                        $.smallBox({
                            title: "Login failed.",
                            content: "<i class='fa fa-clock-o'></i> <i>" + data.description + "</i>",
                            color: "#FF0000",
                            iconSmall: "fa fa-check bounce animated",
                            timeout: 4000
                        });
                        $scope.message = data.description;
                    }
                }, function (error) {
                    $scope.message = error;
                });
            }
        }, function (error) {
            $scope.message = error;
        });
    }

    function checkPasswordExpired(rid) {
        //var strUserInfo = $window.sessionStorage["userInfo"];
        //if (strUserInfo) {
        //    var userInfo = JSON.parse(strUserInfo);
        //    if (userInfo.isPasswordExpired) {

        //        $state.go('lockChangePass');
        //        return;
        //    }
        //    else if (userInfo.passwordExpirePeriod > 0) {
        //        $.bigBox({
        //            title: "Password Expire Notification!",
        //            content: "Your password will Expire in " + userInfo.passwordExpirePeriod + " days.",
        //            color: "#C79121",
        //            timeout: 5000,
        //            icon: "fa fa-bell swing animated",
        //            number: ""
        //        });

        //    }
        //}

        var filters = {
            s_user_id: $rootScope.userInfo.user_id,
            s_role_Id: $rootScope.userInfo.role_Id

        };

        dashboardFactory.getSubHeaderCount(filters).then(function (response) {
            var data = response.data;
            if (data.isComplete) {
                $window.sessionStorage["SubHeaderDataCnt"] = JSON.stringify(data.data.count);;
                $rootScope.SubHeaderDataCnt = data.data.count;

                if (rid == 1) $state.go('app.IotBlotterViewUser');
                if (rid == 2) $state.go('app.IotBlotterViewAdmin');
                if (rid == 3) $state.go('app.Iotdashboardlist');
            }
            else {
                $.smallBox({
                    title: "Login failed.",
                    content: "<i class='fa fa-clock-o'></i> <i>" + data.description + "</i>",
                    color: "#FF0000",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 4000
                });
                $scope.message = data.description;
            }
        }, function (error) {
            $scope.message = error;
        });


    }

    //function loadInitData() {
    //    $scope.ssoinFlag = true;
    //    loginFactory.login('logout', '', AuthenticationService, authData).then(function (response) {

    //        var param = {
    //            'ActionState': 'Logout'
    //        };

    //        loginFactory.ssoOut(param).then(function (response) {

    //            var data = response.data;
    //            if (data.isComplete) {
    //                //$location.absUrl() = data.description;
    //                $window.location.href = data.description;
    //            }
    //            else {
    //                $.smallBox({
    //                    title: "Redirection failed.",
    //                    content: "<i class='fa fa-clock-o'></i> <i>" + data.description + "</i>",
    //                    color: "#FF0000",
    //                    iconSmall: "fa fa-check bounce animated",
    //                    timeout: 4000
    //                });
    //                $scope.message = data.description;
    //            }

    //        }, function (error) {
    //            $scope.message = error;
    //        });

    //    }, function (error) {
    //        $scope.message = error;
    //    });
    //}

    var init = function () {
        $scope.flag = true;
        $scope.socialdata = false;
        $scope.loginPanelVisible = false;
        $scope.ssoinFlag = false;
        AuthenticationService.removeToken();
        AuthenticationService.setHeader($http);

        //if ($location.host() != "localhost") {
        //    loadInitData();
        //}
        //else {
        $scope.loginPanelVisible = true;
        $scope.loginData = {
            user_name: "",
            user_pwd: ""
        };
        //}
    }

    init();
})
