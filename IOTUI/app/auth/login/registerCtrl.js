﻿"use strict";

angular.module('app.auth').controller('registerCtrl', function ($scope, $state, $http, $window, $rootScope, $location, loginFactory, AuthenticationService, authData) {


    $rootScope.$on('event:social-regi-success', function (event, userDetails) {
        var fullname = [];
        console.log(userDetails)
        $scope.registerData.email_id1 = userDetails.email,
        fullname = userDetails.name.split(" ");
        $scope.registerData.first_name = fullname[0];
        $scope.registerData.last_name = fullname[1];
        $scope.registerData.user_name = userDetails.uid,
         $scope.registerData.user_pwd = userDetails.uid,
        $scope.socialdata = true;

        $scope.registerData = {
            user_name: userDetails.uid,
            user_pwd: userDetails.uid,
            email_id1: $scope.registerData.email_id1,
            first_name: $scope.registerData.first_name,
            last_name: $scope.registerData.last_name,
            social_id: userDetails.uid,
            profile_picture_uri: userDetails.imageUrl,
            mobileno: $scope.registerData.mobileno,
            user_login_type: userDetails.provider == "google" ? "g" : "f",
            role_Id: 1,
            vehicle_type: "car",
            o_id:1

        };


    });

    $scope.registerData = {
        user_name: "",
        email_id1: "",
        user_pwd: "",
        passwordConfirm: "",
        first_name: "",
        middle_name: "",
        last_name: "",
        mobileno: "",
        date_of_birth: "",
        address1: "",
        gender: "",
        user_login_type: "regi",
        role_Id: 1,
        vehicle_type: "",
        engine_capacity: "",
        vehiclengine_type:"",
        o_id:1,

    };



    $scope.register = function () {

        // debugger;
        var fd = new FormData();
        if ($scope.socialdata == false) {
            $scope.registerData.user_name = $scope.registerData.email_id1;
            if ($scope.profile_picture_uri != null)
            {
                fd.append('file', $scope.profile_picture_uri[0]);
            }
            else
            {
                fd.append('file', $scope.profile_picture_uri);

            }
           
            fd.append('data', JSON.stringify($scope.registerData));

            loginFactory.Saveregistration(fd).then(function (result) {

                $.smallBox({
                    title: "User register succesfully.",
                    content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                    color: "#5F895F",
                    iconSmall: "fa fa-check bounce animated",
                    timeout: 4000
                });
                $state.go('login');

            }, function (error) {
                console.log(error);
            });
        }
        else {


            $http.get($scope.registerData.profile_picture_uri, { responseType: "blob" }).then(function (result) {
                console.log(result.data);
                var file1 = new File([result.data], "sample.jpg");
                fd.append('file', file1);
                fd.append('data', JSON.stringify($scope.registerData));

                loginFactory.Saveregistration(fd).then(function (result) {

                    $.smallBox({
                        title: "User register succesfully.",
                        content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                        color: "#5F895F",
                        iconSmall: "fa fa-check bounce animated",
                        timeout: 4000
                    });
                    $state.go('login');

                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });


        }


    }
    $scope.conformpassword = function () {
        if ($scope.registerData.user_pwd != $scope.registerData.passwordConfirm) {
            $scope.compare = true;
        }
        else {
            $scope.compare = false;
        }
    }

    function getEngineCapacityList() {
        var filters = {
            s_user_id: 1,
            s_role_Id: 1

        };
        loginFactory.getEngineCapacityList(filters)
        .then(function (result) {
            $scope.EngineCapacityList =JSON.parse( JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function getEngineTypeList() {
        var filters = {
            s_user_id: 1,
            s_role_Id: 1

        };
        loginFactory.getEngineTypeList(filters)
        .then(function (result) {
            $scope.EngineTypeList = JSON.parse(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    function getVehicleTypeList() {
        var filters = {
            s_user_id: 1,
            s_role_Id: 1

        };
        loginFactory.getVehicleTypeList(filters)
        .then(function (result) {
            $scope.VehicleTypeList = JSON.parse(JSON.stringify(result.data.data));
        }, function (error) {
            console.log(error);
        });
    }

    var init = function () {
        $scope.compare = false;
        $scope.socialdata = false;
        $('#txtBirthdate').combodate({
            minYear: 1975,
            maxYear: 2017,
            minuteStep: 10
        });
        $scope.registerData.vehicle_type = "Please Select Vehicle Type";
        getEngineCapacityList();
        getEngineTypeList();
        getVehicleTypeList();
       
    }

   init();
})
