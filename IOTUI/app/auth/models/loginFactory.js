﻿'use strict';

angular.module('app.auth').factory('loginFactory', function ($http, $q, APP_CONFIG) {
    var userInfo;
    var loginServiceURL = APP_CONFIG.apiUrl + '/token';
    var deviceInfo = [];

    var login = function (userName, password, authenticationService, authData) {
       // debugger;
        var deferred = $q.defer();
        var data = "grant_type=password&username=" + userName + "&password=" + password;
        $http.post(loginServiceURL, data, {
            headers:
               { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function (response) {
            userInfo = {
                accessToken: response.data.access_token,
                userName: response.data.userName
            };

            authenticationService.setTokenInfo(userInfo);
            authData.authenticationData.IsAuthenticated = true;
            authData.authenticationData.userName = response.data.userName;
            deferred.resolve(authData);
        }, function (error) {
            authData.authenticationData.IsAuthenticated = false;
            authData.authenticationData.userName = "";
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var logOut = function (authenticationService, authData) {
        authenticationService.removeToken();
        authData.authenticationData.IsAuthenticated = false;
        authData.authenticationData.userName = "";
    }

    var validatelogin = function (userinfo) {
       // debugger;
        var tokenurl = APP_CONFIG.apiUrl + "/api/Login/Auth";
        var deferred = $q.defer();
        $http.post(tokenurl, userinfo).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var Saveregistration = function (fd) {

       
        var apiUrl
        //if (fd.action == "New") {
        apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/CreateUserProfile";
        //}
        //else if (fd.action == "Edit") {
        //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
        //}


        var deferred = $q.defer();

        $http.post(apiUrl, fd, {transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getEngineCapacityList = function (filter) {
        // debugger;
        var tokenurl = APP_CONFIG.apiUrl + "/api/Common/GetEngineCapacityList";
        var deferred = $q.defer();
        $http.post(tokenurl, filter).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getEngineTypeList = function (filter) {
        // debugger;
        var tokenurl = APP_CONFIG.apiUrl + "/api/Common/GetEngineTypeList";
        var deferred = $q.defer();
        $http.post(tokenurl, filter).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    var getVehicleTypeList = function (filter) {
        // debugger;
        var tokenurl = APP_CONFIG.apiUrl + "/api/Common/GetVehicleTypeList";
        var deferred = $q.defer();
        $http.post(tokenurl, filter).then(function (result) {
            deferred.resolve(result);
        }, function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    

    




    //var Updateregistration = function (fd) {


    //    var apiUrl
    //    //if (fd.action == "New") {
    //    apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/UpdateUserProfile";
    //    //}
    //    //else if (fd.action == "Edit") {
    //    //    apiUrl = APP_CONFIG.apiUrl + "/api/ConfigSetting/UpdateConfigSetting";
    //    //}


    //    var deferred = $q.defer();

    //    $http.post(apiUrl, fd, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } }).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}
    
    var forgot_password = function (param) {
       // debugger;
        var apiUrl = APP_CONFIG.apiUrl + "/api/UserProfile/ForgotPassword";
        var deferred = $q.defer();

        $http.post(apiUrl, param).then(function (result) {
            deferred.resolve(result);
        },
        function (error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

  

    //var ssoIn = function (userinfo) {

    //    var tokenurl = APP_CONFIG.apiUrl + "/api/Login/SSOIN";
    //    var deferred = $q.defer();
    //    $http.post(tokenurl, userinfo).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}

    //var ssoOut = function (param) {

    //    var tokenurl = APP_CONFIG.apiUrl + "/api/Login/SSOOUT";
    //    var deferred = $q.defer();
    //    $http.post(tokenurl, param).then(function (result) {
    //        deferred.resolve(result);
    //    }, function (error) {
    //        deferred.reject(error);
    //    });
    //    return deferred.promise;
    //}

    return {
        login: login,
        logOut: logOut,
        validatelogin: validatelogin,
        Saveregistration: Saveregistration,
        forgot_password: forgot_password,
        getEngineCapacityList: getEngineCapacityList,
        getEngineTypeList: getEngineTypeList,
        getVehicleTypeList: getVehicleTypeList
       //Updateregistration: Updateregistration
        //ssoOut: ssoOut
    }
});
