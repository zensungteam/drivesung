"use strict";


angular.module('app.auth', [
    'ui.router'
        ,
        'socialLogin'
        //'ezfb',
        //'googleplus'
]).config(function ($stateProvider
   // //, ezfbProvider
    //, GooglePlusProvider
    , socialProvider

    ) {
    //GooglePlusProvider.init({
    //    clientId: authKeys.googleClientId
    //});

    //ezfbProvider.setInitParams({
    //    appId: authKeys.facebookAppId
    //});
   
    socialProvider.setGoogleKey(window.appConfig.goglekey);
    // socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
    socialProvider.setFbKey({ appId: window.appConfig.facebookAppId, apiVersion: window.appConfig.facebookapiVersion });

    $stateProvider
           //.state('app.auth', {
           //    abstract: true,
           //    data: {
           //        title: ''
           //    }
           //})

       .state('login', {
           url: '/login',
           views: {
               root: {
                   templateUrl: "app/auth/views/login.html",
                   controller: 'LoginCtrl'
               }
           },
           data: {
               title: 'Login',
               rootId: 'extra-page'
           }
       })

    .state('realLogin', {
        url: '/real-login',
        views: {
            root: {
                templateUrl: 'app/auth/views/login.html'
            }
        },
        data: {
            title: 'Login',
            htmlId: 'extr-page'
        },
        resolve: {
            srcipts: function (lazyScript) {
                return lazyScript.register([
                    'build/vendor.ui.js',
                   // 'build/vendor.js'
                ])
            }
        }
    })

    .state('register', {
        url: '/register',
        views: {
            root: {
                templateUrl: 'app/auth/views/register.html',
                controller: 'registerCtrl'

            }
        },
        data: {
            title: 'Register',
            htmlId: 'extr-page'
        }
    })


    .state('forgotPassword', {
        url: '/forgot-password',
        views: {
            root: {
                templateUrl: 'app/auth/views/forgot-password.html',
                controller: 'forgotpasswordCtrl'
            }
        },
        data: {
            title: 'Forgot Password',
            htmlId: 'extr-page'
        }
    })

    .state('lock', {
        url: '/lock',
        views: {
            root: {
                templateUrl: 'app/auth/views/lock.html'
            }
        },
        data: {
            title: 'Locked Screen',
            htmlId: 'lock-page'
        }
    })

    .state('lockChangePass', {
        url: '/lockChangePassword',
        views: {
            root: {
                templateUrl: 'app/auth/views/lock-changePass.html',
                controller: 'lockChangePassCtrl'
            }
        },
        data: {
            title: 'Change Password',
            htmlId: 'lock-page'
        }
    })

        .state('errorPage', {
            url: '/error',
            views: {
                root: {
                    templateUrl: 'app/auth/views/error.html'
                }
            },
            data: {
                title: 'Error',
                htmlId: 'lock-page'
            }
        })

    .state('ssoin', {
        url: '/ssoin',
        views: {
            root: {
                templateUrl: 'app/auth/views/ssoin.html',
                controller: 'ssoinCtrl'
            }
        },
        data: {
            title: 'Single Sign On',
            htmlId: 'extra-page'
        }
    })


}).constant('authKeys', {
    googleClientId: window.appConfig.goglekey,
    facebookAppId: window.appConfig.facebookAppId
});
