﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Reflection;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace IOTUtilities
{

    public enum MsgCode
    {
        //organization
        [Description("empty organization entity not allowed.")]
        orgEntityEmpt = 100,
        [Description("o_id required.")]
        o_id = 101,
        [Description("o_code required.")]
        o_code = 102,
        [Description("o_name required.")]
        o_name = 103,
        [Description("o_type required.")]
        o_type = 104,
        [Description("address1 required.")]
        address1 = 105,
        [Description("address2 required.")]
        address2 = 106,
        [Description("address3 required.")]
        address3 = 107,
        [Description("pincode_no required.")]
        pincode_no = 108,
        [Description("email_id1 required.")]
        email_id1 = 109,
        [Description("email_id2 required.")]
        email_id2 = 110,
        [Description("contact_no1 required.")]
        contact_no1 = 111,
        [Description("contact_no2 required.")]
        contact_no2 = 112,

        //session
        [Description("s_user_id required.")]
        s_user_id = 113,
        [Description("s_role_Id required.")]
        s_role_Id = 114,

        //userEntity     
        [Description("empty entity not allowed.")]
        userEntity = 115,
        [Description("user_id required.")]
        user_id = 116,
        [Description("user_name required.")]
        user_name = 117,
        [Description("username already available.")]
        CheckUserAvailability = 118,
        [Description("role_Id required.")]
        role_Id = 119,
        [Description("first_name required.")]
        first_name = 120,
        [Description("middle_name required.")]
        middle_name = 121,
        [Description("last_name required.")]
        last_name = 122,
        [Description("date_of_birth required.")]
        date_of_birth = 123,
        [Description("gender required.")]
        gender = 124,
        [Description("license_no required.")]
        license_no = 125,
        [Description("license_expiry_date required.")]
        license_expiry_date = 126,
        [Description("profile_picture_uri required.")]
        profile_picture_uri = 127,
        [Description("user_pwd required.")]
        user_pwd = 128,

        //filterEntity     

        [Description("empty filter entity not allowed.")]
        filterEntity = 129,
        [Description("VechileNo required in filter entity.")]
        VechileNo = 130,
        [Description("DeviceNo required in filter entity.")]
        DeviceNo = 131,
        [Description("FromDate required in filter entity.")]
        FromDate = 132,
        [Description("ToDate required in filter entity.")]
        ToDate = 133,
        [Description("NoOfRecords required in filter entity.")]
        NoOfRecords = 134,
        [Description("OverSpeed required in filter entity.")]
        OverSpeed = 135,
        [Description("IgnitionOn required in filter entity.")]
        IgnitionOn = 136,
        [Description("Moving  required in filter entity.")]
        Moving = 137,
        [Description("Parked required in filter entity.")]
        Parked = 138,
        [Description("UnReachable required in filter entity.")]
        UnReachable = 139,
        [Description("HardAccels required in filter entity.")]
        HardAccels = 140,
        [Description("HardBreaking required in filter entity.")]
        HardBreaking = 141,
        [Description("TamperAlert required in filter entity.")]
        TamperAlert = 142,
        [Description("user_id required in filter entity.")]
        user_id_f = 143,
        [Description("o_id required in filter entity.")]
        o_id_f = 145,
        [Description("s_user_id required in filter entity.")]
        s_user_id_f = 146,
        [Description("s_role_Id required in filter entity.")]
        s_role_Id_f = 147,
        [Description("invalid password.")]
        invalid_password = 148,
        [Description("invalid username.")]
        invalid_username = 149,

        //ConfigSettingEntity
        [Description("empty config setting entity not allowed.")]
        cs_configSettingEntityEmpt = 150,
        [Description("o_id required in filter entity.")]
        cs_o_id = 151,
        [Description("o_id already avaliable.")]
        checkConfigSettingAvailability = 152,

        //userEntity    
        [Description("mobileno required.")]
        mobileno = 153,
        [Description("deviceno required.")]
        deviceno = 154,
        [Description("vehicle_no required.")]
        vehicle_no = 155,
        [Description("registration_no required.")]
        registration_no = 156,
        [Description("vehicle_manufacturer required.")]
        vehicle_manufacturer = 157,
        [Description("vehicle_color required.")]
        vehicle_color = 158,
        [Description("vehicle_type required.")]
        vehicle_type = 159,
        [Description("user_login_type required.")]
        user_login_type = 160,
        [Description("insurance_mapping_key required.")]
        insurance_mapping_key = 161,


        [Description("trip_id required.")]
        trip_id = 162,
        [Description("trip_name required.")]
        trip_name = 163,
        [Description("trip_start_time required.")]
        trip_start_time = 165,
        [Description("trip_end_time required.")]
        trip_end_time = 166,
        [Description("created_at required.")]
        created_at = 167,
        [Description("updated_at required.")]
        updated_at = 168,
        [Description("vendor_name required.")]
        vendor_name = 169,
        [Description("framework  required.")]
        framework = 170,
        [Description("versionno required.")]
        versionno = 171,
        [Description("sim_no required.")]
        sim_no = 172,
        [Description("imei_no required.")]
        imei_no = 173,
        [Description("sim_installation required.")]
        sim_installation = 174,
        [Description("sim_activation_date required.")]
        sim_activation_date = 175,
        [Description("sim_expiry_date required.")]
        sim_expiry_date = 176,
        [Description("sms_active required.")]
        sms_active = 177,
        [Description("device_live_date required.")]
        device_live_date = 178,


        [Description("cmpt_id required.")]
        cmpt_id = 179,
        [Description("cmpt_name required.")]
        cmpt_name = 180,
        [Description("cmpt_profile_picture_path required.")]
        cmpt_profile_picture_path = 181,
        [Description("cmpt_type required.")]
        cmpt_type = 182,
        [Description("start_date required.")]
        start_date = 183,
        [Description("end_date required.")]
        end_date = 184,
        [Description("terms_conditions required.")]
        terms_conditions = 185,
        [Description("min_km_travel required.")]
        min_km_travel = 186,
        [Description("cmpt_for_driving required.")]
        cmpt_for_driving = 187,
        [Description("prize2_desc required.")]
        prize2_desc = 188,
        [Description("ec_agefrom required.")]
        ec_agefrom = 189,
        [Description("ec_ageto required.")]
        ec_ageto = 190,
        [Description(" prize3_desc required.")]
        prize3_desc = 191,
        [Description("authentication_required required.")]
        authentication_required = 192,
        [Description("car_type required.")]
        car_type = 193,
        [Description("cmpt_country required.")]
        cmpt_country = 194,
        [Description("cmpt_uniquecode required.")]
        cmpt_uniquecode = 195,
        [Description("prize1_desc required.")]
        prize1_desc = 196,
        [Description("jointimestam required.")]
        jointimestam = 197,
        [Description("trip_from required.")]
        trip_from = 198,
        [Description("trip_to required.")]
        trip_to = 199,
        [Description("userid required.")]
        userid = 201,


        [Description("trip already on.")]
        CheckTripStart = 202,
        [Description("trip already end.")]
        CheckTripEnd = 203,
        [Description("Select atleast one.")]
        atlest_one = 204,
        [Description("Email is already available.")]
        CheckEmailAvailability = 205,
        [Description("Email is Not Match.")]
        CheckEmailMatch = 206,
        [Description("Mobile is already available.")]
        CheckMobileAvailability = 207,
        [Description("Mobile No is Not Match.")]
        CheckMobileMatch = 208,
        [Description("empty trip entity not allowed.")]
        trip_entity = 209,
        [Description("Email id and mobile no is  already register.")]
        CheckEmailMobileAvailablity = 210,

        [Description("gc_id required.")]
        gc_id = 211,
        [Description("geofence_name required.")]
        geofence_name = 212,
        [Description("geofenec_desc required.")]
        geofenec_desc = 213,
        [Description("lat_lng required.")]
        lat_lng = 214,
        [Description("icon_file_name required.")]
        icon_file_name = 215,
        [Description("created_by required.")]
        created_by = 216,
        [Description("updated_by required.")]
        updated_by = 217,
        [Description("deleted required.")]
        deleted = 218,
        [Description("Geofenece is already available.")]
        CheckGeofenceAvailability = 219,
        [Description("Geofenece is already available.")]
        CheckGeofenceAvailabilityforupdate = 220,
        [Description("empty entity not allowed.")]
        geofenceEntity = 221,
        [Description("empty entity not allowed.")]
        notiSettingEntityEmpt = 222,

        [Description("claim_id required.")]
        claim_id = 223,
        [Description("policy_no required.")]
        policy_no = 224,
        [Description("period_from required.")]
        period_from = 225,
        [Description("period_to required.")]
        period_to = 226,
        [Description("date_accident_or_loss required.")]
        date_accident_or_loss = 227,
        [Description("name_of_driver required.")]
        name_of_driver = 228,
        [Description("driver_birth_date required.")]
        driver_birth_date = 229,
        [Description("driving_licence_no required.")]
        driving_licence_no = 230,
        [Description("accident_or_loss_details required.")]
        accident_or_loss_details = 231,
        [Description("status required.")]
        status = 232,
        [Description("remark required.")]
         remark = 232,
        [Description("empty entity not allowed.")]
         policyClaimEntity=233,
        [Description("Please Check you have already joint ")]
        CheckJointAvailablity=234,
        [Description("Please Check competition code doesnot match ")]
        CheckclosecmptAvailablity = 235,
        [Description("Please Engine Capacity Id field is required ")]
        eng_capacity_id = 236,
        [Description("Please Engine Capacity Range field is required  ")]
        eng_capacity_range = 237,
        [Description("Please Engine Type Id field is required  ")]
        eng_type_id = 238,
        [Description("Please Engine Type  field is required  ")]
        eng_type = 239,
        [Description("Please Vehicle Id  field is required  ")]
        vehicle_type_id = 240,
        [Description("Please Assistance Id  field is required  ")]
        assistance_id = 241,
        [Description("Please Country Name  field is required  ")]
        country_name = 242,
        [Description("Please Policy No  field is required  ")]
        police_no = 243,
        [Description("Please Ambulance No  field is required  ")]
        ambulance_no = 244,
        [Description("Please Transaction Id  field is required  ")]
        transaction_id = 245,
        [Description("Please  Premium  field is required  ")]
        premium = 246,
        [Description("Please  Premium Validity  field is required  ")]
        premium_validity = 247,
        

        
        
        
        //common
        [Description("bad request")]
        bad_request = 400,
        [Description("ok")]
        ok = 200


    }

    public class MessageCode
    {

        public static string ErrorMsgInJson(object enumValue)
        {
            string defDesc = "";
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    defDesc = ((DescriptionAttribute)attrs[0]).Description;
            }

            // ErrorMsgInJson
            return getJsonMsg((int)enumValue, defDesc);
        }

        public static string getJsonMsg(int msgcode, string msg)
        {

            return @"{'code':'" + msgcode + "','desc':'" + msg + "'}";
            // return new JavaScriptSerializer().Serialize(new
            //{
            //    code = msgcode,
            //    desc = msg
            //});
        }

        public static string MsgInJson(object enumValue)
        {
            string defDesc = "";
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    defDesc = ((DescriptionAttribute)attrs[0]).Description;
            }

            return getJsonMsg((int)enumValue, defDesc);
        }

    }
}