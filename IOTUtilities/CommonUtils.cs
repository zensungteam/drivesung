﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace IOTUtilities
{
    public class CommonUtils
    {
        #region Conversion

        public static string ConvertToString(object data)
        {
            if (data == null || Convert.IsDBNull(data))
                return "";
            else
                return Convert.ToString(data);
        }

        public static Int32 ConvertToInt(object data)
        {
            Int32 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int32.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static Int32? ConvertToIntWithNull(object data)
        {
            Int32 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int32.TryParse(data.ToString(), out returnData))
                return null;
            else
                return returnData;
        }

        public static Int64 ConvertToInt64(object data)
        {
            Int64 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int64.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static Int64? ConvertToInt64WithNull(object data)
        {
            Int64 returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !Int64.TryParse(data.ToString(), out returnData))
                return null;
            else
                return returnData;
        }

        public static double ConvertToDouble(object data)
        {
            double returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !double.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static decimal ConvertToDecimal(object data)
        {
            decimal returnData = 0;
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)) || !decimal.TryParse(data.ToString(), out returnData))
                return 0;
            else
                return returnData;
        }

        public static bool ConvertToBoolean(object data)
        {
            if (data == null || Convert.IsDBNull(data) || string.IsNullOrEmpty(Convert.ToString(data)))
                return false;
            else
                return Convert.ToBoolean(data);
        }

        public static DateTime? ConvertToDateTime(object data)
        {
            DateTime result = new DateTime();
            if (data == null || Convert.IsDBNull(data) || !DateTime.TryParse(data.ToString(), out result))
                return null;
            else
                return Convert.ToDateTime(data);
        }


        public static double DateDiff(DateTime d1, DateTime d2)
        {
            TimeSpan ts = (d2.Date - d1.Date);
            return ts.TotalDays + 1;
        }

        public static Int32 DateDiffYears(DateTime start, DateTime end)
        {
            return (end.Year - start.Year - 1) +
                (((end.Month > start.Month) ||
                ((end.Month == start.Month) && (end.Day >= start.Day))) ? 1 : 0);
        }

        public static string GetDateFromatyyyy_MM_ddHHmmss(DateTime? date)
        {
            return string.Format("{0:yyyy-MM-dd HH:mm:ss}", date).ToString().Replace(".", ":");
        }

        public static string GetDateFromatyyyy_MM_dd(DateTime? date)
        {
            return string.Format("{0:yyyy-MM-dd}", date).ToString().Replace(".", ":");
        }

        public static string GetstringFromatyyyy_MM_dd(string strdate)
        {
            //return string.Format("{0:yyyy-MM-dd}", ConvertToDateTime(strdate)).ToString().Replace(".", ":");
            string newDate = "";
            string[] arrdatetime = strdate.Split(' ');
            string[] arrDate ;
             if (arrdatetime.Length == 1)
                {
                    arrDate = arrdatetime[0].Split('-');
                    newDate = arrDate[2].ToString() + "-" + arrDate[1].ToString() + "-" + arrDate[0].ToString();
                }
              if (arrdatetime.Length == 2)
                 {
                        arrDate = arrdatetime[0].Split('-');
                        newDate = arrDate[2].ToString() + "-" + arrDate[1].ToString() + "-" + arrDate[0].ToString() + " " + arrdatetime[1];
                 }
            
            return newDate;
        }

        public static string GetDateFromatdd_MM_yyyy(DateTime? date)
        {
            return string.Format("{0:dd-MM-yyyy}", date).ToString().Replace(".", ":");
        }







        public static int ReturnNumericMonth(string strMonth)
        {
            int intMonth = 0;

            switch (strMonth)
            {
                case "April":
                case "Apr":
                    intMonth = 4;
                    break;
                case "May":
                    intMonth = 5;
                    break;
                case "June":
                case "Jun":
                    intMonth = 6;
                    break;
                case "July":
                case "Jul":
                    intMonth = 7;
                    break;
                case "August":
                case "Aug":
                    intMonth = 8;
                    break;
                case "September":
                case "Sept":
                case "Sep":
                    intMonth = 9;
                    break;
                case "October":
                case "Oct":
                    intMonth = 10;
                    break;
                case "November":
                case "Nov":
                    intMonth = 11;
                    break;
                case "December":
                case "Dec":
                    intMonth = 12;
                    break;
                case "January":
                case "Jan":
                    intMonth = 1;
                    break;
                case "February":
                case "Feb":
                    intMonth = 2;
                    break;
                case "March":
                case "Mar":
                    intMonth = 3;
                    break;
            }
            return intMonth;
        }

        public static string Calculate_FinancialYear(long lngMonth, long lngYear)
        {
            string strFinancialYear = null;
            string tempyear = null;
            tempyear = Convert.ToString(lngYear).Remove(0, 2);
            if (lngMonth >= 4)
            {
                strFinancialYear = lngYear + "-" + (lngYear + 1);
            }
            else
            {
                strFinancialYear = (lngYear - 1) + "-" + lngYear;
            }
            return strFinancialYear;
        }

        public static string GetMonthName(int intMonth)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(intMonth);
        }

        public static string GetShortMonthName(int intMonth)
        {
            return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(intMonth);
        }

        #endregion

        #region Date time function

        public static bool CheckDate(String date)
        {
            DateTime Temp;


            if (DateTime.TryParse(date, out Temp) == true)
                return true;
            else
                return false;
        }
        public static bool Isddmmyyyy(string stringDate)
        {
            if (!string.IsNullOrEmpty(stringDate))
            {
                DateTime parsedDate;
                string[] splittedDateAndTime = stringDate.Trim().Split(' ');

                string strDateOnly = splittedDateAndTime[0].Trim();

                Regex objRegex = new Regex(@"^(\d{2})\/(\d{2})\/(\d{4})$");
                Regex objRegex2 = new Regex(@"^(\d{1})\/(\d{1})\/(\d{4})$");
                if (!string.IsNullOrEmpty(strDateOnly) && objRegex.IsMatch(strDateOnly))
                {
                    string[] spittedDate = strDateOnly.Trim().Split('/');

                    int day = Convert.ToInt32(spittedDate[0].Trim());
                    int month = Convert.ToInt32(spittedDate[1].Trim());
                    int year = Convert.ToInt32(spittedDate[2].Trim());

                    if (month >= 1 && month <= 12 && day >= 1 && day <= 31)
                    {
                        return true;
                    }
                }
                else if (!string.IsNullOrEmpty(strDateOnly) && objRegex2.IsMatch(strDateOnly))
                {
                    return true;
                }
                else if (!string.IsNullOrEmpty(strDateOnly) && DateTime.TryParseExact(strDateOnly, "dd/MM/yyyy",
                    CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDate))
                {
                    return true;
                }
            }
            return false;
        }

        public static DateTime? ConvertToDateTimeObject(object date)
        {
            if (date != DBNull.Value || date != null)
            {
                string dateString = ConvertToString(date);
                if (Isddmmyyyy(dateString))
                {
                    string[] splittedDateAndTime = dateString.Trim().Split(' ');

                    string strDateOnly = splittedDateAndTime[0].Trim();

                    string[] splittedDate = strDateOnly.Trim().Split('/');

                    int day = Convert.ToInt32(splittedDate[0].Trim());
                    int month = Convert.ToInt32(splittedDate[1].Trim());
                    int year = Convert.ToInt32(splittedDate[2].Trim());

                    if (splittedDateAndTime.Length > 1 && !string.IsNullOrEmpty(splittedDateAndTime[1].Trim()))
                    {
                        string strTimeOnly = splittedDateAndTime[1].Trim();
                        string[] splittedTime = strTimeOnly.Trim().Split(':');
                        int hour = Convert.ToInt32(splittedTime[0].Trim());
                        int minute = Convert.ToInt32(splittedTime[1].Trim());
                        int second = Convert.ToInt32(splittedTime[2].Trim());
                        return new DateTime(year, month, day, hour, minute, second);
                    }
                    else
                    {
                        DateTime objDatetime = new DateTime(year, month, day);
                        return new DateTime(year, month, day); ;
                    }
                }
            }
            return null;
        }

        public static string ConvertToDateTimeString(DateTime objDateTime)
        {
            if (objDateTime != null)
            {
                return PadZeroAtLeft(objDateTime.Day, 2) + "/" + PadZeroAtLeft(objDateTime.Month, 2) + "/" + objDateTime.Year.ToString() + " "
                    + PadZeroAtLeft(objDateTime.Hour, 2) + ":" + PadZeroAtLeft(objDateTime.Minute, 2) + ":" + PadZeroAtLeft(objDateTime.Second, 2);
            }
            else
            {
                return string.Empty;
            }
        }

        public static string ConvertToDateString(DateTime objDateTime)
        {
            if (objDateTime != null)
            {
                return PadZeroAtLeft(objDateTime.Day, 2) + "/" + PadZeroAtLeft(objDateTime.Month, 2) + "/" + objDateTime.Year.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string PadZeroAtLeft(int digit, int length)
        {
            string strAlteredDigit = string.Empty;
            string strDigit = digit.ToString();

            if (strDigit.Length < length)
            {
                strAlteredDigit = strDigit.PadLeft(length, '0');
                return strAlteredDigit;
            }
            else
                return strDigit;
        }

        #endregion

        #region Convert Currency to Word

        public static string CurrencyToWord(double MyNumber)
        {
            MyNumber = Convert.ToDouble(MyNumber.ToString("#,#0.00"));
            //string strMyNumber = MyNumber.ToString();
            string functionReturnValue = null;
            //object Temp = null;
            //string Rupees = "";
            string Paisa = "";
            //int? iCount = null;
            // string Hundreds = "";
            string Words = "";
            string[] Place = new string[10];
            Place[0] = " Thousand ";
            Place[2] = " Lakh ";
            Place[4] = " Crore ";
            Place[6] = " Arab ";
            Place[8] = " Kharab ";
            bool blnIsNegative = false;
            if (MyNumber < 0)
            {
                blnIsNegative = true;
                MyNumber = MyNumber * -1;
            }

            //Find decimal place.

            // ==========disabled the decimal part=============
            // If we find decimal place...

            int paisa = GetDigitAfterDecimalPoint(MyNumber);
            if (paisa > 0)
            {
                Paisa = " and " + ConvertTens(paisa) + " Paise";
            }
            else
            {
                Paisa = " and Zero Paise";
            }

            int ruppees = GetDigitBeforeDecimalPoint(MyNumber);

            #region code is not capable to convert number after 100 Crore
            if (ruppees > 999999999)
            {
                if (blnIsNegative)
                {
                    return "-" + MyNumber.ToString();
                }
                else
                {
                    return MyNumber.ToString();
                }
            }
            #endregion

            #region old Code
            //================================================
            // Convert last 3 digits of MyNumber to ruppees in word.
            //string hundreds = MyNumber.ToString().Substring(MyNumber.ToString().Length - 3);
            //Hundreds = ConvertHundreds(Convert.ToInt32(hundreds));


            //if (MyNumber.ToString().Length >= 3)
            //{
            //    // Strip off last three digits
            //    MyNumber = Convert.ToInt32(MyNumber.ToString().Substring(0, MyNumber.ToString().Length - 3));
            //    iCount = 0;
            //    while (MyNumber != null)
            //    {
            //        //Strip last two digits
            //        Temp = MyNumber.ToString().Substring(0, MyNumber.ToString().Length - 2);
            //        if (MyNumber.ToString().Length == 1)
            //        {
            //            Words = ConvertDigit(Convert.ToInt32(Temp)) + Place[Convert.ToInt32(iCount)] + Words;
            //            //MyNumber = Strings.Left(MyNumber, Strings.Len(MyNumber) - 1);
            //            MyNumber = Convert.ToInt32(MyNumber.ToString().Substring(0, (MyNumber.ToString().Length - 1)));// Strings.Left(MyNumber, Strings.Len(MyNumber) - 1);
            //        }
            //        else
            //        {
            //            if (Convert.ToInt32(Temp) == 0)
            //            {
            //                // MyNumber = Strings.Left(MyNumber, Strings.Len(MyNumber) - 2);
            //                MyNumber = Convert.ToInt32(MyNumber.ToString().Substring(0, MyNumber.ToString().Length - 2));
            //            }
            //            else
            //            {
            //                Words = ConvertTens(Convert.ToInt32(Temp)) + Place[Convert.ToInt32(iCount)] + Words;
            //                MyNumber = Convert.ToInt32(MyNumber.ToString().Substring(0, MyNumber.ToString().Length - 2));  //  MyNumber = Strings.Left(MyNumber, Strings.Len(MyNumber) - 2);
            //            }
            //        }
            //        iCount = iCount + 2;
            //    }
            //    //================end of the if condition======
            //}
            //========changed the display to show Rs. XYZ Only==
            //if (string.IsNullOrEmpty(Words) & string.IsNullOrEmpty(Hundreds))
            //{
            //    if (blnIsNegative == true)
            //    {
            //        functionReturnValue = "Recovery Rs. Zero " + Paisa + " Only";
            //    }
            //    else
            //    {
            //        functionReturnValue = "Rs. Zero " + Paisa + " Only";
            //    }
            //}
            //else
            //{
            //    if (blnIsNegative == true)
            //    {
            //        functionReturnValue = "Recovery Rs. " + Words + Hundreds + Paisa + " Only";
            //    }
            //    else
            //    {
            //        functionReturnValue = "Rs. " + Words + Hundreds + Paisa + " Only";
            //    }
            //} 
            #endregion

            Words = NumbersToWords(ruppees);
            #region New Code

            if (string.IsNullOrEmpty(Words))
            {
                if (blnIsNegative == true)
                {
                    functionReturnValue = "Recovery Rs. Zero " + Paisa + " Only";
                }
                else
                {
                    functionReturnValue = "Rs. Zero " + Paisa + " Only";
                }
            }
            else
            {
                if (blnIsNegative == true)
                {
                    functionReturnValue = "Recovery Rs. " + Words + Paisa + " Only";
                }
                else
                {
                    functionReturnValue = "Rs. " + Words + Paisa + " Only";
                }
            }

            #endregion

            return functionReturnValue;
        }

        public static string ConvertHundreds(int MyNumber)
        {
            string functionReturnValue = "";
            string strMyNumber = MyNumber.ToString();
            string Result = "";
            // Exit if there is nothing to convert.
            if (MyNumber == 0)
            {
                return functionReturnValue;
            }

            // MyNumber = Strings.Right("000" + MyNumber, 3);
            // Do we have a hundreds place digit to convert?
            if (strMyNumber.Substring(0, 1) != "0")
            {
                Result = ConvertDigit(Convert.ToInt32(strMyNumber.Substring(0, 1))) + " Hundred ";
            }
            // Do we have a tens place digit to convert?
            if (strMyNumber.Substring(1, 1) != "0")
            {
                Result = Result + ConvertTens(Convert.ToInt32(strMyNumber.Substring(1, 1)));
            }
            else
            {
                // If not, then convert the ones place digit.
                Result = Result + ConvertDigit(Convert.ToInt32(strMyNumber.Substring(2, 1)));
            }
            functionReturnValue = Result.Trim(); ;

            return functionReturnValue;
        }

        public static string ConvertTens(int MyTens)
        {
            int firstDigit = Convert.ToInt32(MyTens.ToString().Substring(0, 1));

            string Result = "";
            // Is value between 10 and 19?       
            if (firstDigit == 1)
            {
                switch (MyTens)
                {
                    // Case 0 : Result = "Zero"
                    case 10:
                        Result = "Ten";
                        break;
                    case 11:
                        Result = "Eleven";
                        break;
                    case 12:
                        Result = "Twelve";
                        break;
                    case 13:
                        Result = "Thirteen";
                        break;
                    case 14:
                        Result = "Fourteen";
                        break;
                    case 15:
                        Result = "Fifteen";
                        break;
                    case 16:
                        Result = "Sixteen";
                        break;
                    case 17:
                        Result = "Seventeen";
                        break;
                    case 18:
                        Result = "Eighteen";
                        break;
                    case 19:
                        Result = "Nineteen";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                // .. otherwise it's between 20 and 99.
                switch (firstDigit)
                {
                    //  Case 0 : Result = "Zero"
                    case 2:
                        Result = "Twenty ";
                        break;
                    case 3:
                        Result = "Thirty ";
                        break;
                    case 4:
                        Result = "Forty ";
                        break;
                    case 5:
                        Result = "Fifty ";
                        break;
                    case 6:
                        Result = "Sixty ";
                        break;
                    case 7:
                        Result = "Seventy ";
                        break;
                    case 8:
                        Result = "Eighty ";
                        break;
                    case 9:
                        Result = "Ninety ";
                        break;
                    default:
                        break;
                }
                // Convert ones place digit.
                if (MyTens.ToString().Length > 1)
                {
                    Result = Result + ConvertDigit(Convert.ToInt32(MyTens.ToString().Substring(1, 1)));
                }
            }
            return Result;
        }

        public static string ConvertDigit(int MyDigit)
        {
            string functionReturnValue = null;
            switch (MyDigit)
            {
                // Case 0 : ConvertDigit = "Zero"
                case 1:
                    functionReturnValue = "One";
                    break;
                case 2:
                    functionReturnValue = "Two";
                    break;
                case 3:
                    functionReturnValue = "Three";
                    break;
                case 4:
                    functionReturnValue = "Four";
                    break;
                case 5:
                    functionReturnValue = "Five";
                    break;
                case 6:
                    functionReturnValue = "Six";
                    break;
                case 7:
                    functionReturnValue = "Seven";
                    break;
                case 8:
                    functionReturnValue = "Eight";
                    break;
                case 9:
                    functionReturnValue = "Nine";
                    break;
                default:
                    functionReturnValue = "";
                    break;
            }
            return functionReturnValue;
        }

        public static int GetDigitBeforeDecimalPoint(double MyValue)
        {
            if (MyValue.ToString().IndexOf(".") > 0)
            {
                return Convert.ToInt32(MyValue.ToString().Substring(0, MyValue.ToString().IndexOf(".")));
            }
            else
            {
                return Convert.ToInt32(MyValue);
            }
        }

        public static int GetDigitAfterDecimalPoint(double MyValue)
        {
            string outPut = "0";
            if (MyValue.ToString().Split('.').Length == 2)
            {
                outPut = MyValue.ToString().Split('.')[1].Substring(0, MyValue.ToString().Split('.')[1].Length);
            }
            return Convert.ToInt32(outPut);
        }

        private static string NumbersToWords(int inputNumber)
        {
            int inputNo = inputNumber;

            if (inputNo == 0)
                return "Zero";

            int[] numbers = new int[4];
            int first = 0;
            int u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (inputNo < 0)
            {
                sb.Append("Minus ");
                inputNo = -inputNo;
            }

            string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
            string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
            string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            numbers[0] = inputNo % 1000; // units
            numbers[1] = inputNo / 1000;
            numbers[2] = inputNo / 100000;
            numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
            numbers[3] = inputNo / 10000000; // crores
            numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (numbers[i] != 0)
                {
                    first = i;
                    break;
                }
            }
            for (int i = first; i >= 0; i--)
            {
                if (numbers[i] == 0) continue;
                u = numbers[i] % 10; // ones
                t = numbers[i] / 10;
                h = numbers[i] / 100; // hundreds
                t = t - 10 * h; // tens
                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if (h > 0 || i == 0) sb.Append("");
                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }

        #endregion

        #region " Numeric Function"
        public static bool isNumeric(string val, NumberStyles numberStyle)
        {
            Double result;
            return Double.TryParse(val, numberStyle,
                CultureInfo.CurrentCulture, out result);
        }
        #endregion

        #region " File Related Validation Function "

        public static bool val_AllSpecial(string valuePass)
        {
            bool functionReturnValue = false;
            string strTemp = valuePass.Trim();
            if (strTemp.Contains("*") || strTemp.Contains("[") || strTemp.Contains(";"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }

            if (strTemp.Contains(":") || strTemp.Contains("<") || strTemp.Contains(">"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("{") || strTemp.Contains("}"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("#") || strTemp.Contains(",") || strTemp.Contains(")"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("!") || strTemp.Contains("[") || strTemp.Contains("]"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("@") || strTemp.Contains("?") || strTemp.Contains("/"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("%") || strTemp.Contains("\\") || strTemp.Contains("'"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }

            if (strTemp.Contains("$") || strTemp.Contains("+") || strTemp.Contains("="))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("(") || strTemp.Contains(")") || strTemp.Contains("&"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }

            if (strTemp.Contains("*") || strTemp.Contains("^") || strTemp.Contains("!"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (strTemp.Contains("|") || strTemp.Contains("~") | strTemp.Contains("'"))
            {
                functionReturnValue = false;
                return functionReturnValue;
            }
            return true;
        }

        public static string getFilePath(string FileName, string user_name, string FolderName, out string new_f_name)
        {
            string profile_picture_uri = "";
            if (FileName.Contains("/"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            else if (FileName.Contains(@"\"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            else
                profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["UploadFile"];
            new_f_name = user_name + "_" + profile_picture_uri;
            return strPostedPath + "\\" + FolderName + "\\" + user_name + "_" + profile_picture_uri;
        }

        public static void CheckexistPath( string user_name, string FolderName)
        {
            string new_path;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["UploadFile"];
            new_path = strPostedPath + "\\" + FolderName + "\\" + user_name;
            if (!Directory.Exists(new_path))
            {
                Directory.CreateDirectory(new_path);
            }
        }

        public static string SyncLogPath(string FileName, string user_name, string FolderName, out string new_path)
        {
            string profile_picture_uri = "";
            if (FileName.Contains("/"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            else if (FileName.Contains(@"\"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            else
                profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["UploadFile"];
            new_path = strPostedPath + "\\" + FolderName + "\\" + user_name;
            // Try to create the directory.
            return strPostedPath + "\\" + FolderName + "\\" + user_name + "\\" + profile_picture_uri;
        }

        public static string PolicyClaimPath(string FileName, string user_name,string subname, string FolderName, out string new_path)
        {
            string profile_picture_uri = "";
            if (FileName.Contains("/"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            else if (FileName.Contains(@"\"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            else
                profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["UploadFile"];
            new_path =  subname+"_" + profile_picture_uri;
            // Try to create the directory.
            return strPostedPath + "\\" + FolderName + "\\" + user_name + "\\"+ profile_picture_uri;
        }
        

        public static string getVideoFilePath(string FileName, string user_name, string FolderName, out string new_f_name)
        {
            string profile_picture_uri = "";
            if (FileName.Contains("/"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            else if (FileName.Contains(@"\"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            else
                profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["UploadFile"];
            new_f_name = user_name + "_" + profile_picture_uri;
            return strPostedPath + "\\" + FolderName + "\\" + user_name + "_" + profile_picture_uri;
        }

        public static string getFilename(string FileName, string user_name)
        {
            string profile_picture_uri = "";
            if (FileName.Contains("/"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            else if (FileName.Contains(@"\"))
                profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            else
                profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            return user_name + "_" + profile_picture_uri;
        }

        public static string getUrlPath(string FileName, string FolderName, out string Url)
        {
            //string profile_picture_uri = "";
            //if (FileName.Contains("/"))
            //    profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            //else if (FileName.Contains(@"\"))
            //    profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            //else
            //    profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["URL"];
            Url = strPostedPath + "/" + FolderName + "/" + FileName;
            return Url;
        }
        public static string getUrlDocPath(string FileName, string FolderName, string SubFolderName, out string Url)
        {
            //string profile_picture_uri = "";
            //if (FileName.Contains("/"))
            //    profile_picture_uri = FileName.Substring(FileName.LastIndexOf("/") + 1);
            //else if (FileName.Contains(@"\"))
            //    profile_picture_uri = FileName.Substring(FileName.LastIndexOf(@"\") + 1);
            //else
            //    profile_picture_uri = FileName;
            // userEntity.upload_image = file;
            string strPostedPath = System.Configuration.ConfigurationManager.AppSettings["URL"];
            Url = strPostedPath + "/" + FolderName + "/" + SubFolderName + "/" + FileName;
            return Url;
        }

        #endregion


        //public static bool CheckSessionExpired()
        //{
        //    Sessions sessions = new Sessions();
        //    return sessions.CheckSessionExpired();
        //}

        //public static System.Net.Http.HttpResponseMessage DownloadFile(string path, string FileName)
        //{
        //    if (path.LastIndexOf('.') != -1)
        //    {
        //        string extension = path.Substring(path.LastIndexOf('.'));
        //        return DownloadFile(System.IO.File.ReadAllBytes(path), FileName, extension);
        //    }
        //    else
        //        return DownloadFile(System.IO.File.ReadAllBytes(path), FileName);
        //}

        //public static System.Net.Http.HttpResponseMessage DownloadFile(System.IO.MemoryStream objMemoryStream, string FileName)
        //{
        //    return DownloadFile(objMemoryStream.ToArray(), FileName);
        //}

        //public static System.Net.Http.HttpResponseMessage DownloadFile(System.IO.MemoryStream objMemoryStream, string FileName, string extension)
        //{
        //    return DownloadFile(objMemoryStream.ToArray(), FileName, extension);
        //}

        //public static System.Net.Http.HttpResponseMessage DownloadFile(byte[] objMemoryStream, string FileName)
        //{
        //    string extension = ".xlsx";
        //    return DownloadFile(objMemoryStream, FileName, extension);
        //}

        //public static System.Net.Http.HttpResponseMessage DownloadFile(byte[] objMemoryStream, string FileName, string extension)
        //{

        //    string ContentType = "application/octet-stream";
        //    switch (extension)
        //    {
        //        case ".pdf":
        //            ContentType = "application/pdf";
        //            break;
        //        case ".doc":
        //            ContentType = "application/msword";
        //            break;
        //        case ".docx":
        //            ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        //            break;
        //        case ".xls":
        //            ContentType = "application/vnd.ms-excel";
        //            break;
        //        case ".xlsx":
        //            ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //            break;
        //        case ".jpg":
        //            ContentType = "image/jpg";
        //            break;
        //        case ".txt":
        //            ContentType = "text/plain";
        //            break;
        //        case ".zip":
        //            ContentType = "application/x-zip-compressed";
        //            break;
        //        case ".bmp":
        //            ContentType = "image/bmp";
        //            break;
        //        case ".jpeg":
        //            ContentType = "image/JPEG";
        //            break;
        //        case ".html":
        //            ContentType = "text/HTML";
        //            break;
        //        default:
        //            ContentType = "binary/octet-stream";
        //            break;
        //    }

        //    System.Net.Http.HttpResponseMessage result = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);
        //    result.Content = new System.Net.Http.ByteArrayContent(objMemoryStream);
        //    result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentType);
        //    result.Content.Headers.ContentLength = objMemoryStream.Length;
        //    result.Content.Headers.Add("x-filename", FileName);
        //    result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
        //    result.Content.Headers.ContentDisposition.FileName = FileName;

        //    return result;
        //}

        //public System.Net.Http.HttpResponseMessage ConvertToPDF1(string strHTMLBody, string ReportName)
        //{
        //    string ImageDirPath = System.Web.Hosting.HostingEnvironment.MapPath("~");
        //    string strHTML = @"<html><body>" + strHTMLBody.Replace("{base~url}", ImageDirPath) + "</body></html>";

        //    ExpertPdf.HtmlToPdf.PdfConverter pdfConverter = new ExpertPdf.HtmlToPdf.PdfConverter();

        //    pdfConverter.PdfDocumentOptions.PdfPageSize = ExpertPdf.HtmlToPdf.PdfPageSize.A4;
        //    pdfConverter.PdfDocumentOptions.PdfCompressionLevel = ExpertPdf.HtmlToPdf.PdfCompressionLevel.Normal;
        //    pdfConverter.PdfDocumentOptions.ShowHeader = true;
        //    pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //    pdfConverter.PdfDocumentOptions.LeftMargin = 25;
        //    pdfConverter.PdfDocumentOptions.RightMargin = 25;
        //    pdfConverter.PdfDocumentOptions.TopMargin = 30;
        //    pdfConverter.PdfDocumentOptions.BottomMargin = 10;
        //    pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = true;

        //    pdfConverter.PdfDocumentOptions.ShowHeader = false;
        //    pdfConverter.PdfDocumentOptions.ShowFooter = false;

        //    //pdfConverter.PdfFooterOptions.FooterText = ("")
        //    //pdfConverter.PdfFooterOptions.FooterTextColor = Color.Blue
        //    //pdfConverter.PdfFooterOptions.DrawFooterLine = False
        //    //pdfConverter.PdfFooterOptions.PageNumberText = "Page"
        //    //pdfConverter.PdfFooterOptions.ShowPageNumber = True

        //    pdfConverter.LicenseKey = "I6WAxUuAU3CoCkttn5ixjlS9q0kX6MYVroKil/A31qwITRSt0buj9IAb7RBI7FTX";
        //    byte[] downloadBytes = pdfConverter.GetPdfBytesFromHtmlString(strHTML);

        //    return DownloadFile(downloadBytes, ReportName, ".pdf");
        //    //System.IO.MemoryStream ms = new System.IO.MemoryStream(downloadBytes);
        //    //return ms;
        //}

        //public System.Net.Http.HttpResponseMessage ConvertToPDF(string strHTMLBody, string ReportName)
        //{
        //    string ImageDirPath = System.Web.Hosting.HostingEnvironment.MapPath("~");
        //    string strHTML = @"<html><body>" + strHTMLBody.Replace("{base~url}", ImageDirPath) + "</body></html>";

        //    HiQPdf.HtmlToPdf htmlToPdfConverter = new HiQPdf.HtmlToPdf();
        //    // set a original serial number
        //    htmlToPdfConverter.SerialNumber = "dT0cJCUREzkcFwcUBwxAW0VVRFVEVUdFRlVGRFtER1tMTExM";
        //    htmlToPdfConverter.BrowserWidth = 200;
        //    //   htmlToPdfConverter.HtmlLoadedTimeout = 30
        //    htmlToPdfConverter.Document.PageSize = HiQPdf.PdfPageSize.A4;
        //    htmlToPdfConverter.Document.PageOrientation = HiQPdf.PdfPageOrientation.Portrait;
        //    // set the PDF standard used by the document
        //    //            htmlToPdfConverter.Document.PdfStandard = PdfStandard.PdfA
        //    htmlToPdfConverter.Document.PdfStandard = HiQPdf.PdfStandard.Pdf;
        //    // set PDF page margins
        //    //htmlToPdfConverter.Document.Margins = New PdfMargins(5)
        //    htmlToPdfConverter.Document.Margins = new HiQPdf.PdfMargins(10, 10, 10, 10);

        //    //htmlToPdfConverter.Document.Margins = New PdfMargins(5, 5, 0, 5)
        //    // set whether to embed the true type font in PDF
        //    htmlToPdfConverter.Document.FontEmbedding = true;
        //    //htmlToPdfConverter.Document.Security.OpenPassword = lblEmpCode.Text
        //    htmlToPdfConverter.Document.Security.AllowPrinting = true;

        //    // convert HTML to PDF
        //    byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(strHTML, "");
        //    return DownloadFile(pdfBuffer, ReportName, ".pdf");
        //}



    }
}