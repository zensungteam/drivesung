﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Npgsql;
namespace IOTUtilities
{
    public class ErrorLog
    {
        //public static void LogToFile(Exception logdata, string source)
        //{
        //    //try
        //    //{
        //    string filesname = "ErrorLog";
        //    string filePath = System.Configuration.ConfigurationManager.AppSettings["ErrorFileLocation"].ToString();
        //    if (filePath != "")
        //    {
        //        StreamWriter fs = default(StreamWriter);
        //        string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        //        if (File.Exists(filename))
        //        {
        //            fs = new StreamWriter(filename, true);
        //        }
        //        else
        //        {
        //            fs = new StreamWriter(filename);
        //        }
        //        fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + "> " + source + System.Environment.NewLine + logdata.ToString() + System.Environment.NewLine + System.Environment.NewLine);
        //        fs.Close();
        //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //}

        //public static void LogToTripQuery(string logdata, string source)
        //{
        //    //try
        //    //{
        //    string filesname = "Querylog";
        //    string filePath = System.Configuration.ConfigurationManager.AppSettings["FileLogPath"].ToString();
        //    if (filePath != "")
        //    {
        //        StreamWriter fs = default(StreamWriter);
        //        string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        //        if (File.Exists(filename))
        //        {
        //            fs = new StreamWriter(filename, true);
        //        }
        //        else
        //        {
        //            fs = new StreamWriter(filename);
        //        }
        //        fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + "> " + source + System.Environment.NewLine + logdata.ToString() + System.Environment.NewLine + System.Environment.NewLine);
        //        fs.Close();
        //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //}

        //public static void LogAPIReqRespnse(Int64 deviceno, string logtype, string error, string logdata)
        //{
        //    //try
        //    //{
        //    string filesname = "RequestResponse";
        //    string filePath = System.Configuration.ConfigurationManager.AppSettings["LogAPIReqRespnsefileLocation"].ToString();
        //    if (filePath != "")
        //    {
        //        StreamWriter fs = default(StreamWriter);
        //        string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

        //        if (File.Exists(filename))
        //        {
        //            fs = new StreamWriter(filename, true);
        //        }
        //        else
        //        {
        //            fs = new StreamWriter(filename);
        //        }
        //        fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + "> " + source + System.Environment.NewLine);
        //        fs.Close();
        //    }
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw ex;
        //    //}
        //}


        public static void SaveLogToDatabase(Int64 deviceno, string logtype, string error, string logdata)
        {
            try
            {
                string saveFlag = System.Configuration.ConfigurationManager.AppSettings["flgDBLog"].ToString();
                if (saveFlag == "1")
                {
                    using (NpgsqlConnection connection = new NpgsqlConnection())
                    {
                        connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConStrlog"];
                        connection.Open();
                        NpgsqlCommand cmd = new NpgsqlCommand();
                        cmd.Connection = connection;

                        cmd.CommandText = " INSERT INTO webapi_req_res_error_log(deviceno, logtype, error, logdata, created_at) VALUES (@deviceno, @logtype, @error, @logdata, @created_at)";

                        cmd.Parameters.Add(new NpgsqlParameter("@deviceno", deviceno));
                        cmd.Parameters.Add(new NpgsqlParameter("@logtype", logtype));
                        cmd.Parameters.Add(new NpgsqlParameter("@error", error));
                        cmd.Parameters.Add(new NpgsqlParameter("@logdata", logdata));
                        cmd.Parameters.Add(new NpgsqlParameter("@created_at", Convert.ToDateTime(DateTime.Now)));
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        connection.Close();
                    }

                }
            }
            catch (Exception ex)
            {
                //  throw ex;
                LogToFile(ex, "SaveWebApiLogToDatabase");
            }

        }
        
        public static void LogToFile(Exception logdata, string source)
        {

            string filesname = "IOT_Web_APIUnhandleErrors";
            string filePath = System.Configuration.ConfigurationManager.AppSettings["ErrorFileLocation"].ToString();

            StreamWriter fs = default(StreamWriter);
            string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

            if (File.Exists(filename))
            {
                fs = new StreamWriter(filename, true);
            }
            else
            {
                fs = new StreamWriter(filename);
            }
            fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss.FFF") + "> " + source + System.Environment.NewLine + logdata.ToString() + System.Environment.NewLine + System.Environment.NewLine);
            fs.Write("==================================================================================" + System.Environment.NewLine);
            fs.Close();

        }

    }
}
