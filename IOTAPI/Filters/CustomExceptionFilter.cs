﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using IOTBusinessEntities;
using IOTUtilities;

namespace IOTWebAPI.Filters
{
    public class CustomExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            string exceptionMessage = string.Empty;
            if (actionExecutedContext.Exception.InnerException == null)
            {
                exceptionMessage = actionExecutedContext.Exception.Message;
            }
            else
            {
                exceptionMessage = actionExecutedContext.Exception.InnerException.Message;
            }
            //WebUtils.ErrorHandler.HandleException(actionExecutedContext.Exception, actionExecutedContext.Exception.Source );
            //ErrorLog.LogToFile(actionExecutedContext.Exception, actionExecutedContext.Exception.Source);
            ErrorLog.SaveLogToDatabase(0, "Error", actionExecutedContext.Exception.ToString(), actionExecutedContext.Exception.Source);
            //We can log this exception message
            //var response = new HttpResponseMessage(HttpStatusCode.InternalServerError)
            //{
            //    Message = errorMessage,
            //    ReasonPhrase = "Internal Server Error.Please Contact your Administrator."
            //};

            var response = actionExecutedContext.Request.CreateResponse(HttpStatusCode.InternalServerError,
               new
               {
                   Message = @"{'code':'500','desc':'" + exceptionMessage + "'}"
               });
            actionExecutedContext.Response = response;

        }
    }
}