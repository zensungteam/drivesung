﻿using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
//using System.Web.Mail;


namespace IOTWebAPI.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class CompititionController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> CreateCompitition()
        {
           // CompitionEntity compititionEntity;
           
            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string Compititiondata = HttpContext.Current.Request.Form["data"];
            CompitionEntity compititionEntity = JsonConvert.DeserializeObject<CompitionEntity>(Compititiondata);

            // Validatde entity
            string strResult;
            string Url = " ";
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_name: true, cmpt_type: true,   s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataTable dtCompitition = await objCompititionBS.CreateCompitition(compititionEntity);

                if (dtCompitition.Rows.Count > 0)
                {
                    Int32 cmpt_id = Convert.ToInt32(dtCompitition.Rows[0]["cmpt_id"]);
                    if (cmpt_id > 0)
                    {
                        compititionEntity.cmpt_id = cmpt_id;
                        var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

                        if (file != null && file.ContentLength > 0)
                        {
                            string profile_picture_uri = "";
                            string strPostedPath = CommonUtils.getFilePath(file.FileName, "cmpt_" + CommonUtils.ConvertToString(cmpt_id), "compitition", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            compititionEntity.cmpt_profile_picture_path = profile_picture_uri;
                            await objCompititionBS.UpdateCmptProfilePath(compititionEntity);
                            string strUrlPath = CommonUtils.getUrlPath(profile_picture_uri, "compitition", out Url);
                            dtCompitition.Rows[0]["cmpt_profile_picture_path"] = Url;
                        }
                        else
                        {
                            compititionEntity.cmpt_profile_picture_path = string.Empty;
                        }

                        // compititionEntity.cmpt_id = cmpt_id;
                        return Ok(CustomeResponse.getOkResponse(dtCompitition));
                      }
                    else
                    {
                        strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                        return BadRequest(strResult);
                    }

                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> viewcmpt(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataSet dtCompitition = await objCompititionBS.viewcmpt(compititionEntity);
                return Ok(CustomeResponse.getOkResponse(dtCompitition));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> getCompetitionList(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataSet dtCompitition = await objCompititionBS.competitionlist(compititionEntity);
                return Ok(CustomeResponse.getOkResponse(dtCompitition));

            }
        }

        [HttpPost]


        public async Task<IHttpActionResult> GetCompitionbyId(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataTable dtDevice = await objCompititionBS.GetCompitionbyId(compititionEntity);
                return Ok(CustomeResponse.getOkResponse(dtDevice));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> JoinCmpt(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, CheckJointAvailablity:true,Checkisclose_cmptAvailablity:true, cmpt_id: true, deviceno:true,  jointimestam:true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                 
                 if (await objCompititionBS.JoinCmpt(compititionEntity))
                 {
                    return Ok(CustomeResponse.getOkResponse());
                 }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }


        [HttpPost]
        public async Task<IHttpActionResult> GetMyCompition(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataSet dtCompitition = await objCompititionBS.GetMyCompition(compititionEntity);
                return Ok(CustomeResponse.getOkResponse(dtCompitition));

            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetTop10Player(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataSet dtCompitition = await objCompititionBS.GetTop10Player(compititionEntity);
                return Ok(CustomeResponse.getOkResponse(dtCompitition));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UpdateCompitition()
        {
            // CompitionEntity compititionEntity;

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string Compititiondata = HttpContext.Current.Request.Form["data"];
            CompitionEntity compititionEntity = JsonConvert.DeserializeObject<CompitionEntity>(Compititiondata);

            // Validatde entity
            string strResult;
            string Url = " ";
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_name: true, cmpt_type: true, terms_conditions: true, min_km_travel: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                DataTable dtCompitition = await objCompititionBS.UpdateCompitition(compititionEntity);

                if (dtCompitition.Rows.Count > 0)
                {
                    Int32 cmpt_id = Convert.ToInt32(dtCompitition.Rows[0]["cmpt_id"]);
                    if (cmpt_id > 0)
                    {
                        compititionEntity.cmpt_id = cmpt_id;
                        var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;

                        if (file != null && file.ContentLength > 0)
                        {
                            string profile_picture_uri = "";
                            string strPostedPath = CommonUtils.getFilePath(file.FileName, "cmpt_" + CommonUtils.ConvertToString(cmpt_id), "compitition", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            compititionEntity.cmpt_profile_picture_path = profile_picture_uri;
                            await objCompititionBS.UpdateCmptProfilePath(compititionEntity);
                            string strUrlPath = CommonUtils.getUrlPath(profile_picture_uri, "compitition", out Url);
                            dtCompitition.Rows[0]["cmpt_profile_picture_path"] = Url;
                        }
                        else
                        {
                            compititionEntity.cmpt_profile_picture_path = string.Empty;
                        }

                        // compititionEntity.cmpt_id = cmpt_id;
                        return Ok(CustomeResponse.getOkResponse(dtCompitition));
                    }
                    else
                    {
                        strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                        return BadRequest(strResult);
                    }

                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> CancelCompitition(CompitionEntity compititionEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidationCompitition(compititionEntity, cmpt_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (CompititionBS objCompititionBS = new CompititionBS())
            {
                if (await objCompititionBS.CancelCompitition(compititionEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public IHttpActionResult SendInvitation(CompitionEntity compititionEntity)
        {
            if (SendMail(compititionEntity.Email_id, compititionEntity.Emailbody, compititionEntity.subject))
            {
                return Ok(CustomeResponse.getMsgResponse("Email Send Successfully"));
            }
            else
            {
                return Ok(CustomeResponse.getMsgResponse("Email Send fail"));
            }
        }

         public bool SendMail (string email_id,string body,string subject)
        {
            try
            {
                //string[] Email = email_id.Split(',');
               
                MailMessage msg = new MailMessage();
                string mailfrom = System.Configuration.ConfigurationManager.AppSettings["Mailfrom"];
                msg.From = new MailAddress(mailfrom, mailfrom);
                msg.To.Add(email_id);
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                msg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                msg.Priority = MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = System.Configuration.ConfigurationManager.AppSettings["Hostname"];
                //smtp.EnableSsl = true;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Portno"]);
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["smtpusername"], System.Configuration.ConfigurationManager.AppSettings["smtppassword"]);
                try
                {
                    smtp.Send(msg);
                   return true;
                  
                }
                catch
                {
                    return false;
                }

                //return dtUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
         
        }

         [HttpPost]
         public async Task<IHttpActionResult> ShareCompitition(CompitionEntity compititionEntity)
         {

             // Validatde entity
             string strResult;
             strResult = ValidateEntity.ValidationCompitition(compititionEntity, Checkshare_cmptAvailablity: true,  cmpt_id: true, userid:true, s_user_id: true, s_role_Id: true);

             if (strResult != "")
             {
                 return BadRequest(strResult);
             }

             using (CompititionBS objCompititionBS = new CompititionBS())
             {

                 if (await objCompititionBS.ShareCmpt(compititionEntity))
                 {
                     return Ok(CustomeResponse.getOkResponse());
                 }
                 else
                 {
                     strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                     return BadRequest(strResult);
                 }

             }
         }


        



    }
}
