﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using IOTUtilities;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using System.Text;


//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{
    public class IOTMobileController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> IOTData(object obj)
        {

            if (obj == null)
            {
                return BadRequest("empty data.");
            }

            string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            dynamic jsonObject = JObject.Parse(jsonString);

            await M_IOTData(jsonObject, jsonString);

            return Ok(CustomeResponse.getOkResponse());


        }

        private Task<bool> M_IOTData(dynamic jsonIOT, string strjson)
        {
            TM_SaveLogToFile("IOT_JSON_M_v1_datalog", strjson);
            return Task.FromResult(true);
        }

        private void TM_SaveLogToFile(string filesname, string logdata)
        {
            try
            {
                string filePath = ConfigurationManager.AppSettings["LogFileLocation"].ToString();

                StreamWriter fs = default(StreamWriter);
                string filename = @filePath + filesname + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".txt";

                if (File.Exists(filename))
                {
                    fs = new StreamWriter(filename, true);
                }
                else
                {
                    fs = new StreamWriter(filename);
                }
                fs.Write(String.Format(DateTime.Now.ToString(), "yyyy/MM/dd HH:mm:ss") + " > " + logdata + System.Environment.NewLine);
                fs.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
