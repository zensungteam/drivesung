﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

//suppress the warning of xml documentation 

#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{
    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class TripController : ApiController
    {
        [HttpPost]
        public async Task<IHttpActionResult> StartTrip(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity,CheckTripStart:true, trip_name: true, trip_start_time: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (TripBS objTripBS = new TripBS())
            {
               DataTable dtTrip=  await objTripBS.StartTrip(tripEntity);
            
                return Ok(CustomeResponse.getOkResponse(dtTrip));
            
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> EndTrip(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            if (CommonUtils.ConvertToInt(tripEntity.trip_id) == 0)
            {
                strResult = ValidateEntity.ValidateTrip(tripEntity, CheckTripEnd: true, trip_start_time:true, trip_end_time: true, s_user_id: true, s_role_Id: true);
            }
            else
            {
                strResult = ValidateEntity.ValidateTrip(tripEntity, trip_id: true, CheckTripEnd: true, trip_end_time: true, s_user_id: true, s_role_Id: true);
            }
          

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (TripBS objTripBS = new TripBS())
            {
                if (await objTripBS.EndTrip(tripEntity))
                  {
                      return Ok(CustomeResponse.getOkResponse());
                  }
                    else
                    {
                        strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                        return BadRequest(strResult);
                    }

             }

          }
        

        [HttpPost]
        public async Task<IHttpActionResult> UpdateTrip(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, trip_name:true, trip_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (TripBS objTripBS = new TripBS())
            {
                  DataTable data=  await objTripBS.UpdateTrip(tripEntity);
                  if (data.Rows.Count > 0)
                
                  {
                      return Ok(CustomeResponse.getOkResponse(data));
                  }
                  else
                    {
                        strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                        return BadRequest(strResult);
                    }

             }

          }
        

        [HttpPost]
        public async Task<IHttpActionResult> CancelTrip(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, trip_id: true, s_user_id: true, s_role_Id: true);
           
            if (strResult != "")
            {
                return BadRequest(strResult);
            }
                   
            //Result
            using (TripBS objTripBS = new TripBS())
            {
                if (await objTripBS.DeleteTrip(tripEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetTrips(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (TripBS objTripBS = new TripBS())
            {
                DataTable dtTrip = await objTripBS.GetTripsList(tripEntity);
                return Ok(CustomeResponse.getOkResponse(dtTrip));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetTripById(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, trip_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (TripBS objTripBS = new TripBS())
            {
                DataTable dtTrip = await objTripBS.GetTripById(tripEntity);
                return Ok(CustomeResponse.getOkResponse(dtTrip));
            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> GetTripPlayDataByTripId(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, trip_id: true, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //Result
            using (TripBS objTripBS = new TripBS())
            {
                DataSet dsTripPlayData = await objTripBS.GetTripPlayData(tripEntity);
                return Ok(CustomeResponse.getOkResponse(dsTripPlayData));
                //return Ok(dsData);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> GetActiveTrips(TripEntity tripEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateTrip(tripEntity, s_user_id: true, s_role_Id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(filterEntity.s_user_id), CommonUtils.ConvertToInt(filterEntity.s_role_Id), page_NM: "userprofile", _action: "V");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (TripBS objTripBS = new TripBS())
            {
                DataTable dtTrip = await objTripBS.GetActiveTrips(tripEntity);
                return Ok(CustomeResponse.getOkResponse(dtTrip));
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> UploadVideo()
        {
            // Validatde entity

            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");

            string Tripdata = HttpContext.Current.Request.Form["data"];
            TripEntity tripEntity = JsonConvert.DeserializeObject<TripEntity>(Tripdata);

            var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
            using (TripBS objTripBS = new TripBS())
            {

                if (file != null && file.ContentLength > 0)
                {
                    string trip_vedio_uri = "";
                    string strPostedPath = CommonUtils.getVideoFilePath(file.FileName, CommonUtils.ConvertToString(tripEntity.s_user_id) + "_" + CommonUtils.ConvertToString(tripEntity.trip_id), "tripvideodata", out trip_vedio_uri);
                    file.SaveAs(strPostedPath);
                    tripEntity.uploadfile = trip_vedio_uri;
                    await objTripBS.UpdateTripUploadPath(tripEntity);
                }
               
            }
            return Ok(CustomeResponse.getOkResponse());
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> UploadTripLog()
        {
            if (HttpContext.Current.Request.Form["data"] == null || HttpContext.Current.Request.Form["data"] == "")
                return BadRequest("invalid data entity");
            //UserEntity userEntitytemp;
            string Tripdata = HttpContext.Current.Request.Form["data"];
            TripEntity tripEntity = JsonConvert.DeserializeObject<TripEntity>(Tripdata);
            string strResult;
            string Url = " ";

            using (TripBS objTripBS = new TripBS())
            {

                int count = HttpContext.Current.Request.Files.Count;

                if (count > 0)
                {
                    CommonUtils.CheckexistPath(CommonUtils.ConvertToString(tripEntity.trip_id), "triplogdata");
                    string filename = "";
                    string strPostedPath = "";
                    for (int i = 0; i < count; i++)
                    {
                        var file = HttpContext.Current.Request.Files[i];
                        if (file != null && file.ContentLength > 0)
                        {
                            string profile_picture_uri = "";
                            strPostedPath = CommonUtils.SyncLogPath(file.FileName,  CommonUtils.ConvertToString(tripEntity.trip_id), "triplogdata", out profile_picture_uri);
                            file.SaveAs(strPostedPath);
                            if (filename == "")
                            {
                                filename = file.FileName;
                            }
                            else
                            {
                                filename = filename + "," + file.FileName;
                            }
                        }
                     }
                    tripEntity.triplogfile = filename;
                    await objTripBS.UpdateTripLogPath(tripEntity);
                    return Ok(CustomeResponse.getMsgResponse("Success"));
                   
                }
                else
                {
                    return Ok(CustomeResponse.getMsgResponse("Error"));
                }

            }

        }

      



    }
}
