﻿using PFTrustAPI.Models;
using Newtonsoft.Json;
using PFTrustAPI.Controllers.CommonUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class DocumentNoController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        #region DocumentNo Master List

        [HttpPost]
        public IHttpActionResult GetDocumentNoList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select * from PFT_tblDocumentNoMaster where IsDelete = 0 AND IsActive = 1";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DocumentNoController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult GetDocumentsList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();
                string strQuery = "";

                if (filterEntity.Status == "Edit" || filterEntity.Status == "View")
                {
                    strQuery = @"Select DM.DocumentID,DocumentName,DM.IsActive,DocumentNoDetailsID,DocumentNoID,StartNumber,EndNumber,CurrentSequence
                                 From PFT_tblDocumentMaster DM LEFT JOIN PFT_tblDocumentNoDetails DND ON DND.DocumentID = DM.DocumentID
                                 Where DND.IsActive = 1 AND DocumentNoID = " + filterEntity.Param1 + " OR DocumentNoID IS NULL";
                }
                else
                {
                    strQuery = @"Select DISTINCT DM.DocumentID,DocumentName,DM.IsActive,DocumentNoDetailsID = NULL,DocumentNoID = NULL,StartNumber = NULL,EndNumber = NULL,CurrentSequence = NULL
                                 From PFT_tblDocumentMaster DM LEFT JOIN PFT_tblDocumentNoDetails DND ON DND.DocumentID = DM.DocumentID
                                 Where DM.IsActive = 1
                                 ORDER BY DM.DocumentID";
                }
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DocumentNoController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult GetFinancialYear(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();
                string strQuery = "";

                strQuery = @"Select FinancialYear From PFT_tblDocumentNoMaster where DocumentNoID = " + filterEntity.Param1;

                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DocumentNoController");
                return BadRequest(ex.Message);
            }
        }

        #endregion

        #region Save Document Details List

        [HttpPost]
        public IHttpActionResult SaveDocumentNoMaster(DocumentNoEntity objDocumentNoEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 documentnoID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                SqlParameter sqlParameter;

                if (objDocumentNoEntity.actionEntity != null)
                    _actionState = objDocumentNoEntity.actionEntity.ActionState;

                bool IsExist = Validate(objDocumentNoEntity.FinancialYear);
                if (_actionState == "New" && IsExist)
                {
                    responseEntity.IsComplete = false;
                    responseEntity.Description = "Financial Year Already Exists";
                    return Ok(responseEntity);

                    //_actionState = "Edit";
                    //if (objDocumentNoEntity.actionEntity.RecordId != 0)
                    //    documentnoID = objDocumentNoEntity.actionEntity.RecordId;
                    //else
                    //    documentnoID = GetFinancialYearID(objDocumentNoEntity.FinancialYear);
                }
                else
                {
                    //_actionState = "New";

                    if (objDocumentNoEntity.actionEntity.RecordId != 0)
                        documentnoID = objDocumentNoEntity.actionEntity.RecordId;
                    else
                        documentnoID = GetFinancialYearID(objDocumentNoEntity.FinancialYear);
                }

                foreach (var objDocumentNoDetails in objDocumentNoEntity.DocumentNoDetailsList)
                {
                    if (objDocumentNoDetails.StartNumber != null && objDocumentNoDetails.EndNumber != null && objDocumentNoDetails.CurrentSequence != null)
                    {
                        if (objDocumentNoDetails.StartNumber == objDocumentNoDetails.EndNumber)
                        {
                            responseEntity.IsComplete = false;
                            responseEntity.Description = "Start number and End number should not be same";
                            return Ok(responseEntity);
                        }
                        else if (objDocumentNoDetails.EndNumber <= objDocumentNoDetails.CurrentSequence)
                        {
                            responseEntity.IsComplete = false;
                            responseEntity.Description = "Current Sequence Number Cannot be greater than or equal to End Number";
                            return Ok(responseEntity);
                        }
                        if (objDocumentNoDetails.StartNumber == 0)
                        {
                            responseEntity.IsComplete = false;
                            responseEntity.Description = "Start Number cannot be zero";
                            return Ok(responseEntity);
                        }
                    }
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@DocumentNoID", SqlDbType.Int, 9);
                sqlParameter.Value = documentnoID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@FinancialYear", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objDocumentNoEntity.FinancialYear;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.NVarChar, 250);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsActive", SqlDbType.NVarChar, 250);
                sqlParameter.Value = 1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.NVarChar, 250);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblDocumentNoMasterAddUpdate", ref sqlParameters);

                Int64 cnt = 0;
                if (_actionState.Equals("New"))
                {
                    string SQLQuery = "Select MAX(DocumentNoID) AS DocumentNoID from PFT_tblDocumentNoMaster where IsDelete = 0 AND IsActive = 1";
                    DataTable dtData = objDAL.FetchRecords(SQLQuery);
                    documentnoID = CommonUtils.ConvertToInt64(dtData.Rows[0]["DocumentNoID"]);
                }

                foreach (var objDocumentNoDetails in objDocumentNoEntity.DocumentNoDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@DocumentNoDetailsID", SqlDbType.Int, 9);
                    sqlParameter.Value = objDocumentNoDetails.DocumentNoDetailsID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@DocumentNoID", SqlDbType.Int, 9);
                    sqlParameter.Value = documentnoID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@DocumentID", SqlDbType.Int, 9);
                    sqlParameter.Value = objDocumentNoDetails.DocumentID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@StartNumber", SqlDbType.Int, 9);
                    sqlParameter.Value = objDocumentNoDetails.StartNumber;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@EndNumber", SqlDbType.Int, 9);
                    sqlParameter.Value = objDocumentNoDetails.EndNumber;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@CurrentSequence", SqlDbType.Int, 9);
                    sqlParameter.Value = objDocumentNoDetails.CurrentSequence;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);

                    if (objDocumentNoDetails.DocumentNoDetailsID == 0)
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                        sqlParameter.Value = 1;
                        sqlParameters.Add(sqlParameter);
                    }
                    else
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                        sqlParameter.Value = 0;
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("spPFT_tblDocumentNoDetailsAddUpdate", ref sqlParameters);
                }
                //if (_actionState.Equals("New"))
                //{
                //    sqlParameters = new List<SqlParameter>();

                //    sqlParameter = new SqlParameter("@DocumentNoID", SqlDbType.Int, 9);
                //    sqlParameter.Value = documentnoID;
                //    sqlParameters.Add(sqlParameter);

                //    sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                //    sqlParameter.Value = _userId;
                //    sqlParameters.Add(sqlParameter);

                //    objDAL.ExecuteSP("spPFT_InsertDocuments", ref sqlParameters);
                //}
                //string strQury = "DELETE FROM PFT_tblDocumentNoDetails WHERE DocumentID = " + objDocumentNoDetails.DocumentID + "AND DocumentNoID = " + documentnoID + " AND StartNumber IS NULL AND EndNumber IS NULL";
                //objDAL.ExecuteSQL(strQury);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Document Number Saved Successfully.";

                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DocumentNoController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                //objDAL.CloseConnection();
            }
        }

        public bool Validate(string FinancialYear)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(FinancialYear) As CNT From PFT_tblDocumentNoMaster Where FinancialYear = '" + FinancialYear + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "IssuerController");
                return false;
            }
        }

        public Int64 GetFinancialYearID(string FinancialYear)
        {
            try
            {
                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;
                Int64 DocumentNoID = 0;

                objDAL.CreateConnection();
                string strQuery = "";

                strQuery = @"Select DocumentNoID From PFT_tblDocumentNoMaster where FinancialYear = '" + FinancialYear + "' AND IsDelete = 0";

                DataTable dtData = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    DocumentNoID = CommonUtils.ConvertToInt64(dtData.Rows[0]["DocumentNoID"]);
                }

                return DocumentNoID;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DocumentNoController");
                return 0;
            }
        }

        #endregion

        #region Delete/Inactive Documents

        [HttpPost]
        public IHttpActionResult DeleteFinancialYear(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                DocumentNoEntity docEntity = new DocumentNoEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();

                    Parameters.Add(new SqlParameter("@DocumentNoID", RequestId));
                    Parameters.Add(new SqlParameter("@Icreater", UserId));

                    objDAL.ExecuteSP("spPFT_tblDocumentNoMasterDelete", ref Parameters);
                }

                return Ok(docEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "DocumentNoController");
                return BadRequest(x.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult DeleteDocumentInYear(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                DocumentNoEntity docEntity = new DocumentNoEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();

                    Parameters.Add(new SqlParameter("@DocumentID", RequestId));
                    Parameters.Add(new SqlParameter("@DocumentNoID", objActionEntity.Param1));

                    objDAL.ExecuteSP("spPFT_tblDocumentNoDetailsDelete", ref Parameters);
                }

                return Ok(docEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "DocumentNoController");
                return BadRequest(x.Message);
            }
        }

        #endregion
    }
}
