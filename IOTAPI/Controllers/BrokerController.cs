﻿using PFTrustAPI.Models;
using Newtonsoft.Json;
using PFTrustAPI.Controllers.CommonUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class BrokerController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        #region Broker List

        [HttpPost]
        public IHttpActionResult GetBrokerList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = @"Select BrokerID,BrokerCode,BrokerName,Address1,Address2,Address3,PinCode,City,State,ContactName,Mobile,Telephone,Email,Designation,IsDelete
                                    from PFT_tblBrokerMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "BrokerController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult GetBrokerById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                BrokerEntity queryEntity = new BrokerEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 QueryRequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    QueryRequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                return Ok(queryEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "BrokerController");
                return BadRequest(ex.Message);
            }
        }

        private BrokerEntity ShowRecord(Int64 BrokerID)
        {
            BrokerEntity obj = new BrokerEntity();
            string SQLQuery = "select * from PFT_tblBrokerMaster where BrokerID=" + BrokerID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.BrokerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["BrokerID"]);
                obj.BrokerCode = CommonUtils.ConvertToString(dtData.Rows[0]["BrokerCode"]);
                obj.BrokerName = CommonUtils.ConvertToString(dtData.Rows[0]["BrokerName"]);
                obj.Address1 = CommonUtils.ConvertToString(dtData.Rows[0]["Address1"]);
                obj.Address2 = CommonUtils.ConvertToString(dtData.Rows[0]["Address2"]);
                obj.Address3 = CommonUtils.ConvertToString(dtData.Rows[0]["Address3"]);
                obj.PinCode = CommonUtils.ConvertToString(dtData.Rows[0]["PinCode"]);
                obj.City = CommonUtils.ConvertToString(dtData.Rows[0]["City"]);
                obj.State = CommonUtils.ConvertToString(dtData.Rows[0]["State"]);
                obj.ContactName = CommonUtils.ConvertToString(dtData.Rows[0]["ContactName"]);
                obj.Mobile = CommonUtils.ConvertToString(dtData.Rows[0]["Mobile"]);
                obj.Telephone = CommonUtils.ConvertToString(dtData.Rows[0]["Telephone"]);
                obj.Email = CommonUtils.ConvertToString(dtData.Rows[0]["Email"]);
                obj.Designation = CommonUtils.ConvertToString(dtData.Rows[0]["Designation"]);
                obj.PAN = CommonUtils.ConvertToString(dtData.Rows[0]["PAN"]);
                obj.TAN = CommonUtils.ConvertToString(dtData.Rows[0]["TAN"]);
                obj.BrokerageRate = CommonUtils.ConvertToDecimal(dtData.Rows[0]["BrokerageRate"]);
                obj.BankName = CommonUtils.ConvertToString(dtData.Rows[0]["BankName"]);
                obj.BranchName = CommonUtils.ConvertToString(dtData.Rows[0]["BranchName"]);
                obj.IFSCCode = CommonUtils.ConvertToString(dtData.Rows[0]["IFSCCode"]);
                obj.AccountNo = CommonUtils.ConvertToString(dtData.Rows[0]["AccountNo"]);
                obj.CSGLNo = CommonUtils.ConvertToString(dtData.Rows[0]["CSGLNo"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);

                string SQuery = "select * from PFT_tblBrokerMasterDetails where BrokerID=" + BrokerID;
                DataSet dt = objDAL.FetchDataSetRecords(SQuery);
                obj.BrokerDetailsList = new List<BrokerDetailsEntity>();

                if (dt.Tables[0] != null && dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dt.Tables[0].Rows)
                    {
                        BrokerDetailsEntity objBrokerDetails = new BrokerDetailsEntity();

                        objBrokerDetails.BrokerDPID = CommonUtils.ConvertToInt(dROW["BrokerDPID"]);
                        objBrokerDetails.BrokerID = BrokerID;
                        objBrokerDetails.DPCode = CommonUtils.ConvertToString(dROW["DPCode"]);
                        objBrokerDetails.Description = CommonUtils.ConvertToString(dROW["Description"]);
                        objBrokerDetails.DPID = CommonUtils.ConvertToString(dROW["DPID"]);
                        objBrokerDetails.Preference = CommonUtils.ConvertToBoolean(dROW["Preference"]);

                        obj.BrokerDetailsList.Add(objBrokerDetails);
                    }
                }
            }

            return obj;
        }

        #endregion

        #region Delete Broker

        [HttpPost]
        public IHttpActionResult DeleteBroker(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                BrokerEntity brokerEntity = new BrokerEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();
                    Parameters.Add(new SqlParameter("@BrokerID", RequestId));
                    Parameters.Add(new SqlParameter("@ICreater", UserId));

                    objDAL.ExecuteSP("spPFT_tblBrokerMasterDelete", ref Parameters);
                }

                return Ok(brokerEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "BrokerController");
                return BadRequest(x.Message);
            }
        }

        #endregion

        #region Save Broker

        [HttpPost]
        public IHttpActionResult SaveBrokerMaster(BrokerEntity objBrokerEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 brokerID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objBrokerEntity.actionEntity != null)
                    _actionState = objBrokerEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objBrokerEntity.BrokerCode);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Broker Code Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    brokerID = objBrokerEntity.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@BrokerID", SqlDbType.Int, 9);
                sqlParameter.Value = brokerID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BrokerCode", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objBrokerEntity.BrokerCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BrokerName", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objBrokerEntity.BrokerName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address1", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objBrokerEntity.Address1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address2", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objBrokerEntity.Address2;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address3", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objBrokerEntity.Address3;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PinCode", SqlDbType.NVarChar, 6);
                sqlParameter.Value = objBrokerEntity.PinCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@City", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objBrokerEntity.City;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@State", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objBrokerEntity.State;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ContactName", SqlDbType.NVarChar, 255);
                sqlParameter.Value = objBrokerEntity.ContactName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Mobile", SqlDbType.NVarChar, 10);
                sqlParameter.Value = objBrokerEntity.Mobile;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Telephone", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objBrokerEntity.Telephone;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Email", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objBrokerEntity.Email;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Designation", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objBrokerEntity.Designation;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PAN", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objBrokerEntity.PAN;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@TAN", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objBrokerEntity.TAN;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BrokerageRate", SqlDbType.Decimal, 6);
                sqlParameter.Value = objBrokerEntity.BrokerageRate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BankName", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objBrokerEntity.BankName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BranchName", SqlDbType.NVarChar, 50);
                sqlParameter.Value = objBrokerEntity.BranchName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IFSCCode", SqlDbType.NVarChar, 15);
                sqlParameter.Value = objBrokerEntity.IFSCCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@AccountNo", SqlDbType.NVarChar, 30);
                sqlParameter.Value = objBrokerEntity.AccountNo;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@CSGLNo", SqlDbType.NVarChar, 50);
                sqlParameter.Value = objBrokerEntity.CSGLNo;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblBrokerMasterAddUpdate", ref sqlParameters);

                Int64 cnt = 0;
                if (_actionState.Equals("New"))
                {
                    string SQLQuery = "Select MAX(BrokerID) AS BrokerID from PFT_tblBrokerMaster where IsDelete=0";
                    DataTable dtData = objDAL.FetchRecords(SQLQuery);
                    brokerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["BrokerID"]);
                }

                if (_actionState.Equals("Edit"))
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@BrokerID", SqlDbType.Int, 9);
                    sqlParameter.Value = brokerID;
                    sqlParameters.Add(sqlParameter);

                    objDAL.ExecuteSP("spPFT_tblBrokerMasterDetailsDelete", ref sqlParameters);
                }

                List<BrokerDetailsEntity> objBrokerDetailsList = new List<BrokerDetailsEntity>();

                foreach (var objBrokerDetails in objBrokerEntity.BrokerDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@BrokerDPID", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@BrokerID", SqlDbType.Int, 9);
                    sqlParameter.Value = brokerID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@DPCode", SqlDbType.NVarChar, 20);
                    sqlParameter.Value = objBrokerDetails.DPCode;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Description", SqlDbType.NVarChar, 250);
                    sqlParameter.Value = objBrokerDetails.Description;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@DPID", SqlDbType.NVarChar, 30);
                    sqlParameter.Value = objBrokerDetails.DPID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Preference", SqlDbType.Bit, 1);
                    sqlParameter.Value = objBrokerDetails.Preference;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);

                    objDAL.ExecuteSP("spPFT_tblBrokerMasterDetailsAdd", ref sqlParameters);
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Broker Saved Successfully.";

                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "BrokerController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                //objDAL.CloseConnection();
            }
        }

        public bool Validate(string BrokerCode)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(BrokerCode) As CNT From PFT_tblBrokerMaster Where BrokerCode = '" + BrokerCode + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "BrokerController");
                return false;
            }
        }

        #endregion
    }
}
