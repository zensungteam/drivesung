﻿using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using PFTrustAPI.Models.DPMaster;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class DPController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        [HttpPost]
        public IHttpActionResult GetDPMasterList()
        {
            UserAuthentication auth = new UserAuthentication();
            Sessions session = new Sessions();
            SessionEntity objSessionEntity = session.GetSessionEntity();
            DPMasterEntity objDPMasterEntity = new DPMasterEntity();
            try
            {
                auth.CheckPageSecurity("~/dpMaster/dpmasterlist.html");
                objDPMasterEntity.dtdpList = GetDPMasterListGrid();
                return Ok(objDPMasterEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "DPController");
                return BadRequest(ex.Message);
            }
        }

        public DataTable GetDPMasterListGrid()
        {
            DataTable dtDPMasterList = null;
            try
            {
                BaseDAL objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;
                string StrWhrCond = string.Empty;
                string SQLQuery = "";
                SQLQuery = "select * from PFT_tblwDPMaster where IsDelete=0 and IsActive=1 order by Id";
                objDAL.CreateConnection();
                dtDPMasterList = objDAL.FetchRecords(SQLQuery);
                objDAL.CloseConnection();
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "DPController");
                throw ex;
            }
            return dtDPMasterList;
        }

        [HttpPost]
        public IHttpActionResult getDPMasterDetails(ActionEntity objActionEntity)
        {
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/dpMaster/dpmasterlist.html");

                DPMasterEntity objDPMasterEntity = new DPMasterEntity();
                BaseDAL objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;
                string strQuery = "";
                string errorMsg = "";
                Int64 Id = 0;
                Int64 maxId = 0;

                objDPMasterEntity.responseEntity = new ResponseEntity();
               Id = CommonUtils.ConvertToInt(objActionEntity.RecordId);

                if (objActionEntity.ActionState == "New")
                {
                    Id = 0;
                    objDPMasterEntity.responseEntity.IsComplete = true;
                    objDPMasterEntity.Idate = DateTime.Now;
                    strQuery = " SELECT ISNULL(MAX(Id),0) + 1 AS [MaxId] FROM PFT_tblwDPMaster where IsDelete=0 and IsActive=1";
                    objDAL.CreateConnection();
                    DataTable dtGetMax = objDAL.FetchRecords(strQuery);
                    objDAL.CloseConnection();

                    if (dtGetMax != null && dtGetMax.Rows.Count > 0)
                    {
                        Id = CommonUtils.ConvertToInt64(dtGetMax.Rows[0]["MaxId"]);
                    }

                    objDPMasterEntity = ShowRecord(Id, objActionEntity.ActionState);

                }
                else if (objActionEntity.ActionState == "Edit")
                {
                    objDPMasterEntity = ShowRecord(Id, objActionEntity.ActionState);
                }
                else if (objActionEntity.ActionState == "View")
                {
                    objDPMasterEntity = ShowRecord(Id, objActionEntity.ActionState);
                }

                return Ok(objDPMasterEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "DPController");
                return BadRequest(ex.Message);
            }
        }

        private DPMasterEntity ShowRecord(Int64 Id, string actionState)
        {
            Sessions session = new Sessions();
            DPMasterEntity objDPMasterEntity = new DPMasterEntity();

            string strQuery = "";
            BaseDAL objDAL = new BaseDAL();
            strQuery = " Select Id, DPCode, DPName, DPID From PFT_tblwDPMaster where Id = " + Id + " and IsDelete=0 and IsActive=1";
            objDAL.CreateConnection();
            DataSet dsDPMaster = objDAL.FetchDataSetRecords(strQuery);
            objDAL.CloseConnection();

            objDPMasterEntity.Id = Id;
            if (dsDPMaster != null && dsDPMaster.Tables.Count > 0)
            {
                if (dsDPMaster.Tables[0] != null && dsDPMaster.Tables[0].Rows.Count > 0)
                {
                    objDPMasterEntity.Id = CommonUtils.ConvertToInt64(dsDPMaster.Tables[0].Rows[0]["Id"]);
                    objDPMasterEntity.DPCode = CommonUtils.ConvertToString(dsDPMaster.Tables[0].Rows[0]["DPCode"]);
                    objDPMasterEntity.DPName = CommonUtils.ConvertToString(dsDPMaster.Tables[0].Rows[0]["DPName"]);
                    objDPMasterEntity.DPID = CommonUtils.ConvertToInt(dsDPMaster.Tables[0].Rows[0]["DPID"]);
                    //objDPMasterEntity.DPID = CommonUtils.ConvertToString(dsDPMaster.Tables[0].Rows[0]["DPID"]);

                }

            }
            return objDPMasterEntity;
        }

        [HttpPost]
        public IHttpActionResult deleteDPMasterRecord(ActionEntity objActionEntity)
        {
            DPMasterEntity objDPMasterEntity = new DPMasterEntity();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/dpMaster/dpmasterlist.html");

                objDPMasterEntity.responseEntity = new ResponseEntity();
                BaseDAL objDAL = new BaseDAL();

                SqlParameter sqlParameter;
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                string strQuery = "";

                objDPMasterEntity.responseEntity.IsComplete = false;
                if (objActionEntity == null)
                {
                    objDPMasterEntity.responseEntity.IsComplete = false;
                    objDPMasterEntity.responseEntity.Description = "Required data to remove is not found.";
                    return Ok(objDPMasterEntity);
                }

                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();

                Int64 Id = 0;
                Id = objActionEntity.RecordId;

                if (Id > 0)
                {
                    objDAL.CreateConnection();
                    objDAL.BeginTransaction();

                    strQuery = "Update PFT_tblwDPMaster set IsDelete=1 where Id=" + Id + "";
                    objDAL.ExecuteSQL(strQuery);

                    objDAL.CommitTransaction();
                    objDAL.CloseConnection();

                    objDPMasterEntity.responseEntity.IsComplete = true;
                    objDPMasterEntity.responseEntity.Description = "DP Master Record Deleted Successfully.";

                    return Ok(objDPMasterEntity);
                }
                else
                {
                    objDPMasterEntity.responseEntity.Description = "Required data to remove is not found.";
                }
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DPController");
                return BadRequest(ex.Message);
            }
            return Ok(objDPMasterEntity);
        }

        [HttpPost]
        public IHttpActionResult saveDPMasterRecord(DPMasterEntity objDPMasterEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/dpMaster/dpmasterlist.html");

                ResponseEntity responseEntity = ValidateDPMaster(ref objDPMasterEntity);
                if (responseEntity != null && responseEntity.IsComplete == false)
                    return Ok(responseEntity);

                responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string strQuery = "";
                string _actionState = "";
                Int64 Id = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                SqlParameter sqlParameter;

                if (objDPMasterEntity.actionEntity != null)
                    _actionState = objDPMasterEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    strQuery = "SELECT ISNULL(MAX(Id),0) + 1 AS [MaxId] FROM PFT_tblwDPMaster where IsDelete=0 and IsActive=1";
                    objDAL.CreateConnection();
                    DataSet dsGetMax = objDAL.FetchDataSetRecords(strQuery);
                    objDAL.CloseConnection();

                    if (dsGetMax != null && dsGetMax.Tables.Count > 0)
                    {
                        if (dsGetMax.Tables[0] != null && dsGetMax.Tables[0].Rows.Count > 0)
                        {
                            Id = CommonUtils.ConvertToInt64(dsGetMax.Tables[0].Rows[0]["MaxId"]);
                        }
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    Id = objDPMasterEntity.Id;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@Id", SqlDbType.Int, 9);
                sqlParameter.Value = Id;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DPCode", SqlDbType.VarChar, 20);
                sqlParameter.Value = objDPMasterEntity.DPCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DPName", SqlDbType.VarChar, 250);
                sqlParameter.Value = objDPMasterEntity.DPName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@DPID", SqlDbType.Int, 9);
                sqlParameter.Value = objDPMasterEntity.DPID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Icreator", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                sqlParameter.Value = 1;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 20);
                    sqlParameter.Value = "add";
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 20);
                    sqlParameter.Value = "edit";
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("PFT_SPtblwDPMaster_addedit", ref sqlParameters);
                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "DP Master Record Save Successfully.";
                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "DPController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                objDAL.CloseConnection();
            }
        }

        private ResponseEntity ValidateDPMaster(ref DPMasterEntity objDPMasterEntity)
        {
            string _actionState = "";
            string strQuery = "";
            ResponseEntity objResponseEntity = new ResponseEntity();
            BaseDAL objDAL = new BaseDAL();
            objResponseEntity.IsComplete = true;

            if (objDPMasterEntity == null)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please enter at least one Record.";
            }

            if (objDPMasterEntity.actionEntity != null)
                _actionState = objDPMasterEntity.actionEntity.ActionState;

            if (_actionState.Equals("New"))
            {
                strQuery = " SELECT * from PFT_tblwDPMaster where DPCode='" + objDPMasterEntity.DPCode + "' and IsDelete=0 and IsActive=1";
            }
            else if (_actionState.Equals("Edit"))
            {
                strQuery = " SELECT * from PFT_tblwDPMaster where DPCode='" + objDPMasterEntity.DPCode + "' and Id !=" + objDPMasterEntity.Id + " and IsDelete=0 and IsActive=1";
            }

            objDAL.CreateConnection();
            DataSet dsDPMaster = objDAL.FetchDataSetRecords(strQuery);
            objDAL.CloseConnection();
            if (dsDPMaster.Tables[0].Rows.Count > 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "DP Code already Exist";
                return objResponseEntity;
            }
            return objResponseEntity;
        }

    }
}