﻿using PFTrustAPI.Models;
using Newtonsoft.Json;
using PFTrustAPI.Controllers.CommonUtility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class IssuerController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;

        #region Issuer Master List

        [HttpPost]
        public IHttpActionResult GetIssuerList(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = "Select * from PFT_tblIssuerMaster where IsDelete = 0";
                DataTable dtGi = objDAL.FetchRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dtGi);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "IssuerController");
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IHttpActionResult GetIssuerById(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                IssuerEntity queryEntity = new IssuerEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 QueryRequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    QueryRequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Edit")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                else if (objActionEntity.ActionState == "View")
                {
                    queryEntity = ShowRecord(QueryRequestId);
                }

                return Ok(queryEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return BadRequest(ex.Message);
            }
        }

        private IssuerEntity ShowRecord(Int64 IssuerID)
        {
            IssuerEntity obj = new IssuerEntity();
            string SQLQuery = "select * from PFT_tblIssuerMaster where IssuerID=" + IssuerID;
            DataTable dtData = objDAL.FetchRecords(SQLQuery);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                obj.IssuerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["IssuerID"]);
                obj.IssuerCode = CommonUtils.ConvertToString(dtData.Rows[0]["IssuerCode"]);
                obj.IssuerName = CommonUtils.ConvertToString(dtData.Rows[0]["IssuerName"]);
                obj.Address1 = CommonUtils.ConvertToString(dtData.Rows[0]["Address1"]);
                obj.Address2 = CommonUtils.ConvertToString(dtData.Rows[0]["Address2"]);
                obj.Address3 = CommonUtils.ConvertToString(dtData.Rows[0]["Address3"]);
                obj.PinCode = CommonUtils.ConvertToString(dtData.Rows[0]["PinCode"]);
                obj.City = CommonUtils.ConvertToString(dtData.Rows[0]["City"]);
                obj.State = CommonUtils.ConvertToString(dtData.Rows[0]["State"]);
                obj.ContactName = CommonUtils.ConvertToString(dtData.Rows[0]["ContactName"]);
                obj.Mobile = CommonUtils.ConvertToString(dtData.Rows[0]["Mobile"]);
                obj.Telephone = CommonUtils.ConvertToString(dtData.Rows[0]["Telephone"]);
                obj.Email = CommonUtils.ConvertToString(dtData.Rows[0]["Email"]);
                obj.Designation = CommonUtils.ConvertToString(dtData.Rows[0]["Designation"]);
                obj.IsDelete = CommonUtils.ConvertToBoolean(dtData.Rows[0]["IsDelete"]);

                string SQuery = "select * from PFT_tblIssuerMasterDetails where IssuerID=" + IssuerID;
                DataSet dt = objDAL.FetchDataSetRecords(SQuery);
                obj.IssuerDetailsList = new List<int>();
                obj.RegistarsList = new List<int>();

                if (dt.Tables[0] != null && dt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dt.Tables[0].Rows)
                    {
                        if (CommonUtils.ConvertToInt(dROW["GuaranteeInstitutionID"]) != 0)
                            obj.IssuerDetailsList.Add(CommonUtils.ConvertToInt(dROW["GuaranteeInstitutionID"]));
                        if (CommonUtils.ConvertToInt(dROW["RegistrarID"]) != 0)
                            obj.RegistarsList.Add(CommonUtils.ConvertToInt(dROW["RegistrarID"]));
                    }
                }
            }

            return obj;
        }

        #endregion

        #region Save Issuer Master & Details

        [HttpPost]
        public IHttpActionResult SaveIssuerMaster(IssuerEntity objIssuerEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string _actionState = "";
                Int64 issuerID = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                //clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
                SqlParameter sqlParameter;

                if (objIssuerEntity.actionEntity != null)
                    _actionState = objIssuerEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    bool IsExist = Validate(objIssuerEntity.IssuerCode);
                    if (IsExist)
                    {
                        responseEntity.IsComplete = false;
                        responseEntity.Description = "Issuer Code Already Exists.";
                        return Ok(responseEntity);
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    issuerID = objIssuerEntity.actionEntity.RecordId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@IssuerID", SqlDbType.Int, 9);
                sqlParameter.Value = issuerID;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IssuerCode", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objIssuerEntity.IssuerCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IssuerName", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objIssuerEntity.IssuerName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address1", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objIssuerEntity.Address1;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address2", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objIssuerEntity.Address2;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Address3", SqlDbType.NVarChar, 250);
                sqlParameter.Value = objIssuerEntity.Address3;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@PinCode", SqlDbType.NVarChar, 6);
                sqlParameter.Value = objIssuerEntity.PinCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@City", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objIssuerEntity.City;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@State", SqlDbType.NVarChar, 150);
                sqlParameter.Value = objIssuerEntity.State;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ContactName", SqlDbType.NVarChar, 255);
                sqlParameter.Value = objIssuerEntity.ContactName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Mobile", SqlDbType.NVarChar, 10);
                sqlParameter.Value = objIssuerEntity.Mobile;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Telephone", SqlDbType.NVarChar, 20);
                sqlParameter.Value = objIssuerEntity.Telephone;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Email", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objIssuerEntity.Email;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Designation", SqlDbType.NVarChar, 70);
                sqlParameter.Value = objIssuerEntity.Designation;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("spPFT_tblIssuerMasterAddUpdate", ref sqlParameters);

                Int64 cnt = 0;
                if (_actionState.Equals("New"))
                {
                    string SQLQuery = "Select MAX(IssuerID) AS IssuerID from PFT_tblIssuerMaster where IsDelete=0";
                    DataTable dtData = objDAL.FetchRecords(SQLQuery);
                    issuerID = CommonUtils.ConvertToInt64(dtData.Rows[0]["IssuerID"]);
                }
                //List<Int32> lst = objIssuerEntity.IssuerDetailsList.Concat(objIssuerEntity.RegistarsList).ToList();

                List<IssuerDetailsEntity> objIssuerDetailsList = new List<IssuerDetailsEntity>();
                foreach (var guarantor in objIssuerEntity.IssuerDetailsList)
                {
                    objIssuerDetailsList.Add(new IssuerDetailsEntity { GuaranteeInstitutionID = guarantor });
                }

                foreach (var registrar in objIssuerEntity.RegistarsList)
                {
                    objIssuerDetailsList.Add(new IssuerDetailsEntity { RegistrarID = registrar });
                }

                if (_actionState.Equals("Edit"))
                {
                    List<SqlParameter> sqlParams = new List<SqlParameter>();
                    SqlParameter sqlParam;

                    sqlParam = new SqlParameter("@IssuerID", SqlDbType.Int, 9);
                    sqlParam.Value = issuerID;
                    sqlParams.Add(sqlParam);

                    objDAL.ExecuteSP("spPFT_tblIssuerMasterDetailsDelete", ref sqlParams);
                }

                foreach (var objIssuerDetails in objIssuerDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@IssuerDetailsID", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IssuerID", SqlDbType.Int, 9);
                    sqlParameter.Value = issuerID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@RegistrarID", SqlDbType.Int, 9);
                    sqlParameter.Value = objIssuerDetails.RegistrarID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@GuaranteeInstitutionID", SqlDbType.Int, 9);
                    sqlParameter.Value = objIssuerDetails.GuaranteeInstitutionID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit, 1);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@ICreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);

                    objDAL.ExecuteSP("spPFT_tblIssuerMasterDetailsAdd", ref sqlParameters);
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Issuer Saved Successfully.";

                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "IssuerController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                //objDAL.CloseConnection();
            }
        }

        public bool Validate(string IssuerCode)
        {
            BaseDAL objDAL = new BaseDAL();
            bool IsExist = true;

            try
            {
                string strQuery = "Select COUNT(IssuerCode) As CNT From PFT_tblIssuerMaster Where IssuerCode = '" + IssuerCode + "' AND IsDelete = 0";
                objDAL.CreateConnection();

                DataTable dtData = objDAL.FetchRecords(strQuery);

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    int Records = CommonUtils.ConvertToInt(dtData.Rows[0]["CNT"]);

                    if (Records > 0)
                    {
                        IsExist = true;
                    }
                    else
                    {
                        IsExist = false;
                    }
                }
                return IsExist;
            }
            catch (Exception x)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(x, "IssuerController");
                return false;
            }
        }

        #endregion

        #region Delete Issuer Master & Details

        [HttpPost]
        public IHttpActionResult DeleteIssuer(ActionEntity objActionEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                IssuerEntity issuerEntity = new IssuerEntity();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL = new BaseDAL();
                objDAL.CreateConnection();

                Int32 RequestId = 0;

                if (objActionEntity.RecordId != 0)
                {
                    RequestId = objActionEntity.RecordId;
                }

                if (objActionEntity.ActionState == "Delete")
                {
                    List<SqlParameter> Parameters = new List<SqlParameter>();
                    SqlParameter parameter = new SqlParameter();
                    Parameters.Add(new SqlParameter("@IssuerID", RequestId));

                    objDAL.ExecuteSP("spPFT_tblIssuerMasterDelete", ref Parameters);
                }

                return Ok(issuerEntity);
            }
            catch (Exception x)
            {
                WebUtils.ErrorHandler.HandleException(x, "IssuerController");
                return BadRequest(x.Message);
            }
        }

        #endregion

        #region Issuer Details

        //[HttpPost]
        //public IHttpActionResult getMedReimDetails(ActionEntity objActionEntity)
        //{
        //    try
        //    {
        //        UserAuthentication auth = new UserAuthentication();
        //        auth.CheckPageSecurity("~/CTC Reimb/ce_medical_reimbursement_list.aspx");

        //        ReimbDetailsSaveEntity objReimbDetailsSaveEntity = new ReimbDetailsSaveEntity();
        //        //ReimbDetailsEntity objReimbDetailsEntity = new ReimbDetailsEntity();
        //        clsRembDocumentSettingBO objclsRembDocumentSettingBO = new clsRembDocumentSettingBO();
        //        BaseDAL objDAL = new BaseDAL();

        //        Sessions session = new Sessions();
        //        SessionEntity sessionEntity = session.GetSessionEntity();
        //        Int32 UserId = sessionEntity.UserId;

        //        string strQuery = "";
        //        string errorMsg = "";
        //        Int64 medical_rem_ref_id = 0;
        //        Int64 maxMedicalRemNo = 0;

        //        objReimbDetailsSaveEntity.responseEntity = new ResponseEntity();
        //        medical_rem_ref_id = CommonUtils.ConvertToInt(objActionEntity.RecordId);

        //        if (objActionEntity.ActionState == "New")
        //        {
        //            medical_rem_ref_id = 0;
        //            objReimbDetailsSaveEntity.responseEntity.IsComplete = true;
        //            objReimbDetailsSaveEntity.submissionDate = DateTime.Now;
        //            if (objclsRembDocumentSettingBO.CheckReimbFromBucket(ref errorMsg) == false)
        //            {
        //                objReimbDetailsSaveEntity.responseEntity.IsComplete = false;
        //                objReimbDetailsSaveEntity.responseEntity.Description = errorMsg;
        //            }
        //            else
        //            {
        //                strQuery = " SELECT ISNULL(max(CONVERT(NUMERIC,medical_rem_no)),0) + 1 AS [MaxMedRemNo], ISNULL(MAX(medical_rem_ref_id),0) + 1 AS [MaxRemRefId] FROM tblwemp_medical_remb; ";
        //                objDAL.CreateConnection();
        //                DataTable dtGetMax = objDAL.FetchRecords(strQuery);
        //                objDAL.CloseConnection();

        //                if (dtGetMax != null && dtGetMax.Rows.Count > 0)
        //                {
        //                    maxMedicalRemNo = CommonUtils.ConvertToInt64(dtGetMax.Rows[0]["MaxMedRemNo"]);
        //                    medical_rem_ref_id = CommonUtils.ConvertToInt64(dtGetMax.Rows[0]["MaxRemRefId"]);
        //                }

        //                objReimbDetailsSaveEntity = ShowRecord(medical_rem_ref_id, UserId, objActionEntity.ActionState);

        //            }

        //        }
        //        else if (objActionEntity.ActionState == "Edit")
        //        {
        //            objReimbDetailsSaveEntity = ShowRecord(medical_rem_ref_id, UserId, objActionEntity.ActionState);
        //        }
        //        else if (objActionEntity.ActionState == "View")
        //        {
        //            objReimbDetailsSaveEntity = ShowRecord(medical_rem_ref_id, UserId, objActionEntity.ActionState);
        //        }

        //        return Ok(objReimbDetailsSaveEntity);
        //    }
        //    catch (Exception ex)
        //    {
        //        WebUtils.ErrorHandler.HandleException(ex, "ReimbushmentController");
        //        return BadRequest(ex.Message);
        //    }
        //}


        [HttpPost]
        public IHttpActionResult GetIssuerDetailsDropDown(FilterEntity filterEntity)
        {
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                objDAL.CreateConnection();

                string strQuery = @"Select GuaranteeInstitutionID,GuaranteeInstitutionCode from PFT_tblGuaranteeInstitutionMaster where IsDelete = 0;
                                    Select RegistrarID,RegistrarName from PFT_tblRegistrarMaster where IsDelete = 0";
                DataSet dt = objDAL.FetchDataSetRecords(strQuery);

                objDAL.CloseConnection();

                return Ok(dt);
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "GuaranteeInstitutionController");
                return BadRequest(ex.Message);
            }
        }


        #endregion
    }
}
