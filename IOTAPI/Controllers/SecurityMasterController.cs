﻿using PFTrustAPI.Controllers.CommonUtility;
using PFTrustAPI.Models;
using PFTrustAPI.Models.SecurityMaster;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PFTrustAPI.Controllers
{
    [Authorize]
    public class SecurityMasterController : ApiController
    {
        UserAuthentication auth = new UserAuthentication();
        BaseDAL objDAL;
        [HttpPost]
        public IHttpActionResult GetSecurityMasterList()
        {
            UserAuthentication auth = new UserAuthentication();
            Sessions session = new Sessions();
            SessionEntity objSessionEntity = session.GetSessionEntity();
            SecurityMasterEntity objSecurityMasterEntity = new SecurityMasterEntity();
            try
            {
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");
                objSecurityMasterEntity.dtsecurityMasterList = GetSecurityMasterListGrid();
                return Ok(objSecurityMasterEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "SecurityMasterController");
                return BadRequest(ex.Message);
            }
        }

        public DataTable GetSecurityMasterListGrid()
        {
            DataTable dtRatingMasterList = null;
            try
            {
                BaseDAL objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;

                string StrWhrCond = string.Empty;
                string SQLQuery = "";
                SQLQuery = "select * from PFT_tblwSecurityMaster where IsDelete=0 and IsActive=1 order by SecurityId";
                objDAL.CreateConnection();
                dtRatingMasterList = objDAL.FetchRecords(SQLQuery);
                objDAL.CloseConnection();
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "SecurityMasterController");
                throw ex;
            }
            return dtRatingMasterList;
        }

        [HttpPost]
        public IHttpActionResult getSecurityMasterDetails(ActionEntity objActionEntity)
        {
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");
                SecurityMasterEntity objSecurityMasterEntity = new SecurityMasterEntity();
                BaseDAL objDAL = new BaseDAL();
                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();
                Int32 UserId = sessionEntity.UserId;
                string strQuery = "";
                Int64 securityId = 0;

                objSecurityMasterEntity.responseEntity = new ResponseEntity();
                securityId = CommonUtils.ConvertToInt(objActionEntity.RecordId);

                if (objActionEntity.ActionState == "New")
                {
                    securityId = 0;
                    objSecurityMasterEntity.responseEntity.IsComplete = true;
                    objSecurityMasterEntity.Idate = DateTime.Now;
                    strQuery = " SELECT ISNULL(MAX(SecurityId),0) + 1 AS [MaxSecurityId] FROM PFT_tblwSecurityMaster";
                    objDAL.CreateConnection();
                    DataTable dtGetMax = objDAL.FetchRecords(strQuery);
                    objDAL.CloseConnection();

                    if (dtGetMax != null && dtGetMax.Rows.Count > 0)
                    {
                        securityId = CommonUtils.ConvertToInt64(dtGetMax.Rows[0]["MaxSecurityId"]);
                    }

                    objSecurityMasterEntity = ShowRecord(securityId, objActionEntity.ActionState);

                }
                else if (objActionEntity.ActionState == "Edit")
                {
                    objSecurityMasterEntity = ShowRecord(securityId, objActionEntity.ActionState);
                }
                else if (objActionEntity.ActionState == "View")
                {
                    objSecurityMasterEntity = ShowRecord(securityId, objActionEntity.ActionState);
                }

                return Ok(objSecurityMasterEntity);
            }
            catch (Exception ex)
            {
                WebUtils.ErrorHandler.HandleException(ex, "SecurityMasterController");
                return BadRequest(ex.Message);
            }
        }

        private SecurityMasterEntity ShowRecord(Int64 securityId, string actionState)
        {
            Sessions session = new Sessions();
            SecurityMasterEntity objSecurityMasterDetailsSaveEntity = new SecurityMasterEntity();
            string strQuery = "";
            BaseDAL objDAL = new BaseDAL();
            strQuery = " Select * From PFT_tblwSecurityMaster where SecurityId = " + securityId + " and IsDelete=0 and IsActive=1; ";
            strQuery += "select im.IssuerID as [IssuerId],im.IssuerCode as [IssuerCode] from PFT_tblwSecurityMaster sm,PFT_tblIssuerMaster im where sm.IssuerId=im.IssuerID and sm.IsDelete=0 and sm.IsActive=1 and sm.SecurityId=" + securityId + "; ";
            strQuery += @" select IssuerID as [value],IssuerCode as [text] from PFT_tblIssuerMaster where IsDelete=0 order by IssuerCode; ";
            strQuery += @" select distinct rm.RegistrarID as [value],rm.RegistrarCode as [text],im.IssuerID,im.IssuerCode from PFT_tblIssuerMaster im,PFT_tblRegistrarMaster rm,PFT_tblIssuerMasterDetails imd where im.IssuerID=imd.IssuerID and imd.RegistrarID=rm.RegistrarID and imd.IsDelete=0; ";
            strQuery += @" select distinct RatingCompId as [value],RatingCompName as [text] from PFT_tblwRating_Company_Master where IsActive=1 and IsDelete=0; ";
            strQuery += @" select rm.RatingId as [value],rm.RatingCode as [text],rm.RatingDescription as ratingDesc,rcm.RatingCompId,rcm.RatingCompName from PFT_tblwRatingMaster rm,PFT_tblwRating_Company_Master rcm,PFT_tblwRating_Company_Master_Details rcmd where rcm.RatingCompId=rcmd.RatingCompId and rm.RatingId=rcmd.RatingId and rm.IsActive=1; ";
            strQuery += @" select distinct gim.GuaranteeInstitutionID as [value],gim.GuaranteeInstitutionCode as [text],gim.InstitutionName,im.IssuerID,im.IssuerCode from PFT_tblIssuerMaster im, PFT_tblGuaranteeInstitutionMaster gim,PFT_tblIssuerMasterDetails imd where im.IssuerID=imd.IssuerID and imd.GuaranteeInstitutionID=gim.GuaranteeInstitutionID and imd.IsDelete=0; ";
            strQuery += @" select distinct sm.SecurityId,rcm.RatingCompId,rcm.RatingCompName,rm.RatingId,rm.RatingCode,rm.RatingDescription from PFT_tblwSecurityMaster sm,PFT_tblwRating_Company_Master rcm,PFT_tblwRatingMaster rm,PFT_tblwSecurityMaster_RatingDetails smrd where sm.SecurityId=smrd.SecurityId and smrd.IsActive=1 and rcm.RatingCompId=smrd.RatingCompId and rm.RatingId=smrd.RatingId and smrd.SecurityId=" + securityId + "; ";
            strQuery += @" select distinct sm.SecurityId,gim.GuaranteeInstitutionID,gim.GuaranteeInstitutionCode,gim.InstitutionName,smgd.Guarantee from PFT_tblwSecurityMaster sm,PFT_tblGuaranteeInstitutionMaster gim,PFT_tblwSecurityMaster_GuaranteeDetails smgd where sm.SecurityId=smgd.SecurityId and smgd.IsActive=1 and gim.GuaranteeInstitutionID=smgd.GuaranteeInstitutionID and smgd.SecurityId=" + securityId + "; ";
            strQuery += @" select distinct sm.SecurityId,smfd.Fromdate,smfd.Todate,smfd.RateOfInterest from PFT_tblwSecurityMaster sm,PFT_tblwSecurityMaster_FinantialDetails smfd where sm.SecurityId=smfd.SecurityId and smfd.IsActive=1 and smfd.SecurityId=" + securityId + "; ";
            strQuery += @" select distinct sm.SecurityId,smid.IssueDate,smid.Percentage,smid.Value,smid.Premium,smid.Discount,smid.NetCashFlow from PFT_tblwSecurityMaster sm,PFT_tblwSecurityMaster_IssueDetails smid where sm.SecurityId=smid.SecurityId and smid.IsActive=1 and  smid.SecurityId=" + securityId + "; ";
            strQuery += @" select distinct sm.SecurityId,smmd.MaturityDate,smmd.Percentage,smmd.Value,smmd.Premium,smmd.Discount,smmd.NetCashFlow from PFT_tblwSecurityMaster sm,PFT_tblwSecurityMaster_MaturityDetails smmd where sm.SecurityId=smmd.SecurityId and smmd.IsActive=1 and smmd.SecurityId=" + securityId + "; ";
            strQuery += @" select distinct sm.SecurityId,smpcd.PutCallDate,smpcd.PutCallType,smpcd.Percentage,smpcd.Value,smpcd.Premium,smpcd.Discount,smpcd.NetCashFlow from PFT_tblwSecurityMaster sm,PFT_tblwSecurityMaster_PutCallDetails smpcd where sm.SecurityId=smpcd.SecurityId and smpcd.IsActive=1 and smpcd.SecurityId=" + securityId + "; ";


            objDAL.CreateConnection();
            DataSet dsSecurityMasterDetails = objDAL.FetchDataSetRecords(strQuery);
            objDAL.CloseConnection();

            objSecurityMasterDetailsSaveEntity.SecurityId = securityId;
            if (dsSecurityMasterDetails != null && dsSecurityMasterDetails.Tables.Count > 0)
            {
                if (dsSecurityMasterDetails.Tables[0] != null && dsSecurityMasterDetails.Tables[0].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.SecurityId = CommonUtils.ConvertToInt64(dsSecurityMasterDetails.Tables[0].Rows[0]["SecurityId"]);
                    objSecurityMasterDetailsSaveEntity.SecurityCode = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["SecurityCode"]);
                    objSecurityMasterDetailsSaveEntity.ShortName = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["ShortName"]);
                    objSecurityMasterDetailsSaveEntity.SecurityDescription = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["SecurityDescription"]);
                    objSecurityMasterDetailsSaveEntity.ISIN = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["ISIN"]);
                    objSecurityMasterDetailsSaveEntity.FaceValue = CommonUtils.ConvertToDecimal(dsSecurityMasterDetails.Tables[0].Rows[0]["FaceValue"]);
                    objSecurityMasterDetailsSaveEntity.Pattern = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["Pattern"]);
                    objSecurityMasterDetailsSaveEntity.ReturnPattern = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["ReturnPattern"]);
                    objSecurityMasterDetailsSaveEntity.Type = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["Type"]);
                    objSecurityMasterDetailsSaveEntity.ReturnFrequency = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["ReturnFrequency"]);
                    objSecurityMasterDetailsSaveEntity.InCompleteMonths = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["InCompleteMonths"]);
                    objSecurityMasterDetailsSaveEntity.CompleteMonths = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["CompleteMonths"]);
                    objSecurityMasterDetailsSaveEntity.FirstInterestPmtDate = CommonUtils.ConvertToDateTime(dsSecurityMasterDetails.Tables[0].Rows[0]["FirstInterestPmtDate"]);
                    objSecurityMasterDetailsSaveEntity.BookClosureDate = CommonUtils.ConvertToDateTime(dsSecurityMasterDetails.Tables[0].Rows[0]["BookClosureDate"]);
                    objSecurityMasterDetailsSaveEntity.IssuerId = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["IssuerId"]);
                    objSecurityMasterDetailsSaveEntity.RegistrarId = CommonUtils.ConvertToString(dsSecurityMasterDetails.Tables[0].Rows[0]["RegistrarId"]);
                }
              
                if (dsSecurityMasterDetails.Tables.Count > 1 && dsSecurityMasterDetails.Tables[1] != null && dsSecurityMasterDetails.Tables[1].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.IssuerIds = dsSecurityMasterDetails.Tables[1];
                }
                if (dsSecurityMasterDetails.Tables.Count > 2 && dsSecurityMasterDetails.Tables[2] != null && dsSecurityMasterDetails.Tables[2].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.dtIssuerCodeList = dsSecurityMasterDetails.Tables[2];
                }
                if (dsSecurityMasterDetails.Tables.Count > 3 && dsSecurityMasterDetails.Tables[3] != null && dsSecurityMasterDetails.Tables[3].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.dtAllIssuerRegistrar = dsSecurityMasterDetails.Tables[3];
                }
                if (dsSecurityMasterDetails.Tables.Count > 4 && dsSecurityMasterDetails.Tables[4] != null && dsSecurityMasterDetails.Tables[4].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.dtRatingCompanyList = dsSecurityMasterDetails.Tables[4];
                }
                if (dsSecurityMasterDetails.Tables.Count > 5 && dsSecurityMasterDetails.Tables[5] != null && dsSecurityMasterDetails.Tables[5].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.dtAllRatingCompanyRatingCode = dsSecurityMasterDetails.Tables[5];
                }
                if (dsSecurityMasterDetails.Tables.Count > 6 && dsSecurityMasterDetails.Tables[6] != null && dsSecurityMasterDetails.Tables[6].Rows.Count > 0)
                {
                    objSecurityMasterDetailsSaveEntity.dtAllIssuerGuaranteeInstitution = dsSecurityMasterDetails.Tables[6];
                }

                //
                objSecurityMasterDetailsSaveEntity.SecurityRatingDetailsList = new List<SecurityRatingDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 7 && dsSecurityMasterDetails.Tables[7] != null && dsSecurityMasterDetails.Tables[7].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[7].Rows)
                    {
                        SecurityRatingDetailsEntity objratingDetailsEntity = new SecurityRatingDetailsEntity();
                        objratingDetailsEntity.ratingCompId = CommonUtils.ConvertToInt(dROW["RatingCompId"]);
                        objratingDetailsEntity.ratingId = CommonUtils.ConvertToInt(dROW["RatingId"]);
                        objratingDetailsEntity.ratingCompName = CommonUtils.ConvertToString(dROW["RatingCompName"]);
                        objratingDetailsEntity.ratingCode = CommonUtils.ConvertToString(dROW["RatingCode"]);
                        objratingDetailsEntity.secRatingCodeDescription = CommonUtils.ConvertToString(dROW["RatingDescription"]);                       
                        objSecurityMasterDetailsSaveEntity.SecurityRatingDetailsList.Add(objratingDetailsEntity);
                    }
                }

                objSecurityMasterDetailsSaveEntity.SecurityGuaranteeDetailsList = new List<SecurityGuaranteeDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 8 && dsSecurityMasterDetails.Tables[8] != null && dsSecurityMasterDetails.Tables[8].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[8].Rows)
                    {
                        SecurityGuaranteeDetailsEntity objguaranteeDetailsEntity = new SecurityGuaranteeDetailsEntity();
                        objguaranteeDetailsEntity.guaranteeInstitutionID = CommonUtils.ConvertToInt(dROW["GuaranteeInstitutionID"]);
                        objguaranteeDetailsEntity.guaranteeInstitutionCode = CommonUtils.ConvertToString(dROW["GuaranteeInstitutionCode"]);
                        objguaranteeDetailsEntity.secGuaranteeInstitutionName = CommonUtils.ConvertToString(dROW["InstitutionName"]);
                        objguaranteeDetailsEntity.secInstitutionGuarantee = CommonUtils.ConvertToDecimal(dROW["Guarantee"]);
                        objSecurityMasterDetailsSaveEntity.SecurityGuaranteeDetailsList.Add(objguaranteeDetailsEntity);
                    }
                }

                objSecurityMasterDetailsSaveEntity.SecurityFinantialDetailsList = new List<SecurityFinantialDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 9 && dsSecurityMasterDetails.Tables[9] != null && dsSecurityMasterDetails.Tables[9].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[9].Rows)
                    {
                        SecurityFinantialDetailsEntity objfinantialDetailsEntity = new SecurityFinantialDetailsEntity();
                        objfinantialDetailsEntity.secfdFromDate = CommonUtils.ConvertToDateTime(dROW["Fromdate"]);
                        objfinantialDetailsEntity.secfdToDate = CommonUtils.ConvertToDateTime(dROW["Todate"]);
                        objfinantialDetailsEntity.secfdRateOfInterest = CommonUtils.ConvertToDecimal(dROW["RateOfInterest"]);
                        objSecurityMasterDetailsSaveEntity.SecurityFinantialDetailsList.Add(objfinantialDetailsEntity);
                    }
                }

                objSecurityMasterDetailsSaveEntity.SecurityIssueDetailsList = new List<SecurityIssueDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 10 && dsSecurityMasterDetails.Tables[10] != null && dsSecurityMasterDetails.Tables[10].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[10].Rows)
                    {
                        SecurityIssueDetailsEntity objIssueDetailsEntity = new SecurityIssueDetailsEntity();
                        objIssueDetailsEntity.secIdIssueDate = CommonUtils.ConvertToDateTime(dROW["IssueDate"]);
                        objIssueDetailsEntity.secIdPercentage = CommonUtils.ConvertToDecimal(dROW["Percentage"]);
                        objIssueDetailsEntity.secIdValue = CommonUtils.ConvertToDecimal(dROW["Value"]);
                        objIssueDetailsEntity.secIdPremium = CommonUtils.ConvertToDecimal(dROW["Premium"]);
                        objIssueDetailsEntity.secIdDiscount = CommonUtils.ConvertToDecimal(dROW["Discount"]);
                        objIssueDetailsEntity.secIdNetCashFlow = CommonUtils.ConvertToDecimal(dROW["NetCashFlow"]);
                        objSecurityMasterDetailsSaveEntity.SecurityIssueDetailsList.Add(objIssueDetailsEntity);
                    }
                }

                objSecurityMasterDetailsSaveEntity.SecurityMaturityDetailsList = new List<SecurityMaturityDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 11 && dsSecurityMasterDetails.Tables[11] != null && dsSecurityMasterDetails.Tables[11].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[11].Rows)
                    {
                        SecurityMaturityDetailsEntity objMaturityDetailsEntity = new SecurityMaturityDetailsEntity();
                        objMaturityDetailsEntity.secMdMaturityDate = CommonUtils.ConvertToDateTime(dROW["MaturityDate"]);
                        objMaturityDetailsEntity.secMdPercentage = CommonUtils.ConvertToDecimal(dROW["Percentage"]);
                        objMaturityDetailsEntity.secMdValue = CommonUtils.ConvertToDecimal(dROW["Value"]);
                        objMaturityDetailsEntity.secMdPremium = CommonUtils.ConvertToDecimal(dROW["Premium"]);
                        objMaturityDetailsEntity.secMdDiscount = CommonUtils.ConvertToDecimal(dROW["Discount"]);
                        objMaturityDetailsEntity.secMdNetCashFlow = CommonUtils.ConvertToDecimal(dROW["NetCashFlow"]);
                        objSecurityMasterDetailsSaveEntity.SecurityMaturityDetailsList.Add(objMaturityDetailsEntity);
                    }
                }

                objSecurityMasterDetailsSaveEntity.SecurityPutCallDetailsList = new List<SecurityPutCallDetailsEntity>();
                if (dsSecurityMasterDetails.Tables.Count > 12 && dsSecurityMasterDetails.Tables[12] != null && dsSecurityMasterDetails.Tables[12].Rows.Count > 0)
                {
                    foreach (DataRow dROW in dsSecurityMasterDetails.Tables[12].Rows)
                    {
                        SecurityPutCallDetailsEntity objPutCallDetailsEntity = new SecurityPutCallDetailsEntity();
                        objPutCallDetailsEntity.secpcdPutCallDate = CommonUtils.ConvertToDateTime(dROW["PutCallDate"]);
                        objPutCallDetailsEntity.secpcdPutCallType = CommonUtils.ConvertToString(dROW["PutCallType"]);
                        objPutCallDetailsEntity.secpcdPercentage = CommonUtils.ConvertToDecimal(dROW["Percentage"]);
                        objPutCallDetailsEntity.secpcdValue = CommonUtils.ConvertToDecimal(dROW["Value"]);
                        objPutCallDetailsEntity.secpcdPremium = CommonUtils.ConvertToDecimal(dROW["Premium"]);
                        objPutCallDetailsEntity.secpcdDiscount = CommonUtils.ConvertToDecimal(dROW["Discount"]);
                        objPutCallDetailsEntity.secpcdNetCashFlow = CommonUtils.ConvertToDecimal(dROW["NetCashFlow"]);
                        objSecurityMasterDetailsSaveEntity.SecurityPutCallDetailsList.Add(objPutCallDetailsEntity);
                    }
                }


            }
            return objSecurityMasterDetailsSaveEntity;
        }

        [HttpPost]
        public IHttpActionResult SaveSecurityMasterRecord(SecurityMasterEntity objSecurityMasterDetailsSaveEntity)
        {
            Sessions session = new Sessions();
            SessionEntity sessionEntity = session.GetSessionEntity();
            Int64 _userId = sessionEntity.UserId;
            BaseDAL objDAL = new BaseDAL();
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                ResponseEntity responseEntity = ValidateSecurityMaster(ref objSecurityMasterDetailsSaveEntity);
                if (responseEntity != null && responseEntity.IsComplete == false)
                    return Ok(responseEntity);

                responseEntity = new ResponseEntity();
                responseEntity.IsComplete = false;

                string strQuery = "";
                string _actionState = "";
                Int64 SecurityId = 0;

                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                SqlParameter sqlParameter;

                if (objSecurityMasterDetailsSaveEntity.actionEntity != null)
                    _actionState = objSecurityMasterDetailsSaveEntity.actionEntity.ActionState;

                if (_actionState.Equals("New"))
                {
                    strQuery = " SELECT ISNULL(MAX(SecurityId),0) + 1 AS [SecurityId] FROM PFT_tblwSecurityMaster";
                    objDAL.CreateConnection();
                    DataSet dsGetMax = objDAL.FetchDataSetRecords(strQuery);
                    objDAL.CloseConnection();

                    if (dsGetMax != null && dsGetMax.Tables.Count > 0)
                    {
                        if (dsGetMax.Tables[0] != null && dsGetMax.Tables[0].Rows.Count > 0)
                        {
                            SecurityId = CommonUtils.ConvertToInt64(dsGetMax.Tables[0].Rows[0]["SecurityId"]);
                        }
                    }
                }
                else if (_actionState.Equals("Edit"))
                {
                    SecurityId = objSecurityMasterDetailsSaveEntity.SecurityId;
                }

                // Transition Is Started....
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                sqlParameter.Value = SecurityId;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityCode", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.SecurityCode;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ShortName", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.ShortName;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@SecurityDescription", SqlDbType.VarChar, 250);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.SecurityDescription;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IssuerId", SqlDbType.Int, 9);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.IssuerId;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ISIN", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.ISIN;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@FaceValue", SqlDbType.Decimal);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.FaceValue;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Pattern", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.Pattern;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ReturnPattern", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.ReturnPattern;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Type", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.Type;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@ReturnFrequency", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.ReturnFrequency;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@InCompleteMonths", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.InCompleteMonths;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@CompleteMonths", SqlDbType.VarChar, 50);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.CompleteMonths;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@FirstInterestPmtDate", SqlDbType.DateTime);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.FirstInterestPmtDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@BookClosureDate", SqlDbType.DateTime);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.BookClosureDate;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@RegistrarId", SqlDbType.Int, 9);
                sqlParameter.Value = objSecurityMasterDetailsSaveEntity.RegistrarId;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                sqlParameter.Value = _userId;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsDelete", SqlDbType.Bit);
                sqlParameter.Value = 0;
                sqlParameters.Add(sqlParameter);

                sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                sqlParameter.Value = 1;
                sqlParameters.Add(sqlParameter);

                if (_actionState.Equals("New"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                    sqlParameter.Value = "add";
                    sqlParameters.Add(sqlParameter);
                }
                else if (_actionState.Equals("Edit"))
                {
                    sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                    sqlParameter.Value = "edit";
                    sqlParameters.Add(sqlParameter);
                }

                objDAL.ExecuteSP("PFT_tblwSecurityMaster_addedit", ref sqlParameters);

                if (_actionState.Equals("Edit"))
                {
                    strQuery = "delete from PFT_tblwSecurityMaster_RatingDetails where SecurityId=" + SecurityId + "";
                    strQuery += " delete from PFT_tblwSecurityMaster_GuaranteeDetails where SecurityId=" + SecurityId + "";
                    strQuery += " delete from PFT_tblwSecurityMaster_FinantialDetails where SecurityId=" + SecurityId + "";
                    strQuery += " delete from PFT_tblwSecurityMaster_IssueDetails where SecurityId=" + SecurityId + "";
                    strQuery += " delete from PFT_tblwSecurityMaster_MaturityDetails where SecurityId=" + SecurityId + "";
                    strQuery += " delete from PFT_tblwSecurityMaster_PutCallDetails where SecurityId=" + SecurityId + "";
                    objDAL.ExecuteSQL(strQuery);
                }
                              
                foreach (SecurityRatingDetailsEntity objSecRatingDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityRatingDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityRatingdetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@RatingCompId", SqlDbType.Int, 9);
                    sqlParameter.Value = objSecRatingDetailsEntity.ratingCompId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@RatingId", SqlDbType.Int, 9);
                    sqlParameter.Value = objSecRatingDetailsEntity.ratingId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);

                    
                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_RatingDetails_addedit", ref sqlParameters);
                }

                foreach (SecurityGuaranteeDetailsEntity objSecGuaranteeDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityGuaranteeDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityGuaranteeDetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@GuaranteeInstitutionID", SqlDbType.Int, 9);
                    sqlParameter.Value = objSecGuaranteeDetailsEntity.guaranteeInstitutionID;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Guarantee", SqlDbType.Decimal);
                    sqlParameter.Value = objSecGuaranteeDetailsEntity.secInstitutionGuarantee;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);


                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_GuaranteeDetails_addedit", ref sqlParameters);
                }

                foreach (SecurityFinantialDetailsEntity objSecFinantialDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityFinantialDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityFinantialDetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Fromdate", SqlDbType.DateTime);
                    sqlParameter.Value = objSecFinantialDetailsEntity.secfdFromDate;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Todate", SqlDbType.DateTime);
                    sqlParameter.Value = objSecFinantialDetailsEntity.secfdToDate;
                    sqlParameters.Add(sqlParameter);


                    sqlParameter = new SqlParameter("@RateOfInterest", SqlDbType.Decimal);
                    sqlParameter.Value = objSecFinantialDetailsEntity.secfdRateOfInterest;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);


                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_FinantialDetails_addedit", ref sqlParameters);
                }

                foreach (SecurityIssueDetailsEntity objSecIssueDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityIssueDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityIssueDetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IssueDate", SqlDbType.DateTime);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdIssueDate;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Percentage", SqlDbType.Decimal);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdPercentage;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Value", SqlDbType.Decimal);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdValue;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Premium", SqlDbType.Decimal);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdPremium;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Discount", SqlDbType.Decimal);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdDiscount;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@NetCashFlow", SqlDbType.Decimal);
                    sqlParameter.Value = objSecIssueDetailsEntity.secIdNetCashFlow;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);


                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_IssueDetails_addedit", ref sqlParameters);
                }

                foreach (SecurityMaturityDetailsEntity objSecMaturityDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityMaturityDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityMaturityDetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@MaturityDate", SqlDbType.DateTime);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdMaturityDate;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Percentage", SqlDbType.Decimal);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdPercentage;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Value", SqlDbType.Decimal);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdValue;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Premium", SqlDbType.Decimal);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdPremium;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Discount", SqlDbType.Decimal);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdDiscount;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@NetCashFlow", SqlDbType.Decimal);
                    sqlParameter.Value = objSecMaturityDetailsEntity.secMdNetCashFlow;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);


                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_MaturityDetails_addedit", ref sqlParameters);
                }

                foreach (SecurityPutCallDetailsEntity objSecPutCallDetailsEntity in objSecurityMasterDetailsSaveEntity.SecurityPutCallDetailsList)
                {
                    sqlParameters = new List<SqlParameter>();

                    sqlParameter = new SqlParameter("@SecurityPutCallDetailsId", SqlDbType.Int, 9);
                    sqlParameter.Value = 0;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@SecurityId", SqlDbType.Int, 9);
                    sqlParameter.Value = SecurityId;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@PutCallDate", SqlDbType.DateTime);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdPutCallDate;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@PutCallType", SqlDbType.VarChar, 50);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdPutCallType;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Percentage", SqlDbType.Decimal);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdPercentage;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Value", SqlDbType.Decimal);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdValue;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Premium", SqlDbType.Decimal);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdPremium;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Discount", SqlDbType.Decimal);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdDiscount;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@NetCashFlow", SqlDbType.Decimal);
                    sqlParameter.Value = objSecPutCallDetailsEntity.secpcdNetCashFlow;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@IsActive", SqlDbType.Bit);
                    sqlParameter.Value = 1;
                    sqlParameters.Add(sqlParameter);

                    sqlParameter = new SqlParameter("@Icreater", SqlDbType.Int, 9);
                    sqlParameter.Value = _userId;
                    sqlParameters.Add(sqlParameter);


                    if (_actionState.Equals("New") || _actionState.Equals("Edit"))
                    {
                        sqlParameter = new SqlParameter("@edit", SqlDbType.VarChar, 50);
                        sqlParameter.Value = "add";
                        sqlParameters.Add(sqlParameter);
                    }

                    objDAL.ExecuteSP("PFT_tblwSecurityMaster_PutCallDetails_addedit", ref sqlParameters);
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                responseEntity.IsComplete = true;
                responseEntity.Description = "Rating Security Master Record Save Successfully.";

                return Ok(responseEntity);
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "SecurityMasterController");
                return Ok();
                //return BadRequest(ex.Message);
            }
            finally
            {
                objDAL.CloseConnection();
            }
        }

        private ResponseEntity ValidateSecurityMaster(ref SecurityMasterEntity objSecurityMasterDetailsSaveEntity)
        {
            string _actionState = "";
            string strQuery = "";
            ResponseEntity objResponseEntity = new ResponseEntity();
            BaseDAL objDAL = new BaseDAL();
            //List<RatingCompMasterDetailsEntity> objListRatingDetailsEntity;
            objResponseEntity.IsComplete = true;
            if (objSecurityMasterDetailsSaveEntity == null)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please enter at least one Security Master Details.";
                return objResponseEntity;
            }

            if (objSecurityMasterDetailsSaveEntity.SecurityRatingDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityRatingDetailsList.Count == 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one Rating Details Record.";
                return objResponseEntity;
            }
            if (objSecurityMasterDetailsSaveEntity.SecurityGuaranteeDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityGuaranteeDetailsList.Count == 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one Guarantee Institution Details Record.";
                return objResponseEntity;
            }
            if (objSecurityMasterDetailsSaveEntity.SecurityFinantialDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityFinantialDetailsList.Count == 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one Finantial Details Record.";
                return objResponseEntity;
            }
            if (objSecurityMasterDetailsSaveEntity.SecurityIssueDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityIssueDetailsList.Count==0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one Issue Details Record.";
                return objResponseEntity;
            }
            if (objSecurityMasterDetailsSaveEntity.SecurityMaturityDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityMaturityDetailsList.Count == 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one Matchurity Details Record.";
                return objResponseEntity;
            }
            if (objSecurityMasterDetailsSaveEntity.SecurityPutCallDetailsList == null || objSecurityMasterDetailsSaveEntity.SecurityPutCallDetailsList.Count == 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Please add at least one PUT/CALL Details Record.";
                return objResponseEntity;
            }


            //objListRatingDetailsEntity = objRatingMasterDetailsSaveEntity.ratingCompMasterDetailsList;

            if (objSecurityMasterDetailsSaveEntity.actionEntity != null)
                _actionState = objSecurityMasterDetailsSaveEntity.actionEntity.ActionState;

            if (_actionState.Equals("New"))
            {
                strQuery = " SELECT * from PFT_tblwSecurityMaster where SecurityCode='" + objSecurityMasterDetailsSaveEntity.SecurityCode + "' and IsDelete=0 and IsActive=1";
            }
            else if (_actionState.Equals("Edit"))
            {
                strQuery = " SELECT * from PFT_tblwSecurityMaster where SecurityCode='" + objSecurityMasterDetailsSaveEntity.SecurityCode + "' and SecurityId !=" + objSecurityMasterDetailsSaveEntity.SecurityId + " and IsDelete=0 and IsActive=1";
            }

            objDAL.CreateConnection();
            DataSet dsratingCompMaster = objDAL.FetchDataSetRecords(strQuery);
            objDAL.CloseConnection();
            if (dsratingCompMaster.Tables[0].Rows.Count > 0)
            {
                objResponseEntity.IsComplete = false;
                objResponseEntity.Description = "Security Code already Exist";
                return objResponseEntity;
            }
            return objResponseEntity;
        }

        [HttpPost]
        public IHttpActionResult deleteSecurityMasterRecord(ActionEntity objActionEntity)
        {
            SecurityMasterEntity objSecurityMasterDetailsDeleteEntity = new SecurityMasterEntity();            
            try
            {
                UserAuthentication auth = new UserAuthentication();
                auth.CheckPageSecurity("~/Query/QueriesSample.aspx");

                objSecurityMasterDetailsDeleteEntity.responseEntity = new ResponseEntity();
                BaseDAL objDAL = new BaseDAL();

                SqlParameter sqlParameter;
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                string strQuery = "";

                objSecurityMasterDetailsDeleteEntity.responseEntity.IsComplete = false;
                if (objActionEntity == null)
                {
                    objSecurityMasterDetailsDeleteEntity.responseEntity.IsComplete = false;
                    objSecurityMasterDetailsDeleteEntity.responseEntity.Description = "Required data to remove is not found.";
                    return Ok(objSecurityMasterDetailsDeleteEntity);
                }

                Sessions session = new Sessions();
                SessionEntity sessionEntity = session.GetSessionEntity();

                Int64 SecurityId = 0;
                SecurityId = objActionEntity.RecordId;

                if (SecurityId > 0)
                {
                    objDAL.CreateConnection();
                    objDAL.BeginTransaction();

                    strQuery = "Update PFT_tblwSecurityMaster set IsDelete=1 where SecurityId=" + SecurityId + "";
                    objDAL.ExecuteSQL(strQuery);

                    strQuery = "update PFT_tblwSecurityMaster_RatingDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    strQuery += " update PFT_tblwSecurityMaster_GuaranteeDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    strQuery += " update PFT_tblwSecurityMaster_FinantialDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    strQuery += " update PFT_tblwSecurityMaster_IssueDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    strQuery += " update PFT_tblwSecurityMaster_MaturityDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    strQuery += " update PFT_tblwSecurityMaster_PutCallDetails set IsActive=0 where SecurityId=" + SecurityId + "";
                    objDAL.ExecuteSQL(strQuery);
                   

                    objDAL.CommitTransaction();
                    objDAL.CloseConnection();

                    objSecurityMasterDetailsDeleteEntity.responseEntity.IsComplete = true;
                    objSecurityMasterDetailsDeleteEntity.responseEntity.Description = "Security Master Record Deleted Successfully.";

                    return Ok(objSecurityMasterDetailsDeleteEntity);
                }
                else
                {
                    objSecurityMasterDetailsDeleteEntity.responseEntity.Description = "Required data to remove is not found.";
                }
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                WebUtils.ErrorHandler.HandleException(ex, "SecurityMasterController");
                return BadRequest(ex.Message);
            }
            return Ok(objSecurityMasterDetailsDeleteEntity);
        }



    }
}
