﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using IOTBusinessEntities;
using IOTBusinessServices;
using IOTUtilities;
using IOTWebAPI.Filters;
using System.Threading.Tasks;
//suppress the warning of xml documentation 
#pragma warning disable 1591
namespace IOTWebAPI.Controllers
{

    [Authorize]
    [CustomExceptionFilter]
    [DeflateCompression]
    public class ConfigSettingController : ApiController
    {

        [HttpPost]
        public async Task<IHttpActionResult> CreateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateConfigSetting(configsettingEntity, o_id: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            strResult = ValidateEntity.ValidateConfigSetting(configsettingEntity, checkConfigSettingAvailability: true);

            if (strResult != "")
            {
                return Ok(CustomeResponse.getFailResponse(strResult));

            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                if (await objConfigSettingBS.CreateConfigSetting(configsettingEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }


        [HttpPost]
        public async Task<IHttpActionResult> GetConfigSettingList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                DataTable dtGetConfigSettingList = await objConfigSettingBS.GetConfigSettingList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtGetConfigSettingList));

            }

        }

         [HttpPost]
        public async Task<IHttpActionResult> GetOrganizationList(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                DataTable dtOrganizationList = await objConfigSettingBS.GetOrganizationList(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtOrganizationList));

            }

        }
        
        [HttpPost]
        public async Task<IHttpActionResult> GetConfigSettingById(FilterEntity filterEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateFilterEntity.ValidatefilterEntity(filterEntity, o_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}


            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                DataTable dtGetConfigSetting = await objConfigSettingBS.GetConfigSettingById(filterEntity);
                return Ok(CustomeResponse.getOkResponse(dtGetConfigSetting));

            }
        }


        [HttpPost]
        public async Task<IHttpActionResult> UpdateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            string strResult;
            strResult = ValidateEntity.ValidateConfigSetting(configsettingEntity, o_id: true, s_role_Id: true, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                if (await objConfigSettingBS.UpdateConfigSetting(configsettingEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        [HttpPost]
        public async Task<IHttpActionResult> DeleteConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateConfigSetting(configsettingEntity, o_id: true, s_user_id: true, s_role_Id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                if (await objConfigSettingBS.DeleteConfigSetting(configsettingEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }

        //
        //[AllowAnonymous]
        public async Task<IHttpActionResult> GetSetting()
        {
            var setting = new setting();
            setting.mUrl = System.Configuration.ConfigurationManager.AppSettings["mUrl"];
            setting.dUrl = System.Configuration.ConfigurationManager.AppSettings["dUrl"];
            setting.HA = System.Configuration.ConfigurationManager.AppSettings["HA"];
            setting.HB = System.Configuration.ConfigurationManager.AppSettings["HB"];
            setting.HC = System.Configuration.ConfigurationManager.AppSettings["HC"];
            setting.OverSpeed = System.Configuration.ConfigurationManager.AppSettings["OverSpeed"];
            setting.spike = System.Configuration.ConfigurationManager.AppSettings["spike"];
            setting.HCspeed = System.Configuration.ConfigurationManager.AppSettings["HCspeed"];

            return Ok(CustomeResponse.getOkResponse(setting));

        }



        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> overspeedlimit(List<overspeedlimit> speedlimit)
        {
            var limitdata = new List<responcespeedlimit>();
            var responcespeedlimit = new responcespeedlimit();
            foreach (var data in speedlimit)
            {

                responcespeedlimit.speedLimit = System.Configuration.ConfigurationManager.AppSettings["OverSpeed"];
                responcespeedlimit.units = "KPH";
                limitdata.Add(responcespeedlimit);
            }
            responcespeedlimit = null;
            return Ok(CustomeResponse.getOkResponse(limitdata));

        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> CreateNotificationSetting(NotificationSetting notigsettingEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateNotiSetting(notigsettingEntity, s_user_id: true);

            if (strResult != "")
            {
                return BadRequest(strResult);
            }

            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                if (await objConfigSettingBS.CreateNotificationSetting(notigsettingEntity))
                {
                    return Ok(CustomeResponse.getOkResponse());
                }
                else
                {
                    strResult = MessageCode.MsgInJson(MsgCode.bad_request);
                    return BadRequest(strResult);
                }

            }

        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetNotificationSetting(NotificationSetting notigsettingEntity)
        {
            // Validatde entity
            string strResult;
            strResult = ValidateEntity.ValidateNotiSetting(notigsettingEntity, s_user_id: true);
            if (strResult != "")
            {
                return BadRequest(strResult);
            }
            //// Check Permission and session
            //strResult = PageSecurity.CheckPageSecurity(CommonUtils.ConvertToInt(userEntity.s_user_id), CommonUtils.ConvertToInt(userEntity.s_role_Id), page_NM: "userprofile", _action: "D");
            //if (strResult != "")
            //{
            //    return BadRequest(strResult);
            //}

            //Result
            using (ConfigSettingBS objConfigSettingBS = new ConfigSettingBS())
            {
                DataTable dtGetNotiSettingList = await objConfigSettingBS.GetNotificationSetting(notigsettingEntity);
                return Ok(CustomeResponse.getOkResponse(dtGetNotiSettingList));

            }

        }
    }
}
