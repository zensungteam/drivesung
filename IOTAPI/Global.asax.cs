﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using IOTWebAPI.LogRequest;

namespace IOTWebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

//            if(ConfigurationManager.AppSettings["EnableLogging"] == "true")
//{
            GlobalConfiguration.Configuration.MessageHandlers.Add(new MessageLoggingHandler());
//}
        }

        //protected void Application_PostAuthorizeRequest()
        //{
        //    HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        //}
    }
}
