﻿using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTBusinessServices;

//suppress the warning of xml documentation 
#pragma warning disable 1591

namespace IOTWebAPI
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {

            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");


            if (allowedOrigin == null) allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            //for check profile avaliable or not
            bool paflag = false;
            using (UserAuthenticationBS objUserAuthenticationBS = new UserAuthenticationBS(false))
            {
                paflag = await objUserAuthenticationBS.CheckUserAvailability(context.UserName);
            }


            if (!string.IsNullOrEmpty(context.UserName))
            {
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                identity.AddClaim(new Claim("sub", context.UserName));
                var props = new AuthenticationProperties(new Dictionary<string, string>
                            {
                                { 
                                    "userName", context.UserName
                                 
                                },
                                { 
                                    "message", "{'code':'200','desc':'ok'}"
                                  
                                },
                                {
                                     "paflag", "" + paflag +""
                                }
                            });

                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);


                //using (UserAuthenticationBS objUserAuthenticationBS = new UserAuthenticationBS(false))
                //{
                //    ResponseEntity objResponseEntity = new ResponseEntity();
                //    objResponseEntity = await objUserAuthenticationBS.UserLogin(context.UserName, context.Password);
                //    if (objResponseEntity.Description == "success")
                //    {
                //        var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                //        identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                //        identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                //        identity.AddClaim(new Claim("sub", context.UserName));
                //        var props = new AuthenticationProperties(new Dictionary<string, string>
                //            {
                //                { 
                //                    "userName", context.UserName
                                 
                //                },
                //                { 
                //                    "message", "{'code':'200','desc':'ok'}"
                                  
                //                },
                //                {
                //                     "paflag", "" + paflag +""
                //                }
                //            });

                //        var ticket = new AuthenticationTicket(identity, props);
                //        context.Validated(ticket);

                //    }
                //    else
                //    {
                //        //context.SetError("invalid_grant", "The user name or password is incorrect.");
                //        context.SetError(@"{'code':'400','desc':'The user name or password is incorrect.','paflag':'" + paflag + "'}");
                //        return;
                //    }

                //}
            }
            else
            {
                //context.SetError("invalid_grant", "The user name or password is incorrect.");
                context.SetError(@"{'code':'400','desc':'The user name or password is incorrect.','paflag':'" + paflag + "'}");
                return;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}