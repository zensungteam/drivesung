﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityRatingDetailsEntity
    {
        public int SecurityRatingdetailsId { get; set; }
        public int SecurityId { get; set; }
        public int ratingCompId { get; set; }
        public int ratingId { get; set; }
        public string ratingCompName { get; set; }
        public string ratingCode { get; set; }
        public string secRatingCodeDescription { get; set; }

    }
}