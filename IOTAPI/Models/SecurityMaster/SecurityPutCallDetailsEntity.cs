﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityPutCallDetailsEntity
    {
        public int SecurityPutCallDetailsId { get; set; }
        public int SecurityId { get; set; }
        public DateTime? secpcdPutCallDate { get; set; }
        public string secpcdPutCallType { get; set; }
        public decimal secpcdPercentage { get; set; }
        public decimal secpcdValue { get; set; }
        public decimal secpcdPremium { get; set; }
        public decimal secpcdDiscount { get; set; }
        public decimal secpcdNetCashFlow { get; set; }
    }
}