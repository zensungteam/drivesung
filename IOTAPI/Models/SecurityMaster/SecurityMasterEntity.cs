﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityMasterEntity
    {
        public ActionEntity actionEntity { get; set; }
        public ResponseEntity responseEntity { get; set; }
        public DataTable dtsecurityMasterList { get; set; }
        public DataTable dtIssuerCodeList { get; set; }
        public DataTable dtRatingCompanyList { get; set; }
        public Int32 ReportingUserId { get; set; }
        public Int32 EmpId { get; set; }
        public Int64 SecurityId { get; set; }
        public string SecurityCode { get; set; }
        public string ShortName { get; set; }
        public string SecurityDescription { get; set; }
        public DataTable IssuerIds { get; set; }
        public string IssuerId { get; set; }
        public string RegistrarId { get; set; }
        public string ISIN { get; set; }
        public decimal FaceValue { get; set; }
        public string Pattern { get; set; }
        public string ReturnPattern { get; set; }
        public string Type { get; set; }
        public string ReturnFrequency { get; set; }
        public string InCompleteMonths { get; set; }
        public string CompleteMonths { get; set; }
        public DateTime? FirstInterestPmtDate { get; set; }
        public DateTime? BookClosureDate { get; set; }
        public DateTime? Idate { get; set; }
        public DataTable dtAllIssuerRegistrar { get; set; }
        public DataTable dtAllIssuerGuaranteeInstitution { get; set; }
        public DataTable dtAllRatingCompanyRatingCode { get; set; }
        public List<SecurityRatingDetailsEntity> SecurityRatingDetailsList { get; set; }
        public List<SecurityGuaranteeDetailsEntity> SecurityGuaranteeDetailsList { get; set; }
        public List<SecurityFinantialDetailsEntity> SecurityFinantialDetailsList { get; set; }
        public List<SecurityIssueDetailsEntity> SecurityIssueDetailsList { get; set; }
        public List<SecurityMaturityDetailsEntity> SecurityMaturityDetailsList { get; set; }
        public List<SecurityPutCallDetailsEntity> SecurityPutCallDetailsList { get; set; }
    }
}