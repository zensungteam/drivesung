﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models.SecurityMaster
{
    public class SecurityMaturityDetailsEntity
    {
        public int SecurityMaturityDetailsId { get; set; }
        public int SecurityId { get; set; }
        public DateTime? secMdMaturityDate { get; set; }
        public decimal secMdPercentage { get; set; }
        public decimal secMdValue { get; set; }
        public decimal secMdPremium { get; set; }
        public decimal secMdDiscount { get; set; }
        public decimal secMdNetCashFlow { get; set; }
    }
}