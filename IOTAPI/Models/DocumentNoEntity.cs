﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class DocumentNoEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 DocumentNoID { get; set; }
        public string FinancialYear { get; set; }
        public bool IsDelete { get; set; }
        public bool IsActive { get; set; }
        public Int64 ICreater { get; set; }
        public DateTime IDate { get; set; }
        public List<DocumentNoDetailsEntity> DocumentNoDetailsList { get; set; }
        //public DocumentNoDetailsEntity DocumentNoDetailsList { get; set; }
    }
}