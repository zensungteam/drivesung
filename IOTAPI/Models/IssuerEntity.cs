﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class IssuerEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 IssuerID { get; set; }
        public string IssuerCode { get; set; }
        public string IssuerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PinCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContactName { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public bool IsDelete { get; set; }
        public decimal ICreater { get; set; }
        public DateTime Idate { get; set; }
        public List<IssuerDetailsEntity> IssuerDetailsLst { get; set; }
        public List<Int32> IssuerDetailsList { get; set; }
        public List<Int32> RegistarsList { get; set; }
        public ResponseEntity responseEntity { get; set; }
    }
}