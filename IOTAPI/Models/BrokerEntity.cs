﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class BrokerEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 BrokerID { get; set; }
        public string BrokerCode { get; set; }
        public string BrokerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PinCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContactName { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public string PAN { get; set; }
        public string TAN { get; set; }
        public decimal BrokerageRate { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string IFSCCode { get; set; }
        public string AccountNo { get; set; }
        public string CSGLNo { get; set; }
        public Int64 ICreater { get; set; }
        public DateTime IDate { get; set; }
        public bool IsDelete { get; set; }
        public List<BrokerDetailsEntity> BrokerDetailsList { get; set; }
    }
}