﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace PFTrustAPI.Models.DPMaster
{
    public class DPMasterEntity
    {
        public ActionEntity actionEntity { get; set; }
        public ResponseEntity responseEntity { get; set; }
        public DataTable dtdpList { get; set; }
        public Int32 ReportingUserId { get; set; }
        public Int32 EmpId { get; set; }
        public Int64 Id { get; set; }
        public string DPCode { get; set; }
        public string DPName { get; set; }
        public Int32 DPID { get; set; }
        //public string DPID { get; set; }

        public DateTime? Idate { get; set; }
    }
}