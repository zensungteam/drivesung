﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class IssuerDetailsEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 IssuerDetailsID { get; set; }
        public Int64 IssuerID { get; set; }
        public Int64? RegistrarID { get; set; }
        public Int64? GuaranteeInstitutionID { get; set; }
        public bool IsDelete { get; set; }
        public Int64 ICreater { get; set; }
        public DateTime IDate { get; set; }
    }
}