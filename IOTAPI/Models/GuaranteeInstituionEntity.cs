﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class GuaranteeInstituionEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 GuaranteeInstitutionID { get; set; }
        public string GuaranteeInstitutionCode { get; set; }
        public string InstitutionName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string PinCode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ContactName { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Designation { get; set; }
        public bool IsDelete { get; set; }
    }
}