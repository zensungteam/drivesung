﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PFTrustAPI.Models
{
    public class BrokerDetailsEntity
    {
        public ActionEntity actionEntity { get; set; }
        public Int64 BrokerDPID { get; set; }
        public Int64 BrokerID { get; set; }
        public string DPCode { get; set; }
        public string Description { get; set; }
        public string DPID { get; set; }
        public bool Preference { get; set; }
        public Int64 ICreater { get; set; }
        public DateTime IDate { get; set; }
        public bool IsDelete { get; set; }
    }
}