﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
    public class TripBS : IDisposable
    {
        private TripDM objTripDM;

        public TripBS()
        {
            objTripDM = new TripDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objTripDM != null) { objTripDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~TripBS()
        {
            Dispose(false);
        }

        #endregion


        public Task<DataTable> StartTrip(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.StartTrip(tripEntity));
        }

        public Task<bool> DeleteTrip(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.DeleteTrip(tripEntity));
        }
        public Task<bool> EndTrip(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.EndTrip(tripEntity));
        }


        public Task<DataTable> GetTripsList(TripEntity tripEntity)
        {
            using (DataTable dtUser = objTripDM.GetTripsList(tripEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<DataTable> GetTripById(TripEntity tripEntity)
        {
            using (DataTable dtUser = objTripDM.GetTripById(tripEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<DataSet> GetTripPlayData(TripEntity tripEntity)
        {
            using (DataSet dsTripPlayData = objTripDM.GetTripPlayData(tripEntity))
            {
                return Task.FromResult(dsTripPlayData);
            }
        }

        public bool CheckTripStart(TripEntity tripEntity)
        {
            return objTripDM.CheckTripStart(tripEntity);
        }
        //public Task<DataTable> CheckTripStart(TripEntity tripEntity)
        //{
        //    using (DataTable dtUser = objTripDM.CheckTripStart(tripEntity))
        //    {
        //        return Task.FromResult(dtUser);
        //    }
        //}

        public bool CheckTripEnd(TripEntity tripEntity)
        {
            return objTripDM.CheckTripEnd(tripEntity);
        }

        public Task<DataTable> GetActiveTrips(TripEntity tripEntity)
        {
            using (DataTable dtUser = objTripDM.GetActiveTrips(tripEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<bool> UpdateTripUploadPath(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.UpdateTripUploadPath(tripEntity));
        }

        public Task<DataTable> UpdateTrip(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.UpdateTrip(tripEntity));
        }
        public Task<bool> UpdateTripLogPath(TripEntity tripEntity)
        {
            return Task.FromResult(objTripDM.UpdateTripLogPath(tripEntity));
        }
        
    }
}
