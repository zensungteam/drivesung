﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTDataModel;

namespace IOTBusinessServices
{
    public class PolicyClaimBS : IDisposable
    {
        private PolicyClaimDM objpolicyClaimDM;

        public PolicyClaimBS()
        {
            objpolicyClaimDM = new PolicyClaimDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objpolicyClaimDM != null) { objpolicyClaimDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~PolicyClaimBS()
        {
            Dispose(false);
        }

        #endregion

        public Task<int> createPolicyClaim(PolicyClaimEntity policyClaimEntity)
        {
            return Task.FromResult(objpolicyClaimDM.createPolicyClaim(policyClaimEntity));
        }

        public Task<List<PolicyClaimEntity>> GetPolicyClaimList(PolicyClaimEntity policyClaimEntity)
        {
            List<PolicyClaimEntity> data = objpolicyClaimDM.GetPolicyClaimList(policyClaimEntity);
             return Task.FromResult(data);
          
        }

        public Task<List<PolicyClaimEntity>> GetPolicyClaimById(PolicyClaimEntity policyClaimEntity)
        {
            List<PolicyClaimEntity> data = objpolicyClaimDM.GetPolicyClaimById(policyClaimEntity);
            return Task.FromResult(data);

        }
        public Task<bool> UpdatePolicyClaim(PolicyClaimEntity policyClaimEntity)
        {

            return Task.FromResult(objpolicyClaimDM.UpdatePolicyClaim(policyClaimEntity));
        }
        public Task<bool> DeletePolicyClaim(PolicyClaimEntity policyClaimEntity)
        {

            return Task.FromResult(objpolicyClaimDM.DeletePolicyClaim(policyClaimEntity));
        }

        public Task<bool> UpdatePolicyProfilePath(int claim_id, string doc_file_path,string doc_name)
        {

            return Task.FromResult(objpolicyClaimDM.UpdatePolicyProfilePath(claim_id, doc_file_path, doc_name));
        }

        public Task<bool> AddPolicyClaimRemark(PolicyClaimEntity policyClaimEntity)
        {

            return Task.FromResult(objpolicyClaimDM.AddPolicyClaimRemark(policyClaimEntity));
        }
        
        

        

    }
}
