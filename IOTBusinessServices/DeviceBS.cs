﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
   public class DeviceBS:IDisposable
    {
       
           private DeviceDM objDeviceDM;

           public DeviceBS()
           {
               objDeviceDM = new DeviceDM();
           }

           #region ------------disposed managed and unmanaged objects-----------

           // Flag: Has Dispose already been called?
           bool disposed = false;
           public void Dispose()
           {
               Dispose(true);
               GC.SuppressFinalize(this);
           }
           // Protected implementation of Dispose pattern.
           protected virtual void Dispose(bool disposing)
           {
               if (disposed)
                   return;

               if (disposing)
               {
                   // Free any other managed objects here.
                   if (objDeviceDM != null) { objDeviceDM.Dispose(); }
               }
               // Free any unmanaged objects here.
               disposed = true;
           }

           ~DeviceBS()
           {
               Dispose(false);
           }

           #endregion


           public Task<bool> CreateDevice(DeviceEntity deviceEntity)
           {
               return Task.FromResult(objDeviceDM.CreateDevice(deviceEntity));
           }

           public Task<DataTable> GetDeviceList()
           {
               using (DataTable dtDevice = objDeviceDM.GetDeviceList())
               {
                   return Task.FromResult(dtDevice);
               }
           }
       
           public Task<DataTable> GetDeviceById( DeviceEntity deviceEntity)
           {
               using (DataTable dtDevice = objDeviceDM.GetDeviceById(deviceEntity))
               {
                   return Task.FromResult(dtDevice);
               }
           }

           public Task<bool> Updatedevice(DeviceEntity deviceEntity)
           {
               return Task.FromResult(objDeviceDM.UpdateDevice(deviceEntity));
           }

           public Task<bool> DeleteDevice(DeviceEntity deviceEntity)
           {
               return Task.FromResult(objDeviceDM.DeleteDevice(deviceEntity));
           }
           public bool CheckDeviceAvailability(DeviceEntity deviceEntity)
           {
               return objDeviceDM.CheckDeviceAvailability(deviceEntity);
           }



       

           
       
       

       }

    
}
