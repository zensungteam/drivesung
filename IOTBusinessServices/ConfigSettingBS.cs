﻿using System;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace IOTBusinessServices
{
    public class ConfigSettingBS : IDisposable
    {
        private ConfigSettingDM objConfigSettingDM;
       
        public ConfigSettingBS()
        {
            objConfigSettingDM = new ConfigSettingDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objConfigSettingDM != null) { objConfigSettingDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~ConfigSettingBS()
        {
            Dispose(false);
        }

        #endregion


        public Task<bool> CreateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            return Task.FromResult(objConfigSettingDM.CreateConfigSetting(configsettingEntity));
        }

        

        public Task<DataTable> GetConfigSettingList(FilterEntity filterEntity)
        {

            using (DataTable dtConfigSetting = objConfigSettingDM.GetConfigSettingList(filterEntity))
            {
                return Task.FromResult(dtConfigSetting);
            }
            
        }

        public Task<DataTable> GetOrganizationList(FilterEntity filterEntity)
        {

            using (DataTable dtOrganization = objConfigSettingDM.GetOrganizationList(filterEntity))
            {
                return Task.FromResult(dtOrganization);
            }
            
        }

        

        public Task<DataTable> GetConfigSettingById(FilterEntity filterEntity)
        {
            using (DataTable dtConfigSetting = objConfigSettingDM.GetConfigSettingById(filterEntity))
            {
                return Task.FromResult(dtConfigSetting);
            }
        }


        public Task<bool> UpdateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            return Task.FromResult(objConfigSettingDM.UpdateConfigSetting(configsettingEntity));
        }


        public Task<bool> DeleteConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            return Task.FromResult(objConfigSettingDM.DeleteConfigSetting(configsettingEntity));
        }

        public bool CheckConfigSettingAvailability(ConfigSettingEntity configsettingEntity)
        {
            return objConfigSettingDM.CheckConfigSettingAvailability(configsettingEntity);
        }

        public Task<bool> CreateNotificationSetting(NotificationSetting notisettingEntity)
        {
            return Task.FromResult(objConfigSettingDM.CreateNotificationSetting(notisettingEntity));
        }

        public Task<DataTable> GetNotificationSetting(NotificationSetting notisettingEntity)
        {

            using (DataTable dtNotiSetting = objConfigSettingDM.GetNotificationSetting(notisettingEntity))
            {
                return Task.FromResult(dtNotiSetting);
            }
            
        }
        

        

    }
}