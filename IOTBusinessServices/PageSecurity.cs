﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
   public  class PageSecurity
    {
       public static string CheckPageSecurity(Int32 s_user_id, Int32 s_role_Id, bool chkSession = true, string page_NM = "", string _action="V")
        {
            string _strResult = "";
            if (chkSession)
            {
                using (UserAuthenticationBS objUserAuthenticationBS = new UserAuthenticationBS())
                {
                    _strResult = objUserAuthenticationBS.CheckSessionExpired();
                    if (_strResult != "")
                    {
                        return _strResult;
                    }
                }
            }

            if (page_NM != "")
            {
                using (UserAuthenticationBS objUserAuthenticationBS = new UserAuthenticationBS())
                {
                    _strResult = objUserAuthenticationBS.CheckPageSecurity(page_NM, s_user_id, s_role_Id, _action);
                    if (_strResult != "")
                    {
                        return _strResult;
                    }
                }
            }
           return _strResult;
        }
    }
}
