﻿using System;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace IOTBusinessServices
{
    public class OrganizationBS : IDisposable
    {
        private OrganizationDM objOrganizationDM;
       
        public OrganizationBS()
        {
            objOrganizationDM = new OrganizationDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objOrganizationDM != null) { objOrganizationDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~OrganizationBS()
        {
            Dispose(false);
        }

        #endregion


        public Task<int> CreateOrganization(OrganizationEntity organizationEntity)
        {
            return Task.FromResult(objOrganizationDM.CreateOrganization(organizationEntity));
        }



        public Task<List<OrganizationEntity>> GetOrganizationList(FilterEntity filterEntity)
        {
            List<OrganizationEntity> organizationEntities = objOrganizationDM.GetOrganizationList(filterEntity);
            return Task.FromResult(organizationEntities);
            
        }

        public Task<OrganizationEntity> GetOrganizationById(FilterEntity filterEntity)
        {
            OrganizationEntity organizationEntity = objOrganizationDM.GetOrganizationById(filterEntity);
            return Task.FromResult(organizationEntity);
        }

        public Task<bool> UpdateOrganization(OrganizationEntity organizationEntity)
        {
            return Task.FromResult(objOrganizationDM.UpdateOrganization(organizationEntity));
        }


        public Task<bool> DeleteOrganization(OrganizationEntity organizationEntity)
        {
            return Task.FromResult(objOrganizationDM.DeleteOrganization(organizationEntity));
        }

        public bool CheckOrganizatioAvailability(OrganizationEntity organizationEntity)
        {
            return objOrganizationDM.CheckOrganizatioAvailability(organizationEntity);
        }

        

    }
}