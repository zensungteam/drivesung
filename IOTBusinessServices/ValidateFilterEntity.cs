﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTUtilities;

namespace IOTBusinessServices
{
    public class ValidateFilterEntity
    {
        public static string ValidatefilterEntity(FilterEntity filterEntity,
                 bool VechileNo = false,
                 bool DeviceNo = false,
                 bool FromDate = false,
                 bool ToDate = false,
                 bool FromTime = false,
                 bool ToTime = false,
                 bool NoOfRecords = false,
                 bool OverSpeed = false,
                 bool IgnitionOn = false,
                 bool Moving = false,
                 bool Parked = false,
                 bool UnReachable = false,
                 bool HardAccels = false,
                 bool HardBreaking = false,
                 bool TamperAlert = false,
                 bool user_id = false,
                 bool o_id = false,
                 bool s_user_id = false,
                 bool s_role_Id = false

             )
        {

            string strResult = "";

            // check  user_id
            if (filterEntity == null)
            {
                // return "empty filter entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


            }

            //check VechileNo = false,
            if (o_id)
            {
                if (filterEntity.o_id == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.o_id);
                }
            }

            //check VechileNo = false,
            if (VechileNo)
            {
                if (filterEntity.VechileNo == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.VechileNo);
                }
            }
            //bool DeviceNo = false,
            if (DeviceNo)
            {
                if (filterEntity.DeviceNo == null)
                {
                    //return "DeviceNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.DeviceNo);
                }
            }
            //bool FromDate = false,
            if (FromDate)
            {
                if (filterEntity.sfDate == null)
                {
                    //  return "FromDate required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.FromDate);
                }
            }
            //bool ToDate = false,
            if (ToDate)
            {
                if (filterEntity.stDate == null)
                {
                    // return "ToDate required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.ToDate);
                }
            }
            ////bool FromTime = false,
            //if (FromTime)
            //{
            //    if (filterEntity.FromTime == null)
            //    {
            //        return "FromTime required in filter entity.";
            //    }
            //}
            ////bool ToTime = false,
            //if (ToTime)
            //{
            //    if (filterEntity.ToTime == null)
            //    {
            //        return "ToTime required in filter entity.";
            //    }
            //}
            //bool NoOfRecords = false,
            if (NoOfRecords)
            {
                if (filterEntity.NoOfRecords == null)
                {
                    //return "NoOfRecords required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.NoOfRecords);
                }
            }
            //bool OverSpeed = false,
            if (OverSpeed)
            {
                if (filterEntity.OverSpeed == null)
                {
                    // return "OverSpeed required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.OverSpeed);
                }
            }
            //bool IgnitionOn = false,
            if (IgnitionOn)
            {
                if (filterEntity.IgnitionOn == null)
                {
                    //   return "IgnitionOn required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.IgnitionOn);
                }
            }
            //bool Moving = false,
            if (Moving)
            {
                if (filterEntity.Moving == null)
                {
                    // return "Moving  required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.Moving);
                }
            }
            //bool Parked = false,
            if (Parked)
            {
                if (filterEntity.Parked == null)
                {
                    // return "Parked required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.Parked);
                }
            }
            //bool UnReachable = false,
            if (UnReachable)
            {
                if (filterEntity.UnReachable == null)
                {
                    //return "UnReachable required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.UnReachable);
                }
            }
            //bool HardAccels = false,
            if (HardAccels)
            {
                if (filterEntity.HA == null)
                {
                    //return "HardAccels required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.HardAccels);
                }
            }
            //bool HardBreaking = false,
            if (HardBreaking)
            {
                if (filterEntity.HB == null)
                {
                    // return "HardBreaking required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.HardBreaking);
                }
            }
            ////bool TamperAlert = false,
            //if (TamperAlert)
            //{
            //    if (filterEntity.TA == null)
            //    {
            //      //  return "TamperAlert required in filter entity.";
            //        return MessageCode.ErrorMsgInJson(MsgCode.TamperAlert);    
            //    }
            //}
            //bool user_id = false,
            if (user_id)
            {
                if (filterEntity.user_id == null || filterEntity.user_id == 0)
                {
                    //  return "user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }
            }

            //bool s_user_id = false,
            if (s_user_id)
            {
                if (filterEntity.s_user_id == null || filterEntity.s_user_id == 0)
                {
                    //return "s_user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }
            //bool s_role_Id = false
            if (s_role_Id)
            {
                if (filterEntity.s_role_Id == null || filterEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }





    }

    public class ValidateEngineCapacityEntity
    {


        public static string ValidatenginecapacityEntity(Engine_CapacityEntity enginecapacityEntity,
                bool eng_capacity_id = false,
                bool eng_capacity_range = false,
                bool s_user_id = false,
                bool s_role_Id = false

            )
        {

            string strResult = "";

            // check  user_id
            if (enginecapacityEntity == null)
            {
                // return "empty filter entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


            }

            //check VechileNo = false,
            if (eng_capacity_id)
            {
                if (enginecapacityEntity.eng_capacity_id == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.eng_capacity_id);
                }
            }
            if (eng_capacity_range)
            {
                if (enginecapacityEntity.eng_capacity_range == null || enginecapacityEntity.eng_capacity_range == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.eng_capacity_range);
                }
            }
            if (s_user_id)
            {
                if (enginecapacityEntity.s_user_id == null || enginecapacityEntity.s_user_id == 0)
                {
                    //return "s_user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }
            //bool s_role_Id = false
            if (s_role_Id)
            {
                if (enginecapacityEntity.s_role_Id == null || enginecapacityEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }
    }

    public class ValidateEngineTypeEntity
    {


        public static string ValidatenginetypeEntity(Engine_TypeEntity enginetypeEntity,
                bool eng_type_id = false,
                bool eng_type = false,
                bool s_user_id = false,
                bool s_role_Id = false

            )
        {

            string strResult = "";

            // check  user_id
            if (enginetypeEntity == null)
            {
                // return "empty filter entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


            }

            //check VechileNo = false,
            if (eng_type_id)
            {
                if (enginetypeEntity.eng_type_id == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.eng_type_id);
                }
            }
            if (eng_type)
            {
                if (enginetypeEntity.eng_type == null || enginetypeEntity.eng_type == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.eng_type);
                }
            }
            if (s_user_id)
            {
                if (enginetypeEntity.s_user_id == null || enginetypeEntity.s_user_id == 0)
                {
                    //return "s_user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }
            //bool s_role_Id = false
            if (s_role_Id)
            {
                if (enginetypeEntity.s_role_Id == null || enginetypeEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }
    }

    public class ValidateVehicleTypeEntity
    {


        public static string ValidatvehicletypeEntity(Vehicle_TypeEntity vehicletypeEntity,
                bool vehicle_type_id = false,
                bool vehicle_type = false,
                bool s_user_id = false,
                bool s_role_Id = false

            )
        {

            string strResult = "";

            // check  user_id
            if (vehicletypeEntity == null)
            {
                // return "empty filter entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


            }

            //check VechileNo = false,
            if (vehicle_type_id)
            {
                if (vehicletypeEntity.vehicle_type_id == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_type_id);
                }
            }
            if (vehicle_type)
            {
                if (vehicletypeEntity.vehicle_type == null || vehicletypeEntity.vehicle_type == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.vehicle_type);
                }
            }
            if (s_user_id)
            {
                if (vehicletypeEntity.s_user_id == null || vehicletypeEntity.s_user_id == 0)
                {
                    //return "s_user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }
            //bool s_role_Id = false
            if (s_role_Id)
            {
                if (vehicletypeEntity.s_role_Id == null || vehicletypeEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }
    }

    public class ValidateRoadsideAssistanceEntity
    {


        public static string ValidatroadsideassistanceEntity(Roadside_AssistanceEntity roadsideassistanceEntity,
                bool assistance_id = false,
                bool country_code = false,
                bool country_name = false,
                bool police_no = false,
                bool ambulance_no = false,
                bool s_user_id = false,
                bool s_role_Id = false

            )
        {

            string strResult = "";

            // check  user_id
            if (roadsideassistanceEntity == null)
            {
                // return "empty filter entity not allowed.";
                return MessageCode.ErrorMsgInJson(MsgCode.filterEntity);


            }

            //check VechileNo = false,
            if (assistance_id)
            {
                if (roadsideassistanceEntity.assistance_id == null)
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.assistance_id);
                }
            }
            if (country_name)
            {
                if (roadsideassistanceEntity.country_name == null || roadsideassistanceEntity.country_name == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.country_name);
                }
            }
            if (police_no)
            {
                if (roadsideassistanceEntity.police_no == null || roadsideassistanceEntity.police_no == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.police_no);
                }
            }
            if (ambulance_no)
            {
                if (roadsideassistanceEntity.ambulance_no == null || roadsideassistanceEntity.ambulance_no == "")
                {
                    //  return "VechileNo required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.ambulance_no);
                }
            }
            if (s_user_id)
            {
                if (roadsideassistanceEntity.s_user_id == null || roadsideassistanceEntity.s_user_id == 0)
                {
                    //return "s_user_id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }
            //bool s_role_Id = false
            if (s_role_Id)
            {
                if (roadsideassistanceEntity.s_role_Id == null || roadsideassistanceEntity.s_role_Id == 0)
                {
                    //  return "s_role_Id required in filter entity.";
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            return strResult;
        }
    }
}
