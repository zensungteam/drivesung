﻿using System;
//using System.Collections.Generic;
//using System.Linq;
using System.Net;
using System.Data;
using IOTDataModel;
using IOTBusinessEntities;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
   
       public class CompititionBS : IDisposable
       {

           private CompititionDM objCompititionDM;

           public CompititionBS()
           {
               objCompititionDM = new CompititionDM();
           }

           #region ------------disposed managed and unmanaged objects-----------

           // Flag: Has Dispose already been called?
           bool disposed = false;
           public void Dispose()
           {
               Dispose(true);
               GC.SuppressFinalize(this);
           }
           // Protected implementation of Dispose pattern.
           protected virtual void Dispose(bool disposing)
           {
               if (disposed)
                   return;

               if (disposing)
               {
                   // Free any other managed objects here.
                   if (objCompititionDM != null) { objCompititionDM.Dispose(); }
               }
               // Free any unmanaged objects here.
               disposed = true;
           }

           ~CompititionBS()
           {
               Dispose(false);
           }

           #endregion


           public Task<DataTable> CreateCompitition(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.CreateCompitition(compititionEntity));
           }
           public Task<DataTable> UpdateCompitition(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.UpdateCompitition(compititionEntity));
           }
           

           public Task<DataSet> viewcmpt(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.viewcmpt(compititionEntity));
           }
           public Task<DataSet> competitionlist(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.competitionlist(compititionEntity));
           }
           
           public Task<DataSet> GetMyCompition(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.GetMyCompition(compititionEntity));
           }
           public Task<DataTable> GetCompitionbyId(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.GetCompitionbyId(compititionEntity));
           }
           public Task<bool> JoinCmpt(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.JoinCmpt(compititionEntity));

           }
           public Task<bool> UpdateCmptProfilePath(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.UpdateCompititionProfilePath(compititionEntity));
           }
           public Task<DataSet> GetTop10Player(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.GetTop10Player(compititionEntity));
           }
           public Task<bool> CancelCompitition(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.CancelCompetition(compititionEntity));
           }

           public bool CheckJointAvailablity(CompitionEntity compititionEntity)
           {
               return objCompititionDM.CheckJointAvailablity(compititionEntity);
           }

           public DataTable Checkisclosed_cmptAvailablity(CompitionEntity compititionEntity)
           {
               return objCompititionDM.Checkisclosed_cmptAvailablity(compititionEntity);
           }

           public bool Checkshare_cmptAvailablity(CompitionEntity compititionEntity)
           {
               return objCompititionDM.Checkshare_cmptAvailablity(compititionEntity);
           }

           public Task<bool> ShareCmpt(CompitionEntity compititionEntity)
           {
               return Task.FromResult(objCompititionDM.ShareCmpt(compititionEntity));

           }
           

      }
  
}
