﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTUtilities;

namespace IOTBusinessServices
{
    public class ValidateGeofence
    {
        public static string ValidateGeofencefunc(GeofenceEntity geofenceEntity,
            bool gc_id = false,
            bool geofence_name = false,
            bool geofenec_desc = false,
            bool lat_lng = false,
            bool icon_file_name = false,
            bool created_at = false,
            bool updated_at = false,
            bool deleted = false,
            bool isactive = false,
            bool created_by = false,
            bool updated_by = false,
            bool s_user_id = false,
            bool s_role_Id = false,

            bool user_id = false,
            bool CheckGeofenceAvailability = false,
            bool CheckGeofenceAvailabilityforupdate = false

            )
        {

            string strResult = "";

            if (geofenceEntity == null)
            {
                return MessageCode.ErrorMsgInJson(MsgCode.geofenceEntity);
            }

            if (gc_id)
            {
                if (geofenceEntity.gc_id == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.gc_id);
                }
            }

            if (geofence_name)
            {
                if (geofenceEntity.geofence_name == null || geofenceEntity.geofence_name == "")
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.geofence_name);
                }
                if (CheckGeofenceAvailability)
                {
                    using (GeofenceBS objGeofenceBS = new GeofenceBS())
                    {
                        if (objGeofenceBS.CheckGeofenceAvailability(geofenceEntity))
                        {
                            return MessageCode.ErrorMsgInJson(MsgCode.CheckGeofenceAvailability);
                        }
                    }
                }

                if (CheckGeofenceAvailabilityforupdate)
                {
                    using (GeofenceBS objGeofenceBS = new GeofenceBS())
                    {
                        if (objGeofenceBS.CheckCheckGeofenceAvailabilityforupdate(geofenceEntity))
                        {
                            return MessageCode.ErrorMsgInJson(MsgCode.CheckGeofenceAvailabilityforupdate);
                        }
                    }
                }
            }

            if (geofenec_desc)
            {
                if (geofenceEntity.geofenec_desc == null || geofenceEntity.geofenec_desc == "")
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.geofenec_desc);
                }
            }

            if (lat_lng)
            {
                if (geofenceEntity.lat_lng == null || geofenceEntity.lat_lng == "")
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.lat_lng);
                }
            }

            if (icon_file_name)
            {
                if (geofenceEntity.icon_file_name == null || geofenceEntity.icon_file_name == "")
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.icon_file_name);
                }
            }

            if (created_at)
            {
                if (geofenceEntity.created_at == null)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.created_at);
                }
            }

            if (updated_at)
            {
                if (geofenceEntity.updated_at == null)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.updated_at);
                }
            }

            if (created_by)
            {
                if (geofenceEntity.created_by == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.created_by);
                }
            }

            if (updated_by)
            {
                if (geofenceEntity.updated_by == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.updated_by);
                }
            }

            if (s_user_id)
            {
                if (geofenceEntity.s_user_id == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.s_user_id);
                }
            }

            if (s_role_Id)
            {
                if (geofenceEntity.s_role_Id == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.s_role_Id);
                }
            }

            if (user_id)
            {
                if (geofenceEntity.user_id  == 0)
                {
                    return MessageCode.ErrorMsgInJson(MsgCode.user_id);
                }
            }
            return strResult;
        }
    }
}
