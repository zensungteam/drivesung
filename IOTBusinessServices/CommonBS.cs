﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using IOTBusinessEntities;
using IOTDataModel;

namespace IOTBusinessServices
{
    public class CommonBS : IDisposable
    {
        private CommonDM objCommonDM;

        public CommonBS()
        {
            objCommonDM = new CommonDM();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objCommonDM != null) { objCommonDM.Dispose(); }
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~CommonBS()
        {
            Dispose(false);
        }

        #endregion


        public Task<DataTable> GetUserList(FilterEntity filterEntity)
        {
            using (DataTable dtUser = objCommonDM.GetUserList(filterEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        public Task<DataTable> GetUserToken(NotificationEntity notificationEntity)
        {
            using (DataTable dtUser = objCommonDM.GetUserToken(notificationEntity))
            {
                return Task.FromResult(dtUser);
            }
        }

        #region Engine Capacity

        public Task<DataTable> GetEngineCapacityList()
        {
            using (DataTable dtUser = objCommonDM.GetEngineCapacityList())
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> CreateEngineCapacity(Engine_CapacityEntity enginecapacityEntity)
        {
            return Task.FromResult(objCommonDM.CreateEngineCapacity(enginecapacityEntity));
        }
        public Task<DataTable> GetEngineCapacityById(Engine_CapacityEntity enginecapacityEntity)
        {
            using (DataTable dtUser = objCommonDM.GetEngineCapacityById(enginecapacityEntity))
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> UpdateEngineCapacity(Engine_CapacityEntity enginecapacityEntity)
        {
            return Task.FromResult(objCommonDM.UpdateEngineCapacity(enginecapacityEntity));
        }

        #endregion

        #region Engine Type

        public Task<DataTable> GetEngineTypeList()
        {
            using (DataTable dtUser = objCommonDM.GetEngineTypeList())
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> CreateEngineType(Engine_TypeEntity enginetypeEntity)
        {
            return Task.FromResult(objCommonDM.CreateEngineType(enginetypeEntity));
        }
        public Task<DataTable> GetEngineTypeById(Engine_TypeEntity enginetypeEntity)
        {
            using (DataTable dtUser = objCommonDM.GetEngineTypeById(enginetypeEntity))
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> UpdateEngineType(Engine_TypeEntity enginetypeEntity)
        {
            return Task.FromResult(objCommonDM.UpdateEngineType(enginetypeEntity));
        }

        #endregion

        #region Vehicle Type
        public Task<DataTable> GetVehicleTypeList()
        {
            using (DataTable dtUser = objCommonDM.GetVehicleTypeList())
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> CreateVehicleType(Vehicle_TypeEntity vehicletypeEntity)
        {
            return Task.FromResult(objCommonDM.CreateVehicleType(vehicletypeEntity));
        }
        public Task<DataTable> GetVehicleTypeById(Vehicle_TypeEntity vehicletypeEntity)
        {
            using (DataTable dtUser = objCommonDM.GetVehicleTypeById(vehicletypeEntity))
            {
                return Task.FromResult(dtUser);
            }
        }
        public Task<bool> UpdateVehicleType(Vehicle_TypeEntity vehicletypeEntity)
        {
            return Task.FromResult(objCommonDM.UpdateVehicleType(vehicletypeEntity));
        }
        #endregion


        #region ---- Country Wise Road Assistance ----
        public Task<DataTable> GetRoadSideAssistanceByCountry(Roadside_AssistanceEntity roadsideassistanceEntity)
        {
            using (DataTable dtUser = objCommonDM.GetRoadSideAssistanceByCountry(roadsideassistanceEntity))
            {
                return Task.FromResult(dtUser);
            }
        }
        #endregion






    }
}
