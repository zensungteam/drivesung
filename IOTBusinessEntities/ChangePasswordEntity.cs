﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class ChangePasswordEntity
    {
        public string passwordPolicy { get; set; }
        public bool isEncrypted { get; set; }
        public string userCode { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string confirmPassword { get; set; }
    }
}