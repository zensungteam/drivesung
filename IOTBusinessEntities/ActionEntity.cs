﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class ActionEntity
    {
        public string ActionState { get; set; }
        public string SubActionState { get; set; }
        public Int32 RecordId { get; set; }

        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
    }
}