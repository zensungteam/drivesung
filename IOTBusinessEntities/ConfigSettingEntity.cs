﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
    public class ConfigSettingEntity
    {
        public Int32 o_id { get; set; }
        public double maxspeed { get; set; }
        public double minspeed { get; set; }
        public string overspeedlow { get; set; }
        public string overspeed10 { get; set; }
        public string overspeed20 { get; set; }
        public string overspeed30 { get; set; }
        public double overspeedlow_d_wt { get; set; }
        public double overspeed10_d_wt { get; set; }
        public double overspeed20_d_wt { get; set; }
        public double overspeed30_d_wt { get; set; }
        public double overspeedlow_n_wt { get; set; }
        public double overspeed10_n_wt { get; set; }
        public double overspeed20_n_wt { get; set; }
        public double overspeed30_n_wt { get; set; }
        public double overspeed_percentile { get; set; }
        public double overspeed_overall_wt { get; set; }
        public double harsh_ha_d_percentile { get; set; }
        public double harsh_hb_d_percentile { get; set; }
        public double harsh_cornering_d_percentile { get; set; }
        public double harsh_ha_n_percentile { get; set; }
        public double harsh_hb_n_percentile { get; set; }
        public double harsh_cornering_n_percentile { get; set; }
        public double harsh_overall_wt { get; set; }
        public string age_criteria1 { get; set; }
        public string age_criteria2 { get; set; }
        public string age_criteria3 { get; set; }
        public string age_criteria4 { get; set; }
        public double age_criteria1_wt { get; set; }
        public double age_criteria2_wt { get; set; }
        public double age_criteria3_wt { get; set; }
        public double age_criteria4_wt { get; set; }
        public double age_overall_wt { get; set; }
        public double gender_male_wt { get; set; }
        public double gender_female_wt { get; set; }
        public double gender_other_wt { get; set; }
        public double gender_overall_wt { get; set; }
        public double car_white_wt { get; set; }
        public double car_lightother_wt { get; set; }
        public double car_metallic_light_wt { get; set; }
        public double car_red_wt { get; set; }
        public double car_metallic_dark_wt { get; set; }
        public double car_darkother_wt { get; set; }
        public double car_black_wt { get; set; }
        public double car_overall_wt { get; set; }

        public double overspeed_per_def { get; set; }
        public double ha_d_per_def { get; set; }
        public double hb_d_per_def { get; set; }
        public double hc_d_per_def { get; set; }
        public double ha_n_per_def { get; set; }
        public double hb_n_per_def { get; set; }
        public double hc_n_per_def { get; set; }
        public double weather_d_percentile { get; set; }
        public double weather_n_percentile { get; set; }
        public double weather_d_per_def { get; set; }
        public double weather_n_per_def { get; set; }
        public double weather_wt { get; set; }
        public double call_d_percentile { get; set; }
        public double call_n_percentile { get; set; }
        public double call_d_per_def { get; set; }
        public double call_n_per_def { get; set; }
        public double call_wt { get; set; }


        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public bool isactive { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }


    }
    public class overspeedlimit
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class NotificationSetting
    {
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
        public bool sound_overspeed { get; set; }
        public bool triprating_notification { get; set; }
        public bool cmpt_notification { get; set; }
        public bool promotion_notification { get; set; }
        public DateTime? updated_at { get; set; }


    }
    public class responcespeedlimit
    {
        public Int32 placeId { get; set; }
        public string speedLimit { get; set; }
        public string units { get; set; }

    }


}
