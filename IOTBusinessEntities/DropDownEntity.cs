﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class DropDownEntity
    {
        public Int32 Id { get; set; }
        public string Code { get; set; }
    }
}