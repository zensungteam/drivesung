﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class FilterEntity
    {
        public string VechileNo { get; set; }
        public string DeviceNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string sfDate { get; set; }
        public string  stDate { get; set; }
        //public DateTime? FromTime { get; set; }
        //public DateTime? ToTime { get; set; }
        public Int32? NoOfRecords { get; set; }

        public Boolean? IgnitionOn { get; set; }
        public Boolean? Moving { get; set; }
        public Boolean? Parked { get; set; }
        public Boolean? UnReachable { get; set; }
        public Int32? user_id { get; set; }
        public Int32? o_id { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }

        //for  dashboard ,blotterview
        public Boolean? OverSpeed { get; set; }
        public Boolean? HA { get; set; }
        public Boolean? HB { get; set; }
        public Boolean? HC { get; set; }
        public Boolean? CT { get; set; }

        public Int32? gc_id { get; set; }

        public object  action { get; set; }
        
    }

    public class NotificationEntity
    {
        public string body { get; set; }
        public string title { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
    }


    public class Engine_CapacityEntity
    {
        public int eng_capacity_id { get; set; }
        public string eng_capacity_range { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool isactive { get; set; }
        public bool deleted { get; set; }
        public int s_user_id { get; set; }
        public int s_role_Id { get; set; }
        
    }

    public class Engine_TypeEntity
    {
        public int eng_type_id { get; set; }
        public string eng_type { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool isactive { get; set; }
        public bool deleted { get; set; }
        public int s_user_id { get; set; }
        public int s_role_Id { get; set; }
    }

     public class Vehicle_TypeEntity
    {
        public int vehicle_type_id { get; set; }
        public string vehicle_type { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool isactive { get; set; }
        public bool deleted { get; set; }
        public int s_user_id { get; set; }
        public int s_role_Id { get; set; }
    }

    public class Roadside_AssistanceEntity
    {
        public int assistance_id { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string police_no { get; set; }
        public string ambulance_no { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool deleted { get; set; }
        public int s_user_id { get; set; }
        public int s_role_Id { get; set; }
    }

   






}