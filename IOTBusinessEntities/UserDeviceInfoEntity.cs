﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public class UserDeviceInfoEntity
    {
        //UserDeviceInfo
        public string device_number { get; set; }
        public string device_type { get; set; }
        public string device_name { get; set; }
        public string device_status { get; set; }
        public string os_version { get; set; }
        public string hw_id { get; set; }
        public string user_token { get; set; }
        public string user_id { get; set; }
        public DateTime? created_at { get; set; }
        public bool isactive { get; set; }

    }
}
