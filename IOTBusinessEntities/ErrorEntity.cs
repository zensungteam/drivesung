﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class ResponseEntity
    {
        public bool IsComplete { get; set; }
        public string Description { get; set; }
        public object data { get; set; }
        public object message { get; set; }
    }
}