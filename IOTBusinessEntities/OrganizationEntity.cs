﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IOTBusinessEntities
{
    public class OrganizationEntity
    {
        //Organization_master
        public Int32 o_id { get; set; }
        public string o_code { get; set; }
        public string o_name { get; set; }
        public string o_type { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string pincode_no { get; set; }
        public string email_id1 { get; set; }
        public string email_id2 { get; set; }
        public string contact_no1 { get; set; }
        public string contact_no2 { get; set; }
        public bool isactive { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public bool deleted { get; set; }
        public Int32 s_user_id { get; set; }
        public Int32 s_role_Id { get; set; }
        public string o_logo1 { get; set; }
        public string o_logo2 { get; set; }
        public string o_logo3 { get; set; }
        public string o_logoname1 { get; set; }
        public string o_logoname2 { get; set; }
        public string o_logoname3 { get; set; }
        public List<OrganizationContactEntity> OrgContactEntities  { get; set; }

    }

    public class OrganizationContactEntity
    {
        //Organization contact details
        public Int32 o_id { get; set; }
        public string contact_person_nm { get; set; }
        public string email_id { get; set; }
        public string contact_no { get; set; }
        public DateTime? updated_at { get; set; }
        public bool deleted { get; set; }

    }


}