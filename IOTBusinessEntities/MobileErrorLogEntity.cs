﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public class MobileErrorLogEntity
    {
        public Int32 user_id { get; set; }
        public string erro_discription { get; set; }
        public string error_code { get; set; }
        public string error_file { get; set; }
        public DateTime created_at { get; set; }

    }
}
