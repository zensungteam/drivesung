﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessEntities
{
   public  class TripEntity
    {
       public Int32 trip_id { get; set; }
       public string trip_name { get; set; }
       public Int32 user_id { get; set; }
       public string trip_from { get; set; }
       public string trip_to { get; set; }
       public DateTime? trip_start_time { get; set; }
       public DateTime? trip_end_time { get; set; }
       public DateTime? created_at { get; set; }
       public DateTime? updated_at { get; set; }
       public bool deleted { get; set; }
       public bool isactive { get; set; }
       public Int32 s_user_id { get; set; }
       public Int32 s_role_Id { get; set; }
       public string category { get; set; }
       public Int32 cnt { get; set; }
       public string uploadfile { get; set; }
       public decimal m_distance { get; set; }
       public bool is_self_driving { get; set; }
       public string triplogfile { get; set; }
      

    }


}
