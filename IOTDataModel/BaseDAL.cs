﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IOTBusinessEntities;
using Npgsql;
using System.Configuration;

namespace IOTDataModel
{
    public class BaseDAL : IDisposable
    {
        private string ConnectionString = string.Empty;
        private NpgsqlConnection Connection;
        private NpgsqlCommand Command;
        private NpgsqlTransaction Transaction;

        public BaseDAL(string ConnStr)
        {
            ConnectionString = ConnStr;
            Command = Connection.CreateCommand();
            Connection = new NpgsqlConnection();
        }

        public BaseDAL()
        {
            ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConStr"];
            Connection = new NpgsqlConnection();
            Command = Connection.CreateCommand();
            Connection = new NpgsqlConnection();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                if (Connection != null) { Connection.Dispose(); }
                if (Command != null) { Command.Dispose(); }
                if (Transaction != null) { Transaction.Dispose(); }
                ConnectionString = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~BaseDAL()
        {
            Dispose(false);
        }

        #endregion

        public bool CreateConnection()
        {
            if (Connection.State != ConnectionState.Open)
            {
                Connection.ConnectionString = ConnectionString;
                Connection.Open();
            }
            return true;
        }

        public bool CloseConnection()
        {
            if (Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }

            return true;
        }

        public DataTable ExecuteSP(string SP, ref List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SP;
            Command.CommandType = CommandType.StoredProcedure;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;
            //Execute SP
            DataTable dt = new DataTable();
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            myDataAdapter.Fill(dt);
            myDataAdapter.Dispose();

            Int32 counter = 0;
            foreach (NpgsqlParameter OutParam in ParametersCollection)
            {
                if (OutParam.Direction == ParameterDirection.Output || OutParam.Direction == ParameterDirection.InputOutput)
                    ParametersCollection[counter].Value = Command.Parameters[ParametersCollection[counter].ParameterName].Value.ToString();
                counter = counter + 1;
            }
            return dt;
        }

        public bool ExecuteSQL(string SQL)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SQL;
            Command.CommandType = CommandType.Text;
            int Val = Command.ExecuteNonQuery();
            return true;
        }

        public DataTable FetchRecords(string SQL)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SQL;
            Command.CommandType = CommandType.Text;
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            DataTable dt = new DataTable();
            myDataAdapter.Fill(dt);
            myDataAdapter.Dispose();
            return dt;
        }

        public DataSet ExecuteSPwithDataset(string SP, ref List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SP;
            Command.CommandType = CommandType.StoredProcedure;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;
            //Execute SP
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            DataSet ds = new DataSet();
            myDataAdapter.Fill(ds);
            myDataAdapter.Dispose();

            Int32 counter = 0;
            foreach (NpgsqlParameter OutParam in ParametersCollection)
            {
                if (OutParam.Direction == ParameterDirection.Output || OutParam.Direction == ParameterDirection.InputOutput)
                    ParametersCollection[counter].Value = Command.Parameters[ParametersCollection[counter].ParameterName].Value.ToString();
                counter = counter + 1;
            }
            return ds;
        }

        public DataTable ExecutParameterizedQrywithDataTable(string parameterizedQry, List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = parameterizedQry;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;
            //Execute parameterized query
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            DataSet ds = new DataSet();
            myDataAdapter.Fill(ds);
            myDataAdapter.Dispose();
            return ds.Tables[0];
        }

        public DataSet ExecutParameterizedQrywithDataSet(string parameterizedQry, List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = parameterizedQry;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;
            //Execute parameterized query
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            DataSet ds = new DataSet();
            myDataAdapter.Fill(ds);
            myDataAdapter.Dispose();
            return ds;
        }

        public Boolean ExecutParameterizedQry(string parameterizedQry, List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = parameterizedQry;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;

            int Val = Command.ExecuteNonQuery();
            return true;
        }

        public object  ExecutParameterizedQryAndGetID(string parameterizedQry, List<NpgsqlParameter> ParametersCollection)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = parameterizedQry;

            foreach (NpgsqlParameter Param in ParametersCollection)
            {
                Command.Parameters.Add(Param);
            }
            if (Transaction != null)
                Command.Transaction = Transaction;

            object Val = Command.ExecuteScalar();
            return Val;
        }
        public DataSet FetchDataSetRecords(string SQL)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SQL;
            Command.CommandType = CommandType.Text;
            NpgsqlDataAdapter myDataAdapter = new NpgsqlDataAdapter(Command);
            DataSet ds = new DataSet();
            myDataAdapter.Fill(ds);
            myDataAdapter.Dispose();
            return ds;
        }

        public object GetDBFieldFromLong(string strTableName, string strKeyName, string strCriteria, long lngValue)
        {
            try
            {
                string SQL = "SELECT " + strKeyName + " FROM " + strTableName + " WHERE " + strCriteria + "=" + lngValue;
                Command.Parameters.Clear();
                Command.Connection = Connection;
                Command.CommandText = SQL;
                Command.CommandType = CommandType.Text;
                object functionReturnValue = Command.ExecuteScalar();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string FetchSingleRecord(string SQL)
        {
            Command.Parameters.Clear();
            Command.Connection = Connection;
            Command.CommandText = SQL;
            Command.CommandType = CommandType.Text;
            object objData = Command.ExecuteScalar();
            if (objData == null)
                return null;
            else
                return Convert.ToString(objData);
        }

        public bool BeginTransaction()
        {
            Transaction = Connection.BeginTransaction();
            Command.Transaction = Transaction;
            return true;
        }

        public bool CommitTransaction()
        {
            Transaction.Commit();
            return true;
        }

        public bool RollBackTransaction()
        {
            Transaction.Rollback();
            return true;
        }
    }
}