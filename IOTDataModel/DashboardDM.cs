﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using System.Configuration;
using System.IO;

namespace IOTDataModel
{
    public class DashboardDM : IDisposable
    {

        private BaseDAL objDAL;
        private DataSet dsData;
        private StringBuilder strSqlQry1;

        public DashboardDM()
        {
            objDAL = new BaseDAL();
            dsData = new DataSet();
            strSqlQry1 = new StringBuilder();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                if (dsData != null) dsData.Dispose();
                strSqlQry1 = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~DashboardDM()
        {
            Dispose(false);
        }

        #endregion

        public DataSet GetVehicleList(FilterEntity filterEntity)
        {
            try
            {

                if (filterEntity.s_role_Id == 1)
                {
                    strSqlQry1.Append(" SELECT deviceno as value,case when user_master.vehicle_no is null or user_master.vehicle_no='' then cast( deviceno as text) else user_master.vehicle_no end as text, * FROM user_master where user_id =" + filterEntity.s_user_id + " order by vehicle_no asc");
                }
                else
                {
                    strSqlQry1.Append(" SELECT deviceno as value,case when user_master.vehicle_no is null or user_master.vehicle_no='' then cast( deviceno as text) else user_master.vehicle_no end as text, * FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ") order by vehicle_no asc");
                }

                DataTable dtData;
                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "All";

                objDAL.CloseConnection();
                dtData = null;

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataSet GetDashBoardList(FilterEntity filterEntity)
        {
            StringBuilder strSqlQry2 = new StringBuilder();
            try
            {
                strSqlQry1.Append(@"SELECT round(coalesce(dr_overall_rating.rating,0)) as rating,iot_live.deviceno,CASE WHEN EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600  THEN 'disconnect' WHEN iot_live.speed=0  THEN 'stop'  ELSE 'moving' END as status, to_char(iot_live.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,iot_live.latitude,iot_live.longitude,round(iot_live.speed) as speed ,	iot_live.distance,	iot_live.overspeedstarted,
	                            iot_live.cornerpacket,iot_live.hardacceleration,iot_live.hardbraking,iot_live.mcall,iot_live.ignition,iot_obd_live.rpm,iot_obd_live.speedcan,iot_obd_live.distancecan,
	                            iot_obd_live.throttlepos,iot_obd_live.fuellevel,iot_obd_live.fuelrate,iot_obd_live.coolanttemperature, user_master.obdavaliable, CASE WHEN user_master.user_category='m'  THEN 'Mobile' WHEN user_master.user_category='mo'  THEN 'Mobile + OBD' WHEN user_master.user_category='o'  THEN 'OBD' ELSE '' END as user_category, user_master.user_name as user_name ,user_master.first_name,	user_master.middle_name,user_master.last_name,
	                            user_master.date_of_birth,user_master.gender,user_master.address1,user_master.mobileno,	user_master.email_id1,user_master.email_id2,user_master.profile_picture_uri,
	                             user_master.vehicle_no as vehicle_no,user_master.license_no,	user_master.license_expiry_date,user_master.registration_no,user_master.vehicle_manufacturer,user_master.vehicle_color,
	                            user_master.vehicle_type , CASE WHEN iot_live.overspeedstarted=1  THEN 'OS' WHEN iot_live.hardbraking=1  THEN 'HB' WHEN iot_live.hardacceleration=1  THEN 'HA'  WHEN iot_live.cornerpacket=1  THEN 'HC' WHEN iot_live.mcall=1  THEN 'CT' ELSE '' END as alert FROM  iot_live LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
	                            LEFT OUTER JOIN user_master On iot_live.deviceno = user_master.deviceno  LEFT OUTER JOIN dr_overall_rating On dr_overall_rating.deviceno = iot_live.deviceno ");
                //for end user
                if (filterEntity.s_role_Id == 1)
                {
                    strSqlQry1.Append(" where user_master.isactive=True and iot_live.deviceno in(SELECT deviceno FROM user_master where user_id =" + filterEntity.s_user_id + ") order by iot_live.recorddatetime desc");
                }
                else if (filterEntity.s_role_Id == 2)
                {
                    //for admin
                    strSqlQry1.Append(" where user_master.isactive=True and iot_live.deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ")) order by iot_live.recorddatetime desc");
                }
                else
                {
                    //for sys admin
                    strSqlQry1.Append(" where user_master.isactive=True and iot_live.deviceno in(SELECT deviceno FROM user_master where isactive=True) order by iot_live.recorddatetime desc");
                }

                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //---for live 
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "dashboardlist";
                dtData.Dispose();

                //dt for aggrgate data
                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("rating");
                dtcount.Columns.Add("distance");
                dtcount.Columns.Add("maxspeed");
                dtcount.Columns.Add("parking");
                dtcount.Columns.Add("moving");
                dtcount.Columns.Add("disconnect");
                dtcount.Columns.Add("vehiclescnt");



                //for create row
                DataRow dr = dtcount.NewRow();
                dr["distance"] = 0;
                dr["rating"] = 0;
                dr["maxspeed"] = 0;
                dr["parking"] = 0;
                dr["moving"] = 0;
                dr["disconnect"] = 0;
                dr["vehiclescnt"] = 0;

                //for overall rating 
                if (filterEntity.s_role_Id == 1) //for end user
                {
                    string overall_rating = @" SELECT	round(coalesce(distance,0),1) as distance,round(coalesce(rating,0)) as rating FROM	dr_overall_rating where deviceno in(SELECT deviceno FROM user_master where user_id =" + filterEntity.s_user_id + ");";
                    dtData = objDAL.FetchRecords(overall_rating);
                    overall_rating = null;

                    if (dtData.Rows.Count > 0)
                    {
                        dr["distance"] = CommonUtils.ConvertToDecimal(dtData.Rows[0]["distance"].ToString());
                        dr["rating"] = CommonUtils.ConvertToDecimal(dtData.Rows[0]["rating"].ToString());
                    }

                    dtData.Dispose();

                    //---for maxspeed 
                    string strmaxspeed = @"SELECT  round(max(coalesce(speed,0))) as maxspeed FROM iot_hub where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + filterEntity.s_user_id + ");";
                    _strcnt = objDAL.FetchSingleRecord(strmaxspeed);
                    dr["maxspeed"] = _strcnt;
                    strmaxspeed = null;
                }
                else if (filterEntity.s_role_Id == 2)  //for admin
                {

                    //disconnect
                    Int64 disconnect, parking, vehiclescnt, moving;
                    string strdisconnect = (@"SELECT count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + ")) and  EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600 ");
                    _strcnt = objDAL.FetchSingleRecord(strdisconnect);
                    disconnect = CommonUtils.ConvertToInt64(_strcnt);
                    dr["disconnect"] = _strcnt;

                    strdisconnect = null;

                    //parking
                    string strparking = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + ")) and  iot_live.speed=0 and EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))<=600 ");
                    _strcnt = objDAL.FetchSingleRecord(strparking);
                    parking = CommonUtils.ConvertToInt64(_strcnt);
                    dr["parking"] = _strcnt;
                    strparking = null;

                    //vehiclescnt
                    string strvehiclescnt = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + "))");
                    _strcnt = objDAL.FetchSingleRecord(strvehiclescnt);
                    vehiclescnt = CommonUtils.ConvertToInt64(_strcnt);
                    dr["vehiclescnt"] = _strcnt;
                    strvehiclescnt = null;
                    moving = vehiclescnt - (disconnect + parking);
                    dr["moving"] = moving > 0 ? moving : 0;

                }
                else //for sys admin
                {
                    //disconnect
                    Int64 disconnect, parking, vehiclescnt, moving;
                    string strdisconnect = (@"SELECT count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True) and  EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600 ");
                    _strcnt = objDAL.FetchSingleRecord(strdisconnect);
                    disconnect = CommonUtils.ConvertToInt64(_strcnt);
                    dr["disconnect"] = _strcnt;

                    strdisconnect = null;

                    //parking
                    string strparking = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True) and  iot_live.speed=0 and EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))<=600 ");
                    _strcnt = objDAL.FetchSingleRecord(strparking);
                    parking = CommonUtils.ConvertToInt64(_strcnt);
                    dr["parking"] = _strcnt;
                    strparking = null;

                    //vehiclescnt
                    string strvehiclescnt = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True)");
                    _strcnt = objDAL.FetchSingleRecord(strvehiclescnt);
                    vehiclescnt = CommonUtils.ConvertToInt64(_strcnt);
                    dr["vehiclescnt"] = _strcnt;
                    strvehiclescnt = null;
                    moving = vehiclescnt - (disconnect + parking);
                    dr["moving"] = moving > 0 ? moving : 0;

                }

                // for add new and add in ds               
                dtcount.Rows.Add(dr);

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";


                ////close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataSet GetSubHeaderCount(FilterEntity filterEntity)
        {
            try
            {

                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //dt for aggrgate data
                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("rating");
                dtcount.Columns.Add("distance");
                dtcount.Columns.Add("maxspeed");
                dtcount.Columns.Add("parking");
                dtcount.Columns.Add("moving");
                dtcount.Columns.Add("disconnect");
                dtcount.Columns.Add("vehiclescnt");



                //for create row
                DataRow dr = dtcount.NewRow();
                dr["distance"] = 0;
                dr["rating"] = 0;
                dr["maxspeed"] = 0;
                dr["parking"] = 0;
                dr["moving"] = 0;
                dr["disconnect"] = 0;
                dr["vehiclescnt"] = 0;

                //for overall rating 
                if (filterEntity.s_role_Id == 1) //for end user
                {
                    string overall_rating = @" SELECT	coalesce(distance,0) as distance,round(coalesce(rating,0)) as rating FROM	dr_overall_rating where deviceno in(SELECT deviceno FROM user_master where user_id =" + filterEntity.s_user_id + ");";
                    dtData = objDAL.FetchRecords(overall_rating);
                    overall_rating = null;

                    if (dtData.Rows.Count > 0)
                    {
                        dr["distance"] = Math.Round(CommonUtils.ConvertToDecimal(dtData.Rows[0]["distance"].ToString()), 1);
                        dr["rating"] = CommonUtils.ConvertToDecimal(dtData.Rows[0]["rating"].ToString());
                    }

                    dtData.Dispose();

                    //---for maxspeed 
                    string strmaxspeed = @"SELECT  round(max(coalesce(speed,0))) as maxspeed FROM iot_hub where deviceno in(SELECT deviceno FROM user_master where isactive=True and user_id =" + filterEntity.s_user_id + ");";
                    _strcnt = objDAL.FetchSingleRecord(strmaxspeed);
                    dr["maxspeed"] = _strcnt;
                    strmaxspeed = null;
                }
                else if (filterEntity.s_role_Id == 2)  //for admin
                {

                    //disconnect
                    Int64 disconnect, parking, vehiclescnt, moving;
                    string strdisconnect = (@"SELECT count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + ")) and  EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600 ");
                    _strcnt = objDAL.FetchSingleRecord(strdisconnect);
                    disconnect = CommonUtils.ConvertToInt64(_strcnt);
                    dr["disconnect"] = _strcnt;

                    strdisconnect = null;

                    //parking
                    string strparking = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + ")) and  iot_live.speed=0 and EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))<=600 ");
                    _strcnt = objDAL.FetchSingleRecord(strparking);
                    parking = CommonUtils.ConvertToInt64(_strcnt);
                    dr["parking"] = _strcnt;
                    strparking = null;

                    //vehiclescnt
                    string strvehiclescnt = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where isactive=True and  user_id =" + filterEntity.s_user_id + "))");
                    _strcnt = objDAL.FetchSingleRecord(strvehiclescnt);
                    vehiclescnt = CommonUtils.ConvertToInt64(_strcnt);
                    dr["vehiclescnt"] = _strcnt;
                    strvehiclescnt = null;
                    moving = vehiclescnt - (disconnect + parking);
                    dr["moving"] = moving > 0 ? moving : 0;

                }
                else //for sys admin
                {
                    //disconnect
                    Int64 disconnect, parking, vehiclescnt, moving;
                    string strdisconnect = (@"SELECT count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True) and  EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600 ");
                    _strcnt = objDAL.FetchSingleRecord(strdisconnect);
                    disconnect = CommonUtils.ConvertToInt64(_strcnt);
                    dr["disconnect"] = _strcnt;

                    strdisconnect = null;

                    //parking
                    string strparking = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True) and  iot_live.speed=0 and EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))<=600 ");
                    _strcnt = objDAL.FetchSingleRecord(strparking);
                    parking = CommonUtils.ConvertToInt64(_strcnt);
                    dr["parking"] = _strcnt;
                    strparking = null;

                    //vehiclescnt
                    string strvehiclescnt = (@"SELECT	count(*) as cnt from iot_live where deviceno in(SELECT deviceno FROM user_master where isactive=True)");
                    _strcnt = objDAL.FetchSingleRecord(strvehiclescnt);
                    vehiclescnt = CommonUtils.ConvertToInt64(_strcnt);
                    dr["vehiclescnt"] = _strcnt;
                    strvehiclescnt = null;
                    moving = vehiclescnt - (disconnect + parking);
                    dr["moving"] = moving > 0 ? moving : 0;

                }

                // for add new and add in ds               
                dtcount.Rows.Add(dr);

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";


                ////close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataSet GetLiveTrackingData(FilterEntity filterEntity)
        {

            StringBuilder strSqlQry2 = new StringBuilder();
            StringBuilder strSqlQry3 = new StringBuilder();
            string fromdate = "", todate = "";

            try
            {
                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //---for history
                strSqlQry1.Append("SELECT iot_hub.deviceno, to_char(iot_hub.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime ,CASE WHEN iot_hub.overspeedstarted=1  THEN 'Over Speed' WHEN iot_hub.hardbraking=1  THEN 'Harsh Breaking' WHEN iot_hub.hardacceleration=1  THEN 'Harsh Acceleration'  WHEN iot_hub.cornerpacket=1  THEN 'Cornering' WHEN iot_hub.mcall=1  THEN 'Call Taken' ELSE '' END as alert, iot_hub.recorddatetime as ord ,iot_hub.latitude, iot_hub.longitude, round(iot_hub.speed) as speed , iot_obd_hub.coolanttemperature, iot_obd_hub.rpm, iot_obd_hub.fuellevel,iot_hub.distance,iot_obd_hub.distancecan,coalesce(iot_hub.overspeedstarted,0) as overspeedstarted,coalesce(iot_hub.hardbraking,0) as hardbraking,coalesce(iot_hub.hardacceleration,0) as hardacceleration,coalesce(iot_hub.cornerpacket,0) as cornerpacket,coalesce(iot_hub.mcall,0) as mcall FROM  iot_hub LEFT OUTER JOIN iot_obd_hub ON iot_hub.deviceno = iot_obd_hub.deviceno ");
                strSqlQry1.Append(" AND iot_hub.recorddatetime = iot_obd_hub.recorddatetime where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " order by iot_hub.recorddatetime desc");
                if (filterEntity.NoOfRecords != null)
                {
                    strSqlQry1.Append(" limit " + Convert.ToInt16(filterEntity.NoOfRecords.ToString()) + " ");
                }
                else
                {
                    strSqlQry1.Append(" limit 1000");
                }

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dtData.DefaultView.Sort = "recorddatetime asc";
                dtData = dtData.DefaultView.ToTable();

                if (dtData.Rows.Count > 0)
                {
                    //for stoppage get date 
                    fromdate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", dtData.Rows[0]["ord"]).ToString().Replace(".", ":"); ;
                    todate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", dtData.Rows[dtData.Rows.Count - 1]["ord"]).ToString().Replace(".", ":"); ;
                }

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "history";

                //---for live
                strSqlQry2.Append(@" SELECT	iot_live.deviceno,CASE WHEN EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600  THEN 'disconnect' WHEN iot_live.speed=0  THEN 'stop'  ELSE 'moving' END as status, to_char(iot_live.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,iot_live.latitude,iot_live.longitude,	round(iot_live.speed) as speed ,	iot_live.distance,	iot_live.overspeedstarted,
	                            iot_live.cornerpacket,iot_live.hardacceleration,iot_live.hardbraking,iot_live.ignition,iot_obd_live.rpm,iot_obd_live.speedcan,iot_obd_live.distancecan,
	                            iot_obd_live.throttlepos,iot_obd_live.fuellevel,iot_obd_live.fuelrate,iot_obd_live.coolanttemperature,user_master.user_name,user_master.first_name,	user_master.middle_name,user_master.last_name,
	                            user_master.date_of_birth,user_master.gender,user_master.address1,user_master.mobileno,	user_master.email_id1,user_master.email_id2,user_master.profile_picture_uri,
	                            user_master.vehicle_no,	user_master.license_no,	user_master.license_expiry_date,user_master.registration_no,user_master.vehicle_manufacturer,user_master.vehicle_color,
	                            user_master.vehicle_type,dr_overall_rating.rating FROM  iot_live LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
	                            LEFT OUTER JOIN user_master On iot_live.deviceno = user_master.deviceno  LEFT OUTER JOIN dr_overall_rating On iot_live.deviceno = dr_overall_rating.deviceno");
                strSqlQry2.Append(" where iot_live.deviceno =" + filterEntity.DeviceNo.ToString() + "");
                dtData = objDAL.FetchRecords(strSqlQry2.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "live";

                //---for stoppage 
                if (todate != "")
                {
                    strSqlQry3.Append(@"SELECT  deviceno, count(*) as rcnt ,to_char (recorddatetime, 'dd-mm-yyyy') as rd, latitude, longitude,min(to_char(recorddatetime, 'dd-mm-yyyy HH24:MI:SS') ) as minrecorddatetime, max(to_char(recorddatetime, 'dd-mm-yyyy HH24:MI:SS')) as maxrecorddatetime,
                                        round(EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime)))/60) as diffinmin  
                                         FROM iot_hub  where iot_hub.deviceno =" + filterEntity.DeviceNo.ToString() + " and speed=0 and (recorddatetime between '" + fromdate + "' and '" + todate + "') group by deviceno,latitude,longitude,to_char (recorddatetime, 'dd-mm-yyyy') having round(EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime)))/60)>0 ");
                    dtData = objDAL.FetchRecords(strSqlQry3.ToString());
                    dsData.Tables.Add(dtData);
                    dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppage";

                }

                //dt for aggrgate data
                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("rating");
                dtcount.Columns.Add("hb");
                dtcount.Columns.Add("ha");
                dtcount.Columns.Add("cor");
                dtcount.Columns.Add("os");
                dtcount.Columns.Add("mcall");


                //for create row
                if (todate != "")
                {
                    DataRow dr = dtcount.NewRow();

                    //for overall rating 
                    string overall_rating = @" SELECT	coalesce(rating,0) FROM	dr_overall_rating where deviceno =" + filterEntity.DeviceNo.ToString() + ";";
                    _strcnt = objDAL.FetchSingleRecord(overall_rating);
                    dr["rating"] = _strcnt;
                    overall_rating = null;

                    //---for hardbraking 
                    string strhardbraking = @"SELECT  coalesce(count(*),0) as hardbraking FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and coalesce(hardbraking,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strhardbraking);
                    dr["hb"] = _strcnt;
                    strhardbraking = null;

                    //---for hardacceleration 
                    string strhardacceleration = @"SELECT  coalesce(count(*),0) as hardacceleration FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strhardacceleration);
                    dr["ha"] = _strcnt;
                    strhardacceleration = null;

                    //---for cornerpacket 
                    string strcornerpacket = @"SELECT  coalesce(count(*),0) as cornerpacket FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strcornerpacket);
                    dr["cor"] = _strcnt;
                    strcornerpacket = null;

                    //---for overspeed 
                    string stroverspeedstarted = @"SELECT  coalesce(count(*),0) as overspeedstarted FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(overspeedstarted,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(stroverspeedstarted);
                    dr["os"] = _strcnt;
                    stroverspeedstarted = null;

                    //---for mcall 
                    string strmcall = @"SELECT  coalesce(count(*),0) as mcall FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(mcall,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strmcall);
                    dr["mcall"] = _strcnt;
                    strmcall = null;

                    // for add new and add in ds               
                    dtcount.Rows.Add(dr);
                }
                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";

                //close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                strSqlQry2 = null;
                strSqlQry3 = null;
            }
        }

        public DataSet GetLiveOBDData(FilterEntity filterEntity)
        {

            StringBuilder strSqlQry2 = new StringBuilder();
            StringBuilder strSqlQry3 = new StringBuilder();
            //string fromdate = "", todate = "";

            try
            {
                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //---for history
                strSqlQry1.Append("SELECT to_char(iot_obd_hub.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as timestamp , * from iot_obd_hub where DeviceNo =" + filterEntity.DeviceNo.ToString() + " order by recorddatetime desc");
                if (filterEntity.NoOfRecords != null)
                {
                    strSqlQry1.Append(" limit " + Convert.ToInt16(filterEntity.NoOfRecords.ToString()) + " ");
                }
                else
                {
                    strSqlQry1.Append(" limit 1000");
                }
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "obddata";
                dtData.Dispose();

                DataTable dtdtcdetected = new DataTable();
                dtdtcdetected.Clear();
                dtdtcdetected.Columns.Add("recorddatetime");
                dtdtcdetected.Columns.Add("PID");
                dtdtcdetected.Columns.Add("ParameterNm");
                dtdtcdetected.Columns.Add("FaultDesc");

                DataRow dr = dtdtcdetected.NewRow();
                dr["recorddatetime"] = "16-07-2017 01:02:03";
                dr["PID"] = "65263";
                dr["ParameterNm"] = "Coolant Level";
                dr["FaultDesc"] = "Low Coolant Temprature Derate";
                dtdtcdetected.Rows.Add(dr);

                dsData.Tables.Add(dtdtcdetected);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "dtcdetected";
                dtdtcdetected.Dispose();


                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("idealrunning");
                dtcount.Columns.Add("co2");
                dtcount.Columns.Add("os");
                dtcount.Columns.Add("criticaltemp");
                dtcount.Columns.Add("faultcode");


                DataRow dr1 = dtcount.NewRow();
                dr1["idealrunning"] = 100;
                dr1["co2"] = 25;
                dr1["os"] = 109;
                dr1["criticaltemp"] = 10;
                dr1["faultcode"] = 5;
                dtcount.Rows.Add(dr1);

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";
                dtcount.Dispose();

                //close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                strSqlQry2 = null;
                strSqlQry3 = null;
            }
        }

        public DataSet GetTripPlayData(FilterEntity filterEntity)
        {

            StringBuilder strSqlQry2 = new StringBuilder();
            StringBuilder strSqlQry3 = new StringBuilder();
            string fromdate = "", todate = "";

            try
            {
                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                if (filterEntity.sfDate != null && filterEntity.stDate != null)
                {
                    //ErrorLog.LogToTripQuery("pass by fileter from date=" + filterEntity.sfDate, "GetTripPlayData");
                    //ErrorLog.LogToTripQuery("pass by fileter ToDate =" + filterEntity.stDate, "GetTripPlayData");

                    fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.sfDate);
                    todate = CommonUtils.GetstringFromatyyyy_MM_dd(filterEntity.stDate);

                    //ErrorLog.LogToTripQuery("converted format from date=" + fromdate, "GetTripPlayData");
                    //ErrorLog.LogToTripQuery("converted format ToDate =" + todate, "GetTripPlayData");
                }

                ////---for history
                //strSqlQry1.Append("SELECT iot_hub.deviceno, iot_hub.recorddatetime, iot_hub.latitude, iot_hub.longitude, iot_hub.speed, iot_obd_hub.coolanttemperature, iot_obd_hub.rpm, iot_obd_hub.fuellevel FROM  iot_hub LEFT OUTER JOIN iot_obd_hub ON iot_hub.deviceno = iot_obd_hub.deviceno ");
                //strSqlQry1.Append(" AND iot_hub.recorddatetime = iot_obd_hub.recorddatetime where iot_hub.speed>0 and iot_hub.DeviceNo ='" + filterEntity.DeviceNo.ToString() + "'");

                //---for history
                strSqlQry1.Append("SELECT iot_hub.deviceno, to_char(iot_hub.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime , iot_hub.latitude, iot_hub.longitude, round(iot_hub.speed) as speed , iot_obd_hub.coolanttemperature, iot_obd_hub.rpm, iot_obd_hub.fuellevel,iot_hub.distance,iot_obd_hub.distancecan,coalesce(iot_hub.overspeedstarted,0) as overspeedstarted,coalesce(iot_hub.hardbraking,0) as hardbraking,coalesce(iot_hub.hardacceleration,0) as hardacceleration,coalesce(iot_hub.cornerpacket,0) as cornerpacket,coalesce(iot_hub.mcall,0) as mcall,CASE WHEN iot_hub.overspeedstarted=1  THEN 'Over Speed' WHEN iot_hub.hardbraking=1  THEN 'Harsh Breaking' WHEN iot_hub.hardacceleration=1  THEN 'Harsh Acceleration'  WHEN iot_hub.cornerpacket=1  THEN 'Cornering' WHEN iot_hub.mcall=1  THEN 'Call Taken' ELSE '' END as alert FROM  iot_hub LEFT OUTER JOIN iot_obd_hub ON iot_hub.deviceno = iot_obd_hub.deviceno ");
                strSqlQry1.Append(" AND iot_hub.recorddatetime = iot_obd_hub.recorddatetime where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " ");

                if (filterEntity.sfDate != null && filterEntity.stDate != null)
                {
                    strSqlQry1.Append(" and (iot_hub.recorddatetime>='" + fromdate + "' and iot_hub.recorddatetime<='" + todate + "')");
                }
                strSqlQry1.Append(" order by iot_hub.recorddatetime asc");
                if (filterEntity.sfDate != null && filterEntity.stDate != null)
                {
                    strSqlQry1.Append(" limit 60000");
                }

                //ErrorLog.LogToTripQuery("history Query" + strSqlQry1.ToString(), "GetTripPlayData");

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());


                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "history";

                //---for speed wise driving analysis
                DataTable dtspeed = new DataTable();
                dtspeed.Columns.Add("speedrange", typeof(string));
                dtspeed.Columns.Add("percentage", typeof(double));
                dtspeed.Columns.Add("color", typeof(string));

                if (dtData.Rows.Count > 0)
                {
                    Int64 intRcount = dtData.Rows.Count;
                    string[] speedRange = new[] { "0-10", "11-20", "21-30", "31-40", "41-50", "51-60", "61-70", "71-80", "81-90", "91-100", "101-120", "121-140", "141-500" };
                    string[] speedcolor = new[] { "#f4a641", "#f4b841", "#f4c741", "#f4dc41", "#e5f441", "#c4f441", "#acf441", "#76f441", "#41f44f", "#f4b241", "#f4b841", "#f4b841", "#f4a641" };
                    int[] speedPer = new int[speedRange.Length];

                    for (int i = 0; i <= speedRange.Length - 1; i++)
                    {
                        string[] strval = speedRange[i].ToString().Split('-');

                        DataRow[] datarow = dtData.Select("speed>=" + CommonUtils.ConvertToInt(strval[0].ToString()) + " and speed<=" + CommonUtils.ConvertToInt(strval[1].ToString()));

                        decimal speedPercentage = (100 * datarow.Length) / intRcount;

                        DataRow data = dtspeed.NewRow();
                        data["speedrange"] = speedRange[i].ToString();
                        data["percentage"] = Math.Round(speedPercentage, 2);
                        data["color"] = speedcolor[i].ToString();

                        dtspeed.Rows.Add(data);

                        data = null;
                        datarow = null;
                    }
                }
                dsData.Tables.Add(dtspeed);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "speedrpt";
                dtspeed = null;



                //---for live
                strSqlQry2.Append(@" SELECT	iot_live.deviceno,CASE WHEN EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600  THEN 'disconnect' WHEN iot_live.speed=0  THEN 'stop'  ELSE 'moving' END as status, iot_live.recorddatetime,iot_live.latitude,iot_live.longitude,	round(iot_live.speed) as speed,	iot_live.distance,	iot_live.overspeedstarted,
	                            iot_live.cornerpacket,iot_live.hardacceleration,iot_live.hardbraking,iot_live.ignition,iot_obd_live.rpm,iot_obd_live.speedcan,iot_obd_live.distancecan,
	                            iot_obd_live.throttlepos,iot_obd_live.fuellevel,iot_obd_live.fuelrate,iot_obd_live.coolanttemperature,user_master.user_name,user_master.first_name,	user_master.middle_name,user_master.last_name,
	                            user_master.date_of_birth,user_master.gender,user_master.address1,user_master.mobileno,	user_master.email_id1,user_master.email_id2,user_master.profile_picture_uri,
	                            user_master.vehicle_no,	user_master.license_no,	user_master.license_expiry_date,user_master.registration_no,user_master.vehicle_manufacturer,user_master.vehicle_color,
	                            user_master.vehicle_type,dr_overall_rating.rating FROM  iot_live LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
	                            LEFT OUTER JOIN user_master On iot_live.deviceno = user_master.deviceno  LEFT OUTER JOIN dr_overall_rating On iot_live.deviceno = dr_overall_rating.deviceno");
                strSqlQry2.Append(" where iot_live.deviceno =" + filterEntity.DeviceNo.ToString() + "");
                dtData = objDAL.FetchRecords(strSqlQry2.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "live";

                //---for stoppage 
                if (todate != "")
                {
                    strSqlQry3.Append(@"SELECT  deviceno, count(*) as rcnt ,to_char (recorddatetime, 'dd-mm-yyyy') as rd, latitude, longitude,min(to_char(recorddatetime, 'dd-mm-yyyy HH24:MI:SS') ) as minrecorddatetime, max(to_char(recorddatetime, 'dd-mm-yyyy HH24:MI:SS')) as maxrecorddatetime,
                                        round(EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime)))/60) as diffinmin  
                                         FROM iot_hub  where iot_hub.deviceno =" + filterEntity.DeviceNo.ToString() + " and  speed=0 and (recorddatetime between '" + fromdate + "' and '" + todate + "') group by deviceno,latitude,longitude,to_char (recorddatetime, 'dd-mm-yyyy') having round(EXTRACT(EPOCH FROM (max(recorddatetime) - min(recorddatetime)))/60) >0");

                    //ErrorLog.LogToTripQuery("stoppage Query" + strSqlQry3.ToString(), "GetTripPlayData");

                    dtData = objDAL.FetchRecords(strSqlQry3.ToString());
                    dsData.Tables.Add(dtData);
                    dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppage";


                    dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppage";
                }

                //dt for aggrgate data
                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("rating");
                dtcount.Columns.Add("hb");
                dtcount.Columns.Add("ha");
                dtcount.Columns.Add("cor");
                dtcount.Columns.Add("os");
                dtcount.Columns.Add("mcall");


                //for create row
                DataRow dr = dtcount.NewRow();

                //for overall rating 
                string overall_rating = @" SELECT	coalesce(rating,0) FROM	dr_overall_rating where deviceno =" + filterEntity.DeviceNo.ToString() + ";";
                _strcnt = objDAL.FetchSingleRecord(overall_rating);
                dr["rating"] = _strcnt;
                overall_rating = null;

                //---for hardbraking 
                string strhardbraking = @"SELECT  coalesce(count(*),0) as hardbraking FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and coalesce(hardbraking,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                _strcnt = objDAL.FetchSingleRecord(strhardbraking);
                dr["hb"] = _strcnt;
                strhardbraking = null;

                //---for hardacceleration 
                string strhardacceleration = @"SELECT  coalesce(count(*),0) as hardacceleration FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                _strcnt = objDAL.FetchSingleRecord(strhardacceleration);
                dr["ha"] = _strcnt;
                strhardacceleration = null;

                //---for cornerpacket 
                string strcornerpacket = @"SELECT  coalesce(count(*),0) as cornerpacket FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                _strcnt = objDAL.FetchSingleRecord(strcornerpacket);
                dr["cor"] = _strcnt;
                strcornerpacket = null;

                //---for overspeed 
                string stroverspeedstarted = @"SELECT  coalesce(count(*),0) as overspeedstarted FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(overspeedstarted,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                _strcnt = objDAL.FetchSingleRecord(stroverspeedstarted);
                dr["os"] = _strcnt;
                stroverspeedstarted = null;

                //---for mcall 
                string strmcall = @"SELECT  coalesce(count(*),0) as mcall FROM  iot_hub where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " and  coalesce(mcall,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                _strcnt = objDAL.FetchSingleRecord(strmcall);
                dr["mcall"] = _strcnt;
                strmcall = null;

                // for add new and add in ds               
                dtcount.Rows.Add(dr);

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";

                //close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                strSqlQry2 = null;
                strSqlQry3 = null;
            }

        }
        //for omkar sir
        public DataTable GetLiveData(FilterEntity filterEntity)
        {

            try
            {
                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //---for history
                strSqlQry1.Append("select iot_live.*,iot_obd_live.* FROM  iot_live LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime  where  iot_live.deviceno =" + filterEntity.DeviceNo.ToString());

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());

                //close conn
                objDAL.CloseConnection();

                return dtData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataSet
           GetGraphData(FilterEntity filterEntity)
        {
            try
            {
                DataTable dtData;
                objDAL.CreateConnection();

                string _fromdate = "", _todate = "";
                string limt = "7";

                #region "daily_agg"
                strSqlQry1 = new StringBuilder();
                strSqlQry1.Append("SELECT deviceno,to_char(recorddatetime, 'dd-mm') as recorddate , round (distance,1) as distance , round (distance_day,1) as distance_d ,round (distance_night,1) as distance_n, ((drivinghrs*10)/3600) as drivinghrs, round((daydrivinghrs*10)/3600) as daydrivinghrs, ((nightdrivinghrs*10)/3600) as nightdrivinghrs,round(avgspeed) as avgspeed,round(avgspeed_day) as avgspeed_day, round(avgspeed_night) as avgspeed_night, round(maxspeed) as maxspeed, maxspeed_day, maxspeed_night,hardacceleration, hardacceleration_day, hardacceleration_night, hardbraking, hardbraking_day, hardbraking_night, cornerpacket, cornerpacket_day, cornerpacket_night,overspeedstarted,overspeedstarted_day,overspeedstarted_night,mcall,mcall_day,mcall_night ");
                strSqlQry1.Append(" FROM rpt_daily_agg_parameter where deviceno=" + filterEntity.DeviceNo + " order by recorddatetime desc limit " + limt);
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                strSqlQry1 = null;
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "daily_agg";

                #endregion

                #region "stoppagedata"
                //---get device latest data live for genrate weekly report
                strSqlQry1 = new StringBuilder();
                strSqlQry1.Append("SELECT deviceno, to_char(recorddatetime - cast('7 day' as interval), 'yyyy-MM-dd') as fd, to_char(recorddatetime, 'yyyy-MM-dd') as td from iot_live where DeviceNo =" + filterEntity.DeviceNo.ToString());
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                strSqlQry1 = null;

                if (dtData.Rows.Count == 0)
                {
                    return dsData;
                }

                _fromdate = dtData.Rows[0]["fd"].ToString();
                _todate = dtData.Rows[0]["td"].ToString();


                string[] stoppageRange = new[] { "0-10", "11-20", "21-30", "31-40", "41-999" };
                int[] stoppagecnt = new int[stoppageRange.Length];

                strSqlQry1 = new StringBuilder();
                strSqlQry1.Append("SELECT deviceno,to_char(recorddatetime, 'dd-mm') as recorddatetime , latitude, longitude,to_char(startdatetime, 'dd-mm-yyyy HH24:MI:SS') as startdatetime, to_char(enddatetime, 'dd-mm-yyyy HH24:MI:SS') as enddatetime, sysdatetime, noofrecords,  round (stoppage) as  stoppage_minut FROM rpt_daily_stoppage where deviceno=" + filterEntity.DeviceNo + " and ( to_char (recorddatetime, 'yyyy-MM-dd')  between '" + _fromdate + "' and '" + _todate + "') and round (stoppage)> 0 ");

                //---for history
                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dtData = dtData.DefaultView.ToTable();

                DataView view = new DataView(dtData);
                DataTable distinctValues = view.ToTable(true, "recorddatetime");

                #region create dtstoppage coloumn
                DataTable dtstoppage = new DataTable();
                dtstoppage.Columns.Add("recorddatetime");
                for (int i = 0; i <= stoppageRange.Length - 1; i++)
                {
                    dtstoppage.Columns.Add(stoppageRange[i].ToString(), typeof(int));
                    stoppagecnt[i] = 0;
                }
                #endregion

                foreach (DataRow datarow in distinctValues.Rows)
                {
                    String filter = "recorddatetime='" + datarow["recorddatetime"] + "'";
                    DataRow[] dr;
                    dr = dtData.Select(filter);

                    #region create dtstoppage coloumn
                    foreach (DataRow dtrow in dr)
                    {
                        for (int i = 0; i <= stoppageRange.Length - 1; i++)
                        {
                            string[] strval = stoppageRange[i].ToString().Split('-');
                            if ((CommonUtils.ConvertToInt(dtrow["stoppage_minut"]) >= CommonUtils.ConvertToInt(strval[0].ToString())) && (CommonUtils.ConvertToInt(dtrow["stoppage_minut"]) <= CommonUtils.ConvertToInt(strval[1].ToString())))
                            {
                                stoppagecnt[i] = stoppagecnt[i] + 1;
                                break;
                            }
                        }

                    }
                    #endregion

                    DataRow data = dtstoppage.NewRow();
                    data["recorddatetime"] = datarow["recorddatetime"];
                    for (int i = 0; i <= stoppageRange.Length - 1; i++)
                    {
                        data[stoppageRange[i].ToString()] = CommonUtils.ConvertToInt(stoppagecnt[i].ToString());
                    }
                    dtstoppage.Rows.Add(data);

                    for (int i = 0; i <= stoppageRange.Length - 1; i++)
                    {
                        stoppagecnt[i] = 0;
                    }

                }
                dsData.Tables.Add(dtstoppage);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "stoppagedata";
                #endregion

                objDAL.CloseConnection();
                dtData = null;

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataSet GetMapAllVehiclesData(FilterEntity filterEntity)
        {
            StringBuilder strWherCond = new StringBuilder();
            DataTable dtData;
            try
            {

                if (filterEntity.HB != null)
                {
                    if (Convert.ToBoolean(filterEntity.HB))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" coalesce(iot_live.hardbraking,0)=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or coalesce(iot_live.hardbraking,0)=1 ");
                        }
                    }

                }

                if (filterEntity.OverSpeed != null)
                {
                    if (Convert.ToBoolean(filterEntity.OverSpeed))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" coalesce(iot_live.overspeedstarted,0)=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or coalesce(iot_live.overspeedstarted,0)=1 ");
                        }
                    }

                }

                if (filterEntity.HC != null)
                {
                    if (Convert.ToBoolean(filterEntity.HC))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" coalesce(iot_live.cornerpacket,0)=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or coalesce(iot_live.cornerpacket,0)=1 ");
                        }
                    }

                }

                if (filterEntity.CT != null)
                {
                    if (Convert.ToBoolean(filterEntity.CT))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" coalesce(iot_live.mcall,0)=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or coalesce(iot_live.mcall,0)=1 ");
                        }
                    }

                }

                if (filterEntity.HA != null)
                {
                    if (Convert.ToBoolean(filterEntity.HA))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" coalesce(iot_live.hardacceleration,0)=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or coalesce(iot_live.hardacceleration,0)=1 ");
                        }
                    }

                }

                //if (filterEntity.IgnitionOn != null)
                //{
                //    if (Convert.ToBoolean(filterEntity.IgnitionOn))
                //    {
                //        if (strWherCond.ToString() == "")
                //        {
                //            strWherCond.Append(" coalesce(ignitionstatus,0)=1 ");
                //        }
                //        else
                //        {
                //            strWherCond.Append(" or coalesce(ignitionstatus,0)=1 ");
                //        }
                //    }

                //}

                //if (filterEntity.Moving != null)
                //{
                //    if (Convert.ToBoolean(filterEntity.Moving))
                //    {
                //        if (strWherCond.ToString() == "")
                //        {
                //            strWherCond.Append(" coalesce(speed,0)>0 ");
                //        }
                //        else
                //        {
                //            strWherCond.Append(" or coalesce(speed,0)>0 ");
                //        }
                //    }

                //}

                //if (filterEntity.Parked != null)
                //{
                //    if (Convert.ToBoolean(filterEntity.Parked))
                //    {
                //        if (strWherCond.ToString() == "")
                //        {
                //            strWherCond.Append(" coalesce(speed,0)=0 ");
                //        }
                //        else
                //        {
                //            strWherCond.Append(" or coalesce(speed,0)=0 ");
                //        }
                //    }

                //}

                //if (filterEntity.UnReachable != null)
                //{
                //    if (Convert.ToBoolean(filterEntity.UnReachable))
                //    {
                //        if (strWherCond.ToString() == "")
                //        {
                //            strWherCond.Append(" EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - sysdatetime))>600 ");
                //        }
                //        else
                //        {
                //            strWherCond.Append(" or EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - sysdatetime))>600 ");
                //        }
                //    }

                //}



                //if (filterEntity.TA != null)
                //{
                //    if (Convert.ToBoolean(filterEntity.TA))
                //    {
                //        if (strWherCond.ToString() == "")
                //        {
                //            strWherCond.Append(" tamperalert='1' ");
                //        }
                //        else
                //        {
                //            strWherCond.Append(" or tamperalert='1' ");
                //        }

                //    }

                //}
                //where coalesce(ignitionstatus,0)=0
                strSqlQry1.Append(@"SELECT user_name,first_name,middle_name,last_name,date_of_birth,gender,	address1,address2,address3,	user_master.mobileno,email_id1,	profile_picture_uri,
	                            vehicle_no,	license_no,	license_expiry_date,registration_no,vehicle_manufacturer,vehicle_color,	vehicle_type,user_login_type,insurance_mapping_key, 
                                iot_live.deviceno, to_char(iot_live.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,iot_live.latitude, iot_live.longitude, iot_live.speed,iot_live.ignitionstatus, iot_live.hardacceleration, iot_live.hardbraking,iot_live.tamperalert,iot_live.overspeedstarted,iot_live.cornerpacket,iot_live.mcall,
                                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime)) as diffinSec,iot_obd_live.rpm, 
                                iot_obd_live.fuellevel,iot_live.distance,iot_obd_live.distancecan,iot_obd_live.coolanttemperature  FROM iot_live LEFT OUTER JOIN
                                user_master ON iot_live.deviceno = user_master.deviceno LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
                                where iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "))");

                if (strWherCond.ToString() != "")
                    strSqlQry1.Append(" And (" + strWherCond.ToString() + ")");


                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dtData.DefaultView.Sort = "recorddatetime desc";
                dtData = dtData.DefaultView.ToTable();

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "All";


                //dt for aggrgate data
                string _strcnt;
                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("hb");
                dtcount.Columns.Add("ha");
                dtcount.Columns.Add("cor");
                dtcount.Columns.Add("os");
                dtcount.Columns.Add("mcall");


                //for create row
                DataRow dr = dtcount.NewRow();

                //---for hardbraking 
                string strhardbraking = @"SELECT  coalesce(count(*),0) as hardbraking FROM  iot_live where coalesce(hardbraking,0)=1 and iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "));";
                _strcnt = objDAL.FetchSingleRecord(strhardbraking);
                dr["hb"] = _strcnt;
                strhardbraking = null;

                //---for hardacceleration 
                string strhardacceleration = @"SELECT  coalesce(count(*),0) as hardacceleration FROM  iot_live where  coalesce(hardacceleration,0)=1  and iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "));";
                _strcnt = objDAL.FetchSingleRecord(strhardacceleration);
                dr["ha"] = _strcnt;
                strhardacceleration = null;

                //---for cornerpacket 
                string strcornerpacket = @"SELECT  coalesce(count(*),0) as cornerpacket FROM  iot_live where coalesce(cornerpacket,0)=1  and iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "));";
                _strcnt = objDAL.FetchSingleRecord(strcornerpacket);
                dr["cor"] = _strcnt;
                strcornerpacket = null;

                //---for overspeed 
                string stroverspeedstarted = @"SELECT  coalesce(count(*),0) as overspeedstarted FROM  iot_live where coalesce(overspeedstarted,0)=1  and iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "));";
                _strcnt = objDAL.FetchSingleRecord(stroverspeedstarted);
                dr["os"] = _strcnt;
                stroverspeedstarted = null;

                //---for mcall 
                string strmcall = @"SELECT  coalesce(count(*),0) as mcall FROM  iot_live where coalesce(mcall,0)=1  and iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "));";
                _strcnt = objDAL.FetchSingleRecord(strmcall);
                dr["mcall"] = _strcnt;
                strmcall = null;

                // for add new and add in ds               
                dtcount.Rows.Add(dr);
                dr = null;

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";

                objDAL.CloseConnection();
                dtData = null;

                return dsData;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                dsData = null;
                strWherCond = null;

                throw ex;
            }
        }

        public DataSet GetBlotterViewData(FilterEntity filterEntity)
        {
            StringBuilder strWherCond = new StringBuilder();
            try
            {
                //for end user
                if (filterEntity.s_role_Id == 1)
                {
                    return GetBlotterViewUserData(filterEntity);
                }
                else
                {
                    //for admin
                    return GetBlotterViewAdminData(filterEntity);
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DataSet GetBlotterViewAdminData(FilterEntity filterEntity)
        {
            StringBuilder strWherCond = new StringBuilder();
            DataTable dtData;
            try
            {

                strSqlQry1.Append(@"SELECT CASE WHEN EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600  THEN 'disconnect' WHEN iot_live.speed=0  THEN 'stop'  ELSE 'moving' END as status,user_name,first_name,middle_name,last_name,date_of_birth,date_part('year', age(CURRENT_TIMESTAMP,date_of_birth ))  as age,gender,	address1,address2,address3,	user_master.mobileno,email_id1,	profile_picture_uri,
	                            vehicle_no,	license_no,	license_expiry_date,registration_no,vehicle_manufacturer,vehicle_color,	vehicle_type,user_login_type,insurance_mapping_key, 
                                iot_live.deviceno, to_char(iot_live.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,iot_live.latitude, iot_live.longitude, round(iot_live.speed) as speed,iot_live.ignitionstatus, iot_live.hardacceleration, iot_live.hardbraking,iot_live.tamperalert,iot_live.overspeedstarted,iot_live.cornerpacket,iot_live.mcall,
                                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime)) as diffinSec,iot_obd_live.rpm, 
                                iot_obd_live.fuellevel,iot_live.distance,iot_obd_live.distancecan,iot_obd_live.coolanttemperature  FROM iot_live LEFT OUTER JOIN
                                user_master ON iot_live.deviceno = user_master.deviceno LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
                                where iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "))");

                objDAL.CreateConnection();

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dtData.DefaultView.Sort = "recorddatetime desc";
                dtData = dtData.DefaultView.ToTable();

                #region for count

                DataTable dtcount = new DataTable();
                dtcount.Clear();
                dtcount.Columns.Add("hb");
                dtcount.Columns.Add("ha");
                dtcount.Columns.Add("cor");
                dtcount.Columns.Add("os");
                dtcount.Columns.Add("mcall");

                dtcount.Columns.Add("ageL25");
                dtcount.Columns.Add("ageI26_55");
                dtcount.Columns.Add("agegI55_75");
                dtcount.Columns.Add("ageG75");
                dtcount.Columns.Add("NA");


                dtcount.Columns.Add("male");
                dtcount.Columns.Add("female");
                dtcount.Columns.Add("other");


                dtcount.Columns.Add("car_red");
                dtcount.Columns.Add("car_white");
                dtcount.Columns.Add("car_otherlight");
                dtcount.Columns.Add("car_metalliclight");
                dtcount.Columns.Add("car_metallicdark");
                dtcount.Columns.Add("car_otherdark");
                dtcount.Columns.Add("car_black");
                dtcount.Columns.Add("car_colorother");
                dtcount.Columns.Add("car_color_na");

                //for create row
                DataRow dr = dtcount.NewRow();

                int _intcnt = 0;
                //                string _strCountQryTemp;
                //                string _strCountQry = @"SELECT coalesce(count(*),0) as cnt from iot_live LEFT OUTER JOIN
                //                                user_master ON iot_live.deviceno = user_master.deviceno where iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ")) ";


                //---for hardbraking 
                _intcnt = dtData.Select("hardbraking =1").Length;
                dr["hb"] = _intcnt;

                //---for hardacceleration 
                _intcnt = dtData.Select("hardacceleration =1").Length;
                dr["ha"] = _intcnt;

                //---for cornerpacket 
                _intcnt = dtData.Select("cornerpacket =1").Length;
                dr["cor"] = _intcnt;

                //---for overspeed 
                _intcnt = dtData.Select("overspeedstarted =1").Length;
                dr["os"] = _intcnt;

                //---for mcall 
                _intcnt = dtData.Select("mcall =1").Length;
                dr["mcall"] = _intcnt;


                //---for ageL25 
                _intcnt = dtData.Select("age<=25").Length;
                dr["ageL25"] = _intcnt;

                //---for ageI26_55 
                _intcnt = dtData.Select("age>25 and age<=55").Length;
                dr["ageI26_55"] = _intcnt;


                //---for agegI55_75 
                _intcnt = dtData.Select("age>55 and age<=75").Length;
                dr["agegI55_75"] = _intcnt;


                //---for agegI55_75 
                _intcnt = dtData.Select("age>75").Length;
                dr["ageG75"] = _intcnt;


                //---for NA 
                _intcnt = dtData.Select("age is null").Length;
                dr["NA"] = _intcnt;

                //---for male 
                _intcnt = dtData.Select("gender='male'").Length;
                dr["male"] = _intcnt;

                //---for female 
                _intcnt = dtData.Select("gender='female'").Length;
                dr["female"] = _intcnt;

                //---for other 
                _intcnt = dtData.Select("gender not in ('male','female')").Length;
                dr["other"] = _intcnt;

                //---for car_red 
                _intcnt = dtData.Select("vehicle_color='red'").Length;
                dr["car_red"] = _intcnt;

                //---for car_white 
                _intcnt = dtData.Select("vehicle_color='white'").Length;
                dr["car_white"] = _intcnt;

                //---for car_otherlight 
                _intcnt = dtData.Select("vehicle_color='otherlight'").Length;
                dr["car_otherlight"] = _intcnt;

                //---for female 
                _intcnt = dtData.Select("vehicle_color='metalliclight'").Length;
                dr["car_metalliclight"] = _intcnt;

                //---for car_metallicdark 
                _intcnt = dtData.Select("vehicle_color='metallicdark'").Length;
                dr["car_metallicdark"] = _intcnt;


                //---for car_otherdark 
                _intcnt = dtData.Select("vehicle_color='otherdark'").Length;
                dr["car_otherdark"] = _intcnt;

                //---for car_black 
                _intcnt = dtData.Select("vehicle_color='black'").Length;
                dr["car_black"] = _intcnt;

                //---for car_colorother 
                _intcnt = dtData.Select("vehicle_color <>'' and vehicle_color is not null  and vehicle_color not in ('red','white','otherlight','metalliclight','metallicdark','otherdark','black')").Length;
                dr["car_colorother"] = _intcnt;

                //---for car_color_na 
                _intcnt = dtData.Select("vehicle_color is null or vehicle_color='' ").Length;
                dr["car_color_na"] = _intcnt;


                //add new row and add in ds
                dtcount.Rows.Add(dr);

                #endregion

                dsData.Tables.Add(dtcount);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";

                #region for filter data
                if (filterEntity.HB != null)
                {
                    if (Convert.ToBoolean(filterEntity.HB))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" hardbraking=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or hardbraking=1 ");
                        }
                    }

                }

                if (filterEntity.OverSpeed != null)
                {
                    if (Convert.ToBoolean(filterEntity.OverSpeed))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" overspeedstarted=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or overspeedstarted=1 ");
                        }
                    }

                }

                if (filterEntity.HC != null)
                {
                    if (Convert.ToBoolean(filterEntity.HC))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" cornerpacket=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or cornerpacket=1 ");
                        }
                    }

                }

                if (filterEntity.CT != null)
                {
                    if (Convert.ToBoolean(filterEntity.CT))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" mcall=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or mcall=1 ");
                        }
                    }

                }

                if (filterEntity.HA != null)
                {
                    if (Convert.ToBoolean(filterEntity.HA))
                    {
                        if (strWherCond.ToString() == "")
                        {
                            strWherCond.Append(" hardacceleration=1 ");
                        }
                        else
                        {
                            strWherCond.Append(" or hardacceleration=1 ");
                        }
                    }

                }
                if (strWherCond.ToString() != "")
                {
                    DataView dv1 = dtData.DefaultView;
                    dv1.RowFilter = strWherCond.ToString();
                    dtData = dv1.ToTable();
                }


                #endregion

                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "All";




                objDAL.CloseConnection();
                dtData = null;

                return dsData;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                dsData = null;

                throw ex;
            }
        }

        public DataSet GetBlotterViewUserData(FilterEntity filterEntity)
        {

            try
            {
                DataTable dtData;
                //open conn
                objDAL.CreateConnection();

                //---for history
                strSqlQry1 = new StringBuilder();
                strSqlQry1.Append("SELECT iot_hub.deviceno, to_char(iot_hub.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime , iot_hub.latitude, iot_hub.longitude, round(iot_hub.speed) as speed , iot_obd_hub.coolanttemperature, iot_obd_hub.rpm, iot_obd_hub.fuellevel,iot_hub.distance,iot_obd_hub.distancecan,coalesce(iot_hub.overspeedstarted,0) as overspeedstarted,coalesce(iot_hub.hardbraking,0) as hardbraking,coalesce(iot_hub.hardacceleration,0) as hardacceleration,coalesce(iot_hub.cornerpacket,0) as cornerpacket,coalesce(iot_hub.mcall,0) as mcall,CASE WHEN iot_hub.overspeedstarted=1  THEN 'Over Speed' WHEN iot_hub.hardbraking=1  THEN 'Harsh Breaking' WHEN iot_hub.hardacceleration=1  THEN 'Harsh Acceleration'  WHEN iot_hub.cornerpacket=1  THEN 'Cornering' WHEN iot_hub.mcall=1  THEN 'Call Taken' ELSE '' END as alert FROM  iot_hub LEFT OUTER JOIN iot_obd_hub ON iot_hub.deviceno = iot_obd_hub.deviceno ");
                strSqlQry1.Append(" AND iot_hub.recorddatetime = iot_obd_hub.recorddatetime where iot_hub.DeviceNo =" + filterEntity.DeviceNo.ToString() + " ");
                strSqlQry1.Append(" order by iot_hub.recorddatetime desc limit 20000");

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
               
                //---for speed wise driving analysis
                DataTable dtspeed = new DataTable();
                dtspeed.Columns.Add("speedrange", typeof(string));
                dtspeed.Columns.Add("percentage", typeof(double));
                dtspeed.Columns.Add("color", typeof(string));

                if (dtData.Rows.Count > 0)
                {
                    Int64 intRcount = dtData.Rows.Count;
                    string[] speedRange = new[] { "0-10", "11-20", "21-30", "31-40", "41-50", "51-60", "61-70", "71-80", "81-90", "91-100", "101-120", "121-140", "141-500" };
                    string[] speedcolor = new[] { "#f4a641", "#f4b841", "#f4c741", "#f4dc41", "#e5f441", "#c4f441", "#acf441", "#76f441", "#41f44f", "#f4b241", "#f4b841", "#f4b841", "#f4a641" };
                    int[] speedPer = new int[speedRange.Length];

                    for (int i = 0; i <= speedRange.Length - 1; i++)
                    {
                        string[] strval = speedRange[i].ToString().Split('-');

                        DataRow[] datarow = dtData.Select("speed>=" + CommonUtils.ConvertToInt(strval[0].ToString()) + " and speed<=" + CommonUtils.ConvertToInt(strval[1].ToString()));

                        decimal speedPercentage = (100 * datarow.Length) / intRcount;

                        DataRow data = dtspeed.NewRow();
                        data["speedrange"] = speedRange[i].ToString();
                        data["percentage"] = Math.Round(speedPercentage, 2);
                        data["color"] = speedcolor[i].ToString();

                        dtspeed.Rows.Add(data);

                        data = null;
                        datarow = null;
                    }
                }
                dsData.Tables.Add(dtspeed);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "speedrpt";
                dtspeed = null;

                //---for live
                strSqlQry1 = new StringBuilder();
                strSqlQry1.Append(@"SELECT CASE WHEN EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime))>600  THEN 'disconnect' WHEN iot_live.speed=0  THEN 'stop'  ELSE 'moving' END as status,user_name,first_name,middle_name,last_name,date_of_birth,date_part('year', age(CURRENT_TIMESTAMP,date_of_birth ))  as age,gender,	address1,address2,address3,	user_master.mobileno,email_id1,	profile_picture_uri,
	                            vehicle_no,	license_no,	license_expiry_date,registration_no,vehicle_manufacturer,vehicle_color,	vehicle_type,user_login_type,insurance_mapping_key, 
                                iot_live.deviceno, to_char(iot_live.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime,iot_live.latitude, iot_live.longitude, iot_live.speed,iot_live.ignitionstatus, iot_live.hardacceleration, iot_live.hardbraking,iot_live.tamperalert,iot_live.overspeedstarted,iot_live.cornerpacket,iot_live.mcall,
                                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - iot_live.sysdatetime)) as diffinSec,iot_obd_live.rpm, 
                                iot_obd_live.fuellevel,iot_live.distance,iot_obd_live.distancecan,iot_obd_live.coolanttemperature  FROM iot_live LEFT OUTER JOIN
                                user_master ON iot_live.deviceno = user_master.deviceno LEFT OUTER JOIN iot_obd_live ON iot_live.deviceno = iot_obd_live.deviceno AND iot_live.recorddatetime = iot_obd_live.recorddatetime 
                                where iot_live.deviceno='" + filterEntity.DeviceNo.ToString() + "'");

                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "live";


                //---for count 
                string strcount = @"SELECT  coalesce(sum(hardacceleration),0) as hardacceleration ,coalesce(sum(hardbraking),0) as hardbraking ,coalesce(sum(cornerpacket),0) as cornerpacket ,coalesce(sum(overspeedstarted),0) as overspeedstarted ,coalesce(sum(mcall),0) as mcall  FROM  rpt_daily_agg_parameter where DeviceNo =" + filterEntity.DeviceNo.ToString() + ";";
                dtData = objDAL.FetchRecords(strcount);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "count";
                strcount = null;

                //---for overall_rating 
                string stroverall_rating = @"SELECT deviceno, round(coalesce(distance,0),1) as distance, round(coalesce(overspeedscore,0)) as overspeedscore ,round(coalesce(harsh_score,0)) as harsh_score ,  round(coalesce(age_score,0)) as age_score,round(coalesce(gender_score,0)) as gender_score ,
                                          round(coalesce(car_score,0)) as car_score ,round(coalesce(weather_score,0)) as weather_score  ,round(coalesce(calling_score,0)) as  calling_score ,round(coalesce(rating,0)) as rating from dr_overall_rating where deviceno =" + filterEntity.DeviceNo.ToString() + ";";
                dtData = objDAL.FetchRecords(stroverall_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overall_rating";
                stroverall_rating = null;


                //---for daily_parameter_wise_rating 
                string strdaily_rating = @"select to_char (recorddate, 'dd-mm-yyyy') as recorddate,round(coalesce(final_score,0)) as final_score , round(coalesce(distance,0),1) as distance  from dr_daily_parameter_wise_rating where deviceno =" + filterEntity.DeviceNo.ToString() + " order by recorddate desc;";
                dtData = objDAL.FetchRecords(strdaily_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "daily_rating";
                strdaily_rating = null;


                //---for overall_trip_rating 
                string stroverall_trip_rating = @"select dr_overall_trip_rating.trip_id,trip_planning_master.trip_name, round(coalesce(rating,0)) as rating , round(coalesce(distance,0),1) as distance ,trip_planning_master.m_distance from dr_overall_trip_rating inner join trip_planning_master on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id where deviceno =" + filterEntity.DeviceNo.ToString() + " order by trip_id desc limit 30;";
                dtData = objDAL.FetchRecords(stroverall_trip_rating);
                dsData.Tables.Add(dtData);
                dsData.Tables[dsData.Tables.Count - 1].TableName = "overall_trip_rating";
                stroverall_trip_rating = null;


                //close conn
                objDAL.CloseConnection();

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        //        public DataSet GetBlotterViewAdminfilterData(FilterEntity filterEntity)
        //        {
        //            StringBuilder strWherCond = new StringBuilder();
        //            DataTable dtData;
        //            try
        //            {
        //                if (filterEntity.OverSpeed != null)
        //                {
        //                    if (Convert.ToBoolean(filterEntity.OverSpeed))
        //                    {
        //                        if (strWherCond.ToString() == "")
        //                        {
        //                            strWherCond.Append(" coalesce(speed,0)>100 ");
        //                        }
        //                        else
        //                        {
        //                            strWherCond.Append(" or coalesce(speed,0)>100) ");
        //                        }
        //                    }

        //                }
        //                //if (filterEntity.IgnitionOn != null)
        //                //{
        //                //    if (Convert.ToBoolean(filterEntity.IgnitionOn))
        //                //    {
        //                //        if (strWherCond.ToString() == "")
        //                //        {
        //                //            strWherCond.Append(" coalesce(ignitionstatus,0)=1 ");
        //                //        }
        //                //        else
        //                //        {
        //                //            strWherCond.Append(" or coalesce(ignitionstatus,0)=1 ");
        //                //        }
        //                //    }

        //                //}

        //                //if (filterEntity.Moving != null)
        //                //{
        //                //    if (Convert.ToBoolean(filterEntity.Moving))
        //                //    {
        //                //        if (strWherCond.ToString() == "")
        //                //        {
        //                //            strWherCond.Append(" coalesce(speed,0)>0 ");
        //                //        }
        //                //        else
        //                //        {
        //                //            strWherCond.Append(" or coalesce(speed,0)>0 ");
        //                //        }
        //                //    }

        //                //}

        //                //if (filterEntity.Parked != null)
        //                //{
        //                //    if (Convert.ToBoolean(filterEntity.Parked))
        //                //    {
        //                //        if (strWherCond.ToString() == "")
        //                //        {
        //                //            strWherCond.Append(" coalesce(speed,0)=0 ");
        //                //        }
        //                //        else
        //                //        {
        //                //            strWherCond.Append(" or coalesce(speed,0)=0 ");
        //                //        }
        //                //    }

        //                //}

        //                //if (filterEntity.UnReachable != null)
        //                //{
        //                //    if (Convert.ToBoolean(filterEntity.UnReachable))
        //                //    {
        //                //        if (strWherCond.ToString() == "")
        //                //        {
        //                //            strWherCond.Append(" EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - sysdatetime))>600 ");
        //                //        }
        //                //        else
        //                //        {
        //                //            strWherCond.Append(" or EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - sysdatetime))>600 ");
        //                //        }
        //                //    }

        //                //}

        //                if (filterEntity.HA != null)
        //                {
        //                    if (Convert.ToBoolean(filterEntity.HA))
        //                    {
        //                        if (strWherCond.ToString() == "")
        //                        {
        //                            strWherCond.Append(" coalesce(hardacceleration,0)=1 ");
        //                        }
        //                        else
        //                        {
        //                            strWherCond.Append(" or coalesce(hardacceleration,0)=1 ");
        //                        }
        //                    }

        //                }

        //                if (filterEntity.HB != null)
        //                {
        //                    if (Convert.ToBoolean(filterEntity.HB))
        //                    {
        //                        if (strWherCond.ToString() == "")
        //                        {
        //                            strWherCond.Append(" coalesce(hardbraking,0)=1 ");
        //                        }
        //                        else
        //                        {
        //                            strWherCond.Append(" or coalesce(hardbraking,0)=1 ");
        //                        }
        //                    }

        //                }

        //                //if (filterEntity.TA != null)
        //                //{
        //                //    if (Convert.ToBoolean(filterEntity.TA))
        //                //    {
        //                //        if (strWherCond.ToString() == "")
        //                //        {
        //                //            strWherCond.Append(" tamperalert='1' ");
        //                //        }
        //                //        else
        //                //        {
        //                //            strWherCond.Append(" or tamperalert='1' ");
        //                //        }

        //                //    }

        //                //}
        //                //where coalesce(ignitionstatus,0)=0
        //                strSqlQry1.Append(@"SELECT user_name,first_name,middle_name,last_name,date_of_birth,gender,	address1,address2,address3,	user_master.mobileno,email_id1,	profile_picture_uri,
        //	                            vehicle_no,	license_no,	license_expiry_date,registration_no,vehicle_manufacturer,vehicle_color,	vehicle_type,user_login_type,insurance_mapping_key, 
        //                                iot_live.deviceno, recorddatetime,latitude, longitude, speed,ignitionstatus, hardacceleration, hardbraking,tamperalert,
        //                                EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - sysdatetime)) as diffinSec FROM iot_live LEFT OUTER JOIN
        //                                 user_master ON iot_live.deviceno = user_master.deviceno where iot_live.deviceno in (SELECT deviceno FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + "))");

        //                if (strWherCond.ToString() != "")
        //                    strSqlQry1.Append(" And (" + strWherCond.ToString() + ")");


        //                objDAL.CreateConnection();

        //                dtData = objDAL.FetchRecords(strSqlQry1.ToString());
        //                dtData.DefaultView.Sort = "recorddatetime desc";
        //                dtData = dtData.DefaultView.ToTable();

        //                dsData.Tables.Add(dtData);
        //                dsData.Tables[dsData.Tables.Count - 1].TableName = "All";

        //                objDAL.CloseConnection();
        //                dtData = null;

        //                return dsData;
        //            }
        //            catch (Exception ex)
        //            {
        //                objDAL.CloseConnection();
        //                dsData = null;
        //                strWherCond = null;

        //                throw ex;
        //            }

        //        }


    }
}


