﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Web.Http;

namespace IOTDataModel
{
    public class CommonDM : IDisposable
    {
        private BaseDAL objDAL;
        private String strSqlQry;
        public CommonDM()
        {
            objDAL = new BaseDAL();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                strSqlQry = null;

            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~CommonDM()
        {
            Dispose(false);
        }

        #endregion


        public DataTable GetUserList(FilterEntity filterEntity)
        {
            try
            {
                if (filterEntity.s_role_Id != 1) // forclient admin and sys admin
                {
                    //strSqlQry = @" SELECT distinct user_id,user_name ,first_name,last_name,mobileno,email_id1,deviceno,registration_no, false as chk  FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ") and user_id not in (select user_id from geofence_user_mapping where is_active=true and gc_id=" + filterEntity.gc_id + ") union SELECT distinct user_id,user_name ,first_name,last_name,mobileno,email_id1,deviceno,registration_no, true as chk  FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ") and user_id in (select user_id from geofence_user_mapping where is_active=true and gc_id=" + filterEntity.gc_id + ") order by chk desc ";
                    strSqlQry = @" SELECT user_id,user_name ,first_name,last_name,mobileno,email_id1,deviceno,vehicle_no FROM user_master where o_id in (SELECT o_id FROM user_master where user_id =" + filterEntity.s_user_id + ") and (coalesce(user_master.deleted,false) = false)";
                   
                }
                else // for client user
                {
                    strSqlQry = @" SELECT user_id,user_name ,first_name,last_name,mobileno,email_id1,deviceno,vehicle_no FROM user_master where user_id =" + filterEntity.s_user_id + " and (coalesce(user_master.deleted,false) = false)";
                }

                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        public DataTable GetUserToken(NotificationEntity notificationEntity)
        {
            try
            {

                strSqlQry = @" SELECT  user_token FROM user_device_info where user_id =" + notificationEntity.s_user_id + " and  isactive=true";
              

                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        #region Engine Capacity 

        public DataTable GetEngineCapacityList()
        {
            try
            {
              
               strSqlQry = @" SELECT * FROM engine_capacity_master;";
              
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        public bool CreateEngineCapacity(Engine_CapacityEntity enginecapacityentity)
        {
            try
            {

                strSqlQry = @" INSERT INTO engine_capacity_master ( eng_capacity_range, created_at,  isactive) VALUES (@eng_capacity_range,@created_at,@isactive);";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@eng_capacity_range", CommonUtils.ConvertToInt(enginecapacityentity.eng_capacity_range)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetEngineCapacityById(Engine_CapacityEntity enginecapacityentity)
        {
            try
            {

                strSqlQry = @" SELECT * FROM engine_capacity_master where  eng_capacity_id =" + enginecapacityentity.eng_capacity_id + "";

                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateEngineCapacity(Engine_CapacityEntity enginecapacityentity)
        {
            try
            {

                strSqlQry = @" UPDATE engine_capacity_master SET eng_capacity_range = @eng_capacity_range, updated_at =@updated_at  WHERE eng_capacity_id =@eng_capacity_id";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@eng_capacity_id", CommonUtils.ConvertToInt(enginecapacityentity.eng_capacity_id)));
                Parameters.Add(new NpgsqlParameter("@eng_capacity_range", CommonUtils.ConvertToString(enginecapacityentity.eng_capacity_range)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
               

                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


        #endregion 

        #region Engine type 

        public DataTable GetEngineTypeList()
        {
            try
            {

                strSqlQry = @" SELECT * FROM engine_type_master;";
              
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CreateEngineType(Engine_TypeEntity enginetypeentity)
        {
            try
            {

                strSqlQry = @" INSERT INTO engine_type_master ( eng_type, created_at,  isactive) VALUES (@eng_type,@created_at,@isactive);";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@eng_type", CommonUtils.ConvertToInt(enginetypeentity.eng_type)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetEngineTypeById(Engine_TypeEntity enginetypeentity)
        {
            try
            {

                strSqlQry = @" SELECT * FROM engine_capacity_master where  eng_capacity_id =" + enginetypeentity.eng_type_id + "";
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateEngineType(Engine_TypeEntity enginetypeentity)
        {
            try
            {

                strSqlQry = @"UPDATE engine_type_master SET eng_type = @eng_type, updated_at =@updated_at  WHERE  eng_type_id =@eng_type_id ;";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@eng_type_id", CommonUtils.ConvertToInt(enginetypeentity.eng_type_id)));
                Parameters.Add(new NpgsqlParameter("@eng_type", CommonUtils.ConvertToString(enginetypeentity.eng_type)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
               
                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }
        #endregion 


        #region Vehicle type 

        public DataTable GetVehicleTypeList()
        {
            try
            {

                strSqlQry = @" SELECT * FROM vehicle_type_master";
              
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CreateVehicleType(Vehicle_TypeEntity vehicletypeentity)
        {
            try
            {

                strSqlQry = @" INSERT INTO vehicle_type_master ( vehicle_type, created_at,  isactive) VALUES (@vehicle_type,@created_at,@isactive);";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@vehicle_type", CommonUtils.ConvertToInt(vehicletypeentity.vehicle_type)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetVehicleTypeById(Vehicle_TypeEntity vehicletypeentity)
        {
            try
            {

                strSqlQry = @" SELECT * FROM engine_capacity_master where  eng_capacity_id =" + vehicletypeentity.vehicle_type_id + "";
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateVehicleType(Vehicle_TypeEntity vehicletypeentity)
        {
            try
            {

                strSqlQry = @"UPDATE vehicle_type_master SET vehicle_type =@vehicle_type ,updated_at =@updated_at WHERE vehicle_type_id=@vehicle_type_id ";


                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@vehicle_type_id", CommonUtils.ConvertToInt(vehicletypeentity.vehicle_type_id)));
                Parameters.Add(new NpgsqlParameter("@vehicle_type", CommonUtils.ConvertToString(vehicletypeentity.vehicle_type)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                objDAL.CreateConnection();
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.CloseConnection();

                return true;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }
        #endregion 


        #region ---- Country Wise Road Assistance ----

        public DataTable GetRoadSideAssistanceByCountry(Roadside_AssistanceEntity roadsideassistanceEntity)
        {
            try
            {

                strSqlQry = @" SELECT country_name,police_no,ambulance_no FROM roadside_assistance_master where  country_name ='" + roadsideassistanceEntity.country_name + "'";
                objDAL.CreateConnection();
                DataTable dtUser = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtUser;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        #endregion 




        public List<NpgsqlParameter> Parameters { get; set; }
    }
}
