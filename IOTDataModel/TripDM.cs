﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;

namespace IOTDataModel
{
    public class TripDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private Boolean blnResult;
        private Int32 _t_id;
        private Int32 _user_id;
        private String strMaxQry;
        private DataSet dsData;
        private DataTable dtTrip;
        string Url = "";


        public TripDM()
        {
            objDAL = new BaseDAL();
            dsData = new DataSet();

        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
                strSqlQry = null;
                strMaxQry = null;
                if (dsData != null) dsData.Dispose();
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~TripDM()
        {
            Dispose(false);
        }

        #endregion

        public DataTable StartTrip(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"INSERT INTO trip_planning_master( trip_name, user_id,trip_from,trip_to, trip_start_time,category, created_at,isactive,is_self_driving)
	                               VALUES (@trip_name,@user_id,@trip_from,@trip_to,@trip_start_time,@category, @created_at,@isactive,@is_self_driving)RETURNING trip_id";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_name", CommonUtils.ConvertToString(tripEntity.trip_name)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(tripEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@trip_from", CommonUtils.ConvertToString(tripEntity.trip_from)));
                Parameters.Add(new NpgsqlParameter("@trip_to", CommonUtils.ConvertToString(tripEntity.trip_to)));
                Parameters.Add(new NpgsqlParameter("@trip_start_time", CommonUtils.ConvertToDateTime(tripEntity.trip_start_time)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@category", CommonUtils.ConvertToString(tripEntity.category)));
                Parameters.Add(new NpgsqlParameter("@is_self_driving", CommonUtils.ConvertToBoolean(tripEntity.is_self_driving)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                //objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                var objtrip_id = objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);
                objDAL.CloseConnection();

                if (objtrip_id != null)
                {
                    _t_id = CommonUtils.ConvertToInt(objtrip_id.ToString());
                    strSqlQry = @"SELECT trip_id, trip_name, user_id, trip_from, trip_to, to_char( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_start_time,to_char( trip_end_time,'yyyy-MM-dd HH24:MI:SS') as  trip_end_time  , category, to_char( created_at, 'yyyy-MM-dd HH24:MI:SS') as  created_at , updated_at, deleted, isactive,coalesce(tripprocess,'false') as tripprocess FROM trip_planning_master  where trip_planning_master.trip_id=" + _t_id;

                    objDAL.CreateConnection();
                    dtTrip = objDAL.FetchRecords(strSqlQry);
                    objDAL.CloseConnection();
                }

                return dtTrip;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool EndTrip(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();

                if (CommonUtils.ConvertToInt(tripEntity.trip_id) == 0)
                {
                    strSqlQry = @"UPDATE trip_planning_master SET isactive=false, trip_end_time=@trip_end_time, updated_at=@updated_at, m_distance=@m_distance WHERE  trip_start_time =@trip_start_time and user_id=@user_id ";
                }
                else
                {
                    strSqlQry = @"UPDATE trip_planning_master SET isactive=false, trip_end_time=@trip_end_time, updated_at=@updated_at, m_distance=@m_distance WHERE  trip_id=@trip_id ";

                }

              
                //  strSqlQry = @"UPDATE trip_planning_master SET isactive=false, trip_end_time=@trip_end_time, updated_at=@updated_at WHERE trip_id = (SELECT trip_id FROM trip_planning_master and user_id=@u_id  where isactive=true order  by trip_id  desc limit 1);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt(tripEntity.trip_id)));
                Parameters.Add(new NpgsqlParameter("@trip_end_time", CommonUtils.ConvertToDateTime(tripEntity.trip_end_time)));
                if (CommonUtils.ConvertToDateTime(tripEntity.trip_start_time) !=null)
                {
                    Parameters.Add(new NpgsqlParameter("@trip_start_time", CommonUtils.ConvertToDateTime(tripEntity.trip_start_time)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@trip_start_time", DBNull.Value));
                }
              
                Parameters.Add(new NpgsqlParameter("@m_distance", CommonUtils.ConvertToDecimal(tripEntity.m_distance)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToDecimal(tripEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return true;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable UpdateTrip(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"UPDATE  trip_planning_master SET trip_name = @trip_name WHERE trip_id=@trip_id;";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_name", CommonUtils.ConvertToString(tripEntity.trip_name)));
                Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt(tripEntity.trip_id)));
              

                //objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                objDAL.ExecutParameterizedQry(strSqlQry, Parameters); ;
                objDAL.CloseConnection();

             
                    _t_id = CommonUtils.ConvertToInt(tripEntity.trip_id);
                    strSqlQry = @"SELECT trip_id, trip_name, user_id, trip_from, trip_to, to_char( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_start_time,to_char( trip_end_time,'yyyy-MM-dd HH24:MI:SS') as  trip_end_time  , category, to_char( created_at, 'yyyy-MM-dd HH24:MI:SS') as  created_at , updated_at, deleted, isactive,coalesce(tripprocess,'false') as tripprocess FROM trip_planning_master  where trip_planning_master.trip_id=" + _t_id;

                    objDAL.CreateConnection();
                    dtTrip = objDAL.FetchRecords(strSqlQry);
                    objDAL.CloseConnection();
                

                return dtTrip;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool DeleteTrip(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();

                //strSqlQry = @"update trip_planning_master set deleted=true,isactive=false,trip_end_time=@trip_end_time where trip_id = (SELECT trip_id FROM trip_planning_master and user_id=@u_id  where isactive=true order  by trip_id  desc limit 1)  ";
                strSqlQry = @"UPDATE trip_planning_master SET deleted=true,isactive=false, updated_at=@updated_at WHERE trip_id=@trip_id;";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt(tripEntity.trip_id)));
                // Parameters.Add(new NpgsqlParameter("@trip_end_time", CommonUtils.ConvertToDateTime(tripEntity.trip_end_time)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);


                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetTripsList(TripEntity tripEntity)
        {
            try
            {
               
                objDAL.CreateConnection();

                strSqlQry = @"SELECT trip_planning_master.trip_id, trip_name,category, user_id, trip_from, trip_to,to_char( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_start_time, to_char( trip_end_time, 'yyyy-MM-dd HH24:MI:SS') as trip_end_time , to_char( created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at,to_char( updated_at, 'yyyy-MM-dd HH24:MI:SS') as updated_at, deleted, isactive,round(dr_overall_trip_rating.rating) as rating ,round(dr_overall_trip_rating.distance,1) as distance,round(dr_overall_trip_rating.distance_d) as distance_d,round(dr_overall_trip_rating.distance_n) as distance_n,uploadfile ,trip_planning_master.m_distance ,coalesce(tripprocess,'false') as tripprocess FROM trip_planning_master left outer join  dr_overall_trip_rating on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id where trip_planning_master.user_id=" + tripEntity.s_user_id + "and  (coalesce(deleted,false) = false) order by trip_planning_master.trip_start_time desc";
                
                if (tripEntity.cnt > 0)
                {
                    strSqlQry = strSqlQry + " limit " + tripEntity.cnt + "";
                }
             
                DataTable dtTrips = objDAL.FetchRecords(strSqlQry);

                foreach (DataRow Triprow in dtTrips.Rows)
                {
                    string pathname=Triprow["uploadfile"].ToString();
                    if (pathname != "")
                    {
                        string strUrlPath = CommonUtils.getUrlPath(Triprow["uploadfile"].ToString(), "tripvideodata", out Url);
                        Triprow["uploadfile"] = Url;
                    }
                    else
                    {
                        Triprow["uploadfile"] = "";

                    }
                   
                }
                 objDAL.CloseConnection();
                 return dtTrips;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public DataTable GetTripById(TripEntity tripEntity)
        {
            try
            {
                strSqlQry = @" SELECT trip_planning_master.trip_id, trip_name, user_id, trip_from, trip_to, to_char( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_start_time,to_char( trip_end_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_end_time  , category, to_char( trip_planning_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as  created_at ,dr_overall_trip_rating.rating,round(dr_overall_trip_rating.distance,1) as distance ,round(dr_overall_trip_rating.distance_d,1) as distance_d,round(dr_overall_trip_rating.distance_n,1) as distance_n,uploadfile,trip_planning_master.m_distance,coalesce(tripprocess,'false') as tripprocess FROM trip_planning_master left outer join  dr_overall_trip_rating on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id  where trip_planning_master.trip_id=" + CommonUtils.ConvertToInt64(tripEntity.trip_id);

                objDAL.CreateConnection();
                DataTable dtTrip = objDAL.FetchRecords(strSqlQry);
                //string strUrlPath = CommonUtils.getUrlPath(dtTrip.Rows[0]["uploadfile"].ToString(), "tripdata", out Url);
                //dtTrip.Rows[0]["uploadfile"] = Url;

                string pathname = dtTrip.Rows[0]["uploadfile"].ToString();
                if (pathname != "")
                {
                    string strUrlPath = CommonUtils.getUrlPath(dtTrip.Rows[0]["uploadfile"].ToString(), "tripvideodata", out Url);
                    dtTrip.Rows[0]["uploadfile"] = Url;
                }
                else
                {
                   dtTrip.Rows[0]["uploadfile"]  = "";

                }
                objDAL.CloseConnection();

                return dtTrip;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetTripPlayData(TripEntity tripEntity)
        {

            try
            {
                DataTable dtData;

                objDAL.CreateConnection();

                strSqlQry = @"SELECT trip_planning_master.trip_id, trip_name, user_id, trip_from, trip_to, to_char ( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as trip_start_time , to_char ( trip_end_time, 'yyyy-MM-dd HH24:MI:SS') as trip_end_time , trip_start_time as tstime,trip_end_time as tetime, category, to_char ( trip_planning_master.created_at, 'yyyy-MM-dd HH24:MI:SS') as created_at ,round(dr_overall_trip_rating.rating) as rating ,round(dr_overall_trip_rating.distance,1) as distance ,round(dr_overall_trip_rating.distance_d,1) as distance_d,round(dr_overall_trip_rating.distance_n,1) as distance_n,uploadfile,trip_planning_master.m_distance,coalesce(tripprocess,'false') as tripprocess ,trip_planning_master.is_self_driving FROM trip_planning_master left outer join dr_overall_trip_rating on trip_planning_master.trip_id=dr_overall_trip_rating.trip_id  where trip_planning_master.trip_id=" + CommonUtils.ConvertToInt64(tripEntity.trip_id);

                DataTable dtTrip = objDAL.FetchRecords(strSqlQry);
                if (dtTrip.Rows.Count > 0)
                {
                    string pathname = dtTrip.Rows[0]["uploadfile"].ToString();
                    if (pathname != "")
                    {
                        string strUrlPath = CommonUtils.getUrlPath(dtTrip.Rows[0]["uploadfile"].ToString(), "tripvideodata", out Url);
                        dtTrip.Rows[0]["uploadfile"] = Url;
                    }
                    else
                    {
                        dtTrip.Rows[0]["uploadfile"] = "";

                    }

                    string fromdate, todate;
                    if (dtTrip.Rows[0]["tetime"] != null && dtTrip.Rows[0]["tetime"].ToString() != "")
                    {
                        fromdate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", dtTrip.Rows[0]["tstime"]).ToString().Replace(".", ":");
                        todate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", dtTrip.Rows[0]["tetime"]).ToString().Replace(".", ":");
                    }
                    else
                    {
                        fromdate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", dtTrip.Rows[0]["tstime"]).ToString().Replace(".", ":");
                        todate = string.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now.ToString()).ToString().Replace(".", ":");
                    }
                    string _strcnt;

                    dtTrip.Columns.Add("hb", typeof(int));
                    dtTrip.Columns.Add("ha", typeof(int));
                    dtTrip.Columns.Add("cor", typeof(int));
                    dtTrip.Columns.Add("over_speed", typeof(int));
                    dtTrip.Columns.Add("calls_taken", typeof(int));
                    dtTrip.Columns.Add("deviceno");
                    dtTrip.Columns.Add("rd");
                    dtTrip.Columns.Add("avgspeed", typeof(int));
                    dtTrip.Columns.Add("maxspeed", typeof(int));
                    //dtTrip.Columns.Add("distance", typeof(int));

                    string strhardbraking = @"SELECT  coalesce(count(*),0) as hardbraking FROM  iot_hub where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and coalesce(hardbraking,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strhardbraking);
                    dtTrip.Rows[0]["hb"] = CommonUtils.ConvertToInt(_strcnt);
                    strhardbraking = null;


                    //---for hardacceleration 
                    string strhardacceleration = @"SELECT  coalesce(count(*),0) as hardacceleration FROM  iot_hub where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and  coalesce(hardacceleration,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strhardacceleration);
                    dtTrip.Rows[0]["ha"] = CommonUtils.ConvertToInt(_strcnt);
                    strhardacceleration = null;

                    //---for cornerpacket 
                    string strcornerpacket = @"SELECT  coalesce(count(*),0) as cornerpacket FROM  iot_hub where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and  coalesce(cornerpacket,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(strcornerpacket);
                    dtTrip.Rows[0]["cor"] = CommonUtils.ConvertToInt(_strcnt);
                    strcornerpacket = null;

                    string overspeedstarted = @"SELECT  coalesce(count(*),0) as overspeedstarted FROM  iot_hub where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and  coalesce(overspeedstarted,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(overspeedstarted);
                    dtTrip.Rows[0]["over_speed"] = CommonUtils.ConvertToInt(_strcnt);
                    strcornerpacket = null;

                    string callstaken = @"SELECT  coalesce(count(*),0) as calls_taken FROM  iot_hub where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and  coalesce(mcall,0)=1 and  (recorddatetime between '" + fromdate + "' and '" + todate + "') ;";
                    _strcnt = objDAL.FetchSingleRecord(callstaken);
                    dtTrip.Rows[0]["calls_taken"] = CommonUtils.ConvertToInt(_strcnt);
                    strcornerpacket = null;

                    string strspeeddistanceDay = @"SELECT  deviceno, to_char (recorddatetime, 'dd-mm-yyyy') as rd,round(avg(speed)) as avgspeed,round(max(speed)) maxspeed FROM  iot_hub where speed >0 and deviceno = (select deviceno FROM user_master where user_id ="+tripEntity.s_user_id +") and (recorddatetime between '" + fromdate + "' and '" + todate + "') group by deviceno,to_char (recorddatetime, 'dd-mm-yyyy');";
                    dtData = objDAL.FetchRecords(strspeeddistanceDay);
                    strspeeddistanceDay = null;
                    if (dtData.Rows.Count > 0)
                    {
                        int avgspeed = Convert.ToInt16(dtData.Rows[0]["avgspeed"]);
                        int maxspeed = Convert.ToInt16(dtData.Rows[0]["maxspeed"]);
                        dtTrip.Rows[0]["deviceno"] = dtData.Rows[0]["deviceno"];
                        dtTrip.Rows[0]["rd"] = dtData.Rows[0]["rd"];
                        dtTrip.Rows[0]["avgspeed"] = avgspeed;
                        dtTrip.Rows[0]["maxspeed"] = maxspeed;
                    
                    }
                    else
                    {
                        dtTrip.Rows[0]["deviceno"] = "0";
                        dtTrip.Rows[0]["rd"] = null;
                        dtTrip.Rows[0]["avgspeed"] = CommonUtils.ConvertToInt("0");
                        dtTrip.Rows[0]["maxspeed"] = CommonUtils.ConvertToInt("0");
                        //dtTrip.Rows[0]["distance"] = CommonUtils.ConvertToInt("0");
                    }


                    dsData.Tables.Add(dtTrip);
                    dsData.Tables[dsData.Tables.Count - 1].TableName = "tripinfo";



                    //for tripdata
                    string strtripdata = @"SELECT iot_hub.deviceno,  to_char (  iot_hub.recorddatetime, 'dd-mm-yyyy HH24:MI:SS') as recorddatetime, iot_hub.latitude, iot_hub.longitude, 
                                        iot_hub.speed,coalesce(iot_hub.overspeedstarted,0) as overspeedstarted,coalesce(iot_hub.hardbraking,0) as hardbraking,coalesce(iot_hub.hardacceleration,0) as hardacceleration,coalesce(iot_hub.cornerpacket,0) as cornerpacket,coalesce(iot_hub.mcall,0) as mcall,0 as daynight,
                                        iot_obd_hub.pidsupport1to20,iot_obd_hub.statusdtccleared,iot_obd_hub.freezedtc,iot_obd_hub.fuelsystemstatus,iot_obd_hub.calculatedengineload,iot_obd_hub.coolanttemperature,iot_obd_hub.shorttermfueltrim,iot_obd_hub.longtermfueltrim,iot_obd_hub.shorttermfueltrimbank2,iot_obd_hub.longtermfueltrimbank2,iot_obd_hub.fuelpressure,iot_obd_hub.intakemanifoldabsolutepressure,iot_obd_hub.rpm,iot_obd_hub.speedcan, iot_obd_hub.timingadvance,iot_obd_hub.intakeairtemp,iot_obd_hub.mafairflowrate,iot_obd_hub.throttlepos,iot_obd_hub.commandedsecondaryairstatus,iot_obd_hub.oxygensensorspresent,iot_obd_hub.oxygensensorvoltage1,iot_obd_hub.oxygensensorvoltage2,iot_obd_hub.oxygensensorvoltage3,iot_obd_hub.oxygensensorvoltage4,iot_obd_hub.oxygensensorvoltage5,iot_obd_hub.oxygensensorvoltage6,iot_obd_hub.oxygensensorvoltage7,iot_obd_hub.oxygensensorvoltage8,iot_obd_hub.obdstandards,iot_obd_hub.oxygensensorspresent4banks,iot_obd_hub.auxiliaryinputstatus,iot_obd_hub.runtimesinceenginestart,iot_obd_hub.pidsupported21to40,iot_obd_hub.distancetraveledmil,iot_obd_hub.relativefuelrailpressure,iot_obd_hub.directfuelrailpressure,iot_obd_hub.oxygensensorvoltagefuel1,iot_obd_hub.oxygensensorvoltagefuel2,iot_obd_hub.oxygensensorvoltagefuel3,iot_obd_hub.oxygensensorvoltagefuel4,iot_obd_hub.oxygensensorvoltagefuel5,iot_obd_hub.oxygensensorvoltagefuel6,iot_obd_hub.oxygensensorvoltagefuel7,iot_obd_hub.oxygensensorvoltagefuel8,iot_obd_hub.commandedegr,iot_obd_hub.egrerror,iot_obd_hub.commandedevaporativepurge,
                                        iot_obd_hub.fuellevel,iot_obd_hub.warmupssc,iot_obd_hub.distsincecodecleared,iot_obd_hub.systemvaporpressure,iot_obd_hub.barometricpressure,iot_obd_hub.oxygensensorcurrentfuel1, iot_obd_hub.oxygensensorcurrentfuel2, iot_obd_hub.oxygensensorcurrentfuel3, iot_obd_hub.oxygensensorcurrentfuel4, iot_obd_hub.oxygensensorcurrentfuel5, iot_obd_hub.oxygensensorcurrentfuel6, iot_obd_hub.oxygensensorcurrentfuel7, iot_obd_hub.oxygensensorcurrentfuel8, iot_obd_hub.catalysttempbank1sensor1, iot_obd_hub.catalysttempbank2sensor1, iot_obd_hub.catalysttempbank1sensor2, iot_obd_hub.catalysttempbank2sensor2, iot_obd_hub.pidsupported41to60, iot_obd_hub.monitorstatusthisdrivecycle, iot_obd_hub.controlmodulevoltage, iot_obd_hub.absoluteloadvalue,iot_obd_hub.fuelaircommandedequratio,iot_obd_hub.relativethrottleposition, iot_obd_hub.ambientairtemp, iot_obd_hub.absthrottlepositionb, iot_obd_hub.absolutethrottlepositionc,iot_obd_hub.acceleratorpedalpositiond, iot_obd_hub.acceleratorpedalpositione, iot_obd_hub.acceleratorpedalpositionf, iot_obd_hub.commandedthrottleactuator,iot_obd_hub.timerunwithmil,iot_obd_hub.timesincetroublecodescleared,iot_obd_hub.maximumvalueforfuelair,iot_obd_hub.maximumvalueforairflow,iot_obd_hub.fueltype,iot_obd_hub.ethanolfuel,iot_obd_hub.absoluteevapsystemvaporpressure,iot_obd_hub.evapsystemvaporpressure,iot_obd_hub.shorttermsecoxygenbank1bank3, iot_obd_hub.longtermsecoxygenbank1bank3,iot_obd_hub.shorttermsecoxygenbank2bank4,iot_obd_hub.longtermsecoxygenbank2bank4,iot_obd_hub.fuelrailabsolutepressure,iot_obd_hub.relativeacceleratorpedalposition,iot_obd_hub.hybridbatterypackremaininglife,iot_obd_hub.engineoiltemperature,iot_obd_hub.fuelinjectiontiming,iot_obd_hub.fuelrate,iot_obd_hub.emissionrequirements, iot_obd_hub.pidsupported61to80,iot_obd_hub.driversdemandengine,iot_obd_hub.actualenginepercenttorque,iot_obd_hub.enginereferencetorque,iot_obd_hub.enginepercenttorquedata, iot_obd_hub.auxiliaryinputoutputsupported,iot_obd_hub.massairflowsensor,iot_obd_hub.enginecoolanttemperature,iot_obd_hub.intakeairtemperaturesensor,iot_obd_hub.commandedegrandegrerror,iot_obd_hub.commandeddieselintakeairflow, iot_obd_hub.exhaustgasrecirculationtemperature,iot_obd_hub.commandedthrottleactuatorcontrol,iot_obd_hub.fuelpressurecontrolsystem,iot_obd_hub.injectionpressurecontrolsystem,iot_obd_hub.turbochargercompressorinletpressure,iot_obd_hub.boostpressurecontrol,iot_obd_hub.variablegeometryturbocontrol,iot_obd_hub.wastegatecontrol,iot_obd_hub.exhaustpressure,iot_obd_hub.turbochargerrpm,iot_obd_hub.turbochargertemperature1,iot_obd_hub.turbochargertemperature2,iot_obd_hub.chargeaircoolertemperature,iot_obd_hub.exhaustgastemperaturebank1,iot_obd_hub.exhaustgastemperaturebank2,iot_obd_hub.dieselparticulatefilter1,iot_obd_hub.dieselparticulatefilter2,iot_obd_hub.dieselparticulatefiltertemperature,iot_obd_hub.noxntecontrolareastatus,iot_obd_hub.pmntecontrolareastatus,iot_obd_hub.engineruntime,iot_obd_hub.pidsupported81toa0,iot_obd_hub.engineruntimeforaecd1,iot_obd_hub.engineruntimeforaecd2,iot_obd_hub.noxsensor, iot_obd_hub.manifoldsurfacetemperature, iot_obd_hub.noxreagentsystem,iot_obd_hub.particulatemattersensor,iot_obd_hub.intakemanifoldabsolutepressure2
                                        FROM  iot_hub LEFT OUTER JOIN iot_obd_hub ON iot_hub.deviceno = iot_obd_hub.deviceno AND iot_hub.recorddatetime = iot_obd_hub.recorddatetime where iot_hub.DeviceNo in (SELECT deviceno FROM user_master where user_id =" + tripEntity.s_user_id + ") and  (iot_hub.recorddatetime between '" + fromdate + "' and '" + todate + "')  order by iot_hub.recorddatetime asc";

                    dtData = objDAL.FetchRecords(strtripdata);
                    dsData.Tables.Add(dtData);
                    dsData.Tables[dsData.Tables.Count - 1].TableName = "tripdata";

                    //for speeddistanceDay


                    //dt for aggrgate data



                    //create new row
                    //  DataRow dr = dtcount.NewRow();

                    //---for hardbraking 


                    //add new row and add in ds


                    objDAL.CloseConnection();



                }

                dtData = null;
                dtTrip = null;

                return dsData;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool CheckTripStart(TripEntity tripEntity)
        {
            try
            {
                string SQLQry = @"SELECT count(*) as cnt FROM trip_planning_master where category='" + tripEntity.category + "' and user_id=" + tripEntity.s_user_id + " and isactive=true and  trip_end_time is null";

                objDAL.CreateConnection();
                string tripdata = objDAL.FetchSingleRecord(SQLQry);
                objDAL.CloseConnection();

                if (CommonUtils.ConvertToInt64(tripdata.ToString()) > 0)
                {
                    return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckTripEnd(TripEntity tripEntity)
        {
            try
            {
                string SQLQry = @"SELECT count(*) as cnt FROM trip_planning_master where trip_id=" + tripEntity.trip_id + " and isactive=false";

                objDAL.CreateConnection();
                string tripdata = objDAL.FetchSingleRecord(SQLQry);
                objDAL.CloseConnection();

                if (CommonUtils.ConvertToInt64(tripdata.ToString()) > 0)
                {
                    return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetActiveTrips(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();

                strSqlQry = @"SELECT trip_id,trip_name,user_id,trip_from,trip_to,to_char( trip_start_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_start_time,to_char( trip_end_time, 'yyyy-MM-dd HH24:MI:SS') as  trip_end_time , created_at, updated_at,deleted,isactive,tripprocess,category,uploadfile,m_distance,is_self_driving,triplogfile,synclogdata FROM trip_planning_master where user_id=" + tripEntity.s_user_id + " and isactive=true and  trip_end_time is null";

                DataTable dtTrips = objDAL.FetchRecords(strSqlQry);

                objDAL.CloseConnection();

                return dtTrips;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
        }

        public bool UpdateTripUploadPath(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();
              

                strSqlQry = @"UPDATE trip_planning_master  SET  uploadfile=@uploadfile  WHERE trip_id=@trip_id";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_id", tripEntity.trip_id));
                Parameters.Add(new NpgsqlParameter("@uploadfile", CommonUtils.ConvertToString(tripEntity.uploadfile)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateTripLogPath(TripEntity tripEntity)
        {
            try
            {
                objDAL.CreateConnection();


                strSqlQry = @"UPDATE trip_planning_master  SET  triplogfile=@triplogfile  WHERE trip_id=@trip_id";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@trip_id", tripEntity.trip_id));
                Parameters.Add(new NpgsqlParameter("@triplogfile", CommonUtils.ConvertToString(tripEntity.triplogfile)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        

    }

}
