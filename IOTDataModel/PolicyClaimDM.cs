﻿using IOTBusinessEntities;
using IOTDataModel;
using IOTUtilities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
   public class PolicyClaimDM : IDisposable
    {

        private BaseDAL objDAL;
        private DataSet dsData;
        private string strSqlQry;
        private StringBuilder strSqlQry1;
        private List<NpgsqlParameter> Parameters;
        private object  objC_id ;
        private int _C_id;
        bool blnResult;
       //private string Parameters;
        

        public PolicyClaimDM()
        {
            objDAL = new BaseDAL();
            dsData = new DataSet();
            strSqlQry1 = new StringBuilder();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                if (dsData != null) dsData.Dispose();
                strSqlQry1 = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~PolicyClaimDM()
        {
            Dispose(false);
        }

        #endregion

        public int createPolicyClaim ( PolicyClaimEntity policyClaimEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                if (policyClaimEntity.spf != null && policyClaimEntity.spt != null && policyClaimEntity.sdaccount != null)
                {
                    policyClaimEntity.period_from = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spf));
                    policyClaimEntity.period_to = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spt));
                    policyClaimEntity.date_accident_or_loss = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.sdaccount));
                    
                }

                //string fromdate = CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spf);
                //string todate = CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spt);

                strSqlQry = @"INSERT INTO policy_claim_master (user_id, policy_no, period_from, period_to, date_accident_or_loss, name_of_driver, driver_birth_date, driving_licence_no, accident_or_loss_details, status, remark, created_at,address) VALUES (@user_id ,@policy_no, @period_from,@period_to ,@date_accident_or_loss ,@name_of_driver,@driver_birth_date ,@driving_licence_no,@accident_or_loss_details,@status, @remark, @created_at,@address)RETURNING  claim_id ";
                Parameters = new List<NpgsqlParameter>();
                //Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt(objO_id)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(policyClaimEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@policy_no", CommonUtils.ConvertToString(policyClaimEntity.policy_no)));
                if (policyClaimEntity.period_from != null)
                {
                    Parameters.Add(new NpgsqlParameter("@period_from", CommonUtils.ConvertToDateTime(policyClaimEntity.period_from)));
                }
                else
                {
                     Parameters.Add(new NpgsqlParameter("@period_from", DBNull.Value ));
                }

                if (policyClaimEntity.period_to != null)
                {
                    Parameters.Add(new NpgsqlParameter("@period_to", CommonUtils.ConvertToDateTime(policyClaimEntity.period_to)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@period_to", DBNull.Value));
                }
                if (policyClaimEntity.date_accident_or_loss != null)
                {
                    Parameters.Add(new NpgsqlParameter("@date_accident_or_loss", CommonUtils.ConvertToDateTime(policyClaimEntity.date_accident_or_loss)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@date_accident_or_loss", DBNull.Value));
                }
                if (policyClaimEntity.driver_birth_date != null)
                {
                    Parameters.Add(new NpgsqlParameter("@driver_birth_date", CommonUtils.ConvertToDateTime(policyClaimEntity.driver_birth_date)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@driver_birth_date", DBNull.Value));
                }
              
                Parameters.Add(new NpgsqlParameter("@name_of_driver", CommonUtils.ConvertToString(policyClaimEntity.name_of_driver)));
                Parameters.Add(new NpgsqlParameter("@driving_licence_no", CommonUtils.ConvertToString(policyClaimEntity.driving_licence_no)));
                Parameters.Add(new NpgsqlParameter("@accident_or_loss_details", CommonUtils.ConvertToString(policyClaimEntity.accident_or_loss_details)));
                Parameters.Add(new NpgsqlParameter("@status", CommonUtils.ConvertToString("p")));
                Parameters.Add(new NpgsqlParameter("@remark", CommonUtils.ConvertToString(policyClaimEntity.remark)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@address", CommonUtils.ConvertToString(policyClaimEntity.address)));
                //Parameters.Add(new NpgsqlParameter("@deleted", false));
                objDAL.CreateConnection();
                objC_id =   objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

                if (objC_id != null)
                {
                    _C_id = CommonUtils.ConvertToInt(objC_id.ToString());
                    foreach (policy_claim_attach_docEntity PolicyDocEntities in policyClaimEntity.PolicyDocEntities)
                    {
                        strSqlQry = @"INSERT INTO policy_claim_attach_doc(claim_id, doc_name, doc_file_path, status, remark, created_at)VALUES (@claim_id,@doc_name,@doc_file_path,@status,@remark,@created_at);";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@claim_id", _C_id));
                        Parameters.Add(new NpgsqlParameter("@doc_name", CommonUtils.ConvertToString(PolicyDocEntities.doc_name)));
                        Parameters.Add(new NpgsqlParameter("@doc_file_path", CommonUtils.ConvertToString(PolicyDocEntities.doc_file_path)));
                        Parameters.Add(new NpgsqlParameter("@status", CommonUtils.ConvertToString("P")));
                        Parameters.Add(new NpgsqlParameter("@remark",CommonUtils.ConvertToString(PolicyDocEntities.remark)));
                        Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                        objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }

                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return _C_id;
            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }
           
        }

        public List<PolicyClaimEntity> GetPolicyClaimList(PolicyClaimEntity policyClaimEntity)
        {
            List<PolicyClaimEntity> PolicyClaimEntitys = new List<PolicyClaimEntity>();

            policy_claim_attach_docEntity PolicyDocEntity;
            try
            {

                if (policyClaimEntity.s_role_Id == 2)
                {
                    //for admin
                    strSqlQry = @"SELECT * FROM  policy_claim_master where user_id in(SELECT user_id FROM user_master where o_id = (SELECT o_id FROM user_master where user_id =" + policyClaimEntity.s_user_id + "))   and  status !='d'; SELECT * FROM policy_claim_attach_doc where  status !='d';";
                }
                else
                {
                    strSqlQry = @"SELECT * FROM  policy_claim_master where user_id=" + policyClaimEntity.s_user_id + " and  status !='d'; SELECT * FROM policy_claim_attach_doc where  status !='d'";
                }
                objDAL.CreateConnection();
                DataSet DSPolicyClaim = objDAL.FetchDataSetRecords(strSqlQry);
                objDAL.CloseConnection();

                if (DSPolicyClaim.Tables[0] != null && DSPolicyClaim.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow datarow in DSPolicyClaim.Tables[0].Rows)
                    {
                        policyClaimEntity = new PolicyClaimEntity();

                        policyClaimEntity.claim_id = CommonUtils.ConvertToInt(datarow["claim_id"]);
                        policyClaimEntity.user_id = CommonUtils.ConvertToInt(datarow["user_id"]);
                        policyClaimEntity.policy_no = CommonUtils.ConvertToString(datarow["policy_no"]);
                        policyClaimEntity.period_from = CommonUtils.ConvertToDateTime(datarow["period_from"]);
                        policyClaimEntity.period_to = CommonUtils.ConvertToDateTime(datarow["period_to"]);
                        policyClaimEntity.date_accident_or_loss = CommonUtils.ConvertToDateTime(datarow["date_accident_or_loss"]);
                        policyClaimEntity.name_of_driver = CommonUtils.ConvertToString(datarow["name_of_driver"]);
                        policyClaimEntity.driver_birth_date = CommonUtils.ConvertToDateTime(datarow["driver_birth_date"]);
                        policyClaimEntity.driving_licence_no = CommonUtils.ConvertToString(datarow["driving_licence_no"]);
                        policyClaimEntity.accident_or_loss_details = CommonUtils.ConvertToString(datarow["accident_or_loss_details"]);
                        policyClaimEntity.status = CommonUtils.ConvertToString(datarow["status"]);
                        policyClaimEntity.remark = CommonUtils.ConvertToString(datarow["remark"]);
                        policyClaimEntity.created_at = CommonUtils.ConvertToDateTime(datarow["created_at"]);
                        policyClaimEntity.updated_at = CommonUtils.ConvertToDateTime(datarow["updated_at"]);
                        policyClaimEntity.address = CommonUtils.ConvertToString(datarow["address"]);

                       
                        if (DSPolicyClaim.Tables[1] != null && DSPolicyClaim.Tables[1].Rows.Count > 0)
                        {
                            List<policy_claim_attach_docEntity> PolicyDocEntities = new List<policy_claim_attach_docEntity>();
                            foreach (DataRow datarowCont in DSPolicyClaim.Tables[1].Select("claim_id=" + policyClaimEntity.claim_id))
                            {
                                PolicyDocEntity = new policy_claim_attach_docEntity();
                                PolicyDocEntity.claim_id = CommonUtils.ConvertToInt(datarowCont["claim_id"]);
                                PolicyDocEntity.doc_name = CommonUtils.ConvertToString(datarowCont["doc_name"]);
                                PolicyDocEntity.doc_file_path = CommonUtils.ConvertToString(datarowCont["doc_file_path"]);
                                PolicyDocEntity.status = CommonUtils.ConvertToString(datarowCont["status"]);
                                PolicyDocEntity.remark = CommonUtils.ConvertToString(datarowCont["remark"]);
                                PolicyDocEntity.created_at = CommonUtils.ConvertToDateTime(datarowCont["created_at"]);
                                PolicyDocEntity.updated_at = CommonUtils.ConvertToDateTime(datarowCont["updated_at"]);
                                PolicyDocEntities.Add(PolicyDocEntity);
                            }
                            if (PolicyDocEntities != null)
                            {
                                policyClaimEntity.PolicyDocEntities = PolicyDocEntities;
                            }
                            PolicyDocEntities = null;
                        }

                        PolicyClaimEntitys.Add(policyClaimEntity);

                    }
                }

                return PolicyClaimEntitys;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                PolicyClaimEntitys = null;
                policyClaimEntity = null;
                PolicyDocEntity = null;

            }
        }

        public List<PolicyClaimEntity> GetPolicyClaimById(PolicyClaimEntity policyClaimEntity)
        {
            List<PolicyClaimEntity> PolicyClaimEntitys = new List<PolicyClaimEntity>();

            string Url = "";
            policy_claim_attach_docEntity PolicyDocEntity;
            try
            {
                strSqlQry = @"SELECT claim_id, user_id, policy_no,  to_char( period_from, 'yyyy-MM-dd HH24:MI:SS') as period_from , period_to,date_accident_or_loss,name_of_driver,to_char( driver_birth_date, 'yyyy-MM-dd') as driver_birth_date ,driving_licence_no,accident_or_loss_details,status,remark,created_at,updated_at,address FROM  policy_claim_master where claim_id=" + policyClaimEntity.claim_id + " and  status !='d'; SELECT * FROM policy_claim_attach_doc where  status !='d' ;";

                objDAL.CreateConnection();
                DataSet DSPolicyClaim = objDAL.FetchDataSetRecords(strSqlQry);
                objDAL.CloseConnection();

                if (DSPolicyClaim.Tables[0] != null && DSPolicyClaim.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow datarow in DSPolicyClaim.Tables[0].Rows)
                    {
                        policyClaimEntity = new PolicyClaimEntity();

                        policyClaimEntity.claim_id = CommonUtils.ConvertToInt(datarow["claim_id"]);
                        policyClaimEntity.user_id = CommonUtils.ConvertToInt(datarow["user_id"]);
                        policyClaimEntity.policy_no = CommonUtils.ConvertToString(datarow["policy_no"]);
                        policyClaimEntity.period_from = CommonUtils.ConvertToDateTime(datarow["period_from"]);
                        policyClaimEntity.period_to = CommonUtils.ConvertToDateTime(datarow["period_to"]);
                        policyClaimEntity.date_accident_or_loss = CommonUtils.ConvertToDateTime(datarow["date_accident_or_loss"]);
                        policyClaimEntity.name_of_driver = CommonUtils.ConvertToString(datarow["name_of_driver"]);
                        policyClaimEntity.driver_birth_date = CommonUtils.ConvertToDateTime(datarow["driver_birth_date"]);
                        policyClaimEntity.dbd = CommonUtils.ConvertToString(datarow["driver_birth_date"]);
                        policyClaimEntity.driving_licence_no = CommonUtils.ConvertToString(datarow["driving_licence_no"]);
                        policyClaimEntity.accident_or_loss_details = CommonUtils.ConvertToString(datarow["accident_or_loss_details"]);
                        policyClaimEntity.status = CommonUtils.ConvertToString(datarow["status"]);
                        policyClaimEntity.remark = CommonUtils.ConvertToString(datarow["remark"]);
                        policyClaimEntity.created_at = CommonUtils.ConvertToDateTime(datarow["created_at"]);
                        policyClaimEntity.updated_at = CommonUtils.ConvertToDateTime(datarow["updated_at"]);
                        policyClaimEntity.address = CommonUtils.ConvertToString(datarow["address"]);


                        if (DSPolicyClaim.Tables[1] != null && DSPolicyClaim.Tables[1].Rows.Count > 0)
                        {
                            List<policy_claim_attach_docEntity> PolicyDocEntities = new List<policy_claim_attach_docEntity>();
                            foreach (DataRow datarowCont in DSPolicyClaim.Tables[1].Select("claim_id=" + policyClaimEntity.claim_id ))
                            {
                                PolicyDocEntity = new policy_claim_attach_docEntity();
                                PolicyDocEntity.claim_id = CommonUtils.ConvertToInt(datarowCont["claim_id"]);
                                PolicyDocEntity.doc_name = CommonUtils.ConvertToString(datarowCont["doc_name"]);
                                PolicyDocEntity.doc_file_path = CommonUtils.ConvertToString(datarowCont["doc_file_path"]);
                                PolicyDocEntity.doc_file_save = CommonUtils.ConvertToString(datarowCont["doc_file_path"]);
                                PolicyDocEntity.doc_file_url = CommonUtils.getUrlDocPath(CommonUtils.ConvertToString(datarowCont["doc_file_path"]), "PolicyClaim", CommonUtils.ConvertToString(datarow["claim_id"]), out Url);
                                PolicyDocEntity.status = CommonUtils.ConvertToString(datarowCont["status"]);
                                PolicyDocEntity.remark = CommonUtils.ConvertToString(datarowCont["remark"]);
                                PolicyDocEntity.created_at = CommonUtils.ConvertToDateTime(datarowCont["created_at"]);
                                PolicyDocEntity.updated_at = CommonUtils.ConvertToDateTime(datarowCont["updated_at"]);
                                PolicyDocEntities.Add(PolicyDocEntity);
                            }
                            if (PolicyDocEntities != null)
                            {
                                policyClaimEntity.PolicyDocEntities = PolicyDocEntities;
                            }
                            PolicyDocEntities = null;
                        }

                        PolicyClaimEntitys.Add(policyClaimEntity);

                    }
                }

                return PolicyClaimEntitys;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                PolicyClaimEntitys = null;
                policyClaimEntity = null;
                PolicyDocEntity = null;

            }
        }

        public bool UpdatePolicyClaim (PolicyClaimEntity policyClaimEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                if (policyClaimEntity.spf != null && policyClaimEntity.spt != null && policyClaimEntity.sdaccount != null)
                {
                    policyClaimEntity.period_from = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spf));
                    policyClaimEntity.period_to = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.spt));
                    policyClaimEntity.date_accident_or_loss = CommonUtils.ConvertToDateTime(CommonUtils.GetstringFromatyyyy_MM_dd(policyClaimEntity.sdaccount));

                }

                _C_id = CommonUtils.ConvertToInt(policyClaimEntity.claim_id);

                strSqlQry = @"UPDATE policy_claim_master SET user_id=@user_id ,policy_no=@policy_no,period_from=@period_from ,period_to=@period_to ,date_accident_or_loss=@date_accident_or_loss ,name_of_driver=@name_of_driver,driver_birth_date=@driver_birth_date ,driving_licence_no=@driving_licence_no,accident_or_loss_details=@accident_or_loss_details,status=@status,remark =@remark,updated_at=@updated_at,address=@address WHERE  claim_id = @claim_id ;";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@claim_id", CommonUtils.ConvertToInt(_C_id)));
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt(policyClaimEntity.s_user_id)));
                Parameters.Add(new NpgsqlParameter("@policy_no", CommonUtils.ConvertToString(policyClaimEntity.policy_no)));
                if (policyClaimEntity.period_from != null)
                {
                    Parameters.Add(new NpgsqlParameter("@period_from", CommonUtils.ConvertToDateTime(policyClaimEntity.period_from)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@period_from", DBNull.Value));
                }

                if (policyClaimEntity.period_to != null)
                {
                    Parameters.Add(new NpgsqlParameter("@period_to", CommonUtils.ConvertToDateTime(policyClaimEntity.period_to)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@period_to", DBNull.Value));
                }
                if (policyClaimEntity.date_accident_or_loss != null)
                {
                    Parameters.Add(new NpgsqlParameter("@date_accident_or_loss", CommonUtils.ConvertToDateTime(policyClaimEntity.date_accident_or_loss)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@date_accident_or_loss", DBNull.Value));
                }
                if (policyClaimEntity.driver_birth_date != null)
                {
                    Parameters.Add(new NpgsqlParameter("@driver_birth_date", CommonUtils.ConvertToDateTime(policyClaimEntity.driver_birth_date)));
                }
                else
                {
                    Parameters.Add(new NpgsqlParameter("@driver_birth_date", DBNull.Value));
                }
                Parameters.Add(new NpgsqlParameter("@name_of_driver", CommonUtils.ConvertToString(policyClaimEntity.name_of_driver)));
                Parameters.Add(new NpgsqlParameter("@driving_licence_no", CommonUtils.ConvertToString(policyClaimEntity.driving_licence_no)));
                Parameters.Add(new NpgsqlParameter("@accident_or_loss_details", CommonUtils.ConvertToString(policyClaimEntity.accident_or_loss_details)));
                Parameters.Add(new NpgsqlParameter("@status", CommonUtils.ConvertToString("p")));
                Parameters.Add(new NpgsqlParameter("@remark", CommonUtils.ConvertToString(policyClaimEntity.remark)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@address", CommonUtils.ConvertToString(policyClaimEntity.address)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                if (blnResult)
                {

                    strSqlQry = @"UPDATE policy_claim_attach_doc SET status ='d'  WHERE   claim_id = @claim_id ;";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@claim_id", _C_id));

                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    foreach (policy_claim_attach_docEntity PolicyDocEntities in policyClaimEntity.PolicyDocEntities)
                    {
                        strSqlQry = @"INSERT INTO policy_claim_attach_doc(claim_id, doc_name, doc_file_path, status, remark, created_at)VALUES (@claim_id,@doc_name,@doc_file_path,@status,@remark,@created_at);";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@claim_id", _C_id));
                        Parameters.Add(new NpgsqlParameter("@doc_name", CommonUtils.ConvertToString(PolicyDocEntities.doc_name)));
                        if (PolicyDocEntities.doc_file_path ==null)
                        {
                            Parameters.Add(new NpgsqlParameter("@doc_file_path", CommonUtils.ConvertToString(PolicyDocEntities.doc_file_save)));
                        }
                        else
                        {
                            Parameters.Add(new NpgsqlParameter("@doc_file_path", CommonUtils.ConvertToString(PolicyDocEntities.doc_file_path)));
                        }
                      
                        Parameters.Add(new NpgsqlParameter("@status", CommonUtils.ConvertToString("p")));
                        Parameters.Add(new NpgsqlParameter("@remark", CommonUtils.ConvertToString(PolicyDocEntities.remark)));
                        Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                        objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool DeletePolicyClaim(PolicyClaimEntity policyClaimEntity)
        {
            try
            {
                _C_id = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(policyClaimEntity.claim_id));

                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @"UPDATE policy_claim_master SET status ='d'  WHERE  claim_id = @claim_id ;";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@claim_id", _C_id));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdatePolicyProfilePath(int claim_id, string doc_file_path, string doc_name)
            {
                try
                {
                    objDAL.CreateConnection();


                    strSqlQry = @"UPDATE policy_claim_attach_doc SET doc_file_path =@doc_file_path  WHERE claim_id =@claim_id and doc_name=@doc_name and status='p' ";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@claim_id", claim_id));
                    Parameters.Add(new NpgsqlParameter("@doc_file_path", doc_file_path));
                    Parameters.Add(new NpgsqlParameter("@doc_name", doc_name));
                   

                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    objDAL.CloseConnection();

                    return blnResult;

                }
                catch (Exception ex)
                {
                    objDAL.CloseConnection();
                    throw ex;
                }

            }

        public bool AddPolicyClaimRemark(PolicyClaimEntity policyClaimEntity)
        {
            try
            {
                objDAL.CreateConnection();


                strSqlQry = @"UPDATE policy_claim_master SET status =@status,remark =@remark,updated_at =@updated_at WHERE claim_id = @claim_id ;";
                
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@claim_id",policyClaimEntity.claim_id));
                Parameters.Add(new NpgsqlParameter("@remark", policyClaimEntity.remark));
                Parameters.Add(new NpgsqlParameter("@status", policyClaimEntity.status));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

      }


       
    
    }
}
