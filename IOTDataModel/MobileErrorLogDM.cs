﻿using IOTBusinessEntities;
using IOTDataModel;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOTBusinessServices
{
   public class MobileErrorLogDM: IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private Boolean blnResult;
        private Int32 _O_id;
        private String strMaxQry;

        public  MobileErrorLogDM()
        {
            objDAL = new BaseDAL();
            
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
            
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~ MobileErrorLogDM()
        {
            Dispose(false);
        }

        #endregion

        public bool SaveMob_ErrorLog(MobileErrorLogEntity mobileErrorLogEntity)
        {
            try
            {
                string saveFlag = System.Configuration.ConfigurationManager.AppSettings["m_flgDBLog"].ToString();
                if (saveFlag == "1")
                {
                    using (NpgsqlConnection connection = new NpgsqlConnection())
                    {
                        connection.ConnectionString = System.Configuration.ConfigurationManager.AppSettings["ConStrlog"];
                        connection.Open();
                        NpgsqlCommand cmd = new NpgsqlCommand();
                        cmd.Connection = connection;

                        cmd.CommandText = " INSERT INTO error_mdata_log(user_id,  erro_discription, error_code, error_file,created_at) VALUES (@user_id, @erro_discription, @error_code, @error_file, @created_at)";

                        cmd.Parameters.Add(new NpgsqlParameter("@user_id",mobileErrorLogEntity.user_id));
                        cmd.Parameters.Add(new NpgsqlParameter("@erro_discription",mobileErrorLogEntity.erro_discription));
                        cmd.Parameters.Add(new NpgsqlParameter("@error_code", mobileErrorLogEntity.error_code));
                        cmd.Parameters.Add(new NpgsqlParameter("@error_file", mobileErrorLogEntity.error_file));
                        cmd.Parameters.Add(new NpgsqlParameter("@created_at", Convert.ToDateTime(DateTime.Now)));
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        connection.Close();
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                  throw ex;
                //LogToFile(ex, "SaveLogToDatabase");
            }

        }


        //public bool UpdateErrorFilePath(MobileErrorLogEntity mobileErrorLogEntity)
        //{
        //    try
        //    {
        //        objDAL.CreateConnection();
        //        _cmpt_id = CommonUtils.ConvertToInt(mobileErrorLogEntity.);

        //        strSqlQry = @"UPDATE league_cmpt_master  SET  cmpt_profile_picture_path=@profile_picture_uri  WHERE (cmpt_id = @cmpt_id)";

        //        Parameters = new List<NpgsqlParameter>();
        //        Parameters.Add(new NpgsqlParameter("@cmpt_id", _cmpt_id));
        //        Parameters.Add(new NpgsqlParameter("@profile_picture_uri", CommonUtils.ConvertToString(compititionEntity.cmpt_profile_picture_path)));

        //        blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

        //        objDAL.CloseConnection();

        //        return blnResult;

        //    }
        //    catch (Exception ex)
        //    {
        //        objDAL.CloseConnection();
        //        throw ex;
        //    }

        //}


    }
}
