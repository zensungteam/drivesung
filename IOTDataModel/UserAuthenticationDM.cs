﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IOTBusinessEntities;
using IOTUtilities;
using System.Data;
using Npgsql;
using HashAndSalt;

namespace IOTDataModel
{
    public class UserAuthenticationDM : IDisposable
    {
        private ResponseEntity objResponseEntity;
        private BaseDAL objDAL;
        private SessionEntity sessionEntity;
        private bool IsSessionManage = true;
        private DataTable dtData;
        private List<NpgsqlParameter> Parameters;
        private string SqlQry;
        private PasswordManager pwdManager;
        private string Url = "";

        public UserAuthenticationDM(bool SessionManage = true)
        {
            objResponseEntity = new ResponseEntity();
            objDAL = new BaseDAL();
            sessionEntity = new SessionEntity();
            IsSessionManage = SessionManage;
            pwdManager = new PasswordManager();
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here.
                if (objDAL != null) { objDAL.Dispose(); }
                pwdManager.Dispose();
                objResponseEntity = null;
                sessionEntity = null;
                dtData = null;
                Parameters = null;
                SqlQry = null;

            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~UserAuthenticationDM()
        {
            Dispose(false);
        }

        #endregion


        public ResponseEntity UserLogin(string user_name, string user_pwd, UserEntity userEntity = null)
        {
            try
            {


                if (((userEntity.user_login_type == "f" || userEntity.user_login_type == "g") && (userEntity.social_id != null)) || (userEntity.user_login_type == "m"))
                {
                    #region for social login
                    string SQLQry = @"SELECT user_master.deviceno, user_master.user_id,user_master.role_Id, user_master.user_name, user_master.first_name, user_master.middle_name, user_master.last_name,  to_char(user_master.date_of_birth, 'yyyy-MM-dd') as  date_of_birth, user_master.gender, user_master.license_no, 
                                to_char(  user_master.license_expiry_date, 'yyyy-MM-dd') as license_expiry_date, user_master.address1,user_master.address2,user_master.address3, user_master.mobileno, user_master.email_id1,user_master.email_id2, user_master.profile_picture_uri, user_master.insurance_mapping_key, user_master.user_id, user_master.mobileno,user_master.vehicle_no,user_master.registration_no,user_master.vehicle_manufacturer,user_master.vehicle_color,user_master.vehicle_type,user_master.o_id, user_master.social_type,user_master.country,user_master.state,user_master.vehiclengine_type,user_master.pincode,user_master.region,user_master.city,user_master.vin_no,user_master.insurance_provider,user_master.insurer_emailid,user_master.engine_capacity,user_master.insurance_comp_contact,user_master.sos_contact,user_master.family_contact,   
                                user_master.social_id,user_master.user_login_type,role_master.role_code,user_master.obdavaliable, user_master.user_category,org_master.o_logo1,coalesce(user_master.premium,false) as premium  FROM user_master INNER JOIN
                                role_master ON user_master.role_Id = role_master.role_Id inner join org_master on org_master.o_id = user_master.o_id  WHERE   (coalesce(user_master.deleted,false) = false ) and (user_master.user_name = @user_name)  AND (user_master.isactive = true) ";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_name", user_name));

                    objDAL.CreateConnection();
                    dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);

                    if (dtData != null && dtData.Rows.Count > 0)
                    {
                        sessionEntity = fillsessionentity(dtData);
                        if (IsSessionManage)
                        {
                            Sessions objSession = new Sessions();
                            objSession.SetSessionEntity(sessionEntity);
                            objSession = null;
                        }

                        objResponseEntity.IsComplete = true;
                        objResponseEntity.Description = "success";
                        objResponseEntity.data = sessionEntity;
                        objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

                        //for log login info and device token detials 
                        logLoginAndDevice(userEntity, sessionEntity);
                    }
                    else
                    {
                        objResponseEntity.IsComplete = false;
                        objResponseEntity.Description = "invalid username.";
                        objResponseEntity.message = MessageCode.MsgInJson(MsgCode.invalid_username);
                    }

                    #endregion

                    objDAL.CloseConnection();

                    return objResponseEntity;

                }
                else
                {
                    #region for register user login
                    string SQLQry = @"SELECT  user_pwd.pwdhash, user_pwd.random_salt,user_master.deviceno, user_master.user_id,user_master.role_Id, user_master.user_name, user_master.first_name, user_master.middle_name, user_master.last_name, to_char(user_master.date_of_birth, 'yyyy-MM-dd') as  date_of_birth, user_master.gender, user_master.license_no, 
                                user_master.license_expiry_date, user_master.address1,user_master.address2,user_master.address3, user_master.mobileno, user_master.email_id1,user_master.email_id2, user_master.profile_picture_uri, user_master.insurance_mapping_key, user_master.user_id, user_master.mobileno,user_master.vehicle_no,user_master.registration_no,user_master.vehicle_manufacturer,user_master.vehicle_color,user_master.vehicle_type,user_master.o_id, user_master.social_type,user_master.country,user_master.state,user_master.vehiclengine_type,user_master.pincode,user_master.region,user_master.city,user_master.vin_no,user_master.insurance_provider,user_master.insurer_emailid,user_master.engine_capacity,user_master.insurance_comp_contact,user_master.sos_contact,user_master.family_contact,     
                                role_master.role_code, role_master.role_desc, user_pwd.pwdhash,user_master.social_id,user_master.user_login_type,user_master.obdavaliable, user_master.user_category,org_master.o_logo1,coalesce(user_master.premium,false) as premium
                                FROM user_master INNER JOIN
                                user_pwd ON user_master.user_id = user_pwd.user_id INNER JOIN
                                role_master ON user_master.role_Id = role_master.role_Id inner join org_master on org_master.o_id = user_master.o_id
                                WHERE   (coalesce(user_master.deleted,false) = false ) and (user_master.user_name = @user_name) AND (user_pwd.isactive = true) AND (user_master.isactive = true) ";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_name", user_name));

                    objDAL.CreateConnection();
                    dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);


                    if (dtData != null && dtData.Rows.Count > 0)
                    {
                        string Salt = dtData.Rows[0]["random_salt"].ToString();
                        string pwdhash = dtData.Rows[0]["pwdhash"].ToString();
                        bool result = pwdManager.IsPasswordMatch(user_pwd, Salt, pwdhash);

                        if (result)
                        {
                            sessionEntity = fillsessionentity(dtData);

                            if (IsSessionManage)
                            {
                                Sessions objSession = new Sessions();
                                objSession.SetSessionEntity(sessionEntity);
                                objSession = null;
                            }

                            objResponseEntity.IsComplete = true;
                            objResponseEntity.Description = "success";
                            objResponseEntity.data = sessionEntity;
                            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.ok);

                            //for log login info and device token detials 
                            logLoginAndDevice(userEntity, sessionEntity);

                        }
                        else
                        {
                            objResponseEntity.IsComplete = false;
                            objResponseEntity.Description = "invalid password";
                            objResponseEntity.message = MessageCode.MsgInJson(MsgCode.invalid_password);
                        }
                    }
                    else
                    {
                        objResponseEntity.IsComplete = false;
                        objResponseEntity.Description = "invalid username.";
                        objResponseEntity.message = MessageCode.MsgInJson(MsgCode.invalid_username);
                    }
                    #endregion

                    objDAL.CloseConnection();
                    return objResponseEntity;
                }
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;

            }
        }

        public string CheckPageSecurity(string PageName, Int32 userId, Int32 role_Id, string action)
        {

            string SQLQry = @"SELECT user_permission.user_id, user_permission.page_id, user_permission.C, user_permission.R, user_permission.U, user_permission.D, user_permission.V, user_permission.isactive 
                         , page_master.page_name FROM user_permission INNER JOIN page_master ON user_permission.page_id = page_master.page_id WHERE  (page_master.page_name =@page_name) AND  (user_permission.user_id=@user_id) AND (user_permission.isactive = true);
                        SELECT role_permission.role_Id, role_permission.page_id, role_permission.C, role_permission.R, role_permission.U, role_permission.D, role_permission.V, role_permission.isactive 
                         , page_master.page_name FROM role_permission INNER JOIN page_master ON role_permission.page_id = page_master.page_id WHERE  (page_master.page_name =@page_name) AND  (role_permission.role_id=@role_id) AND (role_permission.isactive = true);";

            Parameters = new List<NpgsqlParameter>();
            Parameters.Add(new NpgsqlParameter("@page_name", PageName));
            Parameters.Add(new NpgsqlParameter("@user_id", userId));
            Parameters.Add(new NpgsqlParameter("@role_Id", role_Id));

            objDAL.CreateConnection();
            DataSet dsData = objDAL.ExecutParameterizedQrywithDataSet(SQLQry, Parameters);
            objDAL.CloseConnection();

            if (dsData.Tables[0].Rows.Count > 0)  ///user specific
            {
                bool IsPageSecurity = CommonUtils.ConvertToBoolean(dsData.Tables[0].Rows[0][action]);
                if (IsPageSecurity)
                {
                    return "";
                }
            }
            else
            {
                if (dsData.Tables[1].Rows.Count > 0) ///role specific
                {
                    bool IsPageSecurity = CommonUtils.ConvertToBoolean(dsData.Tables[1].Rows[0][action]);
                    if (IsPageSecurity)
                    {
                        return "";
                    }
                }
            }
            return "Page security Issue Occured.";
        }

        public bool CheckUserAvailability(string user_name)
        {
            try
            {
                string SQLQry = @"select user_id  from user_master where user_name=@user_name and coalesce(user_master.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_name", CommonUtils.ConvertToString(user_name)));

                objDAL.CreateConnection();
                dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        //Fill session Entity during Authentication process 

        public SessionEntity fillsessionentity(DataTable dtData)
        {
            DataRow row = dtData.Rows[0];
            Int32 _uid = CommonUtils.ConvertToInt(row["user_id"]);
            sessionEntity.user_id = CommonUtils.ConvertToInt(row["user_id"]);
            sessionEntity.user_name = CommonUtils.ConvertToString(row["user_name"]);
            //  sessionEntity.UserPIN = CommonUtils.ConvertToString(row["userpwd"]);
            //if (CommonUtils.ConvertToString(row["user_login_type"]) == "regi")
            //{
            sessionEntity.user_role = CommonUtils.ConvertToString(row["role_code"]);
            sessionEntity.role_Id = CommonUtils.ConvertToInt(row["role_Id"]);

            //}
            sessionEntity.first_name = CommonUtils.ConvertToString(row["first_name"]);
            sessionEntity.middle_name = CommonUtils.ConvertToString(row["middle_name"]);
            sessionEntity.last_name = CommonUtils.ConvertToString(row["last_name"]);
            sessionEntity.deviceno = CommonUtils.ConvertToInt64(row["deviceno"]);
            sessionEntity.profile_picture_uri = CommonUtils.getUrlPath(CommonUtils.ConvertToString(row["profile_picture_uri"]), "userprofile", out Url);
            //if ((row["social_id"] == null || row["social_id"] == ""))
            //{
            //    sessionEntity.profile_picture_uri = CommonUtils.getUrlPath(CommonUtils.ConvertToString(row["profile_picture_uri"]), "userprofile", out Url);
            //}
            //else
            //{
            //    sessionEntity.profile_picture_uri = CommonUtils.ConvertToString(row["profile_picture_uri"]);
            //}
            sessionEntity.social_id = CommonUtils.ConvertToString(row["social_id"]);
            sessionEntity.user_login_type = CommonUtils.ConvertToString(row["user_login_type"]);
            sessionEntity.mobileno = CommonUtils.ConvertToString(row["mobileno"]);
            sessionEntity.gender = CommonUtils.ConvertToString(row["gender"]);
            sessionEntity.email_id1 = CommonUtils.ConvertToString(row["email_id1"]);
            sessionEntity.email_id2 = CommonUtils.ConvertToString(row["email_id2"]);
            sessionEntity.license_no = CommonUtils.ConvertToString(row["license_no"]);
            sessionEntity.license_expiry_date = CommonUtils.ConvertToString(row["license_expiry_date"]);
            sessionEntity.address1 = CommonUtils.ConvertToString(row["address1"]);
            sessionEntity.address2 = CommonUtils.ConvertToString(row["address2"]);
            sessionEntity.address3 = CommonUtils.ConvertToString(row["address3"]);
            sessionEntity.email_id1 = CommonUtils.ConvertToString(row["email_id1"]);
            sessionEntity.email_id2 = CommonUtils.ConvertToString(row["email_id2"]);
            sessionEntity.vehicle_no = CommonUtils.ConvertToString(row["vehicle_no"]);
            sessionEntity.registration_no = CommonUtils.ConvertToString(row["registration_no"]);
            sessionEntity.vehicle_manufacturer = CommonUtils.ConvertToString(row["vehicle_manufacturer"]);
            sessionEntity.vehicle_color = CommonUtils.ConvertToString(row["vehicle_color"]);
            sessionEntity.vehicle_type = CommonUtils.ConvertToString(row["vehicle_type"]);
            sessionEntity.vehicle_manufacturer = CommonUtils.ConvertToString(row["vehicle_manufacturer"]);
            sessionEntity.o_id = CommonUtils.ConvertToInt(row["o_id"]);
            sessionEntity.country = CommonUtils.ConvertToString(row["country"]);
            sessionEntity.state = CommonUtils.ConvertToString(row["state"]);
            sessionEntity.vehiclengine_type = CommonUtils.ConvertToString(row["vehiclengine_type"]);
            sessionEntity.city = CommonUtils.ConvertToString(row["city"]);
            sessionEntity.region = CommonUtils.ConvertToString(row["region"]);
            sessionEntity.pincode = CommonUtils.ConvertToString(row["pincode"]);
            sessionEntity.vin_no = CommonUtils.ConvertToInt(row["vin_no"]);
            sessionEntity.insurance_provider = CommonUtils.ConvertToString(row["insurance_provider"]);
            sessionEntity.insurer_emailid = CommonUtils.ConvertToString(row["insurer_emailid"]);
            sessionEntity.date_of_birth = CommonUtils.ConvertToString(row["date_of_birth"]);
            sessionEntity.insurance_mapping_key = CommonUtils.ConvertToString(row["insurance_mapping_key"]);
            sessionEntity.obdavaliable = CommonUtils.ConvertToBoolean(row["obdavaliable"]);
            sessionEntity.user_category = CommonUtils.ConvertToString (row["user_category"]);
            sessionEntity.o_logo1 = CommonUtils.getUrlDocPath(CommonUtils.ConvertToString(row["o_logo1"]), "organization", CommonUtils.ConvertToString(row["o_id"]), out Url);
            sessionEntity.engine_capacity = CommonUtils.ConvertToString(row["engine_capacity"]);
            sessionEntity.sos_contact = CommonUtils.ConvertToString(row["sos_contact"]);
            sessionEntity.insurance_comp_contact = CommonUtils.ConvertToString(row["insurance_comp_contact"]);
            sessionEntity.family_contact = CommonUtils.ConvertToString(row["family_contact"]);
            sessionEntity.premium = CommonUtils.ConvertToBoolean(row["premium"]);

            row = null;
            dtData.Dispose();

            return sessionEntity;


        }

        private void logLoginAndDevice(UserEntity userEntity, SessionEntity sessionEntity)
        {
            string SQLQry;
            SQLQry = @"INSERT INTO user_login_log(user_id, logindatetime, action)
	                       VALUES (@user_id, @logindatetime, @action)";
            Parameters = new List<NpgsqlParameter>();
            Parameters.Add(new NpgsqlParameter("@user_id", sessionEntity.user_id));
            Parameters.Add(new NpgsqlParameter("@logindatetime", CommonUtils.ConvertToDateTime(DateTime.Now)));
            Parameters.Add(new NpgsqlParameter("@action", "logout"));
            objDAL.ExecutParameterizedQry(SQLQry, Parameters);


            //-------------store device info as per sacin sir requirement
            if (userEntity != null && userEntity.userDeviceInfoEntities != null)
            {
                try
                {

                    SQLQry = @" update user_device_info set isactive=false where  user_id=@user_id;";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", sessionEntity.user_id));
                    objDAL.ExecutParameterizedQry(SQLQry, Parameters);

                    foreach (UserDeviceInfoEntity UserDeviceInfoEntity in userEntity.userDeviceInfoEntities)
                    {
                        SQLQry = @"INSERT INTO public.user_device_info(device_number, device_type, device_name, device_status, os_version, hw_id, user_token, user_id, created_at, isactive)
	                                     VALUES (@device_number, @device_type, @device_name, @device_status, @os_version, @hw_id, @user_token, @user_id, @created_at, @isactive);";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@device_number", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_number)));
                        Parameters.Add(new NpgsqlParameter("@device_type", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_type)));
                        Parameters.Add(new NpgsqlParameter("@device_name", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_name)));
                        Parameters.Add(new NpgsqlParameter("@device_status", CommonUtils.ConvertToString(UserDeviceInfoEntity.device_status)));
                        Parameters.Add(new NpgsqlParameter("@os_version", CommonUtils.ConvertToString(UserDeviceInfoEntity.os_version)));
                        Parameters.Add(new NpgsqlParameter("@hw_id", CommonUtils.ConvertToString(UserDeviceInfoEntity.hw_id)));
                        Parameters.Add(new NpgsqlParameter("@user_token", CommonUtils.ConvertToString(UserDeviceInfoEntity.user_token)));
                        Parameters.Add(new NpgsqlParameter("@user_id", sessionEntity.user_id));
                        Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                        Parameters.Add(new NpgsqlParameter("@isactive", true));
                        objDAL.ExecutParameterizedQry(SQLQry, Parameters);
                    }

                    SQLQry = null;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }
    }
}
