﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;

namespace IOTDataModel
{
  
        public class DeviceDM : IDisposable
        {
            private BaseDAL objDAL;
            private List<NpgsqlParameter> Parameters;
            private String strSqlQry;
            private Boolean blnResult;
            private string objResponseEntity;
           // private Int32 _d_no;
            private Int32 _device_no;
           // private String strMaxQry;

            public DeviceDM()
            {
                objDAL = new BaseDAL();

            }

            #region ------------disposed managed and unmanaged objects-----------

            // Flag: Has Dispose already been called?
            bool disposed = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
            // Protected implementation of Dispose pattern.
            protected virtual void Dispose(bool disposing)
            {
                if (disposed) return;
                if (disposing)
                {
                    // Free any other managed objects here.
                    objDAL.Dispose();
                    Parameters = null;
                    strSqlQry = null;
                  //  strMaxQry = null;
                }
                // Free any unmanaged objects here.
                disposed = true;
            }

            ~DeviceDM()
            {
                Dispose(false);
            }

            #endregion

            public bool CreateDevice(DeviceEntity deviceEntity)
            {
                try
                {
                    objDAL.CreateConnection();
                    objDAL.BeginTransaction();

                    strSqlQry = @"INSERT INTO public.device_master(deviceno, vendor_name, framework, versionno, sim_no, imei_no, sim_installation, sim_activation_date, sim_expiry_date, sms_active, device_live_date, created_at, isactive)
	                              VALUES (@deviceno, @v_name,@framework ,@versionno , @sim_no,@imei_no ,@sim_installation ,@sim_activation_date , @sim_expiry_date, @sms_active, @device_live_date, @created_at, @isactive);";
                    
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt(deviceEntity.deviceno)));
                    Parameters.Add(new NpgsqlParameter("@v_name", CommonUtils.ConvertToString(deviceEntity.vendor_name)));
                    Parameters.Add(new NpgsqlParameter("@framework", CommonUtils.ConvertToString(deviceEntity.framework)));
                    Parameters.Add(new NpgsqlParameter("@versionno", CommonUtils.ConvertToString(deviceEntity.versionno)));
                    Parameters.Add(new NpgsqlParameter("@sim_no", CommonUtils.ConvertToString(deviceEntity.sim_no)));
                    Parameters.Add(new NpgsqlParameter("@imei_no", CommonUtils.ConvertToString(deviceEntity.imei_no)));
                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_installation) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_installation", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_installation", CommonUtils.ConvertToDateTime(deviceEntity.sim_installation)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_activation_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_activation_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_activation_date", CommonUtils.ConvertToDateTime(deviceEntity.sim_activation_date)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_expiry_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_expiry_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_expiry_date", CommonUtils.ConvertToDateTime(deviceEntity.sim_expiry_date)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.device_live_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@device_live_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@device_live_date", CommonUtils.ConvertToDateTime(deviceEntity.device_live_date)));
                    }

                    Parameters.Add(new NpgsqlParameter("@sms_active", CommonUtils.ConvertToBoolean(deviceEntity.sms_active)));
                    Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                    Parameters.Add(new NpgsqlParameter("@isactive", true));


                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    objDAL.CommitTransaction();
                    objDAL.CloseConnection();

                    return true;

                }
                catch (Exception ex)
                {
                    objDAL.RollBackTransaction();
                    objDAL.CloseConnection();
                    throw ex;
                }

            }

            public DataTable GetDeviceList()
            {
                try
                {
                    strSqlQry = @"SELECT deviceno, vendor_name, framework, versionno, sim_no, imei_no, sim_installation, sim_activation_date, sim_expiry_date, sms_active, device_live_date, created_at, updated_at, deleted, isactive FROM device_master  WHERE (coalesce(device_master.deleted,false) = '0')";

                    objDAL.CreateConnection();
                    DataTable dtDevice = objDAL.FetchRecords(strSqlQry);
                    objDAL.CloseConnection();

                    return dtDevice;
                }
                catch (Exception ex)
                {
                    objDAL.CloseConnection();
                    throw ex;
                }
                finally
                {
                    objResponseEntity = null;
                }
            }

            public DataTable GetDeviceById(DeviceEntity deviceEntity)
            {
                try
                {
                    strSqlQry = @"SELECT deviceno, vendor_name, framework, versionno, sim_no, imei_no, sim_installation, sim_activation_date, sim_expiry_date, sms_active, device_live_date, created_at, updated_at, deleted, isactive FROM device_master where deviceno=" + CommonUtils.ConvertToInt(deviceEntity.deviceno);

                    objDAL.CreateConnection();
                    DataTable dtDevice = objDAL.FetchRecords(strSqlQry);
                    objDAL.CloseConnection();

                    return dtDevice;
                }
                catch (Exception ex)
                {
                    objDAL.CloseConnection();
                    throw ex;
                }
                finally
                {
                    objResponseEntity = null;
                }
            }

            public bool UpdateDevice(DeviceEntity deviceEntity)
            {
                try
                {
                    objDAL.CreateConnection();
                    objDAL.BeginTransaction();

                    strSqlQry = @"UPDATE public.device_master SET  vendor_name=@v_name, framework=@framework, versionno=@versionno, sim_no=@sim_no, imei_no=@imei_no, sim_installation=@sim_installation, sim_activation_date=@sim_activation_date, sim_expiry_date=@sim_expiry_date, sms_active=@sms_active, device_live_date=@device_live_date, updated_at=@updated_at
	                               WHERE deviceno=@deviceno ";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt(deviceEntity.deviceno)));
                    Parameters.Add(new NpgsqlParameter("@v_name", CommonUtils.ConvertToString(deviceEntity.vendor_name)));
                    Parameters.Add(new NpgsqlParameter("@framework", CommonUtils.ConvertToString(deviceEntity.framework)));
                    Parameters.Add(new NpgsqlParameter("@versionno", CommonUtils.ConvertToString(deviceEntity.versionno)));
                    Parameters.Add(new NpgsqlParameter("@sim_no", CommonUtils.ConvertToString(deviceEntity.sim_no)));
                    Parameters.Add(new NpgsqlParameter("@imei_no", CommonUtils.ConvertToString(deviceEntity.imei_no)));
                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_installation) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_installation", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_installation", CommonUtils.ConvertToDateTime(deviceEntity.sim_installation)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_activation_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_activation_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_activation_date", CommonUtils.ConvertToDateTime(deviceEntity.sim_activation_date)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.sim_expiry_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_expiry_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@sim_expiry_date", CommonUtils.ConvertToDateTime(deviceEntity.sim_expiry_date)));
                    }

                    if (CommonUtils.ConvertToDateTime(deviceEntity.device_live_date) == null)
                    {
                        Parameters.Add(new NpgsqlParameter("@device_live_date", DBNull.Value));
                    }
                    else
                    {
                        Parameters.Add(new NpgsqlParameter("@device_live_date", CommonUtils.ConvertToDateTime(deviceEntity.device_live_date)));
                    }
                  
                    Parameters.Add(new NpgsqlParameter("@sms_active", CommonUtils.ConvertToBoolean(deviceEntity.sms_active)));
                    Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));



                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    objDAL.CommitTransaction();
                    objDAL.CloseConnection();

                    return blnResult;

                }
                catch (Exception ex)
                {
                    objDAL.RollBackTransaction();
                    objDAL.CloseConnection();
                    throw ex;
                }

            }

            public bool DeleteDevice(DeviceEntity deviceEntity)
            {
                try
                {
                    _device_no = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(deviceEntity.deviceno));

                    objDAL.CreateConnection();
                    objDAL.BeginTransaction();

                    strSqlQry = @"update device_master set deleted=true WHERE deviceno=@deviceno";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@deviceno", _device_no));

                    blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    objDAL.CommitTransaction();
                    objDAL.CloseConnection();


                    return blnResult;

                }
                catch (Exception ex)
                {
                    objDAL.RollBackTransaction();
                    objDAL.CloseConnection();
                    throw ex;
                }

            }


            public bool CheckDeviceAvailability(DeviceEntity deviceEntity)
            {
                try
                {
                    string SQLQry = @"select deviceno  from device_master  where deviceno= @deviceno and coalesce(device_master.deleted,false) = false";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@deviceno", CommonUtils.ConvertToInt64(deviceEntity.deviceno)));

                    objDAL.CreateConnection();
                   DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                    objDAL.CloseConnection();

                    if (dtData != null && dtData.Rows.Count > 0)
                    {
                        objDAL.CloseConnection();
                        return true;
                    }

                    return false;
                }
                catch (Exception ex)
                {
                    objDAL.CloseConnection();
                    throw ex;
                }

            }









            //public bool endtrip(TripEntity tripEntity)
            //{
            //    try
            //    {
            //        objDAL.CreateConnection();
            //        objDAL.BeginTransaction();

            //        strSqlQry = @"UPDATE trip_planning_master SET  trip_end_time=@trip_end_time, updated_at=@updated_at WHERE trip_id=@trip_id;";

            //        Parameters = new List<NpgsqlParameter>();
            //        Parameters.Add(new NpgsqlParameter("@trip_id", CommonUtils.ConvertToInt(tripEntity.trip_id)));
            //        Parameters.Add(new NpgsqlParameter("@trip_end_time", CommonUtils.ConvertToDateTime(tripEntity.trip_end_time)));
            //        Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

            //        objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

            //        objDAL.CommitTransaction();
            //        objDAL.CloseConnection();

            //        return true;

            //    }
            //    catch (Exception ex)
            //    {
            //        objDAL.RollBackTransaction();
            //        objDAL.CloseConnection();
            //        throw ex;
            //    }

            //}

            //public bool DeleteTrip(TripEntity tripEntity)
            //{
            //    try
            //    {
            //        _user_id = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(tripEntity.user_id));

            //        objDAL.CreateConnection();
            //        objDAL.BeginTransaction();

            //        strSqlQry = @"update trip_planning_master set deleted=true where trip_id = (SELECT trip_id FROM trip_planning_master order  by trip_id  desc limit 1) and user_id=@u_id ";
            //        Parameters = new List<NpgsqlParameter>();
            //        Parameters.Add(new NpgsqlParameter("@u_id", _user_id));

            //        blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

            //        objDAL.CommitTransaction();
            //        objDAL.CloseConnection();


            //        return blnResult;

            //    }
            //    catch (Exception ex)
            //    {
            //        objDAL.RollBackTransaction();
            //        objDAL.CloseConnection();
            //        throw ex;
            //    }

            //}




        }
    
}
