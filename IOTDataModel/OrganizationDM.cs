﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;

namespace IOTDataModel
{
    public class OrganizationDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private Boolean blnResult;
        private Int32 _O_id;
        private String strMaxQry;

        public OrganizationDM()
        {
            objDAL = new BaseDAL();
            
        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
                strSqlQry = null;
                strMaxQry = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~OrganizationDM()
        {
            Dispose(false);
        }

        #endregion

        public int CreateOrganization(OrganizationEntity organizationEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strMaxQry = @"select coalesce(max(o_id),0)+ 1 FROM org_master";

                object objO_id = objDAL.FetchSingleRecord(strMaxQry);

                strSqlQry = @"INSERT INTO org_master(o_id,o_code, o_name, o_type, address1, address2, address3, pincode_no, email_id1, email_id2, contact_no1, contact_no2, isactive, created_at,deleted,o_logo1,o_logo2,o_logo3 )
	            VALUES (@o_id,@o_code, @o_name, @o_type, @address1, @address2, @address3, @pincode_no, @email_id1, @email_id2, @contact_no1, @contact_no2, @isactive, @created_at,@deleted,@o_logo1,@o_logo2,@o_logo3);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt(objO_id)));
                Parameters.Add(new NpgsqlParameter("@o_code", CommonUtils.ConvertToString(organizationEntity.o_code)));
                Parameters.Add(new NpgsqlParameter("@o_name", CommonUtils.ConvertToString(organizationEntity.o_name)));
                Parameters.Add(new NpgsqlParameter("@o_type", CommonUtils.ConvertToString(organizationEntity.o_type)));
                Parameters.Add(new NpgsqlParameter("@address1", CommonUtils.ConvertToString(organizationEntity.address1)));
                Parameters.Add(new NpgsqlParameter("@address2", CommonUtils.ConvertToString(organizationEntity.address2)));
                Parameters.Add(new NpgsqlParameter("@address3", CommonUtils.ConvertToString(organizationEntity.address3)));
                Parameters.Add(new NpgsqlParameter("@pincode_no", CommonUtils.ConvertToString(organizationEntity.pincode_no)));
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(organizationEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(organizationEntity.email_id2)));
                Parameters.Add(new NpgsqlParameter("@contact_no1", CommonUtils.ConvertToString(organizationEntity.contact_no1)));
                Parameters.Add(new NpgsqlParameter("@contact_no2", CommonUtils.ConvertToString(organizationEntity.contact_no2)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@deleted", false));
                Parameters.Add(new NpgsqlParameter("@o_logo1", CommonUtils.ConvertToString(organizationEntity.o_logo1)));
                Parameters.Add(new NpgsqlParameter("@o_logo2", CommonUtils.ConvertToString(organizationEntity.o_logo2)));
                Parameters.Add(new NpgsqlParameter("@o_logo3", CommonUtils.ConvertToString(organizationEntity.o_logo3)));

                 objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

                if (objO_id != null)
                {
                    _O_id = CommonUtils.ConvertToInt(objO_id.ToString());
                    foreach (OrganizationContactEntity OrgContactEntity in organizationEntity.OrgContactEntities)
                    {
                        strSqlQry = @"INSERT INTO org_contact_person(o_id, contact_person_nm, email_id, contact_no, updated_at, deleted)
	                            VALUES (@o_id, @contact_person_nm, @email_id, @contact_no, @updated_at, @deleted);";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@o_id", _O_id));
                        Parameters.Add(new NpgsqlParameter("@contact_person_nm", CommonUtils.ConvertToString(OrgContactEntity.contact_person_nm)));
                        Parameters.Add(new NpgsqlParameter("@email_id", CommonUtils.ConvertToString(OrgContactEntity.email_id)));
                        Parameters.Add(new NpgsqlParameter("@contact_no", CommonUtils.ConvertToString(OrgContactEntity.contact_no)));
                        Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                        Parameters.Add(new NpgsqlParameter("@deleted", DBNull.Value));

                        objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }

                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return _O_id;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public List<OrganizationEntity> GetOrganizationList(FilterEntity filterEntity)
        {
            List<OrganizationEntity> organizationEntities = new List<OrganizationEntity>();
            OrganizationEntity organizationEntity;
            OrganizationContactEntity OrgContactEntity;
            try
            {
                strSqlQry = @"SELECT o_id, o_code, o_name, o_type, address1, address2, address3, pincode_no, email_id1, email_id2, contact_no1, contact_no2, isactive, created_at, updated_at, deleted
	                         FROM org_master WHERE (coalesce(deleted,false) = false);
                            SELECT o_id, contact_person_nm, email_id, contact_no, updated_at, deleted FROM org_contact_person WHERE (coalesce(deleted,false) = false)";

                objDAL.CreateConnection();
                DataSet DSOrganization = objDAL.FetchDataSetRecords(strSqlQry);
                objDAL.CloseConnection();

                if (DSOrganization.Tables[0] != null && DSOrganization.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow datarow in DSOrganization.Tables[0].Rows)
                    {
                        organizationEntity = new OrganizationEntity();

                        organizationEntity.o_id = CommonUtils.ConvertToInt(datarow["o_id"]);
                        organizationEntity.o_code = CommonUtils.ConvertToString(datarow["o_code"]);
                        organizationEntity.o_name = CommonUtils.ConvertToString(datarow["o_name"]);
                        organizationEntity.o_type = CommonUtils.ConvertToString(datarow["o_type"]);
                        organizationEntity.address1 = CommonUtils.ConvertToString(datarow["address1"]);
                        organizationEntity.address2 = CommonUtils.ConvertToString(datarow["address2"]);
                        organizationEntity.address3 = CommonUtils.ConvertToString(datarow["address3"]);
                        organizationEntity.pincode_no = CommonUtils.ConvertToString(datarow["pincode_no"]);
                        organizationEntity.email_id1 = CommonUtils.ConvertToString(datarow["email_id1"]);
                        organizationEntity.email_id2 = CommonUtils.ConvertToString(datarow["email_id2"]);
                        organizationEntity.contact_no1 = CommonUtils.ConvertToString(datarow["contact_no1"]);
                        organizationEntity.contact_no2 = CommonUtils.ConvertToString(datarow["contact_no2"]);
                        organizationEntity.isactive = CommonUtils.ConvertToBoolean(datarow["isactive"]);
                        organizationEntity.created_at = CommonUtils.ConvertToDateTime(datarow["created_at"]);
                        organizationEntity.updated_at = CommonUtils.ConvertToDateTime(datarow["updated_at"]);

                        OrgContactEntity = new OrganizationContactEntity();
                        if (DSOrganization.Tables[1] != null && DSOrganization.Tables[1].Rows.Count > 0)
                        {
                            List<OrganizationContactEntity> OrgContactEntities = new List<OrganizationContactEntity>();
                            foreach (DataRow datarowCont in DSOrganization.Tables[1].Select("o_id=" + organizationEntity.o_id))
                            {
                                OrgContactEntity.o_id = CommonUtils.ConvertToInt(datarowCont["o_id"]);
                                OrgContactEntity.contact_person_nm = CommonUtils.ConvertToString(datarowCont["contact_person_nm"]);
                                OrgContactEntity.email_id = CommonUtils.ConvertToString(datarowCont["email_id"]);
                                OrgContactEntity.contact_no = CommonUtils.ConvertToString(datarowCont["contact_no"]);
                                OrgContactEntity.updated_at = CommonUtils.ConvertToDateTime(datarowCont["updated_at"]);
                                OrgContactEntities.Add(OrgContactEntity);
                            }
                            if (OrgContactEntities != null)
                            {
                                organizationEntity.OrgContactEntities = OrgContactEntities;
                            }
                            OrgContactEntities = null;
                        }

                        organizationEntities.Add(organizationEntity);

                    }
                }

                return organizationEntities;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                organizationEntities = null;
                organizationEntity = null;
                OrgContactEntity = null;

            }
        }

        public OrganizationEntity GetOrganizationById(FilterEntity filterEntity)
        {
            OrganizationEntity organizationEntity = new OrganizationEntity();
            OrganizationContactEntity OrgContactEntity;
            string Url = " ";
            try
            {
                strSqlQry = @"SELECT o_id, o_code, o_name, o_type, address1, address2, address3, pincode_no, email_id1, email_id2, contact_no1, contact_no2, isactive, created_at, updated_at, deleted,o_logo1,o_logo2,o_logo3
	                         FROM org_master WHERE (coalesce(deleted,false) = false) and o_id=" + CommonUtils.ConvertToInt(filterEntity.o_id) + ";" +
                            " SELECT o_id, contact_person_nm, email_id, contact_no, updated_at, deleted FROM org_contact_person WHERE (coalesce(deleted,false) = false) and o_id=" + CommonUtils.ConvertToInt(filterEntity.o_id);

                objDAL.CreateConnection();
                DataSet DSOrganization = objDAL.FetchDataSetRecords(strSqlQry);
                objDAL.CloseConnection();

                if (DSOrganization.Tables[0] != null && DSOrganization.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow datarow in DSOrganization.Tables[0].Rows)
                    {
                        organizationEntity = new OrganizationEntity();

                        organizationEntity.o_id = CommonUtils.ConvertToInt(datarow["o_id"]);
                        organizationEntity.o_code = CommonUtils.ConvertToString(datarow["o_code"]);
                        organizationEntity.o_name = CommonUtils.ConvertToString(datarow["o_name"]);
                        organizationEntity.o_type = CommonUtils.ConvertToString(datarow["o_type"]);
                        organizationEntity.address1 = CommonUtils.ConvertToString(datarow["address1"]);
                        organizationEntity.address2 = CommonUtils.ConvertToString(datarow["address2"]);
                        organizationEntity.address3 = CommonUtils.ConvertToString(datarow["address3"]);
                        organizationEntity.pincode_no = CommonUtils.ConvertToString(datarow["pincode_no"]);
                        organizationEntity.email_id1 = CommonUtils.ConvertToString(datarow["email_id1"]);
                        organizationEntity.email_id2 = CommonUtils.ConvertToString(datarow["email_id2"]);
                        organizationEntity.contact_no1 = CommonUtils.ConvertToString(datarow["contact_no1"]);
                        organizationEntity.contact_no2 = CommonUtils.ConvertToString(datarow["contact_no2"]);
                        organizationEntity.isactive = CommonUtils.ConvertToBoolean(datarow["isactive"]);
                        organizationEntity.created_at = CommonUtils.ConvertToDateTime(datarow["created_at"]);
                        organizationEntity.updated_at = CommonUtils.ConvertToDateTime(datarow["updated_at"]);
                        organizationEntity.o_logo1 = CommonUtils.getUrlDocPath(CommonUtils.ConvertToString(datarow["o_logo1"]), "organization", CommonUtils.ConvertToString(datarow["o_id"]), out Url);
                        organizationEntity.o_logo2 = CommonUtils.getUrlDocPath(CommonUtils.ConvertToString(datarow["o_logo2"]), "organization", CommonUtils.ConvertToString(datarow["o_id"]), out Url);
                        organizationEntity.o_logo3 = CommonUtils.getUrlDocPath(CommonUtils.ConvertToString(datarow["o_logo3"]), "organization", CommonUtils.ConvertToString(datarow["o_id"]), out Url);
                        organizationEntity.o_logoname1 = CommonUtils.ConvertToString( datarow["o_logo1"]);
                        organizationEntity.o_logoname2 = CommonUtils.ConvertToString(datarow["o_logo2"]);
                        organizationEntity.o_logoname3 = CommonUtils.ConvertToString(datarow["o_logo3"]);

                        
                        if (DSOrganization.Tables[1] != null && DSOrganization.Tables[1].Rows.Count > 0)
                        {
                            List<OrganizationContactEntity> OrgContactEntities = new List<OrganizationContactEntity>();
                            foreach (DataRow datarowCont in DSOrganization.Tables[1].Select("o_id=" + organizationEntity.o_id))
                            {
                                OrgContactEntity = new OrganizationContactEntity();
                                OrgContactEntity.o_id = CommonUtils.ConvertToInt(datarowCont["o_id"]);
                                OrgContactEntity.contact_person_nm = CommonUtils.ConvertToString(datarowCont["contact_person_nm"]);
                                OrgContactEntity.email_id = CommonUtils.ConvertToString(datarowCont["email_id"]);
                                OrgContactEntity.contact_no = CommonUtils.ConvertToString(datarowCont["contact_no"]);
                                OrgContactEntity.updated_at = CommonUtils.ConvertToDateTime(datarowCont["updated_at"]);
                                OrgContactEntities.Add(OrgContactEntity);
                               
                            }
                            if (OrgContactEntities != null)
                            {
                                organizationEntity.OrgContactEntities = OrgContactEntities;
                            }
                            OrgContactEntities = null;
                        }

                    }
                }

                return organizationEntity;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }
            finally
            {
                organizationEntity = null;
                OrgContactEntity = null;

            }
        }

        public bool UpdateOrganization(OrganizationEntity organizationEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                _O_id = CommonUtils.ConvertToInt(organizationEntity.o_id);

                strSqlQry = @"UPDATE org_master set o_code=@o_code, o_name=@o_name, o_type=@o_type, address1=@address1, address2=@address2, address3=@address3, o_logo1=@o_logo1,o_logo2=@o_logo2,o_logo3=@o_logo3 ,
                            pincode_no=@pincode_no, email_id1=@email_id1, email_id2=@email_id2, contact_no1=@contact_no1, contact_no2=@contact_no2, isactive=@isactive
                           ,updated_at=@update_at  WHERE (o_id = @o_id)";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", _O_id));
                Parameters.Add(new NpgsqlParameter("@o_code", CommonUtils.ConvertToString(organizationEntity.o_code)));
                Parameters.Add(new NpgsqlParameter("@o_name", CommonUtils.ConvertToString(organizationEntity.o_name)));
                Parameters.Add(new NpgsqlParameter("@o_type", CommonUtils.ConvertToString(organizationEntity.o_type)));
                Parameters.Add(new NpgsqlParameter("@address1", CommonUtils.ConvertToString(organizationEntity.address1)));
                Parameters.Add(new NpgsqlParameter("@address2", CommonUtils.ConvertToString(organizationEntity.address2)));
                Parameters.Add(new NpgsqlParameter("@address3", CommonUtils.ConvertToString(organizationEntity.address3)));
                Parameters.Add(new NpgsqlParameter("@pincode_no", CommonUtils.ConvertToString(organizationEntity.pincode_no)));
                Parameters.Add(new NpgsqlParameter("@email_id1", CommonUtils.ConvertToString(organizationEntity.email_id1)));
                Parameters.Add(new NpgsqlParameter("@email_id2", CommonUtils.ConvertToString(organizationEntity.email_id2)));
                Parameters.Add(new NpgsqlParameter("@contact_no1", CommonUtils.ConvertToString(organizationEntity.contact_no1)));
                Parameters.Add(new NpgsqlParameter("@contact_no2", CommonUtils.ConvertToString(organizationEntity.contact_no2)));
                Parameters.Add(new NpgsqlParameter("@o_logo1", CommonUtils.ConvertToString(organizationEntity.o_logo1)));
                Parameters.Add(new NpgsqlParameter("@o_logo2", CommonUtils.ConvertToString(organizationEntity.o_logo2)));
                Parameters.Add(new NpgsqlParameter("@o_logo3", CommonUtils.ConvertToString(organizationEntity.o_logo3)));
                Parameters.Add(new NpgsqlParameter("@isactive", CommonUtils.ConvertToBoolean(organizationEntity.isactive)));
                Parameters.Add(new NpgsqlParameter("@update_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                if (blnResult)
                {

                    strSqlQry = @"UPDATE org_contact_person set deleted=true where o_id=@o_id";
                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@o_id", _O_id));

                    objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                    foreach (OrganizationContactEntity OrgContactEntity in organizationEntity.OrgContactEntities)
                    {
                        strSqlQry = @"INSERT INTO org_contact_person(o_id, contact_person_nm, email_id, contact_no, updated_at, deleted)
	                            VALUES (@o_id, @contact_person_nm, @email_id, @contact_no, @updated_at, @deleted);";
                        Parameters = new List<NpgsqlParameter>();
                        Parameters.Add(new NpgsqlParameter("@o_id", _O_id));
                        Parameters.Add(new NpgsqlParameter("@contact_person_nm", CommonUtils.ConvertToString(OrgContactEntity.contact_person_nm)));
                        Parameters.Add(new NpgsqlParameter("@email_id", CommonUtils.ConvertToString(OrgContactEntity.email_id)));
                        Parameters.Add(new NpgsqlParameter("@contact_no", CommonUtils.ConvertToString(OrgContactEntity.contact_no)));
                        Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                        Parameters.Add(new NpgsqlParameter("@deleted", DBNull.Value));

                        blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);
                    }
                }

                objDAL.CommitTransaction();
                objDAL.CloseConnection();

                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool DeleteOrganization(OrganizationEntity organizationEntity)
        {
            try
            {
                _O_id = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(organizationEntity.o_id));

                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @"UPDATE org_master set deleted=true where o_id=@o_id";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", _O_id));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckOrganizatioAvailability(OrganizationEntity organizationEntity)
        {
            try
            {
                string SQLQry = @"select o_id  from org_contact_person where o_code=@o_code";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_code", CommonUtils.ConvertToString(organizationEntity.o_code)));

                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }


    }
}
