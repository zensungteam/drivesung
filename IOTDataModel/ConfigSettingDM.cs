﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Npgsql;
using IOTBusinessEntities;
using IOTUtilities;
using HashAndSalt;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IOTDataModel
{
    public class ConfigSettingDM : IDisposable
    {
        private BaseDAL objDAL;
        private List<NpgsqlParameter> Parameters;
        private String strSqlQry;
        private Boolean blnResult;
        private Int32 _O_id;

        public ConfigSettingDM()
        {
            objDAL = new BaseDAL();

        }

        #region ------------disposed managed and unmanaged objects-----------

        // Flag: Has Dispose already been called?
        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                // Free any other managed objects here.
                objDAL.Dispose();
                Parameters = null;
                strSqlQry = null;
            }
            // Free any unmanaged objects here.
            disposed = true;
        }

        ~ConfigSettingDM()
        {
            Dispose(false);
        }

        #endregion

        public bool CreateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @"INSERT INTO dr_parameter_config_setting	(o_id, maxspeed, minspeed, overspeedlow, overspeed10, overspeed20, overspeed30, overspeedlow_d_wt, overspeed10_d_wt, overspeed20_d_wt, overspeed30_d_wt, overspeedlow_n_wt, overspeed10_n_wt, overspeed20_n_wt, overspeed30_n_wt, overspeed_percentile, 
                overspeed_overall_wt, harsh_ha_d_percentile, harsh_hb_d_percentile, harsh_cornering_d_percentile, harsh_ha_n_percentile, harsh_hb_n_percentile, harsh_cornering_n_percentile,
                harsh_overall_wt, age_criteria1, age_criteria2, age_criteria3, age_criteria4, age_criteria1_wt, age_criteria2_wt, age_criteria3_wt, age_criteria4_wt, age_overall_wt, gender_male_wt, gender_female_wt,
                gender_other_wt, gender_overall_wt, car_white_wt, car_lightother_wt, car_metallic_light_wt, car_red_wt, car_metallic_dark_wt, car_darkother_wt, car_black_wt, car_overall_wt,
                overspeed_per_def,ha_d_per_def,hb_d_per_def,hc_d_per_def,ha_n_per_def,hb_n_per_def,hc_n_per_def,call_d_percentile, weather_d_percentile, weather_n_percentile, weather_d_per_def, 
	            weather_n_per_def, weather_wt,call_n_percentile, call_d_per_def,call_n_per_def,call_wt,created_at, isactive)
                VALUES (@o_id, @maxspeed, @minspeed, @overspeedlow, @overspeed10, @overspeed20, @overspeed30, @overspeedlow_d_wt, 
                @overspeed10_d_wt, @overspeed20_d_wt, @overspeed30_d_wt, @overspeedlow_n_wt, @overspeed10_n_wt, @overspeed20_n_wt, @overspeed30_n_wt,
                @overspeed_percentile, @overspeed_overall_wt, @harsh_ha_d_percentile, @harsh_hb_d_percentile, @harsh_cornering_d_percentile, @harsh_ha_n_percentile, @harsh_hb_n_percentile, 
                @harsh_cornering_n_percentile, @harsh_overall_wt, @age_criteria1, @age_criteria2, @age_criteria3, @age_criteria4, @age_criteria1_wt, @age_criteria2_wt, @age_criteria3_wt, 
                @age_criteria4_wt, @age_overall_wt, @gender_male_wt, @gender_female_wt, @gender_other_wt, @gender_overall_wt, @car_white_wt, @car_lightother_wt, @car_metallic_light_wt,
                @car_red_wt, @car_metallic_dark_wt, @car_darkother_wt, @car_black_wt, @car_overall_wt, 
                @overspeed_per_def,@ha_d_per_def,@hb_d_per_def,@hc_d_per_def,@ha_n_per_def,@hb_n_per_def,
                @hc_n_per_def,@call_d_percentile, @weather_d_percentile, @weather_n_percentile, @weather_d_per_def,@weather_n_per_def, @weather_wt,@call_n_percentile,
                @call_d_per_def,@call_n_per_def,@call_wt, @created_at, @isactive);";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt64(configsettingEntity.o_id)));
                Parameters.Add(new NpgsqlParameter("@maxspeed", CommonUtils.ConvertToDouble(configsettingEntity.maxspeed)));
                Parameters.Add(new NpgsqlParameter("@minspeed", CommonUtils.ConvertToDouble(configsettingEntity.minspeed)));

                Parameters.Add(new NpgsqlParameter("@overspeedlow", CommonUtils.ConvertToString(configsettingEntity.overspeedlow)));
                Parameters.Add(new NpgsqlParameter("@overspeed10", CommonUtils.ConvertToString(configsettingEntity.overspeed10)));
                Parameters.Add(new NpgsqlParameter("@overspeed20", CommonUtils.ConvertToString(configsettingEntity.overspeed20)));
                Parameters.Add(new NpgsqlParameter("@overspeed30", CommonUtils.ConvertToString(configsettingEntity.overspeed30)));

                Parameters.Add(new NpgsqlParameter("@overspeedlow_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeedlow_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed10_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed10_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed20_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed20_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed30_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed30_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeedlow_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeedlow_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed10_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed10_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed20_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed20_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed30_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed30_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed_percentile", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_percentile)));
                Parameters.Add(new NpgsqlParameter("@overspeed_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@harsh_ha_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_ha_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_hb_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_hb_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_cornering_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_cornering_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_ha_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_ha_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_hb_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_hb_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_cornering_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_cornering_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.harsh_overall_wt)));

                Parameters.Add(new NpgsqlParameter("@age_criteria1", CommonUtils.ConvertToString(configsettingEntity.age_criteria1)));
                Parameters.Add(new NpgsqlParameter("@age_criteria2", CommonUtils.ConvertToString(configsettingEntity.age_criteria2)));
                Parameters.Add(new NpgsqlParameter("@age_criteria3", CommonUtils.ConvertToString(configsettingEntity.age_criteria3)));
                Parameters.Add(new NpgsqlParameter("@age_criteria4", CommonUtils.ConvertToString(configsettingEntity.age_criteria4)));

                Parameters.Add(new NpgsqlParameter("@age_criteria1_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria1_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria2_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria2_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria3_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria3_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria4_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria4_wt)));
                Parameters.Add(new NpgsqlParameter("@age_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_male_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_male_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_female_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_female_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_other_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_other_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@car_white_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_white_wt)));
                Parameters.Add(new NpgsqlParameter("@car_lightother_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_lightother_wt)));
                Parameters.Add(new NpgsqlParameter("@car_metallic_light_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_metallic_light_wt)));
                Parameters.Add(new NpgsqlParameter("@car_red_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_red_wt)));
                Parameters.Add(new NpgsqlParameter("@car_metallic_dark_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_metallic_dark_wt)));
                Parameters.Add(new NpgsqlParameter("@car_darkother_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_darkother_wt)));
                Parameters.Add(new NpgsqlParameter("@car_black_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_black_wt)));
                Parameters.Add(new NpgsqlParameter("@car_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_overall_wt)));

                Parameters.Add(new NpgsqlParameter("@overspeed_per_def", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_per_def)));
                Parameters.Add(new NpgsqlParameter("@ha_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.ha_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@hb_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hb_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@hc_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hc_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@ha_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.ha_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@hb_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hb_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@hc_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hc_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.weather_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@weather_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.weather_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@weather_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.weather_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.weather_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_wt", CommonUtils.ConvertToDouble(configsettingEntity.weather_wt)));
                Parameters.Add(new NpgsqlParameter("@call_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.call_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@call_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.call_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@call_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.call_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@call_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.call_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@call_wt", CommonUtils.ConvertToDouble(configsettingEntity.call_wt)));
                Parameters.Add(new NpgsqlParameter("@created_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();
                return true;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetConfigSettingList(FilterEntity filterEntity)
        {
            try
            {
                strSqlQry = @"SELECT dr_parameter_config_setting.*,o_code,o_name,o_type,(address1  ||' ' || address2  || ' ' ||  address3) as address, (email_id1  ||' ' || email_id2  ) as email_id  FROM dr_parameter_config_setting,org_master 
                WHERE dr_parameter_config_setting.o_id=org_master.o_id and  dr_parameter_config_setting.isactive = '1' and (coalesce(dr_parameter_config_setting.deleted,false) = false) ";

                objDAL.CreateConnection();
                DataTable dtConfigSetting = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtConfigSetting;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetConfigSettingById(FilterEntity filterEntity)
        {
            try
            {
                strSqlQry = @"SELECT * FROM dr_parameter_config_setting
                WHERE  isactive = '1' and (coalesce(deleted,false) = false) and o_id=" + CommonUtils.ConvertToInt(filterEntity.o_id);

                objDAL.CreateConnection();
                DataTable dtConfigSetting = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtConfigSetting;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetOrganizationList(FilterEntity filterEntity)
        {
            try
            {
                string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(filterEntity.action);
                dynamic jsonObject = JObject.Parse(jsonString);

                if (filterEntity.s_role_Id == 3)
                {
                    //strSqlQry = @"SELECT o_id,o_code  FROM org_master WHERE isactive = '1' and (coalesce(deleted,false) = false) and o_id not in (select o_id from dr_parameter_config_setting where isactive = '1' and (coalesce(deleted,false) = false))";
                    if (jsonObject.action == "New")
                    {
                        strSqlQry = @"SELECT o_id as value,o_code as text  FROM org_master WHERE isactive = '1' and (coalesce(deleted,false) = false) and o_id not in (select o_id from dr_parameter_config_setting where isactive = '1' and (coalesce(deleted,false) = false))";
                    }
                    else
                    {
                        strSqlQry = @"SELECT o_id as value,o_code as text  FROM org_master WHERE isactive = '1' and (coalesce(deleted,false) = false) ";
                    }
                    objDAL.CreateConnection();
                    DataTable dtOrg = objDAL.FetchRecords(strSqlQry);
                    objDAL.CloseConnection();

                    return dtOrg;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool UpdateConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            try
            {
                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @" UPDATE dr_parameter_config_setting SET maxspeed=@maxspeed,	minspeed =@minspeed ,overspeedlow =@overspeedlow,
	            overspeed10 =@overspeed10,overspeed20 =@overspeed20,overspeed30 =@overspeed30,overspeedlow_d_wt =@overspeedlow_d_wt ,overspeed10_d_wt =@overspeed10_d_wt,
	            overspeed20_d_wt =@overspeed20_d_wt ,overspeed30_d_wt =@overspeed30_d_wt,overspeedlow_n_wt =@overspeedlow_n_wt ,overspeed10_n_wt =@overspeed10_n_wt,
	            overspeed20_n_wt =@overspeed20_n_wt ,overspeed30_n_wt =@overspeed30_n_wt ,overspeed_percentile =@overspeed_percentile ,overspeed_overall_wt =@overspeed_overall_wt ,
	            harsh_ha_d_percentile =@harsh_ha_d_percentile ,harsh_hb_d_percentile =@harsh_hb_d_percentile ,harsh_cornering_d_percentile =@harsh_cornering_d_percentile,
	            harsh_ha_n_percentile =@harsh_ha_n_percentile ,harsh_hb_n_percentile =@harsh_hb_n_percentile,harsh_cornering_n_percentile =@harsh_cornering_n_percentile ,
	            harsh_overall_wt =@harsh_overall_wt,age_criteria1 =@age_criteria1,age_criteria2 =@age_criteria2,age_criteria3 =@age_criteria3,
	            age_criteria4 =@age_criteria4,age_criteria1_wt =@age_criteria1_wt,age_criteria2_wt =@age_criteria2_wt ,age_criteria3_wt =@age_criteria3_wt ,
	            age_criteria4_wt =@age_criteria4_wt,age_overall_wt =@age_overall_wt,gender_male_wt =@gender_male_wt ,
	            gender_female_wt =@gender_female_wt,gender_other_wt =@gender_other_wt,gender_overall_wt =@gender_overall_wt,car_white_wt =@car_white_wt ,
	            car_lightother_wt =@car_lightother_wt ,car_metallic_light_wt =@car_metallic_light_wt ,car_red_wt =@car_red_wt,car_metallic_dark_wt =@car_metallic_dark_wt ,
	            car_darkother_wt =@car_darkother_wt,car_black_wt =@car_black_wt ,car_overall_wt =@car_overall_wt , 
                overspeed_per_def=@overspeed_per_def,ha_d_per_def=@ha_d_per_def,hb_d_per_def=@hb_d_per_def,hc_d_per_def=@hc_d_per_def,ha_n_per_def=@ha_n_per_def,hb_n_per_def=@hb_n_per_def,
                hc_n_per_def=@hc_n_per_def,call_d_percentile=@call_d_percentile, weather_d_percentile=@weather_d_percentile, weather_n_percentile=@weather_n_percentile, weather_d_per_def=@weather_d_per_def,weather_n_per_def=@weather_n_per_def, weather_wt=@weather_wt,call_n_percentile=@call_n_percentile,
                call_d_per_def= @call_d_per_def,call_n_per_def=@call_n_per_def,call_wt=@call_wt,updated_at =@updated_at,isactive =@isactive 
                where o_id =@o_id ;";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt64(configsettingEntity.o_id)));
                Parameters.Add(new NpgsqlParameter("@maxspeed", CommonUtils.ConvertToDouble(configsettingEntity.maxspeed)));
                Parameters.Add(new NpgsqlParameter("@minspeed", CommonUtils.ConvertToDouble(configsettingEntity.minspeed)));

                Parameters.Add(new NpgsqlParameter("@overspeedlow", CommonUtils.ConvertToString(configsettingEntity.overspeedlow)));
                Parameters.Add(new NpgsqlParameter("@overspeed10", CommonUtils.ConvertToString(configsettingEntity.overspeed10)));
                Parameters.Add(new NpgsqlParameter("@overspeed20", CommonUtils.ConvertToString(configsettingEntity.overspeed20)));
                Parameters.Add(new NpgsqlParameter("@overspeed30", CommonUtils.ConvertToString(configsettingEntity.overspeed30)));

                Parameters.Add(new NpgsqlParameter("@overspeedlow_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeedlow_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed10_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed10_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed20_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed20_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed30_d_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed30_d_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeedlow_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeedlow_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed10_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed10_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed20_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed20_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed30_n_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed30_n_wt)));
                Parameters.Add(new NpgsqlParameter("@overspeed_percentile", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_percentile)));
                Parameters.Add(new NpgsqlParameter("@overspeed_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@harsh_ha_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_ha_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_hb_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_hb_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_cornering_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_cornering_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_ha_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_ha_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_hb_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_hb_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_cornering_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.harsh_cornering_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@harsh_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.harsh_overall_wt)));

                Parameters.Add(new NpgsqlParameter("@age_criteria1", CommonUtils.ConvertToString(configsettingEntity.age_criteria1)));
                Parameters.Add(new NpgsqlParameter("@age_criteria2", CommonUtils.ConvertToString(configsettingEntity.age_criteria2)));
                Parameters.Add(new NpgsqlParameter("@age_criteria3", CommonUtils.ConvertToString(configsettingEntity.age_criteria3)));
                Parameters.Add(new NpgsqlParameter("@age_criteria4", CommonUtils.ConvertToString(configsettingEntity.age_criteria4)));

                Parameters.Add(new NpgsqlParameter("@age_criteria1_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria1_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria2_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria2_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria3_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria3_wt)));
                Parameters.Add(new NpgsqlParameter("@age_criteria4_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_criteria4_wt)));
                Parameters.Add(new NpgsqlParameter("@age_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.age_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_male_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_male_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_female_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_female_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_other_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_other_wt)));
                Parameters.Add(new NpgsqlParameter("@gender_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.gender_overall_wt)));
                Parameters.Add(new NpgsqlParameter("@car_white_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_white_wt)));
                Parameters.Add(new NpgsqlParameter("@car_lightother_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_lightother_wt)));
                Parameters.Add(new NpgsqlParameter("@car_metallic_light_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_metallic_light_wt)));
                Parameters.Add(new NpgsqlParameter("@car_red_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_red_wt)));
                Parameters.Add(new NpgsqlParameter("@car_metallic_dark_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_metallic_dark_wt)));
                Parameters.Add(new NpgsqlParameter("@car_darkother_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_darkother_wt)));
                Parameters.Add(new NpgsqlParameter("@car_black_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_black_wt)));
                Parameters.Add(new NpgsqlParameter("@car_overall_wt", CommonUtils.ConvertToDouble(configsettingEntity.car_overall_wt)));

                Parameters.Add(new NpgsqlParameter("@overspeed_per_def", CommonUtils.ConvertToDouble(configsettingEntity.overspeed_per_def)));
                Parameters.Add(new NpgsqlParameter("@ha_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.ha_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@hb_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hb_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@hc_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hc_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@ha_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.ha_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@hb_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hb_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@hc_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.hc_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.weather_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@weather_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.weather_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@weather_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.weather_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.weather_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@weather_wt", CommonUtils.ConvertToDouble(configsettingEntity.weather_wt)));
                Parameters.Add(new NpgsqlParameter("@call_d_percentile", CommonUtils.ConvertToDouble(configsettingEntity.call_d_percentile)));
                Parameters.Add(new NpgsqlParameter("@call_n_percentile", CommonUtils.ConvertToDouble(configsettingEntity.call_n_percentile)));
                Parameters.Add(new NpgsqlParameter("@call_d_per_def", CommonUtils.ConvertToDouble(configsettingEntity.call_d_per_def)));
                Parameters.Add(new NpgsqlParameter("@call_n_per_def", CommonUtils.ConvertToDouble(configsettingEntity.call_n_per_def)));
                Parameters.Add(new NpgsqlParameter("@call_wt", CommonUtils.ConvertToDouble(configsettingEntity.call_wt)));
                Parameters.Add(new NpgsqlParameter("@updated_at", CommonUtils.ConvertToDateTime(DateTime.Now)));
                Parameters.Add(new NpgsqlParameter("@isactive", true));

                objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();
                return true;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }


        }

        public bool DeleteConfigSetting(ConfigSettingEntity configsettingEntity)
        {
            try
            {
                _O_id = CommonUtils.ConvertToInt(CommonUtils.ConvertToInt(configsettingEntity.o_id));

                objDAL.CreateConnection();
                objDAL.BeginTransaction();

                strSqlQry = @"UPDATE dr_parameter_config_setting set deleted=true where o_id=@o_id";
                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", _O_id));

                blnResult = objDAL.ExecutParameterizedQry(strSqlQry, Parameters);

                objDAL.CommitTransaction();
                objDAL.CloseConnection();


                return blnResult;

            }
            catch (Exception ex)
            {
                objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CheckConfigSettingAvailability(ConfigSettingEntity configsettingEntity)
        {
            try
            {
                string SQLQry = @"select o_id  from dr_parameter_config_setting where o_id=@o_id  and isactive = '1' and coalesce(dr_parameter_config_setting.deleted,false) = false";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@o_id", CommonUtils.ConvertToInt64(configsettingEntity.o_id)));

                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(SQLQry, Parameters);
                objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    objDAL.CloseConnection();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public bool CreateNotificationSetting(NotificationSetting notisettingEntity)
        {
            try
            {
                objDAL.CreateConnection();
                //objDAL.BeginTransaction();

                string strcheckexist = @"select user_id from notification_setting where user_id=@user_id";

                Parameters = new List<NpgsqlParameter>();
                Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(notisettingEntity.s_user_id)));

                objDAL.CreateConnection();
                DataTable dtData = objDAL.ExecutParameterizedQrywithDataTable(strcheckexist, Parameters);
                //objDAL.CloseConnection();

                if (dtData != null && dtData.Rows.Count > 0)
                {
                    strSqlQry = @"UPDATE notification_setting SET sound_overspeed =@sound_overspeed,triprating_notification =@triprating_notification,cmpt_notification =@cmpt_notification,promotion_notification =@promotion_notification,update_at =@update_at WHERE user_id=@user_id;";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(notisettingEntity.s_user_id)));
                    Parameters.Add(new NpgsqlParameter("@sound_overspeed", CommonUtils.ConvertToBoolean(notisettingEntity.sound_overspeed)));
                    Parameters.Add(new NpgsqlParameter("@triprating_notification", CommonUtils.ConvertToBoolean(notisettingEntity.triprating_notification)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_notification", CommonUtils.ConvertToBoolean(notisettingEntity.cmpt_notification)));
                    Parameters.Add(new NpgsqlParameter("@promotion_notification", CommonUtils.ConvertToBoolean(notisettingEntity.promotion_notification)));
                    Parameters.Add(new NpgsqlParameter("@update_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                    objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);
                }
                else
                {
                    strSqlQry = @"INSERT INTO notification_setting(user_id, sound_overspeed, triprating_notification, cmpt_notification, promotion_notification, update_at)VALUES (@user_id, @sound_overspeed,@triprating_notification,@cmpt_notification,@promotion_notification, @update_at);";

                    Parameters = new List<NpgsqlParameter>();
                    Parameters.Add(new NpgsqlParameter("@user_id", CommonUtils.ConvertToInt64(notisettingEntity.s_user_id)));
                    Parameters.Add(new NpgsqlParameter("@sound_overspeed", CommonUtils.ConvertToBoolean(notisettingEntity.sound_overspeed)));
                    Parameters.Add(new NpgsqlParameter("@triprating_notification", CommonUtils.ConvertToBoolean(notisettingEntity.triprating_notification)));
                    Parameters.Add(new NpgsqlParameter("@cmpt_notification", CommonUtils.ConvertToBoolean(notisettingEntity.cmpt_notification)));
                    Parameters.Add(new NpgsqlParameter("@promotion_notification", CommonUtils.ConvertToBoolean(notisettingEntity.promotion_notification)));
                    Parameters.Add(new NpgsqlParameter("@update_at", CommonUtils.ConvertToDateTime(DateTime.Now)));

                    objDAL.ExecutParameterizedQryAndGetID(strSqlQry, Parameters);


                }


                //objDAL.CommitTransaction();
                objDAL.CloseConnection();
                return true;

            }
            catch (Exception ex)
            {
                //objDAL.RollBackTransaction();
                objDAL.CloseConnection();
                throw ex;
            }

        }

        public DataTable GetNotificationSetting(NotificationSetting notisettingEntity)
        {
            try
            {
                strSqlQry = @"SELECT * FROM notification_setting where user_id=" + notisettingEntity.s_user_id;

                objDAL.CreateConnection();
                DataTable dtNotiSetting = objDAL.FetchRecords(strSqlQry);
                objDAL.CloseConnection();

                return dtNotiSetting;
            }
            catch (Exception ex)
            {
                objDAL.CloseConnection();
                throw ex;
            }

        }
    }
}
